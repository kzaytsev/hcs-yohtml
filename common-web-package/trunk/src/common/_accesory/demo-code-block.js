/**
 * TODO Сделать так, чтобы не попадал в общую сборку, а только для собранного примера
 */
angular.module('demo-code-block', [
    /* none */
])
    .directive('demoCodeBlock', function () {
        return {
            restrict: 'E',
            scope: {
                id: '@',
                title: '@',
                demoFile: '@',
                showSource: '='
            },
            templateUrl: '_accesory/demo-code-block.tpl.html',
            controller: function ($scope) {
                /* Именование обусловлено особенностями раскладки файлов в текущем шаблоне проекта */
                $scope.demoFileJs = 'src/app/' + $scope.demoFile.replace('.tpl.html', '.js');
            }
        };
    })
;