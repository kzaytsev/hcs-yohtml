angular.module('ef-vmpg-demo', ['common.ef-vmpg'])
        .controller('EfVmpgCtrl', function ($scope) {

            $scope.startMonth = 4;
            $scope.startYear = 2015;

            $scope.endMonth = 6;
            $scope.endYear = 2016;
        })
;