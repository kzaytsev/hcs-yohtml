/**
 * Используем стандартный bootstrap компонент
 * см. http://angular-ui.github.io/bootstrap/#/alert
 */
angular.module('message-panel-demo', ['common.ef-bp'])
    .controller('messagePanelCtrl', ['$scope', function ($scope) {
        $scope.alerts = [
            { type: 'danger', msg: 'Период сдачи текущих показаний за <месяц> <год> г. еще не наступил.' },
            { type: 'success', msg: 'Well done! You successfully read this important alert message.' }
        ];

        $scope.addAlert = function() {
            $scope.alerts.push({msg: 'Another alert!'});
        };

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

    }])
;