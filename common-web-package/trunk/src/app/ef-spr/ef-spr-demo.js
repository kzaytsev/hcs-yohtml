angular.module('ef-spr-demo', ['common.ef-spr','common.reason-form-control'])
    .controller('EfSprCtrl', ['$scope', 'referenceChooserDialog', function ($scope, referenceChooserDialog) {
        $scope.showHierarchyDialog = function () {
            referenceChooserDialog
                    .setReferenceName('ОКФС')
                    .setType('okfs')
                    .canSelectOnLevel(2)
                    .show().then(function (result) {
                console.log(result);
            }, function () {
                console.log('cancel');
            });
        };

        $scope.showPlaneDialog = function () {
            referenceChooserDialog
                    .setReferenceName('Вид коммунальной услуги')
                    .plane()
                    .hasRadio()
                    .setType('MunicipalService')
                    .show().then(function (result) {
                console.log(result);
            }, function () {
                console.log('cancel');
            });
        };
        $scope.showArgDialog = function () {
            var document = {};
            referenceChooserDialog
                .setReferenceName("Справочник \"Дополнительные услуги\"")
                .setType('AdditionalServiceType')
                .setupForAgreements(document, '', '')
                .setParentNsiObjectNameProperty('parentNsiObjectName')
                .hasCheckbox()
                .onlyLeafs()
                .show()
                .then(function (result) {
                    console.log(result);
                }, function () {
                    console.log('cancel');
                });
        };
    }])
;