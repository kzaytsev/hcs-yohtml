angular.module('ef-prf-demo', ['common.ef-prf'])
    .controller('EfPrfCtrl', ['$scope', function ($scope) {

            $scope.myCallback = function(response) {
                console.log('from myCallback with ' + response);
            };

            $scope.myExtensions = ['pdf', 'png'];

    }])
;