angular.module('ef-org-demo', [
    'common.ef-doborg',
    'common.ef-izmorg',
    'common.ef-prorg',
    'common.required-field'
])

    .controller('efOrgDemoCtrl', [
        '$scope',
        'efDoborgDialog',
        'efIzmorgDialog',
        'efProrgDialog',
        function($scope,
                 efDoborgDialog,
                 efIzmorgDialog,
                 efProrgDialog){
            $scope.organization = {
            };

            $scope.addOrganization = function(){
                efDoborgDialog.create().show().then(function (result) {
                    console.log(result);
                    $scope.organization = result;
                }, function () {
                });
            };

            $scope.editOrganization = function(formIsValid){
                $scope.$broadcast('showError');
                if(formIsValid) {
                    efIzmorgDialog.create().setOrganizationGuid($scope.organization.guid).show().then(function (result) {
                        console.log(result);
                        $scope.organization = result;
                    }, function () {
                    });
                }
            };

            $scope.viewOrganization = function(formIsValid){
                $scope.$broadcast('showError');
                if(formIsValid) {
                    efProrgDialog.create().setOrganizationGuid($scope.organization.guid).show().then(function (result) {
                        console.log(result);
                    }, function () {
                    });
                }
            };

        }
    ])
;

