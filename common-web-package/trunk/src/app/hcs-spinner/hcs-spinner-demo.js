angular.module('hcs-spinner-demo', ['common.hcs-spinner'])
    .controller('HcsSpinnerDemoCtrl', function ($scope) {
        $scope.spinnerOptions = {
            min: -2,
            max: 5
        };
    })
;