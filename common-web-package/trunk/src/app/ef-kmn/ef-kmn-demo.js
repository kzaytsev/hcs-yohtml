angular.module('ef-kmn-demo', [
    'common.ef-kmn',
    'common.hcs-pagination'
])
    .controller('EfKmhDemoController', ['$scope', function ($scope) {
        $scope.pagination = {};
        $scope.pagination.total = 200;
        $scope.pagination.itemsPerPage = 20;
        $scope.pagination.isModal = false;
        $scope.pagination.currentPage = 3;
        $scope.pageChanged = function () {
            // some doing
        };
    }])
    .controller('hcsPaginationDemoController', ['$scope', '$q', '$timeout', function ($scope, $q, $timeout) {
        $scope.pagination = {};
        $scope.pagination.isLast = false;
        $scope.pagination.itemsPerPage = 20;
        $scope.pagination.isModal = false;
        $scope.pagination.currentPage = 3;
        $scope.pageChanged = function (newPage) {
            var query = {
                page: newPage ? newPage : $scope.pagination.currentPage,
                itemsPerPage: $scope.pagination.itemsPerPage
            };
            if (newPage) {
                return getDataService(query)
                    .then(function (responce) {
                        $scope.pagination.currentPage = responce.page;
                        $scope.pagination.isLast = responce.isLast;
                        // просто возвращаем сигнал, что все отработало
                        return;
                    });
            }
            getDataService(query)
                .then(function (responce) {
                    $scope.pagination.currentPage = responce.page;
                    $scope.pagination.isLast = responce.isLast;
                    // просто возвращаем сигнал, что все отработало
                    return;
                });
        };
        function getDataService(query) {
            var isLast = false;
            var page = query.page;
            var defer = $q.defer();
            $timeout(function () {
                // перебирать ответы сервера, пока не найдем заполненную страницу,
                // здесь для простоты сразу правильную ставим
                if (query.page > 2) {
                    page = 2;
                    isLast = true;
                } else {
                }
                defer.resolve({ page: page, isLast: isLast});
            }, 1000);
            return defer.promise;
        }
    }])
;