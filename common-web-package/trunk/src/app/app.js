angular.module('common-demo', [
    'ui.bootstrap',
    'ui.utils',
    'hljs',
    'ui.select2',
    'templates-common',
    'templates-modules',
    'templates-app',
    'app-common-mock',
    'required-field-demo',
    'hcs-table-demo',
    'ef-vmig-demo',
    'ef-vmpg-demo',
    'ef-vop-demo',
    'nqgrid-demo',
    'dialogs-demo',
    'hcs-datepicker-demo',
    'hcs-daterange-demo',
    'hcs-popover-demo',
    'filter-demo',
    'ef-ivs-demo',
    'ef-spr-demo',
    'ef-bp-demo',
    'ef-bpp-demo',
    'ef-pa-demo',
    'flow',
    'ef-prf-demo',
    'ef-vd-demo',
    'ef-vva-demo',
    'ef-vpom-demo',
    'resources-demo',
    'ng-simple-grid-demo',
    'ef-kmn-demo',
    'message-panel-demo',
    'multiselect-demo',
    'user-info-demo',
    'demo-code-block',
    'ef-vkom-demo',
    'ef-poktmo-demo',
    'ef-voktmo-demo',
    'attachment-element-demo',
    'validate-demo',
    'ef-vborg-demo',
    'ef-vbfl-demo',
    'ef-fl-demo',
    'ef-org-demo',
    'ef-kporg-demo',
    'hcs-spinner-demo'
])

    .constant('NSI_BACKEND_CONFIG', {
        baseUrl: '/nsi/api/rest/services'
    })

    .constant('HOME_MGMT_BACKEND_CONFIG', {
        baseUrl: '/homemanagement/api/rest/services'
    })

    .constant('EVENT_JOURNAL_BACKEND_CONFIG', {
        baseUrl: '/eventjournal/api/rest/services'
    })

    .constant('INDICES_BACKEND_CONFIG', {
        baseUrl: '/indices/api/rest/services'
    })

    .constant('PPA_BACKEND_CONFIG', {
        baseUrl: '/ppa/api/rest/services'
    })

    .constant('VOTING_BACKEND_CONFIG', {
        baseUrl: '/voting/api/rest/services'
    })

    .constant('NOTIFICATION_BACKEND_CONFIG', {
        baseUrl: '/notification/api/rest/services'
    })

    .controller('AppController', ['$scope', function ($scope) {
        $scope.showDemo = true;
    }])
;
