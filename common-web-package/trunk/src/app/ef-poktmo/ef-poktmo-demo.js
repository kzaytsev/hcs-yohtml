angular.module('ef-poktmo-demo', ['common.ef-poktmo'])
        .controller('EfPoktmoCtrl', ['$scope', '$ClassifierService', function ($scope, $ClassifierService) {

            $scope.log = function() {
                console.log($scope.oktmo);
            };


            $scope.searchService = function (searchParameters) {
                console.log(searchParameters);
                return $ClassifierService.findOktmoExtendedSearch({
                    level : 1
                }, function(result) {
                    console.log(result);
                }, $scope.errorCallback);
            };
        }])
;