angular.module('ef-vmig-demo', ['common.ef-vmig'])
        .controller('EfVmigCtrl', function ($scope) {

            $scope.years = [];

            for (var i = 2020; i > 2000; i--) {
                $scope.years.push({ val : i, exceptedMonths: [3, 4, 5]});
            }

            $scope.years.push({ val : 1999, exceptedMonths: [9, 10]});

            $scope.month = 4;

            $scope.year = 2015;
        })
;