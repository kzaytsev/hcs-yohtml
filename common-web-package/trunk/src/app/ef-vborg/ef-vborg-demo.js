angular.module('ef-vborg-demo', ['common.ef-vborg'])

.controller('efVborgDemoCtrl', [
        '$scope',
        'efVborgDialog',
        function($scope,
                 efVborgDialog){
            $scope.selectOrganization = function(returnDetailObject){
                efVborgDialog.create().setReturnDetailObject(returnDetailObject).show().then(function (result) {
                    console.log(result);
                }, function () {
                });
            };
        }
    ])
;

