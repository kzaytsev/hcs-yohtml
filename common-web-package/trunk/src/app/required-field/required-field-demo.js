angular.module('required-field-demo', ['common.required-field'])
    .controller('RequiredFieldCtrl', function ($scope) {
        $scope.requiredFields = {
            field3: "field 3"
        };


        $scope.condition = false;
        $scope.changeCondition = function() {
            $scope.condition = !$scope.condition;
        };

        $scope.houseTypes = [
            {guid: -1, houseTypeName: 'один'},
            {guid: -2, houseTypeName: 'два'}
        ];

        $scope.saveRequiredFields = function (form) {
            form.$submitted = true;
            $scope.$broadcast('showError');

            console.log('form valid = ', form.$valid);
            if (form.$invalid) {
                return false;
            }
        };
    })
;