angular.module('resources-demo', ['home-mgmt-resource', 'nsi-resource', 'ppa-resource'])
    .controller('ResourcesCtrl', ['$scope', 'HomeManagementResource', 'NsiResource', 'PpaResource', function ($scope, HomeManagementResource, NsiResource, PpaResource  ) {
        $scope.callHomeMgmtRest = function () {
            new HomeManagementResource('/houses/:houseGuid/apartments').query(
                {
                    houseGuid: '1'
                },
                {
                    page: 1,
                    count: 2,
                    sortDir: 'asc',
                    sortedBy: 'guid'
                },
                function (data) {
                    alert(data);
                },
                function (data, status) {
                    alert("Ошибка " + status);
                }
            );

            new HomeManagementResource('/houses/search').query({}, $scope.queryParams,
                function (data) {
                    alert(data);
                },
                function (data, status) {
                    alert("Ошибка " + status);
                }
            );
        };

        $scope.callNsiRest = function () {
            new NsiResource('/nsi/houses/types').query({}, $scope.queryParams,
                function (data, status) {
                    alert(data);
                },
                function (data, status) {
                    alert("Ошибка " + status);
                }
            );
        };

        $scope.callPpaRest = function () {
            new PpaResource('/ppa/organizations/search').query({}, $scope.queryParams,
                function (data, status) {
                    alert(data);
                },
                function (data, status) {
                    alert("Ошибка " + status);
                }
            );
        };
    }])
;