angular.module ('nqgrid-demo', [
    'common.ngJqGrid'
])

//   Пример работы с jqGrid

    .controller ('ExJqGridCtrl', function ($scope) {

        onCellChanged = function () {
            $scope.gridapi.apply ();
        },
     
     
        $scope.master = [
            {id: "1", name: "Cash", num: "100", debit: "400.00", credit: "250.00", balance: "150.00", enbl: "1", disbl: "0",
                level: "0", parent: "null", isLeaf: false, expanded: true, loaded: true},
            {id: "2", name: "Cash_1", num: "1", debit: "300.00", credit: "200.00", balance: "100.00", enbl: "0", disbl: "1",
                level: "1", parent: "1", isLeaf: false, expanded: true, loaded: true},
            {id: "3", name: "Sub_Cash_1", num: "1", debit: "300.00", credit: "200.00", balance: "100.00", enbl: "1", disbl: "0",
                level: "2", parent: "2", isLeaf: true, expanded: false, loaded: true},
            {id: "4", name: "Cash_2", num: "2", debit: "100.00", credit: "50.00", balance: "50.00", enbl: "0", disbl: "1",
                level: "1", parent: "1", isLeaf: true, expanded: false, loaded: true},
            {id: "5", name: "Bank\'s", num: "200", debit: "1500.00", credit: "1000.00", balance: "500.00", enbl: "1", disbl: "0",
                level: "0", parent: "null", isLeaf: false, expanded: true, loaded: true},
            {id: "6", name: "Bank_1", num: "1", debit: "500.00", credit: "0.00", balance: "500.00", enbl: "0", disbl: "1",
                level: "1", parent: "5", isLeaf: true, expanded: true, loaded: true},
            {id: "7", name: "Bank_2", num: "2", debit: "1000.00", credit: "1000.00", balance: "0.00", enbl: "1", disbl: "0",
                level: "1", parent: "5", isLeaf: true, expanded: false, loaded: true},
            {id: "8", name: "Fixed_asset", num: "300", debit: "0.00", credit: "1000.00", balance: "-1000.00", enbl: "0", disbl: "1",
                level: "0", parent: "null", isLeaf: true, expanded: false, loaded: true}
        ];

        $scope.data = angular.copy ($scope.master);

        $scope.config = {
            height: 250,
            autowidth: true,
            shrinkToFit: true,
            datatype: "jsonstring",
            datastr: $scope.data,
            pager: false,
            colNames: ["Id", "Account", "Acc Num", "Debit", "Credit", "Balance", "Enabled", "Disabled"],
            colModel: [
                {name: 'id', index: 'id', width: 1, hidden: true, key: true},
                {name: 'name', index: 'name', width: 180},
                {name: 'num', index: 'acc_num', width: 80, align: "center", editable: true},
                {name: 'debit', index: 'debit', width: 80, align: "right"},
                {name: 'credit', index: 'credit', width: 80, align: "right"},
                {name: 'balance', index: 'balance', width: 80, align: "right"},
                {name: 'enbl', index: 'enbl', width: 60, align: 'center',
                    formatter: 'checkbox',
                    editoptions: {value: '1:0'},
                    formatoptions: {disabled: false}
                },
                {name: 'disbl', index: 'disbl', width: 60, align: 'center',
                    formatter: 'checkbox',
                    editoptions: {value: '1:0'},
                    formatoptions: {disabled: false}
                }
            ],
            'cellEdit': true,
            'cellsubmit': 'clientArray',
            treeGrid: true,
            caption: "jqTreeGrid Demos",
            ExpandColClick: true,
            gridview: true,
            rowNum: 10000,
            sortname: 'id',
            treeGridModel: 'adjacency',
            ExpandColumn: 'name',
            treeIcons: {
                plus: 'fa fa-plus-circle',
                minus: 'fa fa-minus-circle',
                leaf: 'fa fa-leaf'
            },
            afterSaveCell: function (rowid, cellname, value, iRow, iCol) {
                onCellChanged ();
            },
            jsonReader: {
                repeatitems: false,
                root: function (obj) {
                    return obj;
                },
                page: function (obj) {
                    return 1;
                },
                total: function (obj) {
                    return 1;
                },
                records: function (obj) {
                    return obj.length;
                }
            }
        };

        $scope.reset = function () {
            $scope.data = angular.copy ($scope.master);
            $scope.gridapi.refresh ($scope.data);
        };

        $scope.clear = function () {
            $scope.gridapi.clear ();
        };
    })

    .controller ('ExJqGridCodeCtrl', function ($scope) {
    })

//   Имитация древовидной структуры

    .controller ('MainController', ['$scope', function ($scope) {

            $scope.treeData = [
                {
                    name: 'Action 1',
                    time: 24,
                    depth: 0
                },
                {
                    name: 'Action 2',
                    time: 13,
                    depth: 0
                },
                {
                    name: 'Event',
                    time: 12,
                    depth: 1
                },
                {
                    name: 'Doc',
                    time: 8,
                    depth: 2
                },
                {
                    name: 'Doc',
                    time: 8,
                    depth: 2
                },
                {
                    name: 'Action 3',
                    time: 24,
                    depth: 0
                }
            ];


            $scope.calcIndent = function (result) {
                return result.depth * 24 + 'px';
            };

        }])

    ;

