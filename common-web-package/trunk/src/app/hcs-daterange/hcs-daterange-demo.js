angular.module('hcs-daterange-demo', ['common.validate', 'common.daterange'])
    .controller('HcsDaterangeCtrl', function ($scope) {

        $scope.daterangeOptions = {};
        $scope.rangeFrom = '';
        $scope.rangeTo = '';

        $scope.setForm = function (form) {
            $scope.form = form;
        };
        $scope.save = function(){
            $scope.$broadcast("showError");
            console.log('$invalid: ', $scope.form.$invalid);
        };
    });
