angular.module('validate-demo', ['common.validate'])
    .controller('ValidateCtrl', function ($scope) {
        $scope.validate = {
            money: '12345678.99',
            phones: {
                phoneEmpty: '',
                phone: '83456789012'
            },
            emails: {
                emailEmpty: ''
            },
            lengthValidates: {
                orgn: '12345678901234',
                orgnip: '123456789012345',
                inn: '12345678901'
            }
        };
        $scope.daterangeOptions = {};
        $scope.rangeFrom = '';
        $scope.rangeTo = '';


        $scope.setForm = function (form) {
            $scope.form = form;
        };
        $scope.save = function(){
            $scope.$broadcast("showError");
            console.log('$invalid: ', $scope.form.$invalid);
        };
    });
