angular.module('ef-vd-demo', ['common.ef-vd'])
    .controller('EfVdCtrl', function ($scope, houseChooserDialog) {
        $scope.showDialog = function () {
            houseChooserDialog
                .residentialOnly()
                .returnObject()
                .show().then(function (result) {
                    console.log(result);
                }, function () {
                    console.log('cancel');
                });
        };

        $scope.showOtherDialog = function () {
            houseChooserDialog
                .multiroomOnly()
                .show().then(function (result) {
                    console.log(result);
                }, function () {
                    console.log('cancel');
                });
        };
    })
;