angular.module('hcs-datepicker-demo', ['common.hcs-datepicker'])
    .controller('HcsDatepickerCtrl', function ($scope) {

        $scope.myDate = '10.06.2014';
        $scope.dateOptions = {
            minDate: '2014-05-03',
            maxDate: '2015-01-01',
            initDate: new Date(),

            //available standard formats [dd.MM.yyyy, MM.yyyy] support mask and placeholder
            //other formats like 'MMMM yyyy г.' - non standard
            format: 'MMMM yyyy г.',

            datepickerMode: 'month', //specify intentionally if needed

            open: function () {
                console.log('popup opened');
            },
            disabled: function (date, mode) {
                //Disable weekend selection
                return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
            },
            showButtonBar: false, //default true
            closeOnDateSelection: true, //default true
            appendToBody: true, //default true
            showErrors: function () { //if not specified errors will show on showError event (like common.required-field)
                return true;
            },
            showPlaceholder: true, //default false
            required: true, //default false
            validate: function (date) {
                if (date.getTime() > $scope.check.getTime()) {
                    return false;
                }
                return true;
            }
        };

        $scope.anotherDate = '15.10.2014';
        $scope.anotherDateOptions = {
            minDate: '2014-05-03',
            maxDate: '2015-01-01',
            strongComparing: true,
            required: true,
            dateIsAfter: function () {
                return $scope.myDate;
            }
        };

        $scope.check = new Date('2014-09-01');

        $scope.getDate = function () {
            console.log($scope.myDate);
        };

        $scope.close = function () {
            $scope.dateOptions.close();
        };

        $scope.submitForm = function (valid) {
            $scope.$broadcast('showError');
            alert('form valid = ' + valid);
        };
    })
;