angular.module('app-common-mock', ['ngMockE2E'])
    .run(function ($httpBackend) {
        'use strict';
        $httpBackend.whenGET(/^src\//).passThrough();
        $httpBackend.whenGET(/^http:\/\/psn44-m/).passThrough();
        $httpBackend.whenGET(/\/api\//).passThrough();
        $httpBackend.whenPOST(/^http:\/\/psn44-m/).passThrough();
        $httpBackend.whenPOST(/\/api\//).passThrough();
        $httpBackend.whenPUT(/^http:\/\/psn44-m/).passThrough();
        $httpBackend.whenPUT(/\/api\//).passThrough();
        $httpBackend.whenGET(/\/filestore\//).passThrough();
        $httpBackend.whenPUT(/\/filestore\//).passThrough();
        $httpBackend.whenPOST(/\/filestore\//).passThrough();
    })
;