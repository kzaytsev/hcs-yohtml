angular.module('ef-bp-demo', ['common.ef-bp'])
    .controller('EfBpCtrl', ['$scope', function ($scope) {
        $scope.searchParameters = {};

        $scope.searchService = function (searchParameters) {
            // код вызова поискового запроса
            console.log('Поисковый запрос отправлен');
            // возврат решения удачен ли поиск
            return true;
        };

        $scope.myDate = '10.06.2014';
        $scope.dateOptions = {
            minDate: '2014-05-03',
            maxDate: '2015-01-01',
            initDate: new Date(),
            format: 'dd.MM.yyyy', //available formats [dd.MM.yyyy, MM.yyyy]
            open: function () {
                console.log('popup opened');
            },
            disabled: function (date, mode) {
                //Disable weekend selection
                return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
            },
            showButtonBar: false, //default true
            closeOnDateSelection: true, //default true
            appendToBody: true, //default true
            showErrors: function () {
                return true; //default true
            },
            showPlaceholder: true, //default false
            required: true, //default false
            validate: function (date) {
                return date.getTime() <= new Date('2014-09-01').getTime();
            },
            dateIsAfter: function () {
                return $scope.mustBeBefore;
            }
        };
    }])
;