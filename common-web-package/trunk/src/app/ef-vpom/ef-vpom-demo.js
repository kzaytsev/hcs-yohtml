angular.module('ef-vpom-demo', ['common.ef-vpom'])
    .controller('EfVpomCtrl', function ($scope, placementChooserDialog) {
        $scope.showDialog = function () {
            placementChooserDialog.show().then(function (result) {
                console.log(result);
            }, function () {
                console.log('cancel');
            });
        };

        $scope.showCheckboxDialog = function() {
            placementChooserDialog.hasCheckbox(true);
            $scope.showDialog();
        };

        $scope.showRadioDialog = function() {
            placementChooserDialog.hasRadio(true);
            $scope.showDialog();
        };
    })
;