angular.module('ef-ivs-demo', ['common.ef-ivs', 'common.ef-bp'])
    .controller('EfIvsCtrl', ['$scope', 'eventHistoryDialog', function ($scope, eventHistoryDialog) {


            $scope.showHistoryDialog = function() {

                var groups =['EP_STRUCTURE_CONFIG','EP_PASSPORT_CONFIG'];
                var group_entity_guid = 'group_entity_guid';
                eventHistoryDialog.show(group_entity_guid,groups);

            };

    }])
;