angular.module('ef-fl-demo', [
    'common.ef-izmfl',
    'common.ef-dobfl',
    'common.ef-prfl',
    'common.required-field'
])

    .controller('efIzmflDemoCtrl', [
        '$scope',
        'efIzmflDialog',
        'efDobflDialog',
        'efPrflDialog',
        function($scope,
                 efIzmflDialog,
                 efDobflDialog,
                 efPrflDialog){
            $scope.fiasHouseCode = '239be226-0417-478d-95bf-d73154b166df';
            $scope.person = {
                guid: '82ba383c-0a1f-4516-855b-b3ebb391a8ed'
            };

            $scope.editPerson = function(formIsValid){
                $scope.$broadcast('showError');
                if(formIsValid){
                    efIzmflDialog.create().setPersonGuid($scope.person.guid).show().then(function (result) {
                        console.log(result);
                        $scope.person = result;
                    }, function () {
                    });
                }
            };
            $scope.addPerson = function(formIsValid){
                $scope.$broadcast('showError');
                if(formIsValid){
                    efDobflDialog.create().setFiasHouseCode($scope.fiasHouseCode).show().then(function (result) {
                        console.log(result);
                        $scope.person = result;
                    }, function () {
                    });
                }
            };
            $scope.viewPerson = function(formIsValid){
                $scope.$broadcast('showError');
                if(formIsValid){
                    efPrflDialog.create().setPersonGuid($scope.person.guid).show().then(function (result) {
                        console.log(result);
                    }, function () {
                    });
                }
            };
        }
    ])
;

