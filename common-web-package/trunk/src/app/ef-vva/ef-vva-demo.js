angular.module('ef-vva-demo', ['common.ef-vva'])
    .controller('EfVvaCtrl', function ($scope, addressChooserDialog) {
        $scope.showDialog = function () {
            addressChooserDialog.show().then(function (result) {
                console.log(result);
            }, function () {
                console.log('cancel');
            });
        };
    })
;