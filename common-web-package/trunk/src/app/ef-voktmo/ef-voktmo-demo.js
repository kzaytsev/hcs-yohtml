angular.module('ef-voktmo-demo', ['common.ef-voktmo'])
        .controller('EfVoktmoCtrl', ['$scope', '$ClassifierService', 'oktmoChooserDialog', function ($scope, $ClassifierService, oktmoChooserDialog) {

            $scope.showDialog = function() {
                oktmoChooserDialog
//                    .selectOnLevel(5)
//                    .restrictRegionGuid('fc5cde20-c0cd-4c17-b250-2b9e32bd82dd')
                    .show()
                    .then(function(result) {
                        console.log(result);
                    });
            };

        }])
;