angular
    .module('ef-vkom-demo', [
        'common.ef-vkom'
    ])
    .controller('EfVkomCtrl', function ($scope, roomChooserDialog) {
        $scope.showDialog = function () {
            roomChooserDialog.show().then(
                function (result) {
                    console.log(result);
                }, function () {
                    console.log('cancel');
                }
            );
        };
        $scope.showCheckboxDialog = function() {
            $scope.showDialog();
        };

        $scope.showRadioDialog = function() {
            roomChooserDialog.hasRadio(true);
            $scope.showDialog();
        };

    });