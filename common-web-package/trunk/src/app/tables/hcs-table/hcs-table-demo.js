/* Устарело, используйте ng-simple-grid */
angular.module('hcs-table-demo', ['hcs-table', 'common.ef-kmn'])

    .controller('ExCtrl', function ($scope) {
        $scope.pageChanged = function () {
            $scope.searchResults = angular.copy($scope.searchResults);
            $scope.pagination.total = 50;
        };

        $scope.searchResults = [
            { address: "г.Москва, ул.Весенняя, д.12", id: 123, series: "4А/Блочный", year: 1990, roomCount: '45/5', mngOrg: "ООО Организация", floorCount: "12/6", deterioration: "34"},
            { address: "г.Москва, ул.Весенняя, д.12", id: 123, series: "4А/Блочный", year: 1990, roomCount: '45/5', mngOrg: "ООО Организация", floorCount: "12/6", deterioration: "34"},
            { address: "г.Москва, ул.Весенняя, д.12", id: 123, series: "4А/Блочный", year: 1990, roomCount: '45/5', mngOrg: "ООО Организация", floorCount: "12/6", deterioration: "34"},
            { address: "г.Москва, ул.Весенняя, д.12", id: 123, series: "4А/Блочный", year: 1990, roomCount: '45/5', mngOrg: "ООО Организация", floorCount: "12/6", deterioration: "34"},
            { address: "г.Москва, ул.Весенняя, д.12", id: 123, series: "4А/Блочный", year: 1990, roomCount: '45/5', mngOrg: "ООО Организация", floorCount: "12/6", deterioration: "34"},
            { address: "г.Москва, ул.Весенняя, д.12", id: 123, series: "4А/Блочный", year: 1990, roomCount: '45/5', mngOrg: "ООО Организация", floorCount: "12/6", deterioration: "34"}
        ];

        $scope.headers = [
            { title: 'Адрес', value: 'address' },
            { title: 'Уникальный номер', value: 'id' },
            { title: 'Серия, тип проекта', value: 'series' },
            { title: 'Год ввода в экспл.', value: 'year' },
            { title: 'Кол-во помещений (ж/нж)', value: 'roomCount' },
            { title: 'Обслуживающая организация', value: 'mngOrg' },
            { title: 'Этажей (наибольшее / наименьшее)', value: 'floorCount' },
            { title: 'Общий износ здания', value: 'deterioration' }
        ];

        $scope.pagination = {};
        $scope.pagination.total = 50;
        $scope.pagination.itemsPerPage = 20;
        $scope.pagination.isModal = false;
        $scope.pagination.currentPage = 1;

        $scope.criteria = {
            sortDir: 'asc',
            sortedBy: 'id'
        };

        $scope.onSort = function (sortedBy, sortDir) {
            $scope.criteria.sortDir = sortDir;
            $scope.criteria.sortedBy = sortedBy;
            $scope.criteria.page = 1;

            $scope.pageChanged();
        };

        $scope.getSearchResults = function () {
            return $scope.searchResults;
        };

        $scope.pageChanged();

    })
;