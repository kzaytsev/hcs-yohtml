angular.module('ng-simple-grid-demo', [
    'ui.bootstrap',
    'ng-simple-grid',
    'ng-simple-grid-demo-data-service'
])
    .constant('resourcesTableFilter', {
        lpuIdsCR: null,
        roomNumber: null,
        resourceTitle: null,
        medicalEmployeeLastName: null,
        selectedItem: 'ANY'
    })
    .controller('SimpleGridDemoController', function ($scope, $http, resourcesTableFilter, ngSimpleGridDemoDataService) {
        $scope.someDataFromParent = 'Родительская строка';

        $scope.filter = angular.extend({
            lpuName: "Поликлиника №_0_",
            departmentName: null
        }, resourcesTableFilter);
        $scope.updateResourcesFilter = function () {
            angular.forEach($scope.resourcesTableSettings.filter, function (val, key) {
                $scope.resourcesTableSettings.filter[key] = $scope.filter[key];
            });
        };
        $scope.clearResourcesFilter = function () {
            angular.extend($scope.filter, resourcesTableFilter);
            $scope.updateResourcesFilter();
        };

        $scope.tableSettings = {
            dataSource: function (settings) {
                // Эту часть на себя берут хххResource, реализованные в проекте,
                // поэтому в таблицу вернутся сразу данные
                return ngSimpleGridDemoDataService({
                    page: settings.queryParams.page
                }).then(function (respond) {
                    return respond.data;
                });
            },
            pagination: 'both',
            sortable: false
        };
        $scope.tableState = {
            pagination: {
            },
            sort: {
            }
        };
    })
;
