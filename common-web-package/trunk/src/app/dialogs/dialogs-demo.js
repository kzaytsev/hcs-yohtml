angular.module('dialogs-demo', ['common.dialogs'])
    .controller('dialogsDemoCtrl', ['$scope', 'commonDialogs', function ($scope, commonDialogs) {
        $scope.alertOpen = function () {
            commonDialogs.alert('Сообщаем!!');
        };
        $scope.errorOpen = function () {
            commonDialogs.error([
                'Сообщение об ошибке',
                [
                    'Сообщение об ошибке со списком:',
                    ['элемент списка #1', 'элемент списка #2', 'элемент списка #3']
                ],
                [
                    ['Сообщение об ошибке (строка #1)', 'Сообщение об ошибке (строка #2)', 'Сообщение об ошибке (строка #3)'],
                    ['элемент списка #1', 'элемент списка #2', 'элемент списка #3']
                ],
                [
                    ['Сообщение об ошибке (строка #1)', 'Сообщение об ошибке строка #2'],
                    'элемент списка (строка)'
                ],
                [
                    'Сообщение об ошибке',
                    'элемент списка (строка)'
                ],
                [
                    'Сообщение об ошибке (строка #1)'
                ]
            ], function () {
                console.log('from callback');
            });
        };
        $scope.switchedValue = true;
        $scope.switchValue = function () {
            $scope.switchedValue = !$scope.switchedValue;
        };
        $scope.saveStatus = "Есть несохраненные изменения.";
        $scope.yesNoCancelOpen = function () {
            commonDialogs.yesNoCancel({
                message: "Есть несохраненные данные.",
                yesAction: function () {
                    $scope.saveStatus = "Данные сохранены.";
                },
                noAction: function () {
                    $scope.saveStatus = "Вышли без сохранения.";
                }
            });
        };
        $scope.resetYesNoCancelDemo = function () {
            $scope.saveStatus = "Есть несохраненные изменения.";
        };
        $scope.openAutoHideDemo = function () {
            commonDialogs.autoHideConfirm("Автоматически закрывающееся сообщение.");
        };

        /**
         * Опции для передачи в директиву confirm-on-exit="confirmOptions"
         */
        $scope.confirmOptions = {
            /**
             * Callback (обязательный), вызываемый при нажатии на кнопку Сохранить
             *
             * @param redirectPath - путь куда пользователь решил перейти (пример использования $location.path(redirectPath))
             * Этот путь можно игнорировать, если в функции сохранения уже есть свой редирект.
             *
             * @param deregisterConfirm - функция дерегистрации от диалога подтверждения. Нужна если внутри конкретной
             * функции сохранения уже есть редирект. deregisterConfirm() нужно вызвать до своего редиректа (т.е.
             * передать в свою функцию сохранения и вызывать в нужном месте перед редиректом). Иначе диалог
             * появится еще раз.
             *
             * Если клиентская функция сохранения вызывается и в confirmCallback, и при нажатии на кнопку Сохранить
             * на форме, во втором случае deregisterConfirm отсутствует, поэтому перед редиректом нужно специально
             * выставить все значения по умолчанию (чтобы modelChanged вернула false),
             * или попробовать вызвать $scope.form.$setPristine().
             *
             * Получить объект формы в контроллере можно следующим образом:
             * <form name='myForm' confirm-on-exit='confirmOptions'>
             *     <div ng-init='setForm(myForm)'></div>
             * </form>
             *
             * $scope.setForm = function(form) {
             *      $scope.myForm = form;
             * };
             */
            confirmCallback: function (redirectPath, deregisterConfirm) {
                console.log('confirm');
            },

            /**
             * Callback, возвращающий признак изменились ли значения в форме
             * Если false (или параметр не передан), тогда диалог на подтверждение не будет показан
             * и произойдет уход со страницы
             *
             * @returns {boolean}
             */
            modelChanged: function () {
                return true;
            },

            /**
             * Callback, возвращающий признак valid/не valid значений формы, т.е. можно ли вызвать функцию сохранения
             * Если параметр не передан или возвратил true, при нажатии на Сохранить будет вызван confirmCallback
             *
             * @returns {boolean}
             */
            validForSave: function () {
                return false;
            },

            /**
             * Сообщение для вывода в диалоге подтверждения
             * Необязательный, по умолчанию 'Сохранить изменения?'
             */
            msg: 'Сохранить изменения?'
        };
    }])
;