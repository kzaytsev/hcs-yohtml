angular.module('ef-bpp-demo', ['common.ef-bpp'])
    .controller('EfBppCtrl', ['$scope', function ($scope) {
        $scope.files = [
//                { id: '', name: '', type: '', size: '', unit: '', decs: '' },
            { contentGuid: '-1', fileName: 'file1.txt', contentType: 'text/plain', fileSize: '25725', description: 'Некое описание текстового файла' },
            { contentGuid: '-2', fileName: 'file2.doc', contentType: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', fileSize: '893', description: 'Отчет о поверке систем учета' },
            { contentGuid: '-3', fileName: 'file3.xls', contentType: 'application/vnd.ms-excel', fileSize: '6839', description: 'Описание текущей директивы' },
            { contentGuid: '-3', fileName: 'file4.pdf', contentType: 'application/pdf', fileSize: '6839', description: 'PDF' },
            { contentGuid: '-3', fileName: 'file5.zip', contentType: 'application/zip', fileSize: '6839', description: 'ZIP' },
            { contentGuid: '-3', fileName: 'file6.gif', contentType: 'image/gif', fileSize: '6839', description: 'GIF' }
        ];
    }])
;