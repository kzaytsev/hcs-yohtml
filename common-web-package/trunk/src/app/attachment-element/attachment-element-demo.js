angular
    .module('attachment-element-demo', [
        'common.attachment-element'
    ])
    .controller('AttachmentElementCtrl', ['$scope', function ($scope) {
        $scope.attachment = null;
        $scope.filledAttachment = {
            "contentGuid": "c7727bbc-2310-41f5-b249-0ba2a97808d9", "fileSize": 1071, "fileName": "default.conf", "contentType": "application/msword", "createDate": "11.11.2014 16:15", "description": "Описание default.conf"
        };
        $scope.files = [angular.copy($scope.filledAttachment), angular.copy($scope.filledAttachment)];
        $scope.submit = function (form) {
            $scope.$broadcast('showError');
            console.log('form ' + form.$valid);
        };
    }])
;