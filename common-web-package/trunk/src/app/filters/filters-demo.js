angular.module('filter-demo', ['common.filter'])
    .controller('FilterCtrl', function ($scope) {

            //Структура HouseAddressWithNsi
            $scope.houseAddressObject = {
                region : { formalName : "Волгоградская область" },
                area : {  },
                city : { formalName : "Волгоград" },
                settlement : {  },
                street : { formalName : 'пл.Ленина' },
                house : {
                    houseNumber : 12,
                    buildingNumber : 45,
                    structNumber : 78
                }
            };

            //Структура объекта адреса, полученного из диалога выбора адреса по ФИАС
            $scope.houseAddressObjectFromDialog = {
                address : {
                    region : "Волгоградская область",
                    city : "Волгоград" ,
                    street : 'пл.Ленина'
                },
                houseNumber : 12,
                buildingNumber : 45,
                structNumber : 78
            };


    })
;