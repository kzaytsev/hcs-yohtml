angular.module('ef-vbfl-demo', ['common.ef-vbfl'])

    .controller('efVbflDemoCtrl', [
        '$scope',
        'efVbflDialog',
        function($scope,
                 efVbflDialog){
            $scope.selectPerson = function(){
                efVbflDialog.create().setFiasHouseCode('239be226-0417-478d-95bf-d73154b166df').show().then(function (result) {
                    console.log(result);
                }, function () {
                });
            };
            $scope.selectPersons = function(){
                efVbflDialog.create().setMultipleChoice(true).setFiasHouseCode('239be226-0417-478d-95bf-d73154b166df').show().then(function (result) {
                    console.log(result);
                }, function () {
                });
            };
        }
    ])
;

