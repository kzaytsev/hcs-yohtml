angular.module('person-registry-resource', ['backend-resource'])
    .constant('PERSON_REGISTRY_BACKEND_CONFIG', {
        baseUrl: '/personregistry/api/rest/services'
    })

    .factory('PersonRegistryResource', ['BackendResource', 'PERSON_REGISTRY_BACKEND_CONFIG', function (BackendResource, PERSON_REGISTRY_BACKEND_CONFIG) {
        function PersonRegistryResourceFactory(resourceUrl) {
            return new BackendResource(PERSON_REGISTRY_BACKEND_CONFIG, resourceUrl);
        }
        return PersonRegistryResourceFactory;
    }])
;