/**
 * Created by vkorzhov on 10/31/14.
 */
angular
    .module('inspection-resource', ['backend-resource'])
    .factory('InspectionResource', ['BackendResource', 'INSPECTION_BACKEND_CONFIG',
        function (BackendResource, INSPECTION_BACKEND_CONFIG) {
            function InspectionResourceFactory(resourceUrl) {
                return new BackendResource(INSPECTION_BACKEND_CONFIG, resourceUrl);
            }

            return InspectionResourceFactory;
        }
    ]);
