angular.module('work-planning-resource', ['backend-resource'])

    .factory('WorkPlanningResource', ['BackendResource', 'WORK_PLANNING_BACKEND_CONFIG', function (BackendResource, WORK_PLANNING_BACKEND_CONFIG) {
        function WorkPlanningResourceFactory(resourceUrl) {
            return new BackendResource(WORK_PLANNING_BACKEND_CONFIG, resourceUrl);
        }
        return WorkPlanningResourceFactory;
    }])
;