/**
 * Модуль для коммуникации с бэкендом
 */
angular.module('backend-resource', [
])
    .config(['$httpProvider', function ($httpProvider) {
        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }
        $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
    }])
    .factory('BackendResource', ['$http', '$q', function ($http, $q) {

        function BackendResourceFactory(BACKEND_CONFIG, resourceUrl) {

            function preparePathParams(requestUrl, pathParams) {
                var urlWithParams = requestUrl;
                angular.forEach(pathParams, function(value, key) {
                    urlWithParams = urlWithParams.replace(':' + key, value);
                });

                if (urlWithParams.indexOf(':') != -1) { //remove unspecified params
                    urlWithParams = urlWithParams.replace(/\w+=:\w+&?/g, '');
                }

                return urlWithParams;
            }

            var thenFactoryMethod = function (httpPromise, successcb, errorcb) {
                var scb = successcb || angular.noop;
                var ecb = errorcb || angular.noop;

                return httpPromise.then(function (response) {
                    var result;
                    if (response.data === " null ") {
                            return $q.reject({
                                code: 'resource.notfound',
                                resourceUrl: resourceUrl
                            });
                    } else {
                        result = response.data;
                    }
                    scb(result, response.status, response.headers, response.config);
                    return result;
                }, function (response) {
                    ecb(response.data, response.status, response.headers, response.config);
                    return $q.reject(response);
                });
            };

            $http.defaults.withCredentials = BACKEND_CONFIG.withCredentials;
            $http.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
            $http.defaults.headers.common['Accept'] = 'application/json; charset=utf-8';

            var requestUrl = '';
            if(BACKEND_CONFIG.protocol == null || BACKEND_CONFIG.protocol === '' || BACKEND_CONFIG.host == null || BACKEND_CONFIG.host === '' || BACKEND_CONFIG.port == null || BACKEND_CONFIG.port === ''){
                requestUrl = BACKEND_CONFIG.baseUrl + resourceUrl;//считаем, что используют относительные пути и reverse proxy
            }else{
                requestUrl = BACKEND_CONFIG.protocol + '://' + BACKEND_CONFIG.host + ':' + BACKEND_CONFIG.port + BACKEND_CONFIG.baseUrl + resourceUrl;
            }
            // a constructor for new resources
            var Resource = function (data) {
                angular.extend(this, data);
            };

            Resource.query = function (pathParams, queryJson, successcb, errorcb) {
                var httpPromise = $http.get(preparePathParams(requestUrl, pathParams), {params: queryJson});
                return thenFactoryMethod(httpPromise, successcb, errorcb);
            };

            Resource.queryPost = function (pathParams, data, successcb, errorcb) {
                var httpPromise = $http.post(preparePathParams(requestUrl, pathParams), data);
                return thenFactoryMethod(httpPromise, successcb, errorcb);
            };

            Resource.queryObject = function (pathParams, queryJson, successcb, errorcb) {
                var httpPromise = $http.get(preparePathParams(requestUrl, pathParams), {params: queryJson});
                return thenFactoryMethod(httpPromise, successcb, errorcb);
            };

            Resource.get = function (pathParams, successcb, errorcb) {
                var httpPromise = $http.get(preparePathParams(requestUrl, pathParams));
                return thenFactoryMethod(httpPromise, successcb, errorcb);
            };

            Resource.getById = function (id, successcb, errorcb) {
                var httpPromise = $http.get(requestUrl + '/' + id);
                return thenFactoryMethod(httpPromise, successcb, errorcb);
            };

            Resource.create = function (data, successcb, errorcb, pathParams) {
                var httpPromise = $http.post(preparePathParams(requestUrl, pathParams), data);
                return thenFactoryMethod(httpPromise, successcb, errorcb);
            };

            Resource.update = function (data, successcb, errorcb, pathParams) {
                var httpPromise = $http.put(preparePathParams(requestUrl, pathParams), data);
                return thenFactoryMethod(httpPromise, successcb, errorcb);
            };

            Resource.remove = function (id, successcb, errorcb, pathParams) {
                var httpPromise = $http['delete'](preparePathParams(requestUrl, pathParams) + '/' + id);
                return thenFactoryMethod(httpPromise, successcb, errorcb);
            };

            Resource.removeObject = function (data, successcb, errorcb) {
                var httpPromise = $http.put(requestUrl, angular.extend({}, data));
                return thenFactoryMethod(httpPromise, successcb, errorcb);
            };

            return Resource;
        }

        return BackendResourceFactory;

    }]);
