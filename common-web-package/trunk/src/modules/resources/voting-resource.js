angular.module('voting-resource', ['backend-resource'])

    .factory('VotingResource', ['BackendResource', 'VOTING_BACKEND_CONFIG', function (BackendResource, VOTING_BACKEND_CONFIG) {
        function VotingResourceFactory(resourceUrl) {
            return new BackendResource(VOTING_BACKEND_CONFIG, resourceUrl);
        }
        return VotingResourceFactory;
    }])
;