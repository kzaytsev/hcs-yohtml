angular.module('notification-resource', ['backend-resource'])
    .factory('NotificationResource', ['BackendResource', 'NOTIFICATION_BACKEND_CONFIG', function (BackendResource, NOTIFICATION_BACKEND_CONFIG) {
        return function (resourceUrl) {
            return new BackendResource(NOTIFICATION_BACKEND_CONFIG, resourceUrl);
        };
    }])
;