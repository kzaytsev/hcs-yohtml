angular
    .module('nsi-resource', ['backend-resource'])
    .factory('NsiResource', ['BackendResource', 'NSI_BACKEND_CONFIG', function (BackendResource, NSI_BACKEND_CONFIG) {
        return function (resourceUrl) {
            return new BackendResource(NSI_BACKEND_CONFIG, resourceUrl);
        };
    }]);