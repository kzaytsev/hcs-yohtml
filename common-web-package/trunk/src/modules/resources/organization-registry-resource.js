angular.module('organization-registry-resource', ['backend-resource'])
    .constant('ORGANIZATION_REGISTRY_BACKEND_CONFIG', {
        baseUrl: '/organizationregistry/api/rest/services'
    })

    .factory('OrganizationRegistryResource', ['BackendResource', 'ORGANIZATION_REGISTRY_BACKEND_CONFIG', function (BackendResource, ORGANIZATION_REGISTRY_BACKEND_CONFIG) {
        function OrganizationRegistryResourceFactory(resourceUrl) {
            return new BackendResource(ORGANIZATION_REGISTRY_BACKEND_CONFIG, resourceUrl);
        }
        return OrganizationRegistryResourceFactory;
    }])
;