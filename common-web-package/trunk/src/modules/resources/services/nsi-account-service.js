angular.module('ru.lanit.hcs.nsi.rest.NsiAccountService', [
    'nsi-resource'
]).factory('$NsiAccountService', ['NsiResource',
    function (NsiResource) {
        var self = this;

        var toUrl = function (part) {
            return '/nsi/accounts' + part;
        };
        self.ClosedAccountsResource = new NsiResource(toUrl('/close/reasons'));
        self.AdditionalServicesResource = new NsiResource(toUrl('/additional/services'));

        return {
            // Метод выполняет поиск причин закрытия лицевого счета, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.AccountService#findAccountCloseReason(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных причин закрытия лицевого счета при поиске, создании, редактировании сведений о лицевом счете
            // - поиска причин закрытия лицевого счета для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список причин закрытия лицевого счета, удовлетворяющих критертям поиска
            getAccountCloseReason: function (queryJson, successcb, errorcb) {
                return self.ClosedAccountsResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск дополнительных услуг, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.AccountService#findAccountAdditionalServices(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска дополнительных услуг при создании, редактировании электронного паспорта МКД/ЖД
            // - поиска дополнительных услуг для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - критерий поиска
            // @return Список дополнительных услуг, удовлетворяющих критериям поиска
            getAccountAdditionalServices: function (queryJson, successcb, errorcb) {
                return self.AdditionalServicesResource.query([], queryJson, successcb, errorcb);
            }
        };

}]);

