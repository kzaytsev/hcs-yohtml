angular.module('ru.lanit.hcs.nsi.rest.DeviceService', [
    'home-mgmt-resource'
]).factory('$DeviceService', ['HomeManagementResource',
    function (HomeManagementResource) {
        var self = this;

        var toUrl = function (part) {
            return '/devices/' + part;
        };

        self.DevicesSearchResource = new HomeManagementResource(toUrl('/search'));
        self.MunicipalitiesSearchResource = new HomeManagementResource(toUrl('/municipalities/search'));
        self.DeviceResource = new HomeManagementResource(toUrl('/:deviceGuid'));
        self.DeviceHistoryResource = new HomeManagementResource(toUrl('/:deviceGuid/history'));
        self.DeviceMeteringsMaxResource = new HomeManagementResource(toUrl('/:deviceGuid/meterings/max'));
        self.HousesResource = new HomeManagementResource(toUrl('/houses'));
        self.HousesMeasurementsResource = new HomeManagementResource(toUrl('/houses/:houseGuid/measurements'));
        self.ApartmentsResource = new HomeManagementResource(toUrl('/apartments'));
        self.ApartmentsMeasurementsContorlResource = new HomeManagementResource(toUrl('/apartments/measurements/control'));
        self.CalibrationsResource = new HomeManagementResource(toUrl('/calibrations'));
        self.ArchivalResource = new HomeManagementResource(toUrl('/archival'));
        self.MeasurementsResource = new HomeManagementResource(toUrl('/measurements'));
        self.ApartmentsMeasurementsResource = new HomeManagementResource(toUrl('/apartments/measurements'));
        self.HouseSearchResource = new HomeManagementResource(toUrl('/houses/search'));
        self.FilesResource = new HomeManagementResource(toUrl('/files'));
        self.HousesMeasurementsFilesResource = new HomeManagementResource(toUrl('/houses/measurements/files'));
        self.ApartmentsMeasurementsFilesResource = new HomeManagementResource(toUrl('/apartments/measurements/files'));
        self.MeasurementsPeriodsResource = new HomeManagementResource(toUrl('/measurements/periods'));
        self.DeviceSubstitutesResource = new HomeManagementResource(toUrl('/:deviceGuid/substitutes'));
        self.getCitizenMeteringResource = new HomeManagementResource(toUrl('/citizen/measurements'));
        self.getCitizenResourceConsumptionStatFilter = new HomeManagementResource(toUrl('/citizen/resource-consumption-stat-filter'));
        self.getCitizenResourceConsumptionStat = new HomeManagementResource(toUrl('/citizen/resource-consumption-stat'));

        return {
            // Метод используется для
            // - поиска раткких сведений о приборах учета в реестре приборов учета (для пользователей с правами П_ЖКХПУ_1; ВИ_ЖКХПУ_01, ВИ_ЖКХПУ_03)
            // @param deviceSearchCriteria
            //         - критерии поиска
            // @param pageIndex
            //         - Номер страницы, для которой необходимо сформировать данные
            // @param elementsPerPage
            //         - Количество элементов на странице
            // @return Список кратких сведений о приборах учета, удовлетворяющих критериям поиска
            findDevices: function (searchCriteria, successcb, errorcb, matrixParams) {
                return self.DevicesSearchResource.create(searchCriteria, successcb, errorcb, matrixParams);
            },
            // Метод используется для
            // - поиска кратких сведений о приборах учета в реестре приборов учета для муниципальных образований (для пользователей с правами П_ЖКХПУ_2; ВИ_ЖКХПУ_02, ВИ_ЖКХПУ_03)
            // @param deviceSearchCriteria
            //         - критерии поиска
            // @param pageIndex
            //         - Номер страницы, для которой необходимо сформировать данные
            // @param elementsPerPage
            //         - Количество элементов на странице
            // @return Список кратких сведений о приборах учета, удовлетворяющих критериям поиска
            findMunicipalitiesDevices: function (searchCriteria, successcb, errorcb, matrixParams) {
                return self.MunicipalitiesSearchResource.create(searchCriteria, successcb, errorcb, matrixParams);
            },
            // Метод используется для
            // - поиска прибора учета для его редактирования (ВИ_ЖКХПУ_05)
            // - поиска прибора учета для просмотра сведений о приборе учета (ВИ_ЖКХПУ_06, ВИ_ЖКХПУ_14, ВИ_ЖКХПУ_26)
            // @param deviceDetailSearchCriteria
            //         - критерии поиска
            // @return Сведения о приборе учета
            findDevice: function (deviceGuid, successcb, errorcb) {
                return self.DeviceResource.get({deviceGuid: deviceGuid}, successcb, errorcb);
            },
            // Метод используется для
            // - создания общедомового прибора учета (ВИ_ЖКХПУ_04)
            // @return Идентификационный код прибора учета
            //
            // @throws HouseNotFoundException
            //         - дом не найден в реестре адресных объектов
            // @throws HouseManagementException
            //         - у организации отсутствует подтвержденная связь на управление домом
            // @throws FieldValidationException
            //         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            // @throws DeviceNotUniqueException
            //         - для ОДПУ с установленным признаком "Коллективный прибор учета" сочетание номера ПУ, типа ПУ,\
            //         коммунальной услуги/ресурса, адреса ПУ не является уникальным; для остальных ПУ сочетание номера ПУ,
            //         типа ПУ, коммунальной услуги/ресурса не является уникальным
            // @throws OrganizationDoesNotProvideServiceException
            //         - организация в рамках дома ПУ не указана как предоставляющая коммунальную услугу,
            //         которой соответствует коммунальный ресурс ПУ (проверка №3)
            // @throws DeviceOperationDateLaterThanCurrentException
            //         - дата ввода в эксплуатацию ПУ позже текущей даты
            // @throws DeviceInstallationAndOperationDatesVerificationException
            //         - дата установки + Н_ЖКХ_174 месяц > дата ввода в эксплуатацию (проверка №49)
            addHouseDevice: function (createRequest, successcb, errorcb) {
                return self.HousesResource.create(createRequest, successcb, errorcb);
            },
            // Метод используется для
            // - создания общедомового прибора учета (ВИ_ЖКХПУ_04)
            // @return Идентификационный код прибора учета
            //
            // @throws HouseNotFoundException
            //         - дом не найден в реестре адресных объектов
            // @throws HouseManagementException
            //         - у организации отсутствует подтвержденная связь на управление домом
            // @throws FieldValidationException
            //         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            // @throws DeviceNotUniqueException
            //         - для ОДПУ с установленным признаком "Коллективный прибор учета" сочетание номера ПУ, типа ПУ,\
            //         коммунальной услуги/ресурса, адреса ПУ не является уникальным; для остальных ПУ сочетание номера ПУ,
            //         типа ПУ, коммунальной услуги/ресурса не является уникальным
            // @throws OrganizationDoesNotProvideServiceException
            //         - организация в рамках дома ПУ не указана как предоставляющая коммунальную услугу,
            //         которой соответствует коммунальный ресурс ПУ (проверка №3)
            // @throws DeviceOperationDateLaterThanCurrentException
            //         - дата ввода в эксплуатацию ПУ позже текущей даты
            // @throws DeviceInstallationAndOperationDatesVerificationException
            //         - дата установки + Н_ЖКХ_174 месяц > дата ввода в эксплуатацию (проверка №49)
            addApartmentDevice: function (createRequest, successcb, errorcb) {
                return self.ApartmentsResource.create(createRequest, successcb, errorcb);
            },
            // Метод используется для
            // - обновления общедомового прибора учета (ВИ_ЖКХПУ_05)
            // @param deviceUpdateRequest
            //         - запрос на обновление общедомового прибора учета
            // @throws DeviceNotFoundException
            //         - прибор учета не найден в реестре
            // @throws HouseNotFoundException
            //         - дом не найден в реестре адресных объектов
            // @throws HouseManagementException
            //         - у организации отсутствует подтвержденная связь на управление домом
            // @throws FieldValidationException
            //         - не пройдены проверки на корректность заполнения полей (обязательность, формат, изменение адреса дома,
            //         изменение помещения и т.п.)
            // @throws OrganizationDoesNotProvideServiceException
            //         - организация в рамках дома ПУ не указана как предоставляющая коммунальную услугу,
            //         которой соответствует коммунальный ресурс ПУ (проверка №3)
            // @throws DeviceNotUniqueException
            //         - для ОДПУ с установленным признаком "Коллективный прибор учета" сочетание номера ПУ, типа ПУ,
            //         коммунальной услуги/ресурса, адреса ПУ не является уникальным; для остальных ПУ сочетание номера ПУ, типа
            //         ПУ, коммунальной услуги/ресурса не является уникальным
            // @throws DeviceMeteringAlreadyExistException
            //         - наличия показаний у прибора учета помимо базового (проверка №21)
            // @throws DeviceIsArchivedException
            //         - прибор учёта является архивным
            // @throws DevicePlannedCalibrationDateHasComeException
            //        - планируемая дата поверки прибора учета уже наступила (проверка №7)
            // @throws DeviceInstallationAndOperationDatesVerificationException
            //         - дата установки + Н_ЖКХ_174 месяц > дата ввода в эксплуатацию (проверка №49)
            updateHouseDevice: function (updateRequest, successcb, errorcb) {
                return self.HousesResource.update(updateRequest, successcb, errorcb);
            },
            // Метод используется для
            // - обновления общедомового прибора учета (ВИ_ЖКХПУ_05)
            // @param deviceUpdateRequest
            //         - запрос на обновление общедомового прибора учета
            // @throws DeviceNotFoundException
            //         - прибор учета не найден в реестре
            // @throws HouseNotFoundException
            //         - дом не найден в реестре адресных объектов
            // @throws HouseManagementException
            //         - у организации отсутствует подтвержденная связь на управление домом
            // @throws FieldValidationException
            //         - не пройдены проверки на корректность заполнения полей (обязательность, формат, изменение адреса дома,
            //         изменение помещения и т.п.)
            // @throws OrganizationDoesNotProvideServiceException
            //         - организация в рамках дома ПУ не указана как предоставляющая коммунальную услугу,
            //         которой соответствует коммунальный ресурс ПУ (проверка №3)
            // @throws DeviceNotUniqueException
            //         - для ОДПУ с установленным признаком "Коллективный прибор учета" сочетание номера ПУ, типа ПУ,
            //         коммунальной услуги/ресурса, адреса ПУ не является уникальным; для остальных ПУ сочетание номера ПУ, типа
            //         ПУ, коммунальной услуги/ресурса не является уникальным
            // @throws DeviceMeteringAlreadyExistException
            //         - наличия показаний у прибора учета помимо базового (проверка №21)
            // @throws DeviceIsArchivedException
            //         - прибор учёта является архивным
            // @throws DevicePlannedCalibrationDateHasComeException
            //        - планируемая дата поверки прибора учета уже наступила (проверка №7)
            // @throws DeviceInstallationAndOperationDatesVerificationException
            //         - дата установки + Н_ЖКХ_174 месяц > дата ввода в эксплуатацию (проверка №49)
            updateApartmentDevice: function (updateRequest, successcb, errorcb) {
                return self.ApartmentsResource.update(updateRequest, successcb, errorcb);
            },
            // Метод используется для
            // - внесения контрольного показания по индивидуальному прибору учета (ВИ_ЖКХПУ_07, ВИ_ЖКХПУ_08)
            // @param request
            //         - запрос на внесение контрольного показания по индивидуальному прибору учета
            //
            // @throws FieldValidationException
            //         - не пройдены проверки на корректность заполнения полей (проверка №1)
            // @throws OrganizationDoesNotProvideServiceException
            //         - организация в рамках дома ПУ не указана как предоставляющая коммунальную услугу,
            //         которой соответствует коммунальный ресурс ПУ (проверка №8)
            // @throws DeviceIsArchivedException
            //         - прибор учёта является архивным (проверка №6)
            // @throws ParallelAccessException
            //         - с момента открытия экранной формы внесения контрольного показания ПУ было внесено изменений
            //         (проверка №10)
            // @throws DevicePlannedCalibrationDateHasComeException
            //         - плановая дата поверки ПУ уже наступила (проверка №7)
            // @throws DeviceMeteringInputDateLaterThanCurrentException
            //         - дата снятия показания позже текущей даты (проверка №24)
            // @throws DeviceMeteringCurrentValueLessThenLastValueException
            //         - введенное значение показания должно меньше, чем значение первого показания или больше чем значение
            //         второго показания (проверка №25)
            // @throws DeviceMeteringValueLessThanItPossibleException
            //         - введенное значение показания (по каждому тарифу отдельно) меньше максимального значения показания по ПУ
            //         (проверка №26)
            // @throws DeviceMeteringEnterPeriodExpiredException
            //         - истек период внесения изменений (проверка №27)
            enterControlDeviceMetering: function (createRequest, successcb, errorcb) {
                return self.ApartmentsMeasurementsContorlResource.create(createRequest,successcb , errorcb);
            },
            // Метод используется для
            // - удаления последнего внесенного показания ПУ (ВИ_ЖКХПУ_09)
            // @param request
            //         - запрос на удаления последнего внесенного показания ПУ
            //
            // @throws FieldValidationException-
            //         не пройдены проверки на корректность заполнения полей (проверка №1)
            // @throws OrganizationDoesNotProvideServiceException-
            //         организация в рамках дома ПУ не указана как предоставляющая коммунальную услугу,
            //         которой соответствует коммунальный ресурс ПУ (проверка №8)
            // @throws DeviceIsArchivedException
            //         - прибор учёта является архивным (проверка №6)
            // @throws ParallelAccessException
            //         - с момента открытия экранной формы внесения контрольного показания ПУ было внесено изменений
            //         (проверка №10)
            // @throws DevicePlannedCalibrationDateHasComeException
            //         - плановая дата поверки ПУ уже наступила (проверка №7)
            // @throws DeviceMeteringEnterPeriodExpiredException
            //         - истек период внесения изменений (проверка №27)
            deleteLastControlDeviceMetering: function (deleteRequest, successcb, errorcb) {
                return self.ApartmentsMeasurementsContorlResource.remove(deleteRequest, successcb, errorcb);
            },
            // Метод используется для
            // - возврата прибора учёта после проведения поверки (ВИ_ЖКХПУ_11)
            //
            // Метод вызывает                          ```
            // - метод {@link ru.lanit.hcs.nsi.api.DeviceService#findDeviceCalibrationInterval(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // - метод {@link ru.lanit.hcs.homemanagement.api.DeviceService#returnDeviceAfterCalibration(ru.lanit.hcs.homemanagement.api.dto.request.ReturnDeviceAfterCalibrationRequest)}
            //
            // @param request
            //         - запрос на возврат прибора учёта после проведения поверки
            //
            // @throws FieldValidationException
            //         - не пройдены проверки на корректность заполнения полей (проверка №32)
            // @throws OrganizationDoesNotProvideServiceException
            //         - организация в рамках дома ПУ не указана как предоставляющая коммунальную услугу,
            //         которой соответствует коммунальный ресурс ПУ (проверка №8)
            // @throws DeviceIsArchivedException
            //         - прибор учёта является архивным (проверка №6)
            // @throws ParallelAccessException
            //         - с момента открытия экранной формы архивации в ПУ были внесены изменения (проверка №38)
            // @throws DeviceEndCalibrationDateLaterThanCurrentException
            //         - дата окончания поверки позже текущей даты (проверка №39)
            // @throws DeviceBeginCalibrationDateLaterThanEndCalibrationDate
            //         - дата начала поверки ПУ позже даты окончания поверки (проверка №41)
            // @throws DeviceLastEndCalibrationDateLaterThanCurrentCalibrationDateException
            //         - дата окончания предыдущей поверки ПУ позже или равна дате начала текущей поверки (проверка №40)
            // @throws DeviceMeteringEndValueLessThanBeginValueException
            //         - показание на окончание поверки меньше, чем показание на начало поверки (проверка №42)
            // @throws DeviceMeteringValueLessThanItPossibleException
            //         - введенное значение показания (по каждому тарифу отдельно) меньше максимального значения показания по ПУ
            //          (проверка №44)
            // @throws DeviceMeteringDateValueVerificationException
            //         - (проверка №43)
            returnDeviceAfterCalibration: function (returnDeviceRequest, successcb, errorcb) {
                return self.CalibrationsResource.update(returnDeviceRequest, successcb, errorcb);
            },
            //* Метод используется для
            // - замены прибора учёта после проведения поверки (ВИ_ЖКХПУ_12, ВИ_ЖКХПУ_27)
            // @param request
            //         - запрос на замену прибора учёта после проведения поверки
            //
            // @throws FieldValidationException
            //         - не пройдены проверки на корректность заполнения полей (проверка №32)
            // @throws OrganizationDoesNotProvideServiceException
            //         - организация в рамках дома ПУ не указана как предоставляющая коммунальную услугу,
            //         которой соответствует коммунальный ресурс обоих ПУ (проверка №8)
            // @throws DeviceIsArchivedException
            //         - оба ПУ является архивным (проверка №6)
            // @throws DevicePlannedCalibrationDateHasComeException
            //         - планируемая дата поверки ПУ, на который осуществляется замена, уже наступила (проверку №7)
            // @throws ParallelAccessException
            //         - с момента открытия экранной формы архивации в ПУ были внесены изменения (проверка №33)
            // @throws DeviceEndCalibrationDateLaterThanCurrentException
            //         - дата окончания поверки позже текущей даты (проверка №34)
            // @throws DeviceLastEndCalibrationDateLaterThanCurrentCalibrationDateException
            //         - дата поверки раньше либо равна дате окончания предыдущей поверки ПУ (проверка №45)
            // @throws DeviceMeteringDateValueVerificationException
            //         - (проверка №35)
            // @throws DeviceMeteringValueLessThanItPossibleException
            //         - введенное значение показания (по каждому тарифу отдельно) меньше максимального значения показания по ПУ
            //         (проверка №36)
            // @throws DeviceNotUniqueException
            //         - сочетание номера ПУ, типа ПУ, коммунального ресурса не является уникальным
            substituteDeviceAfterCalibration: function (substituteDeviceRequest, successcb, errorcb) {
                return self.CalibrationsResource.create(substituteDeviceRequest, successcb, errorcb);
            },
            // Метод используется для
            // - архивации прибора учёта (ВИ_ЖКХПУ_13)
            //
            // Метод вызывает                          ```
            // - метод {@link ru.lanit.hcs.homemanagement.api.DeviceService#archiveDevice(ru.lanit.hcs.homemanagement.api.dto.request.ArchiveDeviceRequest)}
            //
            // @param request
            //         - запрос на архивацию прибора учёта
            //
            // @throws FieldValidationException
            //         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            // @throws OrganizationDoesNotProvideServiceException
            //         - организация в рамках дома ПУ не указана как предоставляющая коммунальную услугу,
            //         которой соответствует коммунальный ресурс ПУ (проверка №8)
            // @throws DeviceIsArchivedException
            //         - прибор учёта является архивным
            // @throws DevicePlannedCalibrationDateHasComeException
            //         - плановая дата поверки ПУ уже наступила
            // @throws ParallelAccessException
            //         - с момента открытия экранной формы архивации в ПУ были внесены изменения
            archiveDevice: function (archiveDeviceRequest, successcb, errorcb) {
                return self.ArchivalResource(archiveDeviceRequest, successcb, errorcb);
            },
            // Метод выполняет поиск показаний прибора учета, удовлетворяющих критериям поиска
            //
            // Используется для
            // - просмотра истории показаний прибора учета (ВИ_ЖКХПУ_10).
            // @param deviceMeteringSearchCriteria
            //         - критерии поиска
            // @param pageIndex
            //         - Номер страницы, для которой необходимо сформировать данные
            // @param elementsPerPage
            //         - Количество элементов на странице
            // @return Список показаний прибора учета, удовлетворяющих критериям поиска
            getDeviceMeteringHistory: function (houseGuid, deviceTypeGuid, successcb, errorcb) {
                return self.DeviceHistoryResource.get(houseGuid, deviceTypeGuid, successcb, errorcb);
            },
            // Метод используется для
            // - внесения текущих показаний по общедомовым приборам учета (ВИ_ЖКХПУ_21)
            // - внесения текущих показаний по индивидуальным приборам учета (ВИ_ЖКХПУ_29)
            // - изменения последнего внесенного текущего показания прибора учета (ВИ_ЖКХПУ_31)
            // - внесения текущих показаний по индивидуальным приборам учета гражданином (ВИ_ЖКХПУ_24)
            // @param enterDeviceMeteringRequest
            //         - Запрос на ввод показаний прибора учета
            //
            // @throws DeviceNotFoundException
            //         - прибор учета не найден в реестре
            // @throws HouseManagementException
            //         - у организации отсутствует подтвержденная связь на управление домом
            // @throws FieldValidationException
            //         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            // @throws DeviceMeteringEnterPeriodExpiredException
            //         - истек период ввода показаний прибора учета
            // @throws LinkBetweenCitizenAndAccountNotFoundException
            //         - пользователь не имеет связи с лицевым счетом (проверка №14)
            // @throws DeviceIsArchivedException
            //         - не все приборы учёта являются не архивными
            // @throws DevicePlannedCalibrationDateHasComeException
            //         - для некоторых ПУ, наступила плановая дата поверки
            // @throws DeviceMeteringCurrentValueLessThenLastValueException
            //         - значение показания ПУ меньше, чем значение показания (тип не учитывается),
            //         дата которого наиболее близкая к данному показанию в прошлом
            // @throws DeviceMeteringValueLessThanItPossibleException
            //         - введенное текущее показание ПУ не меньше максимального значения показания по ПУ (проверка №13)
            // @throws ParallelAccessException
            //        - с момента открытия экранной формы в ПУ были внесены изменения (проверка №10)
            // @throws OrganizationDoesNotProvideServiceException
            //         - организация в рамках дома ПУ не указана как предоставляющая коммунальную услугу,
            //         которой соответствует коммунальный ресурс ПУ (проверка №8)
            enterBuildingDevicesMetering: function (createRequest, successcb, errorcb) {
                return self.MeasurementsResource.create(createRequest, successcb, errorcb);
            },
            // Метод используется для
            // - внесения текущих показаний по общедомовым приборам учета (ВИ_ЖКХПУ_21)
            // - внесения текущих показаний по индивидуальным приборам учета (ВИ_ЖКХПУ_29)
            // - изменения последнего внесенного текущего показания прибора учета (ВИ_ЖКХПУ_31)
            // - внесения текущих показаний по индивидуальным приборам учета гражданином (ВИ_ЖКХПУ_24)
            // @param enterDeviceMeteringRequest
            //         - Запрос на ввод показаний прибора учета
            //
            // @throws DeviceNotFoundException
            //         - прибор учета не найден в реестре
            // @throws HouseManagementException
            //         - у организации отсутствует подтвержденная связь на управление домом
            // @throws FieldValidationException
            //         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            // @throws DeviceMeteringEnterPeriodExpiredException
            //         - истек период ввода показаний прибора учета
            // @throws LinkBetweenCitizenAndAccountNotFoundException
            //         - пользователь не имеет связи с лицевым счетом (проверка №14)
            // @throws DeviceIsArchivedException
            //         - не все приборы учёта являются не архивными
            // @throws DevicePlannedCalibrationDateHasComeException
            //         - для некоторых ПУ, наступила плановая дата поверки
            // @throws DeviceMeteringCurrentValueLessThenLastValueException
            //         - значение показания ПУ меньше, чем значение показания (тип не учитывается),
            //         дата которого наиболее близкая к данному показанию в прошлом
            // @throws DeviceMeteringValueLessThanItPossibleException
            //         - введенное текущее показание ПУ не меньше максимального значения показания по ПУ (проверка №13)
            // @throws ParallelAccessException
            //        - с момента открытия экранной формы в ПУ были внесены изменения (проверка №10)
            // @throws OrganizationDoesNotProvideServiceException
            //         - организация в рамках дома ПУ не указана как предоставляющая коммунальную услугу,
            //         которой соответствует коммунальный ресурс ПУ (проверка №8)
            changeBuildingDevicesMetering: function (updateRequest, successcb, errorcb) {
                return self.MeasurementsResource.create(createRequest, successcb, errorcb);
            },
            // Метод выполняет поиск кратких сведение о приборах учета с последними показаниями, удовлетворяющих критериям поиска
            //
            // Используется для
            // - поиска общедомовых приборов учета по дому с последними показаниями для ввода/редактирования текущих показаний (ВИ_ЖКХПУ_21)
            // - поиска индивидуальных приборов учета по лицевым счетам гражданина с последними показаниями для ввода/редактирования текущих показаний (ВИ_ППА_01, ВИ_ЖКХПУ_27)
            // - поиска сведений о индивидуальных приборах учета помещения с последним показанием для ввода/изменения показаний (ВИ_ЖКХПУ_29, ВИ_ЖКХПУ_31)
            // @param deviceWithCurrentMeteringSearchCriteria
            //         - критерии поиска
            // @return Список кратких сведение о приборах учета с последними показаниями, удовлетворяющих критериям поиска
            getBuildingDevicesMetering: function (houseGuid, deviceTypeGuid, successcb, errorcb) {
                return self.HousesMeasurementsResource.get({houseGuid: houseGuid, deviceTypeGuid: deviceTypeGuid}, successcb, errorcb);
            },
            // Метод используется для
            // - внесения текущих показаний по общедомовым приборам учета (ВИ_ЖКХПУ_21)
            // - внесения текущих показаний по индивидуальным приборам учета (ВИ_ЖКХПУ_29)
            // - изменения последнего внесенного текущего показания прибора учета (ВИ_ЖКХПУ_31)
            // - внесения текущих показаний по индивидуальным приборам учета гражданином (ВИ_ЖКХПУ_24)
            // @param enterDeviceMeteringRequest
            //         - Запрос на ввод показаний прибора учета
            //
            // @throws DeviceNotFoundException
            //         - прибор учета не найден в реестре
            // @throws HouseManagementException
            //         - у организации отсутствует подтвержденная связь на управление домом
            // @throws FieldValidationException
            //         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            // @throws DeviceMeteringEnterPeriodExpiredException
            //         - истек период ввода показаний прибора учета
            // @throws LinkBetweenCitizenAndAccountNotFoundException
            //         - пользователь не имеет связи с лицевым счетом (проверка №14)
            // @throws DeviceIsArchivedException
            //         - не все приборы учёта являются не архивными
            // @throws DevicePlannedCalibrationDateHasComeException
            //         - для некоторых ПУ, наступила плановая дата поверки
            // @throws DeviceMeteringCurrentValueLessThenLastValueException
            //         - значение показания ПУ меньше, чем значение показания (тип не учитывается),
            //         дата которого наиболее близкая к данному показанию в прошлом
            // @throws DeviceMeteringValueLessThanItPossibleException
            //         - введенное текущее показание ПУ не меньше максимального значения показания по ПУ (проверка №13)
            // @throws ParallelAccessException
            //        - с момента открытия экранной формы в ПУ были внесены изменения (проверка №10)
            // @throws OrganizationDoesNotProvideServiceException
            //         - организация в рамках дома ПУ не указана как предоставляющая коммунальную услугу,
            //
            enterPersonalDevicesMetering: function (createRequest, successcb, errorcb) {
                return self.ApartmentsMeasurementsResource.create(createRequest, successcb, errorcb);
            },
            // Метод используется для
            // - внесения текущих показаний по общедомовым приборам учета (ВИ_ЖКХПУ_21)
            // - внесения текущих показаний по индивидуальным приборам учета (ВИ_ЖКХПУ_29)
            // - изменения последнего внесенного текущего показания прибора учета (ВИ_ЖКХПУ_31)
            // - внесения текущих показаний по индивидуальным приборам учета гражданином (ВИ_ЖКХПУ_24)
            // @param enterDeviceMeteringRequest
            //         - Запрос на ввод показаний прибора учета
            //
            // @throws DeviceNotFoundException
            //         - прибор учета не найден в реестре
            // @throws HouseManagementException
            //         - у организации отсутствует подтвержденная связь на управление домом
            // @throws FieldValidationException
            //         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            // @throws DeviceMeteringEnterPeriodExpiredException
            //         - истек период ввода показаний прибора учета
            // @throws LinkBetweenCitizenAndAccountNotFoundException
            //         - пользователь не имеет связи с лицевым счетом (проверка №14)
            // @throws DeviceIsArchivedException
            //         - не все приборы учёта являются не архивными
            // @throws DevicePlannedCalibrationDateHasComeException
            //         - для некоторых ПУ, наступила плановая дата поверки
            // @throws DeviceMeteringCurrentValueLessThenLastValueException
            //         - значение показания ПУ меньше, чем значение показания (тип не учитывается),
            //         дата которого наиболее близкая к данному показанию в прошлом
            // @throws DeviceMeteringValueLessThanItPossibleException
            //         - введенное текущее показание ПУ не меньше максимального значения показания по ПУ (проверка №13)
            // @throws ParallelAccessException
            //        - с момента открытия экранной формы в ПУ были внесены изменения (проверка №10)
            // @throws OrganizationDoesNotProvideServiceException
            //         - организация в рамках дома ПУ не указана как предоставляющая коммунальную услугу,
            //
            changePersonalDevicesMetering: function (updateRequest, successcb, errorcb) {
                return self.ApartmentsMeasurementsResource.update(updateRequest, successcb, errorcb);
            },
            // Метод выполняет поиск кратких сведение о приборах учета с последними показаниями, удовлетворяющих критериям поиска
            //
            // Используется для
            // - поиска общедомовых приборов учета по дому с последними показаниями для ввода/редактирования текущих показаний (ВИ_ЖКХПУ_21)
            // - поиска индивидуальных приборов учета по лицевым счетам гражданина с последними показаниями для ввода/редактирования текущих показаний (ВИ_ППА_01, ВИ_ЖКХПУ_27)
            // - поиска сведений о индивидуальных приборах учета помещения с последним показанием для ввода/изменения показаний (ВИ_ЖКХПУ_29, ВИ_ЖКХПУ_31)
            // @param deviceWithCurrentMeteringSearchCriteria
            //         - критерии поиска
            // @return Список кратких сведение о приборах учета с последними показаниями, удовлетворяющих критериям поиска
            getPersonalDevicesMetering: function (houseGuid, apartmentGuid, deviceTypeGuid, accountNumber, successcb, errorcb) {
                return self.ApartmentsMeasurementsResource.get({houseGuid: houseGuid, apartmentGuid: apartmentGuid,
                    deviceTypeGuid: deviceTypeGuid, accountNumber: accountNumber},successcb, errorcb);
            },
            // Метод выполняет поиск кратких сведений о домах с приборами учета, удовлетворяющих критериям поиска
            // Используется для
            // - поиска домов с перечнем ОДПУ в реестре приборов учета
            // @param houseWithDeviceSearchCriteria - критерии поиска
            // @param pageIndex - Номер страницы, для которой необходимо сформировать данные
            // @param elementsPerPage - Количество элементов на странице
            // @return Список кратких сведений о домах с приборами учета, удовлетворяющих критериям поиска
            searchHousesWithDevices: function (regionGuid,areaGuid, cityGuid, settlementGuid, streetGuid, houseNumber,
                                               buildingNumber, structNumber, municipalResourceGuid, deviceNumber, collectiveDevice,
                                               meteringStatus, page, itemsPerPage, sortDir, sortedBy, successcb, errorcb) {
                return self.HouseSearchResource.get({
                        regionGuid: regionGuid,
                        areaGuid: regionGuid,
                        cityGuid: cityGuid,
                        settlementGuid: settlementGuid,
                        streetGuid: streetGuid,
                        buildingNumber: buildingNumber,
                        structNumber: structNumber,
                        municipalResourceGuid: municipalResourceGuid,
                        deviceNumber: deviceNumber,
                        collectiveDevice: collectiveDevice,
                        meteringStatus: meteringStatus,
                        page: page,
                        itemsPerPage: itemsPerPage,
                        sortDir: sortDir,
                        sortedBy: sortedBy
                    }, successcb, errorcb);
            },
            // Метод осуществляет импорт сведений о приборах учёта
            //
            // Метод используется для
            // - ВИ_ЖКХПУ_30
            // @param request
            //         - запрос на импорт сведений о приборах учёта
            //
            // @return - результат импорта сведений о приборах учёта
            uploadDeviceInfoFile: function (inputFileData, successcb, errorcb) {
                return self.FilesResource.create(inputFileData, successcb, errorcb);
            },
            // Метод осуществляет импорт сведений о показаниях приборов учёта
            //
            // Метод используется для
            // - ВИ_ЖКХПУ_34
            // @param request
            //         - запрос на импорт сведений о показаниях приборов учёта
            //
            // @return - результат импорта сведений о показаниях приборов учёта
            uploadDeviceHouseMeteringInfoFile: function (inputFileData, successcb, errorcb) {
                return self.HousesMeasurementsFilesResource.create(inputFileData, successcb, errorcb);
            },
            // Метод осуществляет импорт сведений о показаниях приборов учёта
            //
            // Метод используется для
            // - ВИ_ЖКХПУ_34
            // @param request
            //         - запрос на импорт сведений о показаниях приборов учёта
            //
            // @return - результат импорта сведений о показаниях приборов учёта
            uploadDeviceApartmentMeteringInfoFile: function (inputFileData, successcb, errorcb) {
                return self.ApartmentsMeasurementsFilesResource.create(inputFileData, successcb, errorcb);
            },
            // Метод определяет текущий период сдачи показаний по ПУ
            // @param request
            //         - запрос на определение текущего периода сдачи показаний по ПУ
            //
            // @return текущий период сдачи показаний ПУ
            getDeviceMeteringReportingPeriod: function (deviceGuid, successcb, errorcb) {
                return self.MeasurementsPeriodsResource.get(deviceGuid, successcb, errorcb);
            },
            // Метод выполняет поиск показаний прибора учета, удовлетворяющих критериям поиска
            //
            // Используется для
            // - просмотра истории показаний прибора учета (ВИ_ЖКХПУ_10).
            // @param deviceMeteringSearchCriteria
            //         - критерии поиска
            // @param pageIndex
            //         - Номер страницы, для которой необходимо сформировать данные
            // @param elementsPerPage
            //         - Количество элементов на странице
            //
            // @return Список показаний прибора учета, удовлетворяющих критериям поиска
            getMaxDeviceMeteringValue: function (deviceGuid, successcb, errorcb) {
                return self.DeviceMeteringsMaxResource.get({deviceGuid: deviceGuid}, successcb, errorcb);
            },
            // Метод используется для
            // - контекстного поиска номеров ПУ при заполнении полей формы замены прибора учёта (ВИ_ЖКХПУ_12, ВИ_ЖКХПУ_27)
            //
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.homemanagement.api.DeviceService#findDeviceNumbers(ru.lanit.hcs.homemanagement.api.dto.search.DeviceContextSearchCriteria)}
            //
            // @param searchCriteria
            //         - критерии поиска
            //
            // @return Список номеров ПУ, удовлетворяющих критерию поиска
            getSubstitutes: function(deviceGuid, successcb, errorcb) {
                return self.DeviceSubstitutesResource.get({deviceGuid: deviceGuid}, successcb, errorcb);
            },
            /**
             * ВИ_ЖКХПУ_24: Внесение текущих показаний по индивидуальным приборам учета гражданином.
             *
             * @param queryParam - houseGuid Идентификатор Жилого дома, apartmentGuid Идентификатор помещения, roomGuid Идентификационный код комнаты
             * @param successcb
             * @param errorcb
             * @return список ПУ с сообщениями
             */
            getCitizenMetering: function(queryParam, successcb, errorcb) {
                return self.getCitizenMeteringResource.queryObject({}, queryParam, successcb, errorcb);
            },
            /**
             * Метод получения значений фильтров для запроса годовой статистики потребления коммунальных ресурсов гражданином.
             *
             * @param queryParam - houseGuid Идентификатор Жилого дома, apartmentGuid Идентификатор помещения, roomGuid Идентификационный код комнаты
             * @param successcb
             * @param errorcb
             * @return значения фильтров для получения статистика ежемесячного потребления коммунальных ресурсов
             * гражданином за год.
             */
            getCitizenResourceConsumptionStatFilter: function (queryParam, successcb, errorcb) {
                return self.getCitizenResourceConsumptionStatFilter.queryObject({}, queryParam, successcb, errorcb);
            },
            /**
             * Метод получения годовой статистики потребления коммунальных ресурсов гражданином.
             *
             * @param queryParam - houseGuid Идентификатор Жилого дома, apartmentGuid Идентификатор помещения, roomGuid Идентификационный код комнаты,
             * year год потребления, municipalResourceCode потребляемый комунальный ресурс
             * @param successcb
             * @param errorcb
             * @return статистика по годовому потреблению коммунальных ресурсов гражданином.
             */
            getCitizenResourceConsumptionStat: function (queryParam, successcb, errorcb) {
                return self.getCitizenResourceConsumptionStat.queryObject({}, queryParam, successcb, errorcb);
            }
        };
}]);