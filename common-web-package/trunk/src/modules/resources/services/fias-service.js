angular.module('ru.lanit.hcs.nsi.rest.FiasService', [
    'nsi-resource'
]).factory('$FiasService', ['NsiResource',
    function (NsiResource) {
        var self = this;

        var toUrl = function (part) {
            return '/nsi/fias' + part;
        };

        self.RegionsResource = new NsiResource(toUrl('/regions'));
        self.AreasResource = new NsiResource(toUrl('/areas'));
        self.CitiesResource = new NsiResource(toUrl('/cities'));
        self.SettlementsResource = new NsiResource(toUrl('/settlements'));
        self.StreetsResource = new NsiResource(toUrl('/streets'));
        self.HouseNumbersResource = new NsiResource(toUrl('/house/numbers'));
        self.BuildingResource = new NsiResource(toUrl('/buildings'));
        self.StructsResource = new NsiResource(toUrl('/structs'));
        self.HousesResource = new NsiResource(toUrl('/houses'));
        self.SearchResource = new NsiResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.Address = new NsiResource(toUrl('/addresses/hierarchies'));        
        self.GuidResource = new NsiResource(toUrl('/FiasAddress'));        

        return {

            /**
             * Метод выполняет поиск записей ФИАС, удовлетворяющих критериям поиска
             * Используется для (1ЭФ_НСИ_КЛСФ_БП_ФИАС,ЭФ_НСИ_КЛСФ_ПРСМ_ФИАС)
             * - поиска оъектов по ФИАС для отображения на экранной форме (А_НСИ_04)
             * Метод вызывает
             * - метод {@link ru.lanit.hcs.nsi.api.FiasService#search(ru.lanit.hcs.nsi.api.dto.search.FiasObjectSearchCriteria, Integer, Integer)}
             * @param fiasObjectSearchCriteria - критерий поиска
             * @param pageIndex - Номер страницы, для которой необходимо сформировать данные
             * @param elementsPerPage - Количество элементов на странице
             * @return объект ФИАС в общем виде
             */
            search: self.SearchResource.queryPost,

            /**
             * Метод выполняет поиск записи ФИАС, удовлетворяющих критериям поиска
             * Используется для (1ЭФ_НСИ_КЛСФ_БП_ФИАС,ЭФ_НСИ_КЛСФ_ПРСМ_ФИАС)
             * - поиска полной информации по оъекту ФИАС для отображения на экранной форме (А_НСИ_04)
             * @param fiasAddressSearchCriteria - критерий поиска
             * @return объект ФИАС подробное описание
             */
            findFiasObjectByGuid: function (guid, successcb, errorcb) {
                return self.GuidResource.getById(guid, successcb, errorcb);
            },
            
            // Метод выполняет поиск субъектов РФ по ФИАС, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.FiasService#findFiasRegion(ru.lanit.hcs.nsi.api.dto.search.FiasRegionSearchCriteria)}
            // Используется для
            // - поиска актуальных субъектов РФ по ФИАС на формах ЭФ_ВвА, ЭФ_ПА, доступных организации для выбора
            // - поиска субъектов РФ по ФИАС для отображения на экранных формах при просмотре
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.FiasService#findFiasRegion(ru.lanit.hcs.nsi.api.dto.search.FiasRegionSearchCriteria)}
            // @param fiasRegionSearchCriteria - криткерий поиска
            // @return Список субъектов РФ по ФИАС, удовлетворяющих критериям поиска
            findRegions: function (queryJson, successcb, errorcb) {
                return self.RegionsResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск районов по ФИАС, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.FiasService#findFiasArea(ru.lanit.hcs.nsi.api.dto.search.FiasAreaSearchCriteria)}
            // Используется для
            // - поиска актуальных районов по ФИАС на формах ЭФ_ВвА, ЭФ_ПА, доступных организации для выбора
            // - поиска районов по ФИАС для отображения на экранных формах при просмотре
            // @param fiasAreaSearchCriteria - критерии поиска
            // @return Список районов по ФИАС, удовлетворяющих критериям поиска
            findAreas: function (queryJson, successcb, errorcb) {
                return self.AreasResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск городов по ФИАС, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.FiasService#findFiasCity(ru.lanit.hcs.nsi.api.dto.search.FiasCitySearchCriteria)}
            // Используется для
            // - поиска актуальных городов по ФИАС на формах ЭФ_ВвА, ЭФ_ПА, доступных организации для выбора
            // - поиска городов по ФИАС для отображения на экранных формах при просмотре
            // @param fiasCitySearchCriteria - критерии поиска
            // @return Список городов по ФИАС, удовлетворяющих критериям поиска
            findCities: function (queryJson, successcb, errorcb) {
                return self.CitiesResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск населенных пунктов по ФИАС, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.FiasService#findFiasSettlement(ru.lanit.hcs.nsi.api.dto.search.FiasSettlementSearchCriteria)}
            // Используется для
            // - поиска актуальных населенных пунктов по ФИАС на формах ЭФ_ВвА, ЭФ_ПА, доступных организации для выбора
            // - поиска населенных пунктов по ФИАС для отображения на экранных формах при просмотре
            // @param fiasSettlementSearchCriteria - критерии поиска
            // @return Список населенных пунктов по ФИАС, удовлетворяющих критериям поиска
            findSettlements: function (queryJson, successcb, errorcb) {
                return self.SettlementsResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск улиц по ФИАС, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.FiasService#findFiasStreet(ru.lanit.hcs.nsi.api.dto.search.FiasStreetSearchCriteria)}
            // Используется для
            // - поиска актуальных улиц по ФИАС на формах ЭФ_ВвА, ЭФ_ПА, доступных организации для выбора
            // - поиска улиц по ФИАС для отображения на экранных формах при просмотре
            // @param fiasStreetSearchCriteria - критерии поиска
            // @return Список улиц по ФИАС, удовлетворяющих критериям поиска
            findStreets: function (queryJson, successcb, errorcb) {
                return self.StreetsResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск номеров домов по ФИАС среди домов, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.FiasService#findFiasNumber(ru.lanit.hcs.nsi.api.dto.search.FiasNumberSearchCriteria)}
            // Используется для
            // - поиска номеров актуальных домов по ФИАС на формах ЭФ_ВвА, ЭФ_ПА, доступных организации для выбора
            // @param fiasNumberSearchCriteria - критерии поиска
            // @return Список номеров домов по ФИАС среди домов, удовлетворяющих критериям поиска
            findHouseNumbers: function (queryJson, successcb, errorcb) {
                return self.HouseNumbersResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск корпусов домов по ФИАС среди домов, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.FiasService#findFiasBuilding(ru.lanit.hcs.nsi.api.dto.search.FiasBuildingSearchCriteria)}
            // Используется для
            // - поиска корпусов актуальных домов по ФИАС на формах ЭФ_ВвА, ЭФ_ПА, доступных организации для выбора
            // @param fiasBuildingSearchCriteria - критерии поиска
            // @return Список корпусов домов по ФИАС среди домов, удовлетворяющих критериям поиска
            findBuildings: function (queryJson, successcb, errorcb) {
                return self.BuildingResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск строений актуальных домов по ФИАС среди домов, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.FiasService#findFiasStruct(ru.lanit.hcs.nsi.api.dto.search.FiasStructSearchCriteria)}
            // Используется для
            // - поиска строений домов по ФИАС на формах ЭФ_ВвА, ЭФ_ПА, доступных организации для выбора
            // @param fiasStructSearchCriteria - критерии поиска
            // @return Список строений домов по ФИАС среди домов, удовлетворяющих критериям поиска
            findStructs: function (queryJson, successcb, errorcb) {
                return self.StructsResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск домов по ФИАС, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.FiasService#findFiasHouse(ru.lanit.hcs.nsi.api.dto.search.FiasHouseSearchCriteria)}
            // Используется для
            // - поиска домов по ФИАС для отображения на экранных формах при просмотре
            // @param fiasHouseSearchCriteria - критерии поиска
            // @return Список домов по ФИАС, удовлетворяющих критериям поиска
            findHouses: function (queryJson, successcb, errorcb) {
                return self.HousesResource.query([], queryJson, successcb, errorcb);
            },
            /**
             * Метод выполняет адреса по ФИАС, удовлетворяющих критериям поиска
             * Метод вызывает
             * - метод {@link ru.lanit.hcs.nsi.api.FiasService#findFiasAddress(ru.lanit.hcs.nsi.api.dto.search.FiasAddressSearchCriteria)}
             * @param fiasAddressSearchCriteria - критерии поиска
             * @return Иерархию адресов по справочнику ФИАС
             */
            findAddress: function (queryJson, successcb, errorcb) {
                return self.Address.query([], queryJson, successcb, errorcb);
            }
        };
}]);