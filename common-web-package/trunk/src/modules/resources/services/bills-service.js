angular
    .module('ru.lanit.hcs.bills.rest.BillsService', ['bills-resource'])
    .factory('$BillsService', ['BillsResource', function (BillsResource) {
        var self = this;

        self.resources = {};
        self.pathPrefix = '/bills';

        self.getResource = function (address) {
            var resource = null;
            if (self.resources[address]) {
                resource = self.resources[address];
            } else {
                resource = new BillsResource(self.pathPrefix + address);
                self.resources[address] = resource;
            }

            return resource;
        };

        return {
            createPaymentDocument: function (request) {
                return self.getResource('/payment/documents').create(request);
            },
            updatePaymentDocument: function (request) {
                return self.getResource('/payment/documents').update(request);
            },
            deletePaymentDocument: function (request) {
                return self.getResource('/payment/documents/removal').update(request);
            },
            invoicePaymentDocument: function (request) {
                return self.getResource('/payment/documents/invoice').update(request);
            },
            withdrawPaymentDocument: function (request) {
                return self.getResource('/payment/document/withdrawal').update(request);
            },
            invoicePaymentDocuments: function (request) {
                return self.getResource('/payment/documents/invoices').update(request);
            },
            withdrawPaymentDocuments: function (request) {
                return self.getResource('/payment/documents/withdrawal').update(request);
            },
            findManagementPaymentDocuments: function (request, queryParams) {
                return self.getResource('/account/payment/documents/search;page=:page;itemsPerPage=:itemsPerPage')
                    .create(request, angular.noop, angular.noop, queryParams);
            },
            findPaymentDocumentWithNsi: function (request) {
                return self.getResource('/payment/documents/detail/search').create(request);
            },
            findAccountServicesWithNsi: function (request) {
                return self.getResource('/account/services/search').create(request);
            },
            findOrganizationAccountPeriod: function (request) {
                return self.getResource('/organization/periods/search').create(request);
            },
            findAccountChargesWithNsi: function (request) {
                return self.getResource('/account/charges/detail/search').create(request);
            },
            findAccountCharges: function (request, queryParams) {
                return self.getResource('/account/charges/search;page=:page;itemsPerPage=:itemsPerPage')
                    .create(request, angular.noop, angular.noop, queryParams);
            },
            findCitizenAccountCharges: function (request, queryParams) {
                return self.getResource('/citizen/account/charges/search;page=:page;itemsPerPage=:itemsPerPage')
                    .create(request, angular.noop, angular.noop, queryParams);
            },
            enterAccountCharges: function (request) {
                return self.getResource('/account/charges/enter').update(request);
            },
            openOrganizationAccountPeriod: function (request) {
                return self.getResource('/organization/periods/open').create(request);
            },
            closeOrganizationAccountPeriod: function (request) {
                return self.getResource('/organization/periods/closing').update(request);
            },
            findAccountChargesWithOrgAndNsi: function (request, queryParams) {
                return self.getResource('/account/charges/history/search;page=:page;itemsPerPage=:itemsPerPage')
                    .create(request, angular.noop, angular.noop, queryParams);
            },
            closeOrganizationHouseAccountPeriod: function (request) {
                return self.getResource('/house/periods/closing').update(request);
            },
            findPaymentDocumentsReferences: function (request) {
                return self.getResource('/payment/documents/references/search').create(request);
            },
            findPaymentDocuments: function (request) {
                return self.getResource('/payment/documents/search').create(request);
            },
            findPaymentDocumentWithOrgAndNsi: function (request) {
                return self.getResource('/payment/documents').getById(request);
            },
            //TODO добавить корректный url, когда появится rest
            findPaymentStatistic: function (request, queryParams) {
                return self.getResource('/payment/statistic/search;page=:page;itemsPerPage=:itemsPerPage')
                    .create(request, angular.noop, angular.noop, queryParams);
            }
        };
    }]);