angular
    .module('ru.lanit.hcs.eventjournal.rest.JournalPpaRestService', [
        'eventjournal-resource'
    ])
    .factory('$JournalPpaRestService', function (EventJournalResource) {
        var self = this,
            toUrl = function (part) {
                return '/ppa/journal' + part;
            };

        self.detailedFindEventsResource = new EventJournalResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.getEventGroupListResource = new EventJournalResource(toUrl('/event/grouplist'));
        self.getEventTypesByGroupResource = new EventJournalResource(toUrl('/event/typelist;groups=:groups'));

        return {

            detailedFindEvents: function (pathParams, searchCriteria, successCalback, errorCallback) {
                return self.detailedFindEventsResource.queryPost(pathParams, searchCriteria, successCalback, errorCallback);
            },
            getEventGroupList: function (params, successCalback, errorCallback) {
                return self.getEventGroupListResource.get({}, successCalback, errorCallback);
            },
            getEventTypesByGroup: function (pathParams, successCalback, errorCallback) {
                return self.getEventTypesByGroupResource.get(pathParams, successCalback, errorCallback);
            }

        };
    })

;