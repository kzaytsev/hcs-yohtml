angular.module('ru.lanit.hcs.nsi.rest.OrgNsiService', [
    'org-nsi-resource'
])


    .factory('$OrgNsiService', ['OrgNsiResource',
    function (OrgNsiResource) {
        var self = this;

        var toUrl = function (part) {
            return '/organizations' + part;
        };
        
        self.ReferencesResource = new OrgNsiResource(toUrl('/dictionaries/references'));
        self.ReferencesHeaders = new OrgNsiResource(toUrl('/hierarchical/dictionaries/headers'));
        self.techMunicipalServices = new OrgNsiResource(toUrl('/technical/municipal/services?guid=:guid'));
        self.mainMunicipalServices = new OrgNsiResource(toUrl('/main/municipal/services?guid=:guid'));
        self.worksResource = new  OrgNsiResource(toUrl('/works?guid=:guid&code=:code'));
        self.TechMunicipalServicesResource = new OrgNsiResource(toUrl('/technical/municipal/services'));
        self.AdditionalServicesResource = new OrgNsiResource(toUrl('/additional/services'));
        self.historyResource = new OrgNsiResource(toUrl('/entities/:nsiObjectName/:guid/history'));

        return {
            // Метод выполняет поиск справочников, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiOrganizationServic#findNsiOrgReferences(ru.lanit.hcs.nsi.api.dto.search.ReferenceSearchCriteria)}
            // Используется для
            // - поиска справочников для отображения на экранных формах при просмотре
            // @param referenceSearchCriteria - критерий поиска
            // @return Список справочников, удовлетворяющих критериям поиска
            findOrgNsiReferences: function (name, successcb, errorcb) {
                return self.ReferencesResource.query([],{name: name}, successcb, errorcb);
            },
            findOrgNsiHeaders: function (queryJson, successcb, errorcb) {
                return self.ReferencesResource.query([], queryJson, successcb, errorcb);
            },
            findMainMunicipalServices:function(guid, successcb, errorcb){
                return self.mainMunicipalServices.get({guid: guid}, successcb, errorcb);
            },
            findTechMunicipalServices:function(guid, successcb, errorcb){
                return self.techMunicipalServices.get({guid: guid}, successcb, errorcb);
            },
            findOrganizationWorks:function(queryJson, successcb, errorcb){
                return self.worksResource.query([], queryJson, successcb, errorcb);
            },

            //FIXME должен быть один метод findTechMunicipalServices,
            //который принимает объект queryParams
            /**
             * @QueryParam("code") String code,
             * @QueryParam("name") String name,
             * @QueryParam("guid") List<String> guids,
             * @QueryParam("status") @DefaultValue("ACTIVE") Status status
             * */
            findTechServices:function(queryParams, successcb, errorcb){
                return self.TechMunicipalServicesResource.query([], queryParams, successcb, errorcb);
            },

            /**
             * @QueryParam("code") String code,
             * @QueryParam("name") String name,
             * @QueryParam("guid") List<String> guids,
             * @QueryParam("status") @DefaultValue("ACTIVE") Status status
             * */
            findAdditionalService:function(queryParams, successcb, errorcb){
                return self.AdditionalServicesResource.query([], queryParams, successcb, errorcb);
            },
            getHistory:function(rootGuid,nsiObjectName, successcb, errorcb){
                return self.historyResource.get({guid: rootGuid, nsiObjectName:nsiObjectName}, successcb, errorcb);
            }
        };
    }])
    .factory('OrgClassifierResource', ['OrgNsiResource',
        function (OrgNsiResource) {
            var rootResource = new OrgNsiResource("/organizations/hierarchical/dictionaries/nodes/roots");
            var headerResource = new OrgNsiResource("/organizations/dictionaries/references");
            var searchResource = new OrgNsiResource("/organizations/hierarchical/dictionaries/nodes/search");
            var childrenResource = new OrgNsiResource("/organizations/hierarchical/dictionaries/nodes/children");

            return {
                roots : function() {
                    return rootResource;
                },
                search: function() {
                    return searchResource;
                },
                children : function() {
                    return childrenResource;
                },
                header :function(){
                    return headerResource;
                }

            };
        }
    ]).factory('OrgReferenceResource', ['OrgNsiResource',
        function (OrgNsiResource) {
            var self = this;
            self.prefix = '/organizations';
            //для post/put
            self.createResources={
                AdditionalService: new OrgNsiResource(self.prefix + '/additional/services'),
                AdditionalServiceType: new OrgNsiResource(self.prefix + '/additional/service/types'),
                MainMunicipalService: new OrgNsiResource(self.prefix + '/main/municipal/services'),
                TechnicalMunicipalService: new OrgNsiResource(self.prefix + '/technical/municipal/services'),
                OrganizationWork: new OrgNsiResource(self.prefix + '/organization/work')
                // OperationPrincipleType: new NsiResource(self.prefix + '/operation/principle/types'),
                // FuelType: new NsiResource(self.prefix + '/fuel/types'),
                // CooperativeSupportService: new NsiResource(self.prefix + '/cooperative/support/services'),
                // HousingService: new NsiResource(self.prefix + '/housing/services'),
                // GeneralMeetingDecisionsType: new  NsiResource(self.prefix + '/general/meeting/decisions/types')
            };

            self.resources = {};
            self.recordResource = new OrgNsiResource(self.prefix + '/standard/dictionaries');
            self.headerResource = new OrgNsiResource(self.prefix + '/dictionaries/references');
            self.copyRequest =  new OrgNsiResource(self.prefix+'/organization/works/copy');
            self.nextHierarchyCodeResource = new OrgNsiResource(self.prefix + '/next/hierarchy/code');
            self.hierarchyChildsResource = new OrgNsiResource(self.prefix + '/hierarchy/children');
            self.recordDeleteResource = new OrgNsiResource(self.prefix + '/hierarchical/dictionaries/removal');
            self.recordRestoreResource = new OrgNsiResource(self.prefix + '/hierarchical/dictionaries/recovery');


           // self.searchReferenceResource = new OrgNsiResource(self.prefix + '/references');
            self.nextCodeResource = new OrgNsiResource(self.prefix + '/next/code');

            self.searchReferenceResource = new OrgNsiResource(self.prefix + '/dictionaries/references');            

            //self.roots = new OrgNsiResource(self.prefix + '/hierarchical/dictionaries/nodes/root');
            //self.search = new OrgNsiResource(self.prefix + '/hierarchical/dictionaries/nodes/search');

            /*self.recordDeleteResource = new NsiResource(self.prefix + '/standard/dictionaries/removal');
             self.recordRestoreResource = new NsiResource(self.prefix + '/standard/dictionaries/recovery');
             self.nextCodeResource = new NsiResource(self.prefix + '/next/code');
             self.referenceHeaders = new NsiResource(self.prefix + '/hierarchical/dictionaries/headers');
             self.hierarchyChildsResource = new NsiResource(self.prefix + '/hierarchy/children');
             //self.DictionariesTableDataResource = new NsiResource(self.prefix +'/dictionaries/table/data');*/

            return {
                searchReferences: function() {
                    // Метод выполняет поиск справочников, удовлетворяющих критериям поиска
                    // Метод вызывает
                    // - метод {@link ru.lanit.hcs.nsi.api.NsiOrganizationServic#findNsiOrgReferences(ru.lanit.hcs.nsi.api.dto.search.ReferenceSearchCriteria)}
                    // Используется для
                    // - поиска справочников для отображения на экранных формах при просмотре
                    // @param referenceSearchCriteria - критерий поиска
                    // @return Список справочников, удовлетворяющих критериям поиска
                    //return self.searchReferenceResource;
                    return self.headerResource;
                },
                /*load: function(reference) {
                 if (!self.resources[reference]) {
                 self.resources[reference] = new NsiResource(self.prefix + '/' + reference);
                 }
                 return self.resources[reference];
                 },*/

                record: function(nsiObjectName) {
                    var resource = self.createResources[nsiObjectName]?self.createResources[nsiObjectName]:self.recordResource;
                    return {
                        on : function() {
                            return resource ? resource : self.recordResource;
                        },
                        remove : function() {
                            return self.recordDeleteResource;
                        },
                        restore : function() {
                            return self.recordRestoreResource;
                        }
                    };
                },

                nextCode: function() {
                    return self.nextCodeResource;
                },
                nextHierarchyCode: function() {
                    return self.nextHierarchyCodeResource;

                },
                getReferenceHeaders:function(){
                    return self.headerResource;

                },
                hierarchyChilds: function() {
                    return self.hierarchyChildsResource;

                },
                //для копирования записей из общесистемного справочника в справочник организаций.
                //1.35	ЭФ_НСИ_КП_ОРГ: копирование позиций в справочник организации из общесистемного справочника
                copyOrgWorkRequest:function(){
                    return self.copyRequest;
                }
            };
        }
    ]);
