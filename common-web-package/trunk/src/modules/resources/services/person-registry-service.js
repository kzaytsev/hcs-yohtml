/*
 * Сервис для работы в реестре физических лиц.
 * Взаимодействие с ru.lanit.hcs.personregistry.PersonRegistryService
 * **/

angular.module('ru.lanit.hcs.personregistry.PersonRegistryService', [
    'person-registry-resource'
])

    .factory('$PersonRegistryService', [
        'PersonRegistryResource',
        function(PersonRegistryResource){
            var self = this;

            var servicePath = '/personregistry';

            var toUrl = function (part) {
                return servicePath + part;
            };

            self.PersonsSearchResource = new PersonRegistryResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
            self.PersonResource = new PersonRegistryResource(toUrl(''));

            return{
                /**
                 * ВИ_ВБФЛ
                 * Метод производит поиск физ.лиц в реестре.
                 * Поиск идет только по актуальным версиям (по умолчанию).
                 * @param request
                 *         - критерии поиска
                 * @param pathParams
                 *         {
                 *              page: Номер страницы, для которой необходимо сформировать данные,
                 *              itemsPerPage: Количество элементов на странице
                 *         }
                 *         - Номер страницы, для которой необходимо сформировать данные
                 * @return
                 */
                findRegisteredPersons: function(request, scb, ecb, pathParams){
                    return self.PersonsSearchResource.create(request, scb, ecb, pathParams);
                },

                /**
                 * ВИ_ДОБФЛ
                 * Метод добавляет в реестр новые физ.лица.
                 * @param request
                 * @throws RegistryPersonAlreadyExistsException если уже существует физ.лицо с такими же уникальными параметрами
                 * @throws ru.lanit.hcs.personRegistry.api.exceptions.FieldValidationException
                 *  - если не заполнены обязательные поля,
                 *  - если значения полей не соответствуют ограничениям формата,
                 *  - если заполнены guid и rootEntityGuid
                 *  - если заполнено поле actual
                 * @return guid версии сущности
                 */
                addPerson: function (request, scb, ecb) {
                    return self.PersonResource.create(request, scb, ecb);
                },

                /**
                 * ВИ_ИЗМФЛ
                 * Метод изменяет сведения о существующем в реестре физ.лице.
                 * @param request
                 * @throws ru.lanit.hcs.personRegistry.api.exceptions.RegistryPersonDoesNotExistException если обновляемого физ.лица не существует.
                 * @throws ru.lanit.hcs.personRegistry.api.exceptions.FieldValidationException
                 *  - если не заполнены обязательные поля,
                 *  - если значения полей не соответствуют ограничениям формата,
                 *  - если не заполнены guid и rootEntityGuid
                 *  - если поле actual != true
                 *  @return guid версии сущности
                 */
                updatePerson: function (request, scb, ecb) {
                    return self.PersonResource.update(request, scb, ecb);
                },

                /**
                 * ВИ_ПРФЛ
                 * Метод производит поиск физ.лиц в реестре.
                 * @param guid - guid ФЛ
                 * @return ru.lanit.hcs.personregistry.api.dto.RegistryPersonDetail
                 */
                getRegistryPersonDetailByGuid: function (guid, scb, ecb) {
                    return self.PersonResource.getById(guid, scb, ecb);
                }

            };
        }
    ]);