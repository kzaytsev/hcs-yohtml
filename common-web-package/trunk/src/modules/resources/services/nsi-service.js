angular.module('ru.lanit.hcs.nsi.rest.NsiService', [
    'nsi-resource'
])

    .factory('$NsiService', ['NsiResource',
    function (NsiResource) {
        var self = this;

        var toUrl = function (part) {
            return '/nsi' + part;
        };

        self.ReferencesResource = new NsiResource(toUrl('/references'));
        self.StandartDictionariesResource = new NsiResource(toUrl('/standard/dictionaries'));
        self.StandartDictionariesRecoveryResource = new NsiResource(toUrl('/standard/dictionaries/recovery'));
        self.StandartDictionariesRemovalResource = new NsiResource(toUrl('/standard/dictionaries/removal'));
        self.DictionariesTableDataResource = new NsiResource(toUrl('/dictionaries/table/data'));
        self.DictionariesPositionDataResource = new NsiResource(toUrl('/dictionaries/positions/data'));
        self.HierarchicalDictionariesNodesChildrenResource = new NsiResource(toUrl('/hierarchical/dictionaries/nodes/children'));
        self.HierarchicalDictionariesNodesSearchResource = new NsiResource(toUrl('/hierarchical/dictionaries/nodes/search'));
        self.HierarchicalDictionariesNodesRootResource = new NsiResource(toUrl('/hierarchical/dictionaries/nodes/roots'));
        self.AdditionalServicesResource = new NsiResource(toUrl('/additional/services'));
        self.MunicipalServices = new NsiResource(toUrl('/municipal/services'));
        self.OperationPrincipleTypes = new NsiResource(toUrl('/operation/principle/types'));
        self.NextCodeResource = new NsiResource(toUrl('/next/code'));
        self.nextHierarchyCodeResource = new NsiResource(toUrl('/next/hierarchy/code'));
        self.historyResource = new NsiResource(toUrl('/entities/:nsiObjectName/:guid/history'));
        self.generalMeetingDecisionsType=new  NsiResource(toUrl('/general/meeting/decisions/types'));
        self.loadProtocolsResource=new  NsiResource(toUrl('/classifiers/intermediate/load/protocols'));
        self.loadProtocolLastResource=new  NsiResource(toUrl('/classifiers/intermediate/load/protocols/last'));

        return {
            // Метод выполняет поиск справочников, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#findNsiRefs(ru.lanit.hcs.nsi.api.dto.search.ReferenceSearchCriteria)}
            // Используется для
            // - поиска справочников для отображения на экранных формах при просмотре
            // @param referenceSearchCriteria - критерий поиска
            // @return Список справочников, удовлетворяющих критериям поиска
            findNsiReferences: function (name, successcb, errorcb) {
                return self.ReferencesResource.get({name: name}, successcb, errorcb);
            },
            // Метод выполняет проверки
            // - на корректность заполнения полей (обязательность, формат и т.п.)
            // - на не существование дублирующей записи
            // При выполнении всех проверок создается позиция в справочнике
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#createEntry(ru.lanit.hcs.nsi.api.dto.NsiEntryCreateRequest)}
            // @param nsiEntryCreateRequest - Запрос на создание позиции справочника
            // @throws ru.lanit.hcs.nsi.api.exception.FieldValidationException - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            // @throws ru.lanit.hcs.nsi.api.exception.DuplicateRecordException - не пройдена проверка на уникальность записи
            // @return Идентификационный позиции справочника
            //
            saveBaseDictionary: function (nsiEntryCreateRequest, successcb, errorcb) {
                return self.StandartDictionariesResource.create(nsiEntryCreateRequest, successcb, errorcb);
            },
            // Метод выполняет проверки
            // - на корректность заполнения полей (обязательность, формат и т.п.)
            // - на не существование дублирующей записи
            // При выполнении всех проверок обновляется позиция в справочнике
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#updateEntry(ru.lanit.hcs.nsi.api.dto.NsiEntryUpdateRequest)}
            // @param nsiEntryUpdateRequest - Запрос на изменение позиции справочника
            // @throws FieldValidationException - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            // @throws DuplicateRecordException - не пройдена проверка на уникальность записи
            updateBaseDictionary: function (nsiEntryUpdateRequest, successcb, errorcb) {
                return self.StandartDictionariesResource.update(nsiEntryUpdateRequest, successcb, errorcb);
            },
            // Востанавливает запись со статусом «актуальна»
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#recoverEntry(ru.lanit.hcs.nsi.api.dto.NsiEntryRecoverRequest)}
            // @param nsiEntryRecoverRequest - Запрос на перевод записи справочника в статус "актуальна"
            recoverBaseDictionary: function (nsiEntryRecoverRequest, successcb, errorcb) {
                return self.StandartDictionariesRecoveryResource.update(nsiEntryRecoverRequest, successcb, errorcb);
            },
            // Переводит запись на статус «неактуальна»
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#deleteEntry(ru.lanit.hcs.nsi.api.dto.NsiEntryDeleteRequest)}
            // @param nsiEntryDeleteRequest - Запрос на перевод записи справочника в статус "неактуальна"
            deleteBaseDictionary: function (nsiEntryDeleteRequest, successcb, errorcb) {
                return self.StandartDictionariesRemovalResource.remove(nsiEntryDeleteRequest, successcb, errorcb);
            },
            // Метод выполняет поиск позиций справочника, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#findNsiEntity(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска позиций справочника для отображения на экранных формах при просмотре
            // @param  nsiSearchCriteria - критерий поиска
            // @return Список позиций справочника, удовлетворяющих критериям поиска
            getNsiEntities: function (guid, name, nsiObjectName, actual, successcb, errorcb) {
                return self.DictionariesTableDataResource.queryObject({}, {guid: guid, name: name, nsiObjectName: nsiObjectName, actual: actual}, successcb, errorcb);
            },
            // Метод выполняет поиск позиций справочника, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#findNsiEntity(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска позиций справочника для отображения на экранных формах при просмотре
            // @param  nsiSearchCriteria - критерий поиска
            // @return Список позиций справочника, удовлетворяющих критериям поиска
            getNsiEntityForView: function (guid, name, nsiObjectName, code, successcb, errorcb) {
                return self.DictionariesPositionDataResource.get({guid: guid, name: name, nsiObjectName: nsiObjectName, code: code}, successcb, errorcb);
            },
            // Метод выполняет поиск позиций справочника, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#findNsiEntity(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска позиций справочника для отображения на экранных формах при просмотре
            // @param  nsiSearchCriteria - критерий поиска
            // @return Список позиций справочника, удовлетворяющих критериям поиска
            getHierarchicalNsiEntityForView: function (parentGuid, nsiObjectName, successcb, errorcb) {
                return self.HierarchicalDictionariesNodesChildrenResource.get({parentGuid: parentGuid, nsiObjectName: nsiObjectName}, successcb, errorcb);
            },
            // Метод выполняет поиск позиций справочника, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#findNsiEntity(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска позиций справочника для отображения на экранных формах при просмотре
            // @param  nsiSearchCriteria - критерий поиска
            // @return Список позиций справочника, удовлетворяющих критериям поиска
            findRootEntities: function (nsiObjectName, successcb, errorcb) {
                return self.HierarchicalDictionariesNodesRootResource.get({nsiObjectName: nsiObjectName}, successcb, errorcb);
            },
            // Метод выполняет поиск позиций иерархического справочника, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#findNsiEntity(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска позиций иерархического справочника для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - критерий поиска
            // @return Список позиций иерархического справочника, удовлетворяющих критериям поиска
            findHierarchyByCode: function (code, name, nsiObjectName, successcb, errorcb) {
                return self.HierarchicalDictionariesNodesSearchResource.get({code: code, name: name, nsiObjectName: nsiObjectName}, successcb, errorcb);
            },
            // Метод выполняет проверки
            // - на корректность заполнения полей (обязательность, формат и т.п.)
            // - на не существование дублирующей записи
            // При выполнении всех проверок создается вид дополнительной услуги
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#createAdditionalService(ru.lanit.hcs.nsi.api.dto.AdditionalServiceCreateRequest)}
            // @param additionalServiceCreateRequest - Запрос на создание вида дополнительной услуги
            // @throws FieldValidationException - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            // @throws DuplicateRecordException - не пройдена проверка на уникальность записи
            // @return Идентификационный код вида дополнительной услуги
            createAdditionalService: function (additionalServiceCreateRequest, successcb, errorcb) {
                return self.AdditionalServicesResource.create(additionalServiceCreateRequest, successcb, errorcb);
            },
            // Метод выполняет проверки
            // - на корректность заполнения полей (обязательность, формат и т.п.)
            // - на не существование дублирующей записи
            // При выполнении всех проверок обновляется вид дополнительной услуги
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#updateAdditionalService(ru.lanit.hcs.nsi.api.dto.AdditionalServiceUpdateRequest)}
            // @param additionalServiceUpdateRequest - Запрос на изменение вида дополнительной услуги
            // @throws FieldValidationException - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            // @throws DuplicateRecordException - не пройдена проверка на уникальность записи
            updateAdditionalService: function (additionalServiceUpdateRequest, successcb, errorcb) {
                return self.AdditionalServicesResource.update(additionalServiceUpdateRequest, successcb, errorcb);
            },
            // Метод выполняет проверки
            // - на корректность заполнения полей (обязательность, формат и т.п.)
            // - на не существование дублирующей записи
            // При выполнении всех проверок создается вид коммунальной услуги
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#createMunicipalService(ru.lanit.hcs.nsi.api.dto.MunicipalServiceCreateRequest)}
            // @param municipalServiceCreateRequest - Запрос на создание вида коммунальной услуги
            // @throws FieldValidationException - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            // @throws DuplicateRecordException - не пройдена проверка на уникальность записи
            // @throws DuplicateSortKeyException - не пройдена проверка на уникальность значения атрибута «Порядок сортировки»
            // @return Идентификационный код вида коммунальной услуги
            createMunicipalService: function (municipalServiceCreateRequest, successcb, errorcb) {
                return self.MunicipalServices.create(municipalServiceCreateRequest, successcb, errorcb);
            },
            // Метод выполняет проверки
            // - на корректность заполнения полей (обязательность, формат и т.п.)
            // - на не существование дублирующей записи
            // При выполнении всех проверок обновляется вид коммунальной услуги
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#updateMunicipalService(ru.lanit.hcs.nsi.api.dto.MunicipalServiceUpdateRequest)}
            // @param municipalServiceUpdateRequest - Запрос на изменение вида коммунальной услуги
            // @throws FieldValidationException - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            // @throws DuplicateRecordException - не пройдена проверка на уникальность записи
            // @throws DuplicateSortKeyException - не пройдена проверка на уникальность значения атрибута «Порядок сортировки»
            updateMunicipalSergvice: function (municipalServiceUpdateRequest, successcb, errorcb) {
                return self.MunicipalServices.update(municipalServiceUpdateRequest, successcb, errorcb);
            },
            // Метод выполняет проверки
            // - на корректность заполнения полей (обязательность, формат и т.п.)
            // - на не существование дублирующей записи
            // При выполнении всех проверок создается вид прибора учета по принципу действия
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#createDeviceOperationPrincipleType(ru.lanit.hcs.nsi.api.dto.DeviceOperPrinTypeCreateRequest)}
            // @param deviceOperPrinTypeCreateRequest - Запрос на создание вида прибора учета по принципу действия
            // @throws FieldValidationException - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            // @throws DuplicateRecordException - не пройдена проверка на уникальность записи
            // @return Идентификационный код вида прибора учета по принципу действия
            createOperationPrincipleType: function (deviceOperPrinTypeCreateRequest, successcb, errorcb) {
                return self.OperationPrincipleTypes.create(deviceOperPrinTypeCreateRequest, successcb, errorcb);
            },
            // Метод выполняет проверки
            // - на корректность заполнения полей (обязательность, формат и т.п.)
            // - на не существование дублирующей записи
            // При выполнении всех проверок обновляется вид прибора учета по принципу действия
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#updateDeviceOperationPrincipleType(ru.lanit.hcs.nsi.api.dto.DeviceOperPrinTypeUpdateRequest)}
            // @param deviceOperPrinTypeUpdateRequest - Запрос на изменение вида прибора учета по принципу действия
            // @throws FieldValidationException - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            // @throws DuplicateRecordException - не пройдена проверка на уникальность записи
            updateOperationPrincipleType: function (deviceOperPrinTypeUpdateRequest, successcb, errorcb) {
                return self.OperationPrincipleTypes.update(deviceOperPrinTypeUpdateRequest, successcb, errorcb);
            },
            // Метод выполняет поиск максимального кода для уже сохраненных позиций справочника
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.NsiService#getNextCode(ru.lanit.hcs.nsi.api.dto.search.ReferenceSearchCriteria)}
            // Используется для (ЭФ_НСИ_СПР_ПОЗ)
            // - поиска максимального кода справочника для отображения на экранных формах при создании позиции (ВИ_НСИ_04)
            // @param referenceSearchCriteria - критерий поиска
            // @return Список справочников, удовлетворяющих критериям поиска
            getNextCode: function (nextCodeRequest, successcb, errorcb) {
                return self.NextCodeResource.queryObject([], { nsiObjectName : reference }, successcb, errorcb);
            },
            getNextHierarchyCode: function (nextCodeRequest, successcb, errorcb) {
                return self.nextHierarchyCodeResource.queryObject([], nextCodeRequest, successcb, errorcb);
            },
            getHistory:function(rootGuid,nsiObjectName, successcb, errorcb){
                return self.historyResource.get({guid: rootGuid, nsiObjectName:nsiObjectName}, successcb, errorcb);
            },
            loadProtocols:function(request, successcb, errorcb){
                return  self.loadProtocolsResource.queryObject([], request,successcb, errorcb);
            },
            loadProtocolLast:function(request, successcb, errorcb){
                return  self.loadProtocolLastResource.queryObject([], request,successcb, errorcb);
            }

        };
    }])
    .factory('ReferenceResource', ['NsiResource',
        function (NsiResource) {
            var self = this;
            self.prefix = '/nsi';
            self.createResources={
                AdditionalService: new NsiResource(self.prefix + '/additional/services'),
                MunicipalService: new NsiResource(self.prefix + '/municipal/services'),
                OperationPrincipleType: new NsiResource(self.prefix + '/operation/principle/types'),
                FuelType: new NsiResource(self.prefix + '/fuel/types'),
                CooperativeSupportService: new NsiResource(self.prefix + '/cooperative/support/services'),
                HousingService: new NsiResource(self.prefix + '/housing/services'),
                GeneralMeetingDecisionsType: new  NsiResource(self.prefix + '/general/meeting/decisions/types')
            };

            self.resources = {};
            self.recordResource = new NsiResource(self.prefix + '/standard/dictionaries');
            self.recordDeleteResource = new NsiResource(self.prefix + '/standard/dictionaries/removal');
            self.recordRestoreResource = new NsiResource(self.prefix + '/standard/dictionaries/recovery');
            self.searchResource = new NsiResource(self.prefix + '/references');
            self.nextCodeResource = new NsiResource(self.prefix + '/next/code');
            self.nextHierarchyCodeResource = new NsiResource(self.prefix + '/next/hierarchy/code');
            self.referenceHeaders = new NsiResource(self.prefix + '/hierarchical/dictionaries/headers');
            self.hierarchyChildsResource = new NsiResource(self.prefix + '/hierarchy/children');

            return {
                search: function() {
                    return self.searchResource;
                },
                load: function(reference) {
                    if (!self.resources[reference]) {
                        self.resources[reference] = new NsiResource(self.prefix + '/' + reference);
                    }
                    return self.resources[reference];
                },

                record: function(nsiObjectName) {
                    var resource = self.createResources[nsiObjectName]?self.createResources[nsiObjectName]:self.recordResource;


                    return {
                        on : function() {
                            return resource ? resource : self.recordResource;
                        },
                        remove : function() {
                            return self.recordDeleteResource;
                        },
                        restore : function() {
                            return self.recordRestoreResource;
                        }
                    };
                },

                nextCode: function() {
                    return self.nextCodeResource;
                },
                nextHierarchyCode: function() {
                    return self.nextHierarchyCodeResource;

                },
                getReferenceHeaders:function(){
                    return self.referenceHeaders;

                },
                hierarchyChilds: function() {
                    return self.hierarchyChildsResource;

                }
            };
        }
    ]);
