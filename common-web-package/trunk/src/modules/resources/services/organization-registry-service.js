/*
 * Сервис для работы в реестре организаций.
 * Взаимодействие с ru.lanit.hcs.organizationregistry.OrganizationRegistryService
 * **/

angular.module('ru.lanit.hcs.organizationregistry.OrganizationRegistryService', [
    'organization-registry-resource'
])

    .factory('$OrganizationRegistryService', [
        'OrganizationRegistryResource',
        function(OrganizationRegistryResource){
            var self = this;

            var servicePath = '/organizationregistry';

            var toUrl = function (part) {
                return servicePath + part;
            };

            self.OrganizationsSearchResource = new OrganizationRegistryResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
            self.OrganizationResource = new OrganizationRegistryResource(toUrl(''));

            return{
                /**
                 * ВИ_ВБОРГ Метод производит поиск организаций в реестре. Поиск идет только
                 * по актуальным версиям.
                 * @param request
                 *         - критерии поиска
                 * @param pathParams
                 *         {
                 *              page: Номер страницы, для которой необходимо сформировать данные,
                 *              itemsPerPage: Количество элементов на странице
                 *         }
                 *         - Номер страницы, для которой необходимо сформировать данные
                 * @return
                 */
                findRegistryOrganizations: function(request, scb, ecb, pathParams){
                    return self.OrganizationsSearchResource.create(request, scb, ecb, pathParams);
                },

                /**
                 * ВИ_ДОБОРГ Метод добавляет в реестр новую организацию.
                 * @param request данные об организации
                 * @return guid версии сущности
                 */
                addOrganization: function (request, scb, ecb) {
                    return self.OrganizationResource.create(request, scb, ecb);
                },

                /**
                 * ВИ_ИЗОРГ
                 * Метод изменяет сведения о существующей в реестре организации.
                 * @param request данные об организации
                 * @return guid версии сущности
                 */
                updateOrganization: function (request, scb, ecb) {
                    return self.OrganizationResource.update(request, scb, ecb);
                },

                /**
                 * ВИ_ПРОРГ
                 * Метод производит поиск организации в реестре.
                 * @param guid - guid организации
                 * @return ru.lanit.hcs.intbus.organizationregistry.api.dto.RegistryOrganizationLegalEntityDetailWithNsi
                 *         либо ru.lanit.hcs.intbus.organizationregistry.api.dto.RegistryOrganizationBusinessmanDetailWithNsi
                 *         в зависимости от типа организации
                 */
                getRegistryOrganizationDetailByGuid: function (guid, scb, ecb) {
                    return self.OrganizationResource.getById(guid, scb, ecb);
                }
            };
        }
    ]);