angular.module('ru.lanit.hcs.nsi.rest.NsiPpaService', [
    'nsi-resource'
]).factory('$NsiPpaService', ['NsiResource',
    function (NsiResource) {
        var self = this;

        var toUrl =  function(part) {
            return '/nsi/ppa' + part;
        };

        self.OrganizationRolesResource = new NsiResource(toUrl('/organization/roles'));
        self.EmployeeRolesResource = new NsiResource(toUrl('/employee/roles'));
        self.TimezonesResource = new NsiResource(toUrl('/timezones?regionGuid=:regionGuid&guid=:guid'));
        self.PersonsTypeResource = new NsiResource(toUrl('/persons/types'));

        return {
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.PpaService#findTimeZone(ru.lanit.hcs.nsi.api.dto.search.TimeZoneSearchCriteria)}
            // Используется для
            // - поиска часовых зон, соответствующих адресу организации, при регистрации организации
            // - поиска часовых зон организациий для отображения на экранных формах при просмотре
            // @param timeZoneSearchCriteria - криткерий поиска
            // @return Список часовых зон
            findTimeZone: function (regionGuid, guid, successcb, errorcb) {
                if (regionGuid) {
                    return self.TimezonesResource.get({regionGuid: regionGuid}, successcb, errorcb);
                } else if (guid) {
                    return self.TimezonesResource.get({guid: guid}, successcb, errorcb);
                } else {
                    return self.TimezonesResource.get(successcb, errorcb);
                }
            },
            // Метод выполняет поиск полномочий должностного лица организации, удовлетворяющих критериям поиска
            // Используется для
            // - поиска актуальных полномочий должностного лица организации при аутентификации, регистрации должностного лица организации
            // - поиска полномочий должностного лица организации для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список полномочий должностного лица организации, удовлетворяющих критертям поиска
            findEmployeeRole: function (queryJson, successcb, errorcb) {
                return self.EmployeeRolesResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск полномочий организациий, удовлетворяющих критериям поиска
            // Используется для
            // - поиска актуальных полномочий организациий при аутентификации, регистрации организации
            // - поиска полномочий организациий для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список полномочий организациий, удовлетворяющих критертям поиска
            findOrganizationRole: function (queryJson, successcb, errorcb) {
                return self.OrganizationRolesResource.query([], queryJson, successcb, errorcb);
            },

            /**
             * Метод выполняет поиск типов лиц, удовлетворяющих критериям поиска
             * Используется для
             * - поиска актуальных типов лиц при аутентификации, регистрации должностного лица организации
             * - поиска типа лиц для отображения на экранных формах при просмотре
             * @param queryJson - критерий поиска (code, name, guids, actual)
             * @return Список типов лиц, удовлетворяющих критериям поиска
             */
            findPersonsType: function (queryJson, successcb, errorcb) {
                return self.PersonsTypeResource.query([], queryJson, successcb, errorcb);
            }
        };
    }
]);
