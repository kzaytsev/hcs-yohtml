/*
* Сервис для работы в реестре адресных объектов.
* Взаимодействие с ru.lanit.hcs.homemanagement.rest.HouseService
* **/

angular.module('ru.lanit.hcs.homemanagement.rest.HouseService', [
    'home-mgmt-resource'
])

.factory('$HouseService', [
    'HomeManagementResource',
    function(HomeManagementResource){
        var self = this;

        var servicePath = '/houses';

        var toUrl = function (part) {
            return servicePath + part;
        };

        self.HouseSaveResource = new HomeManagementResource(servicePath);
        self.HouseResource =  new HomeManagementResource(toUrl('/:houseTypeCode/:houseGuid'));
        self.HousesSearchResource = new HomeManagementResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.HouseApartmentsResource = new HomeManagementResource(toUrl('/house-apartments/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.ApartmentResource = new HomeManagementResource(toUrl('/apartments'));
        self.ResidentalApartmentResource = new HomeManagementResource(toUrl('/apartments/residential'));
        self.NonResidentalApartmentResource = new HomeManagementResource(toUrl('/apartments/nonresidential'));
        self.ApartmentRemoveResource = new HomeManagementResource(toUrl('/apartments/removal'));
        self.ApartmentSearchResource = new HomeManagementResource(toUrl('/apartments/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.MunicipalServicesResource = new HomeManagementResource(toUrl('/services/municipal'));
        self.MunicipalServiceSaveResource = new HomeManagementResource(toUrl('/services/municipal/save'));
        self.HousingServicesResource = new HomeManagementResource(toUrl('/services/housing'));
        self.HousingServiceSaveResource = new HomeManagementResource(toUrl('/services/housing/save'));
        self.HousePorchesResource = new HomeManagementResource(toUrl('/:houseGuid/porches'));
        self.PorchRemoveResource = new HomeManagementResource(toUrl('/porches/removal'));
        self.PorchSaveResource = new HomeManagementResource(toUrl('/porches/save'));
        self.PorchInfoResource = new HomeManagementResource(toUrl('/porches/search-by-id'));
        self.OwnerSaveResource = new HomeManagementResource(toUrl('/owners/save'));
        self.OwnerInfoResource = new HomeManagementResource(toUrl('/owners/search-by-id'));
        self.OwnerRemoveResource = new HomeManagementResource(toUrl('/owners/removal'));
        self.OwnersSearchResource = new HomeManagementResource(toUrl('/owners/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.RoomsForSelectSearchResource = new HomeManagementResource(toUrl('/rooms/select/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.RoomsSearchResource = new HomeManagementResource(toUrl('/rooms/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.RoomsSaveResource = new HomeManagementResource(toUrl('/rooms/save'));
        self.RoomsInfoResource = new HomeManagementResource(toUrl('/rooms/search-by-id'));
        self.RoomRemoveResource = new HomeManagementResource(toUrl('/rooms/removal'));
        self.getHouseAddressesByUserResource = new HomeManagementResource(toUrl('/search-by-user;page=:page;itemsPerPage=:itemsPerPage'));
        self.SharesSearchResource = new HomeManagementResource(toUrl('/shares/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.AddressSharesSearchResource = new HomeManagementResource(toUrl('/shares/search-address;page=:page;itemsPerPage=:itemsPerPage'));
        self.ShareRemoveResource = new HomeManagementResource(toUrl('/shares/removal'));
        self.ShareResource = new HomeManagementResource(toUrl('/shares/search-by-id'));
        self.ShareSaveResource = new HomeManagementResource(toUrl('/shares/save'));
        self.ShareApproveResource = new HomeManagementResource(toUrl('/shares/approve'));
        self.EncumbrancesSearchResource = new HomeManagementResource(toUrl('/encumbrances/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.EncumbranceResource = new HomeManagementResource(toUrl('/encumbrances/search-by-id'));
        self.EncumbranceSaveResource = new HomeManagementResource(toUrl('/encumbrances/save'));
        self.EncumbranceRemoveResource = new HomeManagementResource(toUrl('/encumbrances/removal'));
        self.EncumbranceApproveResource = new HomeManagementResource(toUrl('/encumbrances/approve'));
        self.ApartmentRestoreResource = new HomeManagementResource(toUrl('/apartments/restore'));
        self.HouseOrganizationsResource = new HomeManagementResource(toUrl('/search-house-org'));
        self.HouseManagementBaseResource = new HomeManagementResource(toUrl('/search-management-base'));
        self.FindHouseOwnersResource = new HomeManagementResource(toUrl('/shares/owners/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.HouseOwnerResource = new HomeManagementResource(toUrl('/shares/owners/search-by-id'));
        self.HouseAddressesResource = new HomeManagementResource(toUrl('/search-addresses?elementsCount=:elementsCount'));
        self.HousesByAddressResource = new HomeManagementResource(toUrl('/search-by-address;page=:page;itemsPerPage=:itemsPerPage'));
        self.FindLatestApprovedShareVersionResource = new HomeManagementResource(toUrl('/shares/search-latest-version-by-id'));

        return{
            /**
             * Метод выполняет поиск кратких сведений об объектах жилищного фонда, удовлетворяющих критериям поиска.
             *
             * Используется для (ЭФ_ВД)
             * - «Выбор дома» (ВИ_ВД);
             * - поиска объектов жилищного фонда для выбора при создании прибора учета (ВИ_ВД);
             * - поиска объектов жилищного фонда для выбора при создании/редактировании ЛС (ВИ_ЛС_03, ВИ_ЛС_04).
             *
             * @param request
             *         - критерии поиска
             * @param page
             *         - номер страницы, для которой необходимо сформировать данные
             * @param itemsPerPage
             *         - количество элементов на странице
             *
             * @return Список кратких сведений о домах, удовлетворяющих критериям поиска
             */
            findHousesByAddress: function(request, scb, ecb, pathParams){
                return self.HousesByAddressResource.create(request, scb, ecb, pathParams);
            },
            /**
             * Метод выполняет полнотекстовый поиск адресов объектов жилищного фонда по адресной строке.
             *
             * Используется для
             * - формирования атоподсказки при поиске объектов жилищного фонда в реестре адресных объектов
             * (ВИ_ЖКХРАО_2, ЭФ_ЖКХРАО_ПАО).
             *
             * @param searchCriteria
             *         - критерии поиска
             * @param elementsCount
             *         - количество элементов
             *
             * @return Список адресов объектов жилищного фонда, удовлетворяющих критериям поиска
             *
             * @since 4.0
             */
            findHouseAddresses: function (request, elementsCount, scb, ecb) {
                return self.HouseAddressesResource.create(request, scb, ecb, {elementsCount: elementsCount});
            },
            /**
             * Метод осуществляет поиск основания на управление домом.
             *
             * Метод используется для
             * - заполнения блока "Основание управления домом" (ЭФ_ЖКХРАО_ХД) при добавлении дома (ВИ_ЖКХРАО_9).
             *
             * @param id
             *         - ФИАС адрес дома.
             *
             * @return сведения об основании на управление домом.
             *
             * @since 4.0
             */
            findHouseManagementBase: function (id, scb, ecb) {
                return self.HouseManagementBaseResource.getById(id, scb, ecb);
            },
            /**
             * Метод осуществляет поиск УО, обслуживающих дома, ОКТМО которых являются дочерними к искомым ОКТМО поиска.
             *
             * Метод используется в
             * - для заполнения списка ЭФ_ЖКХРАО_ БП.5 (ВИ_ЖКХРАО_2).
             *
             * @return список УО, удовлетворяющих критерию поиска.
             *
             * @since 4.0
             */
            findHouseOrg: function (scb, ecb) {
                return self.HouseOrganizationsResource.query([], {}, scb, ecb);
            },
            /**
             * Метод осуществляет восстановление неактуального помещения.
             *
             * Метод используется в
             * - ВИ_ЖКХРАО_14.
             *
             * @param request
             *         - запрос на восстановление неактуального помещения.
             *
             * @return ссулка на восстановленное помещение.
             *
             * @since 4.0
             */
            restoreApartment : function (request, scb, ecb){
                return self.ApartmentRestoreResource.update(request, scb, ecb);
            },
            /**
             * Метод осуществляет утверждение электронного документа «Обременение» (ВИ_ЖКХРАО_51).
             *
             * @param request
             *         - запрос на утверждение электронного документа «Обременение».
             *
             * @since 4.0
             */
            approveEncumbrance: function(request, scb, ecb){
                return self.EncumbranceApproveResource.update(request, scb, ecb);
            },

            /**
             * Метод осуществляет утверждение электронного документа «Доля» (ВИ_ЖКХРАО_48).
             *
             * @param request
             *         - запрос на утверждение электронного документа «Доля».
             *
             * @since 4.0
             */
            approveShare: function(request, scb, ecb){
                return self.ShareApproveResource.update(request, scb, ecb);
            },

            /**
             * Метод осуществляет удаление версии электронного документа "Обременение" (ВИ_ЖКХРАО_36).
             * @param request
             *         - запрос на удаление версии электронного документа "Обременение".
             *
             * @since 4.0
             */
            deleteEncumbrance: function(request, scb, ecb){
                return self.EncumbranceRemoveResource.update(request, scb, ecb);
            },
            /**
             * Метод осуществляет поиск сведеней об обременении по его идентификатору.
             *
             * Метод используется для
             * - отображения ЭФ_ЖКХРАО_ОБР_СО;
             * - отображения ЭФ_ЖКХРАО_ОБР_СО_ПР.
             *
             * @param searchCriteria
             *         - критерий поиска обременений по идентификатору.
             *
             * @return полные сведения о версии документа "Обременение".
             *
             * @since 4.0
             */
            findEncumbrance: function(request, scb, ecb){
                return self.EncumbranceResource.create(request, scb, ecb);
            },
            /**
             * Метод создания и изменения версии электронного документа  «Обременение» (ВИ_ЖКХРАО_49, ВИ_ЖКХРАО_50).
             *
             * @param request
             *         - запрос на создание и изменение версии электронного документа "Обременение".
             *
             * @return версия электронного документа  «Обременение».
             *
             * @since 4.0
             */
            updateEncumbrance: function(request, scb, ecb){
                return self.EncumbranceSaveResource.create(request, scb, ecb);
            },
            /**
             * Метод осуществляет поиск кратких сведеней об обременеиях версии документа "Доля".
             *
             * Метод используется для
             * - отображения ЭФ_ЖКХРАО_ДОЛЯ_ОБ;
             * - отображения ЭФ_ЖКХРАО_ДОЛЯ_ОБ_ПР.
             *
             * @param request
             *         - критерий поиска обременений доли по GUID'у доли.
             *
             * @return Результат поиска кратких сведений об обременениях доли.
             *
             * @since 4.0
             */
            findEncumbrances: function(request, scb, ecb, pathParams){
                return self.EncumbrancesSearchResource.create(request, scb, ecb, pathParams);
            },
            /**
             * Метод осуществляет удаление версии электронного документа "Доля" (ВИ_ЖКХРАО_36).
             * @param request
             *         - запрос на удаление версии электронного документа "Доля".
             *
             * @since 4.0
             */
            deleteShare: function(request, scb, ecb){
                return self.ShareRemoveResource.update(request, scb, ecb);
            },
            /**
             * Метод осуществляет поиск кратких сведеней о долях собственности для жилого дома/помещения/комнаты.
             *
             * Метод используется для
             * - отображения ЭФ_ЖКХРАО_СВ_ДОЛЯ.
             *
             * @param request
             *         - критерий поиска кратких сведеней о долях собственности для жилого дома/помещения/комнаты.
             *
             * @return результат поиска кратких сведеней о долях собственности для жилого дома/помещения/комнаты.
             *
             * @since 4.0
             */
            findAddressShares: function(request, scb, ecb, pathParams){
                return self.AddressSharesSearchResource.create(request, scb, ecb, pathParams);
            },
            /**
             * Метод осуществляет поиск кратких сведеней о долях собственности для жилого дома/помещения/комнаты.
             *
             * Метод используется для
             * - отображения ЭФ_ЖКХРАО_СВ_ДОЛЯ.
             *
             * @param request
             *         - критерий поиска кратких сведеней о долях собственности для жилого дома/помещения/комнаты.
             *
             * @return результат поиска кратких сведеней о долях собственности для жилого дома/помещения/комнаты.
             *
             * @since 4.0
             */
            findShares: function(request, scb, ecb, pathParams){
                return self.SharesSearchResource.create(request, scb, ecb, pathParams);
            },
            /**
             * Метод осуществляет поиск кратких сведений о комнатах, удовлетворяющих критериям поиска.
             *
             * Метод используется для
             * - отрисовки окна выбора комнаты (ЭФ_ВКОМ).
             *
             * @param request
             *         - критерии поиска
             * @param pageIndex
             *         - номер страницы, для которой необходимо сформировать данные
             * @param elementsPerPage
             *         - количество элементов на странице
             *
             * @return Список кратких сведений о помещениях, удовлетворяющих критериям поиска
             *
             * @since 3.0
             */
            findRoomsForSelect: function(request, scb, ecb, pathParams){
                return self.RoomsForSelectSearchResource.create(request, scb, ecb, pathParams);
            },
            /**
             * ВИ_ЖКХРАО_10, ЭФ_ЖКХРАО_СОК. Поиск кратких сведений о комнатах и их лицевых счетах
             *
             * @param request     @link{ru.lanit.hcs.homemanagement.api.dto.search.RoomWithAccountSummarySearchCriteria}
             * @param  pathParams
             *          {
             *              pageIndex : Номер страницы,
             *              itemsPerPage : Количество строк на странице
             *           }
             * @return javax.ws.rs.core.Response with Status.OK and search result
             */
            findRooms: function(request, scb, ecb, pathParams){
                return self.RoomsSearchResource.create(request, scb, ecb, pathParams);
            },
            /**
             * ВИ_ЖКХРАО_28. Добавление комнаты
             *
             * @param request @link{ru.lanit.hcs.homemanagement.api.dto.request.RoomCreateRequest}
             * @return javax.ws.rs.core.Response with Status.CREATED and @link{ru.lanit.hcs.intbus.homeManagement.api.dto.RoomRefWithNsi} or Status.BAD_REQUEST and error info
             */
            createRoom: function (request, scb, ecb) {
                return self.RoomsSaveResource.create(request, scb, ecb);
            },
            /**
             * ВИ_ЖКХРАО_29. Изменение характеристик комнаты
             *
             * @param request @link{ru.lanit.hcs.homemanagement.api.dto.request.RoomUpdateRequest}
             * @return javax.ws.rs.core.Response with Status.CREATED or Status.BAD_REQUEST and error info
             */
            updateRoom: function (request, scb, ecb) {
                return self.RoomsSaveResource.update(request, scb, ecb);
            },
            /**
             * ВИ_ЖКХРАО_28, ВИ_ЖКХРАО_29, ВИ_ЖКХРАО_47. Поиск сведений о комнате по её идентификационному коду
             *
             * @param request @link{ru.lanit.hcs.homemanagement.api.dto.search.FindRoomSearchCriteria}
             * @return javax.ws.rs.core.Response with Status.OK and search result @link{ru.lanit.hcs.intbus.homeManagement.api.dto.response.FindRoomWithNsiResponse} or Status.NOT_FOUND
             */
            findRoom: function(request, scb, ecb){
                return self.RoomsInfoResource.create(request, scb, ecb);
            },
            /**
             * ВИ_ЖКХРАО_30. Удаление комнаты
             *
             * @param request @link{ru.lanit.hcs.homemanagement.api.dto.request.RoomDeleteRequest}
             * @return javax.ws.rs.core.Response with Status.OK or Status.BAD_REQUEST and error info
             */
            deleteRoom: function(request, scb, ecb){
                return self.RoomRemoveResource.update(request, scb, ecb);
            },
            /**
             * Метод выполняет поиск кратких сведений о собственниках и их лицевых счетах, удовлетворяющих критериям поиска.
             *
             * Используется для
             * - поиска собственников в карточке дома (ВИ_ЖКХРАО_34, ВИ_ЖКХРАО_35, ЭФ_ЖКХРАО_СВ_СОБС).
             *
             * @param searchCriteria
             *         - критерии поиска
             * @param pageIndex
             *         - номер страницы, для которой необходимо сформировать данные
             * @param elementsPerPage
             *         - количество элементов на странице
             *
             * @return Список кратких сведений о собственниках и их лицевых счетах
             *
             * @since 3.0
             */
            findOwners: function(request, scb, ecb, pathParams){
                return self.OwnersSearchResource.create(request, scb, ecb, pathParams);
            },
            /**
             * Метод осуществляет удаление сведений о собственнике (ВИ_ЖКХРАО_36).
             *
             * Метод выполняет проверки
             * - на существование активных ЛС, привязанных к удаляемому собственнику или иному лицу;
             * - на существование неактивных ЛС, привязанных к удаляемому собственнику или иному лицу.
             *
             * После успешного выполнений всех проверок метод
             * - удаляет все записи в сущности «История событий», привязанные к удаляемому экземпляру класса
             * «Собственник или иное лицо»;
             * - создает запись в сущности «История событий», привязанную к экземпляру класса «Комната» или
             * «Помещение» или «Жилой дом», у которого удаляют собственника или иное лицо, с кодом:
             * - С_РАО_24 (в случае привязки к комнате собственника или иного лица),
             * - С_РАО_29 (в случае привязки к помещению собственника или иного лица),
             * - С_РАО_30 (в случае привязки к жилому дому собственника или иного лица),
             * дата события = текущая дата;
             * - удаляет экземпляр класса «Собственник или иное лицо».
             *
             * @param request
             *         - запрос на удаление сведений о собственнике.
             *
             * @throws OwnerNotFoundException
             *         - сведения о собственнике уже не существуют;
             * @throws LinkedActiveAccountsExistException
             *         - существуют активные ЛС, привязанные к удаляемому собственнику или иному лицу;
             * @throws LinkedNotActiveAccountsExistException
             *         - существуют неактивные ЛС, привязанные к удаляемому собственнику или иному лицу.
             * @since 3.0
             */
            deleteOwner: function(request, scb, ecb){
                return self.OwnerRemoveResource.update(request, scb, ecb);
            },
            /**
             * Метод выполняет поиск сведений о собственнике по его идентификационному коду.
             *
             * Используется для (ЭФ_ЖКХРАО_СОБС, ЭФ_ЖКХРАО_СОБС_ПР)
             * - поиска сведений о собственнике для редактирования (ВИ_ЖКХРАО_34, ВИ_ЖКХРАО_35).
             *
             * @param searchCriteria
             *         - критерии поиска
             *
             * @return Сведения о собственнике
             *
             * @since 3.0
             */
            findOwner: function(request, scb, ecb){
                return self.OwnerInfoResource.create(request, scb, ecb);
            },
            /**
             * Метод осуществляет добавление собственника или иного лица (ВИ_ЖКХРАО_34).
             *
             * Метод выполняет проверки
             * - на заполнение всех обязательных полей для заполнения;
             * - на уникальность добавляемого значения ФИО (составное из значений в полях Фамилия, Имя, Отчество)
             * в рамках помещения, комнаты или жилого дома в зависимости от инициирующего события (Для физ. лица);
             * - на уникальность добавляемого значения «Наименование организации» в рамках помещения, комнаты или жилого дома
             * в зависимости от инициирующего события (Для юр. лица);
             * - на корректность данных, введенных в поле Доля (сумма значений долей у собственников или иных лиц,
             * привязанных к помещению, комнате или жилому дому должно быть не больше 100).
             *
             * После успешного выполнений всех проверок метод
             * - удаляет все записи в сущности «История событий», привязанные к удаляемому подъезду;
             * - создает запись в сущности «История событий», привязанную к экземпляру класса «Многоквартирный дом»,
             * у которого удаляют подъезд, с кодом С_РАО_15, дата события = текущая дата;
             * - удаляет подъезд.
             *
             * @param request
             *         - запрос на добавление сведений о собственнике.
             *
             * @return Ссылка на сведения о подъезде.
             *
             * @throws HouseNotFoundException
             *         - дом не найден в реестре адресных объектов;
             * @throws ApartmentNotFoundException
             *         - помещение не найдено в реестре адресных объектов;
             * @throws RoomNotFoundException
             *         - комната не найдена в реестре адресных объектов;
             * @throws NsiValueValidationException
             *         - значения справочников не актуальны или не существуют;
             * @throws FieldValidationException
             *         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.);
             * @throws OwnerPersonNameNotUniqueException
             *         - бизнес идентификатор собственника (составленный из значений в полях Фамилия, Имя, Отчество) не уникален
             *         в рамках помещения, комнаты или жилого дома в зависимости от инициирующего события (для физ. лица);
             * @throws OwnerCompanyNameNotUniqueException
             *         - «Наименование организации» не уникально в рамках помещения, комнаты или жилого дома
             *         в зависимости от инициирующего события (для юр. лица);
             * @throws OwnerPercentageMoreThan100
             *         - сумма значений долей у собственников или иных лиц, привязанных к помещению, комнате или жилому дому
             *         превышает 100%.
             * @since 3.0
             */
            createOwner: function (request, scb, ecb) {
                return self.OwnerSaveResource.create(request, scb, ecb);
            },
            /**
             * Метод осуществляет изменение характеристик собственника или иного лица (ВИ_ЖКХРАО_35).
             *
             * Метод выполняет проверки
             * - на заполнение всех обязательных полей для заполнения;
             * - на уникальность изменненного значения ФИО (составное из значений в полях Фамилия, Имя, Отчество)
             * в рамках помещения, комнаты или жилого дома в зависимости от инициирующего события (Для физ. лица).
             * - на уникальность измен значения «Наименование организации» в рамках помещения, комнаты или жилого дома
             * в зависимости от инициирующего события (Для юр. лица);
             * - на корректность данных, введенных в поле Доля (сумма значений долей у собственников или иных лиц,
             * привязанных к помещению, комнате или жилому дому должно быть не больше 100).
             *
             * После успешного выполнений всех проверок метод
             * - обновляет экземпляр класса «Подъезд»;
             * - создает запись в сущности «История событий», привязанную к изменяемому экземпляру класса
             * «Собственник или иное лицо» с кодом С_РАО_27 дата события = текущая дата.
             *
             * @param request
             *         - запрос на изменение сведений о собственнике.
             *
             * @throws OwnerNotFoundException
             *         - сведения о собственнике уже не существуют;
             * @throws NsiValueValidationException
             *         - значения справочников не актуальны или не существуют;
             * @throws FieldValidationException
             *         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.);
             * @throws OwnerPersonNameNotUniqueException
             *         - бизнес идентификатор собственника (составленный из значений в полях Фамилия, Имя, Отчество) не уникален
             *         в рамках помещения, комнаты или жилого дома в зависимости от инициирующего события (для физ. лица);
             * @throws OwnerCompanyNameNotUniqueException
             *         - «Наименование организации» не уникально в рамках помещения, комнаты или жилого дома
             *         в зависимости от инициирующего события (для юр. лица);
             * @throws OwnerPercentageMoreThan100
             *         - сумма значений долей у собственников или иных лиц, привязанных к помещению, комнате или жилому дому
             *         превышает 100%.
             * @since 3.0
             */
            updateOwner: function (request, scb, ecb) {
                return self.OwnerSaveResource.update(request, scb, ecb);
            },
            /**
             * Метод получает краткую информацию по подъездам дома.
             *
             * Используется для
             * - поиска помещений на странице «Сведения о подъездах» (ВИ_ЖКХРАО_26, ЭФ_ЖКХРАО_СП).
             *
             * @param houseGuid
             *         - guid дома
             *
             * @return - перечень подъездов дома
             */
            getHousePorchesSummary:function(houseGuid, scb, ecb){
                return self.HousePorchesResource.query({houseGuid: houseGuid}, {}, scb, ecb);
            },
            /**
             * Метод осуществляет удаление подъезда (ВИ_ЖКХРАО_33).
             *
             * Метод выполняет проверки
             * - на существование актуальных помещений, привязанных к удаляемому подъезду;
             * - на существование неактуальных помещений, привязанных к удаляемому подъезду.
             *
             * @param request
             *         - запрос на удаление сведений о подъезде.
             *
             * @throws PorchNotFoundException
             *         - удаляемый подъезд уже не существует;
             * @throws LinkedActiveApartmentsExistException
             *         - существуют актуальные помещения, привязанные к удаляемому подъезду;
             * @throws LinkedNotActiveApartmentsExistException
             *         - существуют неактуальные помещения, привязанные к удаляемому подъезду.
             * @since 3.0
             */
            deletePorch: function(request, scb, ecb){
                return self.PorchRemoveResource.update(request, scb, ecb);
            },
            /**
             * Метод осуществляет добавление подъезда (ВИ_ЖКХРАО_31).
             *
             * Метод выполняет проверки
             * - на заполнение всех обязательных полей для заполнения;
             * - на уникальность введенного номера подъезда;
             * - на корректность введенного значения в поле «Этажность» (значение в поле «Этажность» должно быть не больше,
             * чем «Количество этажей, наибольшее» у МКД, к которому привязан подъезд)
             * - на корректность введенных данных в поле «Конечная квартира в подъезде» (Конечная квартира должна быть больше,
             * чем Начальная квартира);
             * - на корректность введенного значения в поле «Дата начала» (год даты начала должен быть не меньше,
             * чем год постройки дома);
             * - на корректность введенного значения в поле «Дата окончания» (Дата окончания подъезда не может быть больше,
             * чем дата окончания дома);
             * - на корректность введенного интервала квартир в подъезде (введенный интервал квартир не должен пересекаться
             * с интервалами квартир в других подъездах).
             *
             * После успешного выполнений всех проверок метод
             * - создает экземпляр класса «Подъезд»;
             * - создает запись в сущности «История событий», привязанную к добавляемому экземпляру класса «Подъезд»
             * с кодом С_РАО_16, дата события = текущая дата.
             *
             * @param request
             *         - запрос на добавление сведений о подъезде.
             *
             * @return Ссылка на сведения о подъезде.
             *
             * @throws HouseNotFoundException
             *         - дом не найден в реестре адресных объектов;
             * @throws NsiValueValidationException
             *         - значения справочников не актуальны или не существуют;
             * @throws FieldValidationException
             *         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.);
             * @throws PorchNumberNotUniqueException
             *         - номера подъезда в доме не уникален;
             * @throws PorchFloorCountMoreThanHouseFloorCountException
             *         - значение в поле «Этажность» больше, чем «Количество этажей, наибольшее» у МКД,
             *         к которому привязан подъезд;
             * @throws PorchLastApartmentNumberLessThanFirstOneException
             *         - конечная квартира меньше, чем Начальная квартира;
             * @throws PorchStartDateYearLessThanBuildingYearException
             *         - год даты начала подъезда меньше, чем год постройки дома;
             * @throws PorchEndDateLaterThanHouseEndDateException
             *         - дата окончания подъезда больше, чем дата окончания дома;
             * @throws PorchApartmentNumbersSetIsOverlappedException
             *         - введенный интервал квартир пересекается с интервалами квартир в других подъездах.
             * @since 3.0
             */
            createPorch: function(request, scb, ecb){
                return self.PorchSaveResource.create(request, scb, ecb);
            },
            /**
             * Метод осуществляет изменение характеристик подъезда (ВИ_ЖКХРАО_32).
             *
             * Метод выполняет проверки
             * - на заполнение всех обязательных полей для заполнения;
             * - на уникальность введенного номера подъезда в рамках дома;
             * - на корректность введенного значения в поле «Этажность» (значение в поле «Этажность» должно быть не больше,
             * чем «Количество этажей, наибольшее» у МКД, к которому привязан подъезд);
             * - на корректность введенного значения в поле «Дата начала» (год даты начала должен быть не меньше,
             * чем год постройки дома);
             * - на корректность введенного значения в поле «Дата окончания» (Дата окончания подъезда не может быть больше,
             * чем дата окончания дома);
             * - на корректность введенных данных в поле «Конечная квартира в подъезде» (Конечная квартира должна быть больше,
             * чем Начальная квартира);
             * - на корректность введенного интервала квартир в подъезде (введенный интервал квартир не должен пересекаться
             * с интервалами квартир в других подъездах).
             *
             * После успешного выполнений всех проверок метод
             * - обновляет экземпляр класса «Подъезд»;
             * - создает запись в сущности «История событий», привязанную к изменяемому экземпляру класса «Подъезд»
             * с кодом С_РАО_17, дата события = текущая дата.
             *
             * @param request
             *         - запрос на изменение сведений о подъезде.
             *
             * @throws PorchNotFoundException
             *         - подъезд уже не существует;
             * @throws NsiValueValidationException
             *         - значения справочников не актуальны или не существуют;
             * @throws FieldValidationException
             *         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.);
             * @throws PorchNumberNotUniqueException
             *         - номера подъезда в доме не уникален;
             * @throws PorchFloorCountMoreThanHouseFloorCountException
             *         - значение в поле «Этажность» больше, чем «Количество этажей, наибольшее» у МКД,
             *         к которому привязан подъезд;
             * @throws PorchLastApartmentNumberLessThanFirstOneException
             *         - конечная квартира меньше, чем Начальная квартира;
             * @throws PorchStartDateYearLessThanBuildingYearException
             *         - год даты начала подъезда меньше, чем год постройки дома;
             * @throws PorchEndDateLaterThanHouseEndDateException
             *         - дата окончания подъезда больше, чем дата окончания дома;
             * @throws PorchApartmentNumbersSetIsOverlappedException
             *         - введенный интервал квартир пересекается с интервалами квартир в других подъездах.
             * @since 3.0
             */
            updatePorch: function(request, scb, ecb){
                return self.PorchSaveResource.update(request, scb, ecb);
            },
            /**
             * Метод выполняет поиск сведений о подъезде по его идентификационному коду.
             *
             * Используется для (ЭФ_ЖКХРАО_ПОД,	ЭФ_ЖКХРАО_ПОД_ПР)
             * - поиска сведений о подъезде для редактирования (ВИ_ЖКХРАО_31, ВИ_ЖКХРАО_32).
             *
             * @param request
             *         - критерии поиска
             *
             * @return Сведения о подъезде
             *
             * @since 3.0
             */
            findPorch: function(request, scb, ecb){
                return self.PorchInfoResource.create(request, scb, ecb);
            },
            /**
             * Используется для
             * - создания заявки на управление домом и создается дом в реестре адресных объектов (ВИ_ЖКХРАО_9);
             * - создания заявки на управление при информационном обмене (ВИ_ЖКХИО_3).
             *
             * Метод выполняет проверки
             * - на заполнение всех обязательных полей для заполнения;
             * - на уникальность значения в поле «Инвентарный номер»;
             * - добавляемый адрес дома существует в списке адресов домов, указанных при регистрации организации,
             * под которой пользователь выполнил вход в личный кабинет (Для ОМС);
             * - на существование добавляемого адреса в реестре объектов жилищного фонда среди актуальных объектов
             * (проверка осуществляется по составному полю «Код ФИАС», номеру дома, корпуса, строения);
             * - на корректность введенных данных в поле «Количество этажей, наибольшее» (значение в поле «Количество этажей, наибольшее» должно быть не меньше значения в поле «Количество этажей, наименьшее»);
             * - на корректность введенных данных в поле «Общая площадь» (значение в поле «Общая площадь» должно быть не меньше суммы значений полей «Нежилая площадь», «Жилая площадь») (Если Тип дома= МКД);
             * - на корректность введенных данных в поле «Общая площадь» (значение в поле «Общая площадь» должно быть не меньше значения поля  «Жилая площадь») (Если Тип дома = Жилой дом)
             * - на корректность введенных данных в поле «Год постройки» (значение в поле «Год постройки» должно быть не больше, чем год текущей даты);
             * - на корректность введенных данных в поле «Год ввода в эксплуатацию» (значение в поле «Год ввода в эксплуатацию» должно быть не меньше значения в поле «Год постройки»);
             * - на корректность введенных данных в поле «Год последнего капитального ремонта» (значение в поле «Год последнего капитального ремонта» должно быть не меньше значения в поле «Год ввода в эксплуатацию»);
             * - если поле  «Тип дома» = «Жилой дом», то поле «Способ управления» может принимать значения «ТСЖ» или «Непосредственное управление»;
             * - на корректность выбора значения ОКТМО (Пользователь должен выбрать самый значения с самого нижнего уровня справочника ОКТМО).
             *
             * @param request
             *         - Запрос на управление домом
             *
             * @return Идентификационный код дома
             *
             * @throws OrganizationNotFoundException
             *         - организации отсутствует в реестре организаций;
             * @throws NsiValueValidationException
             *         - значения справочников не актуальны или не существуют;
             * @throws FieldValidationException
             *         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.);
             * @throws HouseNotUniqueInventoryNumberException
             *         - не пройдена проверка на уникальность инвентарного номера дома;
             * @throws HouseManagementException
             *         - адрес дома не существует в списке адресов домов, указанных при регистрации организации,
             *         под которой пользователь выполнил вход в личный кабинет (для ОМС);
             * @throws HouseNotFoundException
             *         - адрес дома не существует в реестре объектов жилищного фонда среди актуальных объектов
             *         (проверка осуществляется по составному полю «Код ФИАС», номеру дома, корпуса, строения);\
             * @throws FloorCountEqualsZeroException
             *         - Значение полей «Количество этажей»,«Количество этажей, наибольшее»,"Количество этажей, наименьшее" меньше или равно 0
             * @throws MaxFloorCountLessThanMinFloorCountException
             *         - «Количество этажей, наибольшее» меньше значения в поле «Количество этажей, наименьшее»;
             * @throws TotalSquareLessThanResidentialAndNonResidentialSquareSumException
             *         - поле «Общая площадь» меньше суммы значений полей «Нежилая площадь», «Жилая площадь» (для МКД);
             * @throws TotalSquareLessThanResidentialSquareException
             *         - поле «Общая площадь» меньше значения поля «Жилая площадь» (для ЖД);
             * @throws BuildingYearException
             *         - поле «Год постройки» больше, чем год текущей даты;
             * @throws OperationYearBeforeBuildingYearException
             *         - поле «Год ввода в эксплуатацию» меньше значения в поле «Год постройки»;
             * @throws LastRebuildingYearBeforeOperationYearException
             *         - поле «Год последнего капитального ремонта» меньше значения в поле «Год ввода в эксплуатацию»;
             * @throws HouseManagementTypeException
             *         - «Способ управления» не «ТСЖ» или «Непосредственное управление» (для ЖД);
             * @throws OktmoLevelException
             *         - указаное значение не принадлежит самому нижнему уровню справочника ОКТМО;
             * @throws FileNotFoundException
             *         - не пройдены проверки на существование файла;
             * @throws InvalidStatusException
             *         - не пройдены проверки на корректный статус файла.
             */
            addHouse: function (request, scb, ecb) {
                return self.HouseSaveResource.create(request, scb, ecb);
            },
            /**
             * Используется для
             * - обновления значения характеристик объекта жилищного фонда в реестре адресных объектов (ВИ_ЖКХРАО_4).
             *
             * Метод выполняет проверки
             * - на существование дома, сведения о котором необходимо обновить;
             * - на корректность заполнения полей (обязательность, формат и т.п.);
             * - на существование привязанных к объекту жилищного фонда активных лицевых счетов;
             * - на существование привязанных к объекту жилищного фонда активных приборов учета;
             * - на существование привязанного к объекту жилищного фонда ЭП;
             * - на существование привязанных к объекту жилищного фонда актуальных помещений.
             *
             * При выполнении всех проверок обновляет значения характеристик объекта жилищного фонда в реестре адресных объектов (ВИ_ЖКХРАО_4).
             *
             * @param request
             *         - запрос на обновление сведение о доме.
             *
             * @throws HouseNotFoundException
             *         - дом не найден в реестре адресных объектов;
             * @throws OrganizationNotFoundException
             *         - организации отсутствует в реестре организаций;
             * @throws NsiValueValidationException
             *         - значения справочников не актуальны или не существуют;
             * @throws FieldValidationException
             *         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.);
             * @throws HouseNotUniqueInventoryNumberException
             *         - не пройдена проверка на уникальность инвентарного номера дома;
             * @throws FloorCountEqualsZeroException
             *         - Значение полей «Количество этажей»,«Количество этажей, наибольшее»,"Количество этажей, наименьшее" меньше или равно 0
             * @throws MaxFloorCountLessThanMinFloorCountException
             *         - «Количество этажей, наибольшее» меньше значения в поле «Количество этажей, наименьшее»;
             * @throws TotalSquareLessThanResidentialAndNonResidentialSquareSumException
             *         - поле «Общая площадь» меньше суммы значений полей «Нежилая площадь», «Жилая площадь» для МКД;
             * @throws TotalSquareLessThanResidentialSquareException
             *         - поле «Общая площадь» меньше значения поля «Жилая площадь» для ЖД;
             * @throws BuildingYearException
             *         - поле «Год постройки» больше, чем год текущей даты;
             * @throws LastRebuildingYearBeforeOperationYearException
             *         -  поле «Год последнего капитального ремонта» меньше значения в поле «Год ввода в эксплуатацию»;
             * @throws OperationYearBeforeBuildingYearException
             *         - поле «Год ввода в эксплуатацию» меньше значения в поле «Год постройки»;
             * @throws ApartmentsTotalSquareGreaterThanHouseTotalSquareException
             *         - суммарное значение общих площадей всех актуальных жилых помещений, привязанных к выбранному МКД больше
             *         чем общая площадь МКД;
             * @throws ApartmentsResidentialSquareGreaterThanHouseResidentialSquareException
             *         - суммарное значение жилых площадей всех актуальных жилых помещений, привязанных к выбранному МКД больше
             *         чем жилая площадь МКД;
             * @throws HouseManagementTypeException
             *         - «Способ управления» не «ТСЖ» или «Непосредственное управление» для ЖД;
             * @throws FileNotFoundException
             *         - не пройдены проверки на существование файла;
             * @throws InvalidStatusException
             *         - не пройдены проверки на корректный статус файла;
             * @throws PassportAlreadyExistException
             *         - существует карточка с настройками ЭП привязанная к объекту жилищного фонда;
             * @throws LinkedActiveAccountsExistException
             *         - существуют активные лицевые счета, привязанные к выбранному объекту жилищного фонда;
             * @throws LinkedActiveDevicesExistException
             *         - существуют активные приборы учета, привязанные к выбранному объекту жилищного фонда;
             * @throws LinkedActiveApartmentsExistException
             *         - существуют активные помещения, привязанные к выбранному объекту жилищного фонда.
             */
            updateHouse: function(request, scb, ecb){
                return self.HouseSaveResource.update(request, scb, ecb);
            },
            /**
             * Метод выполняет поиск сведений о многоквартирном/жилом доме по его идентификационному коду.
             *
             * Используется для (ЭФ_ЖКХРАО_КД_МКД, ЭФ_ЖКХРАО_СП)
             * - поиска сведений о объекте жилищного фонда для редактирования сведений о МКД/ЖД.
             * @param request
             *         - houseTypeCode, houseGuid
             *
             * @return Сведения о МКД/ЖД
             */
            getHouse: function(request, scb, ecb){
                return self.HouseResource.get(request, scb, ecb);
            },
            /**
             * Метод выполняет поиск кратких сведений об объектах жилищного фонда, удовлетворяющих критериям поиска.
             *
             * Используется для (ЭФ_ВД)
             * - «Выбор дома» (ВИ_ВД);
             * - поиска объектов жилищного фонда для выбора при создании прибора учета (ВИ_ВД);
             * - поиска объектов жилищного фонда для выбора при создании/редактировании ЛС (ВИ_ЛС_03, ВИ_ЛС_04).
             *
             * @param request
             *         - критерии поиска
             * @param pathParams
             *         {
             *              page: Номер страницы, для которой необходимо сформировать данные,
             *              itemsPerPage: Количество элементов на странице
             *         }
             *         - Номер страницы, для которой необходимо сформировать данные
             *
             * @return Список кратких сведений о домах, удовлетворяющих критериям поиска
             */
            getHouseList: function(request, scb, ecb, pathParams){
                return self.HousesSearchResource.create(request, scb, ecb, pathParams);
            },
            /**
             * Метод выполняет поиск кратаких сведений о помещениях и их лицевых счетах, удовлетворяющих критериям поиска.
             *
             * Используется для
             * - поиска помещений в карточке дома (ВИ_ЖКХРАО_10);
             * - поиска помещений на странице «Сведения о помещениях» (ВИ_ЖКХРАО_27);
             * - поиска помещений в ЛК гражданина при входе в ЛК гражданиа (ВИ_ППА_01).
             *
             * @param request
             *         - критерии поиска
             * @param pathParams
             *         {
             *              page: Номер страницы, для которой необходимо сформировать данные,
             *              itemsPerPage: Количество элементов на странице
             *         }
             *         - Номер страницы, для которой необходимо сформировать данные
             *
             * @return Список кратаких сведений о помещениях и их лицевых счетах, удовлетворяющих критериям поиска
             */
            getApartmentWithAccountList: function (request, scb, ecb, pathParams) {
                return self.HouseApartmentsResource.create(request, scb, ecb, pathParams);
            },
            /**
             * Метод выполняет поиск сведений о помещении по его идентификационному коду.
             *
             * Используется для (ЭФ_ЖКХРАО_КП_Ж,ЭФ_ЖКХРАО_КП_ПР_Ж)
             * - поиска сведений о помещении для редактирования сведений о помещении (ВИ_ЖКХРАО_5);
             * - поиска сведений о помещении для просмотра (ВИ_ЖКХРАО_14);
             * - поиска сведений о помещении  в ЛК гражданина (ВИ_ЖКХРАО_7);
             * - поиск помещения при информационном обмене (ВИ_ЖКХИО_24).
             *
             * @param id
             *         - идентификатор помещения
             *
             * @return Сведения о помещении
             */
            findApartment: function (id, scb, ecb) {
                return self.ApartmentResource.getById(id, scb, ecb);
            },
            /**
             * Метод осуществляет создание жилого помещения (ВИ_ЖКХРАО_3, ВИ_ЖКХИО_10).
             *
             * Метод выполняет проверки:
             * - на заполнение всех обязательных полей для заполнения;
             * - на корректность введенных данных в поле «Общая площадь» (значение в поле «Общая площадь» должно быть не меньше
             * значения поля «Жилая площадь»);
             * - на корректность введенного значения в поле «Номер» (Номер добавляемого помещения должен входить в интервал
             * квартир подъезда, к которому добавляется помещение);
             * - на корректность введенного значения в поле «Этаж» (значение в поле этаж должно быть не больше,
             * чем значение в поле Этажность у подъезда, к которому добавляется помещение);
             * - на корректность введенного значения в поле «Дата начала» (дата начала не должна быть меньше, чем дата начала
             * подъезда);
             * - на корректность введенного значения в поле «Дата окончания» (дата окончания помещения не может быть больше,
             * чем дата окончания подъезда);
             * - на корректность введенных данных в поле «Общая площадь» (суммарное значение общих площадей всех актуальных
             * помещений, привязанных к выбранному МКД, должно быть не больше, чем общая площадь МКД);
             * - на корректность введенных данных в поле «Жилая площадь» (суммарное значение жилых площадей всех актуальных
             * жилых помещений, привязанных к выбранному МКД, должно быть не больше, чем жилая площадь МКД);
             * - на существование добавляемого номера помещения  в списке помещений у выбранного объекта жилищного фонда.
             *
             * По окончании выполнения проверок система создает экземпляр класса «Жилое помещение» или «Нежилое помещение».
             * Система создает запись в сущности «История событий», привязанную к добавляемому экземпляру класса «Жилое помещение» или «Нежилое помещение» с кодом С_РАО_9, дата события = текущая дата.
             *
             * Метод вызывает
             * - метод {@link ru.lanit.hcs.homemanagement.api.HouseService#createResidentialPremise(ru.lanit.hcs.homemanagement.api.dto.request.ResidentialPremiseCreateRequest)}
             * - метод {@link ru.lanit.hcs.nsi.api.ApartmentService#findApartmentCategory(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
             *
             * @param request
             *         - запрос на создание жилого помещения.
             *
             * @return Ссылка на помещение.
             *
             * @throws HouseNotFoundException
             *         - дом не найден в реестре адресных объектов;
             * @throws NsiValueValidationException
             *         - значения справочников не актуальны или не существуют;
             * @throws FieldValidationException
             *         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.);
             * @throws PorchNotFoundException
             *         - подъезд не найден;
             * @throws TotalSquareLessThanResidentialSquareException
             *         - поле «Общая площадь» меньше значения поля «Жилая площадь»;
             * @throws ApartmentNotInPorchException
             *         - номер добавляемого помещения не входит в интервал квартир подъезда, к которому добавляется помещение;
             * @throws ApartmentFloorHigherThanPorchFloorCountException
             *         - значение в поле этаж больше, чем значение в поле Этажность у подъезда,
             *         к которому добавляется помещение;
             * @throws ApartmentStartDateEarlierThanPorchStartDateException
             *         - дата начала помещения меньше, чем дата начала подъезда;
             * @throws ApartmentEndDateLaterThanPorchEndDateException
             *         - дата окончания помещения больше, чем дата окончания подъезда;
             * @throws ApartmentsTotalSquareGreaterThanHouseTotalSquareException
             *         - суммарное значение общих площадей всех актуальных жилых помещений,
             *         привязанных к выбранному МКД больше чем общая площадь МКД;
             * @throws ApartmentsResidentialSquareGreaterThanHouseResidentialSquareException
             *         - суммарное значение жилых площадей всех актуальных жилых помещений,
             *         привязанных к выбранному МКД больше чем жилая площадь МКД;
             * @throws ApartmentNotUniqueFlatNumberException
             *         - не пройдена проверка на уникальность номера помещения в списке жилых помещений.
             * @since 3.0
             */
            addResidentialPremise: function (request, scb, ecb) {
                return self.ResidentalApartmentResource.create(request, scb, ecb);
            },
            /**
             * Метод осуществляет изменение сведений о жилом помещении.
             *
             * Используется для
             * - обновления помещения у дома в реестре адресных объектов(ВИ_ЖКХРАО_5);
             * - обновления помещения при информационном обмене (ВИ_ЖКХИО_10).
             *
             * Метод выполняет проверки
             * - на существование активных лицевых счетов, привязанных к выбранному помещению;
             * - на существование активных приборов учета, привязанных к выбранному помещению;
             * - на заполнение всех обязательных полей для заполнения;
             * - на корректность введенных данных в поле «Общая площадь» (значение в поле «Общая площадь» должно быть
             * не меньше значения поля «Жилая площадь»);
             * - на корректность введенных данных в поле «Общая площадь» (суммарное значение общих площадей всех
             * актуальных помещений, привязанных к выбранному МКД, должно быть не больше, чем общая площадь МКД);
             * - на корректность введенного значения в поле «Номер» (Номер добавляемого помещения должен входить в
             * интервал квартир подъезда, к которому добавляется помещение);
             * - на корректность введенного значения в поле «Этаж» (значение в поле этаж должно быть не больше,
             * чем значение в поле Этажность у подъезда, к которому добавляется помещение);
             * - на корректность введенного значения в поле «Дата начала» (дата начала не должна быть меньше,
             * чем дата начала подъезда);
             * - на корректность введенного значения в поле «Дата окончания» (дата окончания помещения не может быть больше,
             * чем дата окончания подъезда);
             * - на корректность введенных данных в поле «Жилая площадь» (суммарное значение жилых площадей всех
             * актуальных жилых помещений, привязанных к выбранному МКД, должно быть не больше, чем жилая площадь МКД).
             *
             * При выполнении всех проверок система
             * - сохраняет измененную запись в списке помещений, относящихся к выбранному объекту жилищного фонда;
             * - создает запись в сущности «История событий», привязанную к измененному экземпляру класса «Жилое помещение».
             *
             * Метод вызывает
             * - метод {@link ru.lanit.hcs.homemanagement.api.HouseService#updateResidentialPremise(ru.lanit.hcs.homemanagement.api.dto.request.ResidentialPremiseUpdateRequest)}
             *
             * @param request
             *         - запрос на изменение сведений о жилом помещении.
             *
             * @throws ApartmentNotFoundException
             *         - помещение не найдено в реестре адресных объектов;
             * @throws NsiValueValidationException
             *         - значения справочников не актуальны или не существуют;
             * @throws LinkedActiveAccountsExistException
             *         - существуют активные приборы учета, привязанные к выбранному помещению;
             * @throws LinkedActiveDevicesExistException
             *         - существуют активные лицевые счета, привязанные к выбранному помещению;
             * @throws FieldValidationException
             *         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.);
             * @throws PorchNotFoundException
             *         - подъезд не найден;
             * @throws TotalSquareLessThanResidentialSquareException
             *         - поле «Общая площадь» меньше значения поля «Жилая площадь»;
             * @throws ApartmentsTotalSquareGreaterThanHouseTotalSquareException
             *         - суммарное значение общих площадей всех актуальных жилых помещений, привязанных к выбранному МКД больше
             *         чем общая площадь МКД;
             * @throws ApartmentNotInPorchException
             *         - номер добавляемого помещения не входит в интервал квартир подъезда, к которому добавляется помещение;
             * @throws ApartmentFloorHigherThanPorchFloorCountException
             *         - значение в поле этаж больше, чем значение в поле Этажность у подъезда, к которому добавляется помещение;
             * @throws ApartmentStartDateEarlierThanPorchStartDateException
             *         - дата начала помещения меньше, чем дата начала подъезда;
             * @throws ApartmentEndDateLaterThanPorchEndDateException
             *         - дата окончания помещения больше, чем дата окончания подъезда;
             * @throws ApartmentNotUniqueUidException
             *         - не пройдена проверка на уникальность единого идетификатора помещения;
             * @throws ApartmentNotUniqueFlatNumberException
             *         - не пройдена проверка на уникальность номера помещения в списке жилых помещений;
             * @throws LastEventDateValidationException
             *         - Переданное значение "Дата модификации" меньше значения в системе.
             * @since 3.0
             */
            updateResidentialPremise: function (request, scb, ecb) {
                return self.ResidentalApartmentResource.update(request, scb, ecb);
            },
            /**
             * Метод осуществляет создание нежилого помещения (ВИ_ЖКХРАО_3, ВИ_ЖКХИО_10).
             *
             * Метод выполняет проверки:
             * - на заполнение всех обязательных полей для заполнения;
             * - на корректность введенного значения в поле «Дата начала» (год даты начала должен быть не меньше,
             * чем год постройки дома);
             * - на корректность введенного значения в поле «Дата окончания» (Дата окончания помещения не может быть больше,
             * чем дата окончания дома);
             * - на корректность введенных данных в поле «Общая площадь» (суммарное значение общих площадей всех актуальных
             * помещений, привязанных к выбранному МКД, должно быть не больше, чем общая площадь МКД);
             * - на существование добавляемого номера помещения  в списке помещений у выбранного объекта жилищного фонда.
             *
             * По окончании выполнения проверок система создает экземпляр класса «Жилое помещение» или «Нежилое помещение».
             * Система создает запись в сущности «История событий», привязанную к добавляемому экземпляру класса «Жилое помещение» или «Нежилое помещение» с кодом С_РАО_9, дата события = текущая дата.
             *
             * Метод вызывает
             * - метод {@link ru.lanit.hcs.homemanagement.api.HouseService#createNonResidentialPremise(ru.lanit.hcs.homemanagement.api.dto.request.NonResidentialPremiseCreateRequest)}
             * - метод {@link ru.lanit.hcs.nsi.api.ApartmentService#findApartmentCategory(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
             *
             * @param request
             *         - запрос на создание нежилого помещения.
             *
             * @return Ссылка на помещение.
             *
             * @throws HouseNotFoundException
             *         - дом не найден в реестре адресных объектов;
             * @throws NsiValueValidationException
             *         - значения справочников не актуальны или не существуют;
             * @throws FieldValidationException
             *         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.);
             * @throws ApartmentStartDateYearLessThanBuildingYearException
             *         - год начала помещения меньше, чем год постройки дома;
             * @throws ApartmentEndDateLaterThanHouseEndDateException
             *         - дата окончания помещения больше, чем дата окончания дома;
             * @throws ApartmentsTotalSquareGreaterThanHouseTotalSquareException
             *         - суммарное значение общих площадей всех актуальных нежилых помещений,
             *         привязанных к выбранному МКД больше чем общая площадь МКД;
             * @throws ApartmentNotUniqueFlatNumberException
             *         - не пройдена проверка на уникальность номера помещения в списке нежилых помещений.
             * @since 3.0
             */
            addNonResidentialPremise: function (request, scb, ecb) {
                return self.NonResidentalApartmentResource.create(request, scb, ecb);
            },
            /**
             * Метод осуществляет изменение сведений о нежилом помещении.
             *
             * Используется для
             * - обновления помещения у дома в реестре адресных объектов(ВИ_ЖКХРАО_5);
             * - обновления помещения при информационном обмене (ВИ_ЖКХИО_10).
             *
             * Метод выполняет проверки
             * - на существование активных лицевых счетов, привязанных к выбранному помещению;
             * - на существование активных приборов учета, привязанных к выбранному помещению;
             * - на заполнение всех обязательных полей для заполнения;
             * - на корректность введенных данных в поле «Общая площадь» (суммарное значение общих площадей всех
             * актуальных помещений, привязанных к выбранному МКД, должно быть не больше, чем общая площадь МКД);
             * - на корректность введенного значения в поле «Дата начала» (год даты начала должен быть не меньше,
             * чем год постройки дома);
             * - на корректность введенного значения в поле «Дата окончания» (Дата окончания помещения не может быть больше,
             * чем дата окончания дома).
             *
             * При выполнении всех проверок система
             * - сохраняет измененную запись в списке помещений, относящихся к выбранному объекту жилищного фонда;
             * - создает запись в сущности «История событий», привязанную к измененному экземпляру класса «Нежилое помещение».
             *
             * Метод вызывает
             * - метод {@link ru.lanit.hcs.homemanagement.api.HouseService#updateNonResidentialPremise(ru.lanit.hcs.homemanagement.api.dto.request.NonResidentialPremiseUpdateRequest)}
             *
             * @param request
             *         - запрос на изменение сведений о нежилом помещении.
             *
             * @throws ApartmentNotFoundException
             *         - помещение не найдено в реестре адресных объектов;
             * @throws NsiValueValidationException
             *         - значения справочников не актуальны или не существуют;
             * @throws LinkedActiveAccountsExistException
             *         - существуют активные приборы учета, привязанные к выбранному помещению;
             * @throws LinkedActiveDevicesExistException
             *         - существуют активные лицевые счета, привязанные к выбранному помещению;
             * @throws FieldValidationException
             *         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.);
             * @throws ApartmentsTotalSquareGreaterThanHouseTotalSquareException
             *         - суммарное значение общих площадей всех актуальных нежилых помещений, привязанных к выбранному МКД
             *         больше, чем общая площадь МКД;
             * @throws ApartmentStartDateYearLessThanBuildingYearException
             *         - год начала помещения меньше, чем год постройки дома;
             * @throws ApartmentEndDateLaterThanHouseEndDateException
             *         - дата окончания помещения больше, чем дата окончания дома;
             * @throws ApartmentNotUniqueUidException
             *         - не пройдена проверка на уникальность единого идетификатора помещения;
             * @throws ApartmentNotUniqueFlatNumberException
             *         - не пройдена проверка на уникальность номера помещения в списке нежилых помещений;
             * @throws LastEventDateValidationException
             *         - Переданное значение "Дата модификации" меньше значения в системе.
             * @since 3.0
             */
            updateNonResidentialPremise: function (request, scb, ecb) {
                return self.NonResidentalApartmentResource.update(request, scb, ecb);
            },
            /**
             * Метод осуществляет удаление помещения у дома в реестре адресных объектов (ВИ_ЖКХРАО_6) (ВИ_ЖКХИО_14).
             *
             * Метод выполняет проверки
             * - на существование активных лицевых счетов, привязанных к удаляемому помещению;
             * - на существование активных приборов учета, привязанных к удаляемому помещению;
             * - на существование неактивных лицевых счетов или неактивных приборов учета, привязанных к удаляемому помещению.
             *
             * При выполнении всех проверок система
             * - удаляет все записи в сущности «История событий», привязанные к удаляемому помещению;
             * - создает запись в сущности «История событий», привязанную к экземпляру класса «Многоквартирный дом» или «Подъезд», у которого удаляют помещение, с кодом С_РАО_7 (при привязке к МКД), С_РАО_19 (при привязке к подъезду) дата события = текущая дата;
             * - удаляет помещение, привязанное к выбранному объекту жилищного фонда;
             * - (?) закрывает лицевой счет с удалением связей со всеми помещениями.
             *
             * Метод вызывает
             * - метод {@link ru.lanit.hcs.homemanagement.api.HouseService#deleteApartment(ru.lanit.hcs.homemanagement.api.dto.request.ApartmentDeleteRequest)}
             *
             * @param request
             *         - запрос на удаление помещения.
             *
             * @throws ApartmentNotFoundException
             *         - помещение не найдено в реестре адресных объектов;
             * @throws LinkedActiveAccountsExistException
             *         - существуют активные лицевые счета, привязанные к удаляемому помещению;
             * @throws LinkedActiveDevicesExistException
             *         - существуют активные приборы учёта, привязанные к удаляемому помещению;
             * @throws LinkedNotActiveAccountsExistException
             *         - существуют неактивные лицевые счета или неактивные приборы учёта, привязанные к удаляемому помещению;
             * @throws LinkedNotActiveDevicesExistException
             *         - существуют неактивные лицевые счета или неактивные приборы учёта, привязанные к удаляемому помещению;
             * @throws LastEventDateValidationException
             *         - Переданное значение "Дата модификации" меньше значения в системе.
             */
            removeApartment: function (request, scb, ecb) {
                return self.ApartmentRemoveResource.removeObject(request, scb, ecb);
            },
            /**
             * Метод выполняет поиск кратких сведений о помещениях, удовлетворяющих критериям поиска.
             *
             * Используется для
             * - поиска помещений для выбора при создании прибора учета (ВИ_ЖКХПУ_04);
             * - поиска помещений для выбора при создании/редактировании ЛС (ВИ_ЛС_03, ВИ_ЛС_04).
             *
             * @param request
             *         - критерии поиска
             *
             * @param pathParams
             *         {
             *              page: Номер страницы, для которой необходимо сформировать данные,
             *              itemsPerPage: Количество элементов на странице
             *         }
             *
             * @return Список кратких сведений о помещениях, удовлетворяющих критериям поиска
             */
            getApartmentList: function (request, scb, ecb, pathParams) {
                return self.ApartmentSearchResource.create(request, scb, ecb, pathParams);
            },
            /**
             * Метод для формирования перечня коммунальных услуг.
             *
             * Используется для
             * - получения списка коммунальных услуг по дому для конкретной организации на конкретный момент времени (момент времени должен входить в период действия услуги) (ВИ_ЖКХРАО_10)
             * - получения перечня КУ при информационном обмене (ВИ_ЖКХИО_19,ВИ_ЖКХИО_20)
             *
             * @param request
             *         - критерии поиска
             *
             * @return - перечень коммунальных услуг
             */
            getMunicipalServices: function(request, scb, ecb){
                return self.MunicipalServicesResource.create(request, scb, ecb);
            },
            /**
             * Метод для получения коммунальной услуги по guid-у
             *
             * @param guid
             *         - guid услуги
             *
             * @return - перечень коммунальных услуг
             */
            getMunicipalService: function(guid, scb, ecb){
                return self.MunicipalServicesResource.getById(guid, scb, ecb);
            },
            /**
             * Метод создает коммунальную услугу (ВИ_ЖКХРАО_21, ВИ_ЖКХРАО_23).
             *
             * @param request
             *         - Запрос на добавление коммунальной услуги
             *
             * @return Коммунальная услуга
             *
             * @throws FieldValidationException
             *         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.);
             * @throws ServiceAlreadyExistException
             *         - услуга такого же вида с пересекающимся периодом уже существует;
             * @throws DatePeriodValidationException
             *         - не пройдена проверка на то, что дата начала действия услуги меньше, чем дата окончания действия;
             * @throws ServiceEndDateLessOrEqualThanCurrentException
             *         - не пройдена проверка на то, что значение даты окончания услуги больше, чем текущая дата;
             * @throws OrganizationNotFoundException
             *         - указанная организация не найдена в реестре организаций
             */
            addMunicipalService: function(request, scb, ecb){
                return self.MunicipalServiceSaveResource.create(request, scb, ecb);
            },
            /**
             * Метод обновляет коммунальную услугу (ВИ_ЖКХРАО_24).
             *
             * @param request
             *         - Запрос на обновление коммунальной услуги
             *
             * @return Коммунальная услуга
             *
             * @throws MunicipalServiceNotFoundException
             *         - коммунальная услуга не найдена в реестре адресных объектов;
             * @throws FieldValidationException
             *         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.);
             * @throws ServiceAlreadyExistException
             *         - услуга такого же вида с пересекающимся периодом уже существует;
             * @throws DatePeriodValidationException
             *         - не пройдена проверка на то, что дата начала действия услуги меньше, чем дата окончания действия;
             * @throws ServiceEndDateLessOrEqualThanCurrentException
             *         - не пройдена проверка на то, что значение даты окончания услуги больше, чем текущая дата;
             * @throws ServiceStartDateIsNotInInitialMonthException
             *         - не пройдена проверка на то, что дата начала услуги находится в том же месяце,
             *         в котором она была изначально до внесения изменения;
             * @throws ModificationDateException
             *         - услуга в системе имеет более позднюю дату модификации, чем передаваемая.
             */
            updateMunicipalService: function(request, scb, ecb){
                return self.MunicipalServiceSaveResource.update(request, scb, ecb);
            },
            /**
             * Метод для формирования перечня жилищных услуг.
             *
             * Используется для
             * - получения списка жилищных услуг по дому для конкретной организации на конкретный момент времени (момент времени должен входить в период действия услуги) (ВИ_ЖКХРАО_10)
             * - получения перечня КУ при информационном обмене (ВИ_ЖКХИО_20)
             *
             * @param request
             *         - критерии поиска
             *
             * @return - перечень жилищных услуг
             */
            getHousingServices: function(request, scb, ecb){
                return self.HousingServicesResource.create(request, scb, ecb);
            },
            /**
             * Метод для получения жилищной услуги по guid.
             *
             * @param guid
             *         - guid услуги
             *
             * @return - жилищная услуга
             */
            getHousingService: function(guid, scb, ecb){
                return self.HousingServicesResource.getById(guid, scb, ecb);
            },
            /**
             * Метод создает жилищные услуги (ВИ_ЖКХРАО_22, ВИ_ЖКХИО_20)
             *
             * @param request
             *         - Запрос на добавление коммунальной услуги
             *
             * @return Список созданых услуг
             *
             * @throws OrganizationNotFoundException
             *         - организации отсутствует в реестре организаций;
             * @throws NsiValueValidationException
             *         - значения справочников не актуальны или не существуют;
             * @throws FieldValidationException
             *         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
             * @throws HousingServiceDuplicateException
             *         - в сохраняемом списке есть жилищные услуги одинакового вида, периоды действия которых пересекаются
             * @throws DefaultHousingServiceNotExistException
             *         - у дома отсуствуют в текущем месяце жилищные услуги вида «Плата за содержание и ремонт жилого помещения», «Взнос на капитальный ремонт».
             * @throws ServiceAlreadyExistException
             *         - не пройдена проверка на существование у дома жилищной услуги, того же вида, что и добавляемая услуга, и период действия которой, пересекается с периодом действия добавляемой услуги;
             * @throws DatePeriodValidationException
             *         - не пройдена проверка на то, что начало периода действия услуги меньше, чем окончание периода;
             * @throws ServiceEndMonthLessOrEqualThanCurrentException
             *         - не пройдена проверка на то, что месяц окончания услуги больше, чем текущая дата;
             */
            addHousingService: function(request, scb, ecb){
                return self.HousingServiceSaveResource.create(request, scb, ecb);
            },
            /**
             * Метод обновляет жилищную услугу (ВИ_ЖКХРАО_25)

             * @param request
             *         - Запрос на добавление коммунальной услуги
             *
             * @return Жилищная услуга
             *
             * @throws OrganizationNotFoundException
             *         - организации отсутствует в реестре организаций;
             * @throws NsiValueValidationException
             *         - значения справочников не актуальны или не существуют;
             * @throws FieldValidationException
             *         - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
             * @throws ServiceAlreadyExistException
             *         - услуга такого же вида с пересекающимся периодом уже существует
             * @throws HousingServiceNotFoundException
             *         - «Идентификатор ЖУ» не в справочнике ЖУ.
             * @throws DatePeriodValidationException
             *         - не пройдена проверка на то, что начало периода действия услуги меньше, чем окончание периода;
             * @throws ServiceEndMonthLessThanNextMonthException
             *         - не пройдена проверка на то, что месяц окончания периода действия услуги не меньше,
             *         чем следующий месяц после текущего;
             * @throws ModificationDateException
             *         - услуга в системе имеет более позднюю дату модификации, чем передаваемая.
             */
            updateHousingService: function(request, scb, ecb){
                return self.HousingServiceSaveResource.update(request, scb, ecb);
            },
            /**
             * Метод осуществляет поиск МКД для
             * - жильцов,
             * - администраторов общего собрания собственников,
             * - уполномоченных специалистов УО с привилегией «Ведение реестра сведений о голосовании».
             * - уполномоченных специалистов организации с полномочием организации «Администратор общего собрания собственников» с привилегией «Ведение реестра сведений о голосовании».
             * (ЭФ_ЖКХГ_БПСГ, ВИ_ЖКХГ_ПСГ)
             *
             * Метод вызывает
             * - метод {@link ru.lanit.hcs.ppa.api.PpaService#findEmployeeDetails(ru.lanit.hcs.ppa.api.dto.search.EmployeeSearchCriteria, int, int)}
             * - метод {@link ru.lanit.hcs.ppa.api.PpaService#findCitizenAdminsOfGeneralMeeting(ru.lanit.hcs.ppa.api.dto.search.CitizenAdminsOfGeneralMeetingSearchCriteria, int, int)}
             * - метод {@link ru.lanit.hcs.homemanagement.api.AccountService#findLinkBetweenCitizenAndAccount(ru.lanit.hcs.homemanagement.api.dto.search.LinkBetweenCitizenAndAccountSearchCriteria, Integer, Integer)}
             * - метод {@link ru.lanit.hcs.homemanagement.api.HouseService#findHousesForVotingResponse(ru.lanit.hcs.homemanagement.api.dto.search.FindHousesForVotingSearchCriteria, Integer, Integer)}
             *
             * @param searchCriteria
             *         - критерии поиска;
             * @param pageIndex
             *         - номер страницы, для которой необходимо сформировать данные;
             * @param elementsPerPage
             *         - количество элементов на странице.
             *
             * @return Список кратких сведений об МКД, удовлетворяющих критериям поиска.
             *
             * @since 3.0
             */
            getHouseAddressesByUser: function(request, scb, ecb, pathParams){
                return self.getHouseAddressesByUserResource.create(request, scb, ecb, pathParams);
            },

            /**
             * Метод осуществляет поиск электронного документа "Доля" по его идентификатору.
             *
             * Метод используется для отображения
             * - ЭФ_ЖКХРАО_ДОЛЯ_СД,
             * - ЭФ_ЖКХРАО_ДОЛЯ_СД_ПР.
             *
             * @param request
             *         - критерий поиска электронного документа "Доля" по его идентификатору.
             *
             * @return результат поиска электронного документа "Доля" по его идентификатору.
             *
             * @since 4.0
             */
            findShare: function (request, scb, ecb) {
                return self.ShareResource.create(request, scb, ecb);
            },

            /**
             * Метод создания и изменения версии электронного документа  «Доля» (ВИ_ЖКХРАО_34, ВИ_ЖКХРАО_35).
             *
             * @param request
             *         - запрос на создание и изменение версии электронного документа "Доля".
             *
             * @return версия электронного документа  «Доля».
             *
             * @since 4.0
             */
            updateShare: function (request, scb, ecb) {
                return self.ShareSaveResource.create(request, scb, ecb);
            },

            /**
             * Метод осуществляет поиск собственников помещений в многоквартирном доме.
             *
             * Метод вызывает
             * - метод {@link ru.lanit.hcs.homemanagement.api.HouseService#findHouseOwners(ru.lanit.hcs.homemanagement.api.dto.search.FindHouseOwnersSearchCriteria, Integer, Integer)}
             *
             * @param searchCriteria
             *         - критерий поиска;
             * @param pageIndex
             *         - номер страницы, для которой необходимо сформировать данные;
             * @param elementsPerPage
             *         - количество элементов на странице.
             *
             * @return список собственников, удовлетворяющий критерию поиска.
             *
             * @since 4.0
             */
            findHouseOwners: function(request, pathParams, scb, ecb){
                return self.FindHouseOwnersResource.create(request, scb, ecb, pathParams);
            },

            /**
             * Метод осуществляет поиск информации о собственнике.
             *
             * Метод вызывает
             * - метод {@link ru.lanit.hcs.homemanagement.api.HouseService#findHouseOwner(ru.lanit.hcs.homemanagement.api.dto.search.FindHouseOwnerSearchCriteria)}
             *
             * @param searchCriteria
             *         - критерий поиска.
             *
             * @return информации о собственнике.
             *
             * @since 4.0
             */
            findHouseOwner: function(request, scb, ecb){
                return self.HouseOwnerResource.create(request, scb, ecb);
            },

            findLatestApprovedShareVersion: function(request, scb, ecb){
                return self.FindLatestApprovedShareVersionResource.create(request, scb, ecb);
            }
        };
    }
]);