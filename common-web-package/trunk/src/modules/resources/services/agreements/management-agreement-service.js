angular
    .module('ru.lanit.hcs.agreements.rest.ManagementAgreementService', [
        'ru.lanit.hcs.agreements.rest.AbstractAgreementService'
    ])
    .factory('ManagementAgreementService', [
        'AbstractAgreementService',
        'AgreementResource',
        function (AbstractAgreementService, AgreementResource) {
            var service = {};
            AbstractAgreementService.mixinAbstractSimpleDocumentInterface(service, '/managements');
            AbstractAgreementService.mixinAbstractManagedObjectInterface(service, '/managements');

            function toUrl(part) {
                return '/agreements/managements' + part;
            }

            var checkHouseResource = new AgreementResource(toUrl('/checkhouse'));

            service.checkHouseResource = function (queryJson, successcb, errorcb) {
                return checkHouseResource.query(null, queryJson, successcb, errorcb);
            };

            return service;
        }
    ]);