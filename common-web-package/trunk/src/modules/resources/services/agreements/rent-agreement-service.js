angular
    .module('ru.lanit.hcs.agreements.rest.RentAgreementService', [
        'agreements-resource',
        'ru.lanit.hcs.agreements.rest.AbstractAgreementService'
    ])
    .factory('$RentAgreementService', [
        'AgreementResource',
        'AbstractAgreementService',
        function (AgreementResource, AbstractAgreementService) {
            var service = {};
            AbstractAgreementService.mixinAbstractSimpleDocumentInterface(service, '/rents');

            function toUrl(part) {
                return '/agreements/rents' + part;
            }

            var searchResource = new AgreementResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));

            service.findByCriteria = function (searchCriteria, page, itemsPerPage, successcb, errorcb) {
                return searchResource.queryPost({
                    page: page,
                    itemsPerPage: itemsPerPage
                }, searchCriteria, successcb, errorcb);
            };

            return service;
        }
    ]);