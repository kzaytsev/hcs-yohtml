angular.module('ru.lanit.hcs.contract.rest.ContractService', [])
    .factory('$ContractService', ['$q', function ($q) {
        function toUrl(part) {
            return '/contracts' + part;
        }

        return {
            findAgreement: function (guid, successcb, errorcb) {
                var services = [
                    {
                        guid: _.uniqueId(),
                        name: 'Услуга 1'
                    },
                    {
                        guid: _.uniqueId(),
                        name: 'Услуга 2'
                    },
                    {
                        guid: _.uniqueId(),
                        name: 'Услуга 3'
                    },
                    {
                        guid: _.uniqueId(),
                        name: 'Услуга 4'
                    },
                    {
                        guid: _.uniqueId(),
                        name: 'Услуга 5'
                    },
                    {
                        guid: _.uniqueId(),
                        name: 'Услуга 6'
                    }
                ];

                var agreement = {
                    guid: guid,
                    number: '11124127',
                    status: 'DRAFT',
                    addresses: []
                };

                for (var i = 0; i < 25; i++) {
                    agreement.addresses.push({
                        guid: _.uniqueId(),
                        publicServices: _.clone(services),
                        extraServices: _.clone(services),
                        formattedAddress: 'ул Улица ' + i + ', д ' + i + ', кв ' + i
                    });
                }

                successcb(agreement);
            },
            /**
             *
             * @param guid
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            findCharterById: function (guid, successcb, errorcb) {
                return $q.when({
                    guid: guid,
                    status: 'NOT_REQUIRE_APPROVAL',
                    hoa: {
                        guid: _.uniqueId(),
                        name: "TestNameHoa",
                        inn: "121412414214",
                        knn: "41244132",
                        registrationDate: new Date()
                    },
                    number: "6542345234",
                    startDate: new Date(),
                    validityYear: 0,
                    validityMonth: 0,
                    protocolMeetingOwners: [],
                    startDuringPuDate: null,
                    endDuringPuDate: null,
                    puttingPaymentDate: null,
                    withoutDuring: false,
                    compositionControl: "",
                    chairman: {
                        guid: _.uniqueId(),
                        lastName: "Ivanov",
                        firstName: "Ivan",
                        middleName: "Ivanovich"
                    },
                    files: [
                        { contentGuid: '-1', fileName: 'file1.txt', contentType: 'text/plain', fileSize: '25725', description: 'Некое описание тестового файла' },
                        { contentGuid: '-2', fileName: 'file2.txt', contentType: 'text/plain', fileSize: '25725', description: 'Некое описание тестового файла' },
                        { contentGuid: '-3', fileName: 'file3.txt', contentType: 'text/plain', fileSize: '25725', description: 'Некое описание тестового файла' }
                    ],
                    terminateFiles: [
                        { contentGuid: '-1', fileName: 'file1.txt', contentType: 'text/plain', fileSize: '25725', description: 'Некое описание тестового файла' },
                        { contentGuid: '-2', fileName: 'file2.txt', contentType: 'text/plain', fileSize: '25725', description: 'Некое описание тестового файла' },
                        { contentGuid: '-3', fileName: 'file3.txt', contentType: 'text/plain', fileSize: '25725', description: 'Некое описание тестового файла' }
                    ],
                    closureActualDate: new Date(),
                    terminateReason: "",
                    houses: [],
                    version: 2
                });
            },
            /**
             * ВИ_ЖКХДОГ_14. Расторжение договора, устава
             * @param {Object} terminateContractObject - договор или устав на росторжение
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            terminateContract: function (terminateContractObject, successcb, errorcb) {
                successcb();
            },
            /**
             * Выбор из существующих уставов со статусом «Действующий», «Не вступивший в силу»
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            findActualCharter: function (successcb, errorcb) {
                var data = [
                    {name: "First Charter", guid: _.uniqueId()},
                    {name: "Second Charter", guid: _.uniqueId()},
                    {name: "Third Charter", guid: _.uniqueId()}
                ];
                successcb({items:data});
            },
            /**
             * ВИ_ЖКХДОГ_3. Добавление управляемого объекта
             * @param {Object} object - управляемого объекта
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            addManagedObject: function (object, successcb, errorcb) {
                successcb();
            },
            /**
             * ВИ_ЖКХДОГ_6. Изменение услуг
             * @param {Object} services - услугa
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            editServices: function (services, successcb, errorcb) {
                successcb();
            },
            /**
             * ВИ_ЖКХДОГ_19. Удаление управляемого объекта.
             * @param {Object} address - адрес управляемого объекта
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            deleteManagedObject: function (address, successcb, errorcb) {
                successcb();
            }
        };
    }])
;