angular
    .module('ru.lanit.hcs.agreements.rest.AdditionalAgreementService', [
        'ru.lanit.hcs.agreements.rest.AbstractAgreementService',
        'agreements-resource'
    ])
    .factory('$AdditionalAgreementService', [
        'AbstractAgreementService',
        'AgreementResource',
        function (AbstractAgreementService, AgreementResource) {
            var service = {};
            AbstractAgreementService.mixinAbstractSimpleDocumentInterface(service, '/additionals');
            AbstractAgreementService.mixinAbstractManagedObjectInterface(service, '/additionals');


            function toUrl(part) {
                return '/agreements/additionals' + part;
            }

            var searchResource = new AgreementResource(toUrl('/search'));
            var removeResource = new AgreementResource(toUrl('/removal'));

            /**
             * Поиск документов по критериям
             * Метод используется для:
             *  - 5.4    ВИ_ЖКХДОГ_4. Добавление дополнительного соглашения.
             * @param criteria - критерий поиска
             * @return список документов
             */
            service.findByCriteria = function (criteria, successcb, errorcb) {
                return searchResource.queryPost({}, criteria, successcb, errorcb);
            };

            //для v4.0.1
            service.getList = function(scope, guid, errorcb){
                scope.baseAgreementReasons = [];
                scope.baseAgreementReasons.push({
                    name: 'Договор управления',
                    guid: guid
                });

                service.findByCriteria({parentContractGuid: guid}).then(function (result) {
                    if (result) {
                        for (var i = 0; i < result.length; i++) {
                            scope.baseAgreementReasons.push({
                                name: 'Доп. соглашение №' + result[i].number + ' от ' + result[i].effectiveDate,
                                guid: result[i].guid
                            });
                        }
                    }

                }, errorcb);
            };

            //для v4.1.0 (то же, что и service.getList)
            service.getAgreementReasonsList = function(scope, guid, versionGuid, successcb, errorcb){
                scope.baseAgreementReasons = [];
                scope.baseAgreementReasons.push({
                    name: 'Договор управления',
                    guid: guid
                });

                service.findByCriteria({parentAgreementVersionGuid: versionGuid}).then(function (result) {
                    if (result) {
                        for (var i = 0; i < result.length; i++) {
                            scope.baseAgreementReasons.push({
                                name: 'Доп. соглашение №' + result[i].number + ' от ' + result[i].signDate,
                                guid: result[i].guid
                            });
                        }
                        if(successcb){
                            successcb(result);
                        }
                    }

                }, errorcb);
            };

            service.removeAdditionalService = function (data, successcb, errorcb) {
                return removeResource.removeObject(data, successcb, errorcb);
            };

            return service;
        }
    ]);