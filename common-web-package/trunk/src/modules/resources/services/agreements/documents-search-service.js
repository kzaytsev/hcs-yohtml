angular
    .module('ru.lanit.hcs.agreements.rest.DocumentsSearchService', [
        'agreements-resource'
    ])
    .factory('$DocumentsSearchService', [
        'AgreementResource',
        function (AgreementResource) {

            function toUrl(part) {
                return '/agreements/documents' + part;
            }

            var searchResource = new AgreementResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));

            return {
                /**
                 * Поиск документов по критериям
                 * Метод используется для:
                 *  - 5.1    ВИ_ЖКХДОГ_1. Поиск  договоров управления/уставов.
                 * @param searchCriteria - критерий поиска
                 * @return список документов
                 */
                findByCriteria: function (searchCriteria, page, itemsPerPage, successcb, errorcb) {
                    return searchResource.queryPost({
                        page: page,
                        itemsPerPage: itemsPerPage
                    }, searchCriteria, successcb, errorcb);
                }
            };
        }
    ]);