angular
    .module('ru.lanit.hcs.agreements.rest.ManagedObjectsService', [
        'agreements-resource'
    ])
    .factory('$ManagedObjectsService', [
        'AgreementResource',
        function (AgreementResource) {

            function toUrl(part) {
                return '/agreements/objects' + part;
            }

            var managedObjectsResource = new AgreementResource(toUrl(""));
            var searchResource = new AgreementResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
            var managedObjectsRemovalResource = new AgreementResource(toUrl('/removal'));
            var managedObjectExcludeResource = new AgreementResource(toUrl('/:guid/exclusion'));

            return {
                /**
                 * Поиск документов по критериям
                 * Метод используется для:
                 *  - 5.1    ВИ_ЖКХДОГ_1. Поиск  договоров управления/уставов.
                 * @param searchCriteria - критерий поиска
                 * @return список документов
                 */
                findByCriteria: function (searchCriteria, page, itemsPerPage, successcb, errorcb) {
                    return searchResource.queryPost({
                        page: page,
                        itemsPerPage: itemsPerPage
                    }, searchCriteria, successcb, errorcb);
                },
                /**
                 *  Удаление управляемого объекта.
                 *  Используется для:
                 *   - 5.19    ВИ_ЖКХДОГ_19. Удаление управляемого объекта.
                 *  @param request - guid'ы объектов
                 *  @param successcb -
                 *  @param errorcb -
                 *  @throws FieldValidationException, NotFoundException
                 */
                deleteManagedObject: function (request, successcb, errorcb) {
                    return managedObjectsRemovalResource.update(request, successcb, errorcb);
                },

                /**
                 *  Добавление управляемого объекта.
                 *  Используется для:
                 *   - 5.3    ВИ_ЖКХДОГ_3. Добавление управляемого объекта.
                 *  @param request -
                 *  @param successcb -
                 *  @param errorcb -
                 *  @throws FieldValidationException, NotFoundException
                 */
                create: function (request, successcb, errorcb) {
                    return managedObjectsResource.create(request, successcb, errorcb);
                },

                /**
                 *  Изменение услуг.
                 *  Используется для:
                 *   - 5.6    ВИ_ЖКХДОГ_6. Изменение услуг.
                 *  @param request -
                 *  @param successcb -
                 *  @param errorcb -
                 *  @throws FieldValidationException, NotFoundException
                 */
                update: function (request, successcb, errorcb) {
                    return managedObjectsResource.update(request, successcb, errorcb);
                },

                exclude: function (guid, request, successcb, errorcb) {
                    return managedObjectExcludeResource.update(request, successcb, errorcb, {guid: guid});
                }
            };

        }
    ])
;