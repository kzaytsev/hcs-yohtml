angular
    .module('ru.lanit.hcs.agreements.rest.RegulationService', [
        'ru.lanit.hcs.agreements.rest.AbstractAgreementService'
    ])
    .factory('RegulationService', [
        'AbstractAgreementService',
        function (AbstractAgreementService) {
            var service = {};
            AbstractAgreementService.mixinAbstractSimpleDocumentInterface(service, '/regulations');
            AbstractAgreementService.mixinAbstractManagedObjectInterface(service, '/regulations');

            return service;
        }
    ]);