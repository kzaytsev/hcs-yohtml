angular
    .module('ru.lanit.hcs.agreements.rest.AbstractAgreementService', [
        'agreements-resource'
    ])
    .factory('AbstractAgreementService', [
        'AgreementResource',
        function (AgreementResource) {

            function toUrl(restUrl, part) {
                return '/agreements' + restUrl + part;
            }

            return {

                mixinAbstractManagedObjectInterface: function (service, restUrl) {

                    var managedObjectsGetResource = new AgreementResource(toUrl(restUrl, '/managed/objects/:docGuid'));
                    var managedObjectsResource = new AgreementResource(toUrl(restUrl, '/managed/objects'));
                    var managedObjectsRemovalResource = new AgreementResource(toUrl(restUrl, '/managed/objects/removal'));

                    /**
                     * Список управляемых объектов для докумета
                     * @throws NotFoundException
                     */
                    service.getManagedObjects = function (documentGuid, successcb, errorcb) {
                        return managedObjectsGetResource.get({docGuid: documentGuid}, successcb, errorcb);
                    };

                    /**
                     *  Добавление управляемого объекта.
                     *  Используется для:
                     *   - 5.3    ВИ_ЖКХДОГ_3. Добавление управляемого объекта.
                     *  @param request -
                     *  @param successcb -
                     *  @param errorcb -
                     *  @throws FieldValidationException, NotFoundException
                     */
                    service.addManagedObject = function (request, successcb, errorcb) {
                        return managedObjectsResource.create(request, successcb, errorcb);
                    };

                    /**
                     *  Изменение услуг.
                     *  Используется для:
                     *   - 5.6    ВИ_ЖКХДОГ_6. Изменение услуг.
                     *  @param request -
                     *  @param successcb -
                     *  @param errorcb -
                     *  @throws FieldValidationException, NotFoundException
                     */
                    service.updateManagedObject = function (request, successcb, errorcb) {
                        return managedObjectsResource.update(request, successcb, errorcb);
                    };

                    /**
                     *  Удаление управляемого объекта.
                     *  Используется для:
                     *   - 5.19    ВИ_ЖКХДОГ_19. Удаление управляемого объекта.
                     *  @param request - guid'ы объектов
                     *  @param successcb -
                     *  @param errorcb -
                     *  @throws FieldValidationException, NotFoundException
                     */
                    service.deleteManagedObject = function (request, successcb, errorcb) {
                        return managedObjectsRemovalResource.update(request, successcb, errorcb);
                    };

                    return service;
                },
                mixinAbstractSimpleDocumentInterface: function (service, restUrl) {

                    var agreementGetResource = new AgreementResource(toUrl(restUrl, '/:guid'));
                    var agreementActionResource = new AgreementResource(toUrl(restUrl, '/'));
                    var agreementRemoveResource = new AgreementResource(toUrl(restUrl, '/:guid/removal'));
                    var terminationResource = new AgreementResource(toUrl(restUrl, '/termination'));
                    var approvalResource = new AgreementResource(toUrl(restUrl, '/:guid/approval'));
                    var prolongationResource = new AgreementResource(toUrl(restUrl, '/:guid/prolongation'));
                    var versionsResource = new AgreementResource(toUrl(restUrl, '/:documentGuid/versions'));
                    var baseListResource = new AgreementResource(toUrl(restUrl, '/:documentGuid/services/files'));
                    var versionResource = new AgreementResource(toUrl(restUrl, '/versions/:versionGuid'));
                    var restoreResource = new AgreementResource(toUrl(restUrl, '/restoration'));
                    var updateOrganizationInfoResource = new AgreementResource(toUrl(restUrl, '/:documentGuid/organizations'));

                    /**
                     *  Получить документ по guid
                     *  Метод используется для
                     *   - 5.5    ВИ_ЖКХДОГ_5. Изменение версии договора управления.
                     *   - 5.7    ВИ_ЖКХДОГ_7. Изменение версии дополнительного соглашения.
                     *   - 5.11    ВИ_ЖКХДОГ_11. Добавление новой версии существующего устава.
                     *   - 5.12    ВИ_ЖКХДОГ_12. Изменение версии устава.
                     *
                     *   - 5.17    ВИ_ЖКХДОГ_17. Просмотр договора управления.
                     *   - 5.18    ВИ_ЖКХДОГ_18. Просмотр устава.
                     *   - 5.20    ВИ_ЖКХДОГ_20. Просмотр дополнительного соглашения.
                     *
                     *  @param guid - guid документа (required).
                     *  @return документ
                     *  @throws NotFoundException
                     */
                    service.get = function (guid, successcb, errorcb) {
                        return agreementGetResource.get({guid: guid}, successcb, errorcb);
                    };

                    /**
                     *  Добавить документ
                     *  Используется для:
                     *   - 5.2    ВИ_ЖКХДОГ_2. Добавление договора управления.
                     *   - 5.4    ВИ_ЖКХДОГ_4. Добавление дополнительного соглашения.
                     *   - 5.10    ВИ_ЖКХДОГ_10. Добавление нового устава.
                     *   - 5.11    ВИ_ЖКХДОГ_11. Добавление новой версии существующего устава.
                     *  @param request - тело документа
                     *  @return guid индекса
                     *  @ throws FieldValidationException, SuchNumberAlreadyExistsException
                     */
                    service.add = function (request, successcb, errorcb) {
                        return agreementActionResource.create(request, successcb, errorcb);
                    };

                    /**
                     *  Редактировать документ
                     *  Используется для:
                     *   - 5.5    ВИ_ЖКХДОГ_5. Изменение версии договора управления.
                     *   - 5.7    ВИ_ЖКХДОГ_7. Изменение версии дополнительного соглашения.
                     *   - 5.12    ВИ_ЖКХДОГ_12. Изменение версии устава.
                     *  @param request - тело документа
                     *  @throws FieldValidationException, SuchNumberAlreadyExistsException
                     */
                    service.update = function (request, successcb, errorcb) {
                        return agreementActionResource.update(request, successcb, errorcb);
                    };

                    /**
                     *  Удалить документ
                     *  Используется для:
                     *   - 5.8    ВИ_ЖКХДОГ_8. Удаление версии договора управления.
                     *   - 5.9    ВИ_ЖКХДОГ_9. Удаление версии дополнительного соглашения.
                     *   - 5.13    ВИ_ЖКХДОГ_13. Удаление версии устава.
                     * @param request - guid документа (required)
                     * @throws NotFoundException
                     *
                     * название изменено на remove,
                     * потому что delete зарезервированное слово в js
                     */
                    service.remove = function (guid, successcb, errorcb) {
                        return agreementRemoveResource.update({}, successcb, errorcb, {guid: guid});
                    };

                    /**
                     *  Расторжение договора, устава, дополнительных соглашений
                     *  Используется для:
                     *   - 5.14    ВИ_ЖКХДОГ_14. Расторжение договора, устава.
                     * @param request - guid'ы документов (required)
                     * @throws NotFoundException, FieldValidationException
                     */
                    service.terminate = function (request, successcb, errorcb) {
                        return terminationResource.update(request, successcb, errorcb);
                    };

                    /**
                     *  Утверждение версии договора/ устава/ дополнительного соглашения.
                     *  Используется для:
                     *   - 5.15    ВИ_ЖКХДОГ_15. Утверждение версии договора/
                     *             устава/ дополнительного соглашения.
                     * @param request - guid документа (required)
                     * @throws NotFoundException, FieldValidationException
                     */
                    service.approve = function (guid, successcb, errorcb) {
                        return approvalResource.update({}, successcb, errorcb, {guid: guid});
                    };


                    /**
                     *  Пролонгация версии договора/ устава..
                     *  Используется для:
                     *  4.16	ВИ_ЖКХДОГ_16. Пролонгация версии договора/устава.
                     * @param request - guid документа (required)
                     * @throws NotFoundException
                     */
                    service.prolong = function (guid, successcb, errorcb) {
                        return prolongationResource.update({}, successcb, errorcb, {guid: guid});
                    };
                    /**
                     *  Получить все версии документа по его guid
                     *  @param guid - guid документа (required).
                     *  @return документ
                     */
                    service.getVersions = function (guid, successcb, errorcb) {
                        return versionsResource.get({documentGuid: guid}, successcb, errorcb);
                    };

                    /**
                     *  Получить версию документа по его guid
                     *  @param guid - guid документа (required).
                     *  @return документ
                     */
                    service.getVersion = function (guid, successcb, errorcb) {
                        return versionResource.get({versionGuid: guid}, successcb, errorcb);
                    };

                    /**
                     * Восстановить версию документа
                     * @param request – {documentGuid: 'guid документа', all: '0 - все значения, 1 - только отклоненные или на утверждении' }
                     * @param successcb
                     * @param errorcb
                     * @returns {*}
                     */
                    service.restoreVersion = function(request, successcb, errorcb){
                        return restoreResource.update(request, successcb, errorcb);
                    };

                    /**
                     *  Получить список файлов-оснований
                     *
                     *  @param guid - guid документа (required).
                     *  @return документ
                     */
                    service.getBaseList = function (guid, successcb, errorcb) {
                        return baseListResource.get({documentGuid: guid}, successcb, errorcb);
                    };

                    /**
                     *  Обновление данных об организации
                     *
                     *  @param documentGuid - guid документа.
                     */
                    service.updateOrganizationInfo = function (documentGuid, successcb, errorcb) {
                        return updateOrganizationInfoResource.update(null, successcb, errorcb, {documentGuid: documentGuid});
                    };

                    return service;
                }
            };
        }
    ]);