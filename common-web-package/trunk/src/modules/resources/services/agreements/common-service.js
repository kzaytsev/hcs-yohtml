angular
    .module('ru.lanit.hcs.agreements.rest.CommonService', [
        'agreements-resource'
    ])
    .factory('$AgreementsCommonService', [
        'AgreementResource',
        function (AgreementResource) {

            function toUrl(part) {
                return '/agreements/common' + part;
            }

            var searchResource = new AgreementResource(toUrl('/search'));
            var meteringReportPeriodResource = new AgreementResource(toUrl('/metering/report/period'));

            return {
                /**
                 *  Метод поиска договоров/уставов(1.13	ЭФ_ЛС_ОРГ).
                 *  Метод используется для
                 *  @param searchCriteria -
                 *  @return информация о договоре
                 */
                findAgreements: function (searchCriteria, successcb, errorcb) {
                    return searchResource.queryPost({}, searchCriteria, successcb, errorcb);
                },

                /**
                 *  Метод получения параметров: период ввода показаний ПУ.
                 *  Метод вызывает:
                 *   - {@link ru.lanit.hcs.ppa.api.PpaService#findRegionsOfOrg(ru.lanit.hcs.ppa.api.dto.search.RegionsOfOrgSearchCriteria, int, int)}
                 *   - {@link ru.lanit.hcs.ppa.api.PpaService#findRegionalSettingsDetail(ru.lanit.hcs.ppa.api.dto.search.RegionalSettingsDetailSearchCriteria)}
                 *  @return информация о параметрах
                 */
                getMeteringReportPeriod: function (successcb, errorcb) {
                    return meteringReportPeriodResource.get({}, successcb, errorcb);
                }
            };
        }
    ]);