/**
 * Created by vkorzhov on 10/31/14.
 */
angular
    .module('ru.lanit.hcs.inspection.rest.InspectionService', [
        'inspection-resource'
    ])
    .factory('$InspectionService', ['$q', 'InspectionResource', function ($q, InspectionResource) {
        var self = this,
            toUrl = function (part) {
                return '/examinations' + part;
            };

        self.searchResource = new InspectionResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.publicSearchResource = new InspectionResource(toUrl('/public/search;page=:page;itemsPerPage=:itemsPerPage'));

        self.examinationResource = new InspectionResource(toUrl('/'));
        self.examinationDeleteResource = new InspectionResource(toUrl(''));
        self.examinationFindResource = new InspectionResource(toUrl('/:guid'));
        self.examinationPublishResource = new InspectionResource(toUrl('/publish/:guid'));
        self.examinationCancelResource = new InspectionResource(toUrl('/cancel'));

        return {
            /**
             * метод для поиска на ЭФ_РП из ЭФ_РП_БПРП
             */
            search: function (pathParams, searchParams, successcb, errorcb) {
                return self.searchResource.queryPost({
                    page: pathParams.pageIndex ? pathParams.pageIndex : 1,
                    itemsPerPage: pathParams.itemsPerPage
                }, searchParams, successcb, errorcb);
            },
            /**
             * метод для поиска в ОЧ
             */
            publicSearch: function (pathParams, searchParams, successcb, errorcb) {
                return self.publicSearchResource.queryPost({
                    page: pathParams.pageIndex ? pathParams.pageIndex : 1,
                    itemsPerPage: pathParams.itemsPerPage
                }, searchParams, successcb, errorcb);
            },
            /**
             * Получение карточки документа проверки
             * Метод используется для
             *  - 2.3.1.3    ВИ_РП_013: Просмотр реестровой записи о проверке (Карточки)
             *
             * @param {String} guid - guid документа (required).
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @return {Object} информация о проверкe ru.lanit.hcs.inspection.api.dto.ExaminationDescriptionDTO
             * @throws NotFoundException - документа не найден
             */
            getExamination: function (guid, successcb, errorcb) {
                return self.examinationFindResource.get({guid: guid}, successcb, errorcb);
            },
            /**
             * Создание нового документа
             * Используется для:
             *  - 2.3.2.1    ВИ_РП_021: Создание информации о проверке из РП
             *
             * @param {Object} examinationDescription информация о проверкe ru.lanit.hcs.inspection.api.dto.ExaminationDescriptionDTO
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @return {String} guid документа
             * @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             * @throws SuchNumberAlreadyExistsException - такой номер документа уже сущес
             */
            createExamination: function (examinationDescription, successcb, errorcb) {
                return self.examinationResource.create(examinationDescription, successcb, errorcb);
            },
            /**
             * Удаление документа
             * Используется для:
             *  - 2.3.2.4    ВИ_РП_025: Удаление проверки (версии проверки
             *
             * @param {String} guid - guid документа (required)
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws NotFoundException - документа не найден
             */
            deleteExamination: function (guid, successcb, errorcb) {
                return self.examinationDeleteResource.remove(guid, successcb, errorcb);
            },
            /**
             * Редактирование документа
             * Используется для:
             *  - 2.3.2.2    ВИ_РП_023: Редактирование записи о проверки
             *
             * @param {Object} examinationDescription информация о проверкe ru.lanit.hcs.inspection.api.dto.ExaminationDescriptionDTO
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             * @throws SuchNumberAlreadyExistsException - такой номер документа уже существует
             */
            updateExamination: function (examinationDescription, successcb, errorcb) {
                return self.examinationResource.update(examinationDescription, successcb, errorcb);
            },
            /**
             * Публикация документа
             *  Используется для:
             *   - 2.1.5    ВИ_ПИЖФ_07 Подписание и публикация электронного документа
             *
             * @param {String} guid - guid документа (required)
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws NotFoundException - документа не найден
             * @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             * @throws SuchNumberAlreadyExistsException - такой номер документа уже существует
             * @throws DocumentAlreadyPublished - документ уже опубликован
             */
            publishExamination: function (guid, successcb, errorcb) {
                return self.examinationPublishResource.queryPost({guid: guid}, {}, successcb, errorcb);
            },


            cancelExamination: function(dto, successcb, errorcb){
                successcb = successcb || angular.noop;
                errorcb = errorcb || angular.noop;
                return self.examinationCancelResource.queryPost({}, dto, successcb, errorcb);
            }
        };
    }])
    .factory('$InspectionOffenceService', ['InspectionResource', function (InspectionResource) {
        var self = this,
            toUrl = function (part) {
                return '/examinations/offences' + part;
            };

        self.offenceResource = new InspectionResource(toUrl('/'));
        self.offenceDeleteResource = new InspectionResource(toUrl(''));
        self.offenceSearchResource = new InspectionResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.offenceCancelResource = new InspectionResource(toUrl('/cancel'));

        return {
            /**
             * Поиск протоколов по guid проверки
             *
             * @param {String} examinationGuid - guid результата проверки
             * @param {Number} pageIndex - Номер страницы, для которой необходимо сформировать данные
             * @param {Number} elementsPerPage - Количество элементов на странице
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             * @return {Array} список протоколов ru.lanit.hcs.inspection.api.dto.ExaminationOffenceDTO
             */
            findOffencesByCriteria: function (examinationGuid, pageIndex, elementsPerPage, successcb, errorcb) {
                return self.offenceSearchResource.queryPost({
                    page: pageIndex ? pageIndex : 1,
                    itemsPerPage: elementsPerPage
                }, examinationGuid, successcb, errorcb);
            },
            /**
             * Удаление протокола
             *
             * @param {String} guid - протокола
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws NotFoundException - объект не найден
             */
            deleteOffence: function (guid, successcb, errorcb) {
                return self.offenceDeleteResource.remove(guid, successcb, errorcb);
            },

            /**
             *  Добавление протокола
             *  Используется для:
             *   - 2.3.2.6    ВИ_РП_027: Создание сведений о протоколе
             *
             *  @param {Object} offence - протокол ru.lanit.hcs.inspection.api.dto.ExaminationOffenceDTO
             *  @param {Function} successcb - Callback Deferred.then
             *  @param {Function} errorcb - Callback Deferred.then.fail
             *  @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             */
            addOffence: function (offence, successcb, errorcb) {
                self.offenceResource.create(offence, successcb, errorcb);
            },

            /**
             *  Редактирование протокола
             *  Используется для:
             *   - 2.3.2.6    ВИ_РП_027: Создание сведений о протоколе
             *
             *  @param {Object} offence - протокол ru.lanit.hcs.inspection.api.dto.ExaminationOffenceDTO
             *  @param {Function} successcb - Callback Deferred.then
             *  @param {Function} errorcb - Callback Deferred.then.fail
             *  @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             */
            updateOffence: function (offence, successcb, errorcb) {
                self.offenceResource.update(offence, successcb, errorcb);
            },
            /**
             * Отмена протокола
             * @param data
             * @param successcb
             * @param errorcb
             */
            cancelOffence: function (data, successcb, errorcb) {
                successcb = successcb || angular.noop;
                errorcb = errorcb || angular.noop;
                return self.offenceCancelResource.queryPost({}, data, successcb, errorcb);
            }
        };
    }])
    .factory('$InspectionPlaceService', ['InspectionResource', function (InspectionResource) {
        var self = this,
            toUrl = function (part) {
                return '/examinations/places' + part;
            };

        self.placeResource = new InspectionResource(toUrl('/'));
        self.placeDeleteResource = new InspectionResource(toUrl(''));
        self.placeSearchResource = new InspectionResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.placeMaxNumberResource = new InspectionResource(toUrl('/:guid'));

        return {
            /**
             *  Добавление нового дома
             *  Используется для:
             *   - 2.3.2.8    ВИ_РП_026: Создание сведений о доме
             *
             *  @param {Object} place - дом ru.lanit.hcs.inspection.api.dto.ExaminationPlaceDTO
             *  @param {Function} successcb - Callback Deferred.then
             *  @param {Function} errorcb - Callback Deferred.then.fail
             *  @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             */
            addPlace: function (place, successcb, errorcb) {
                return self.placeResource.create(place, successcb, errorcb);
            },
            /**
             *  Редактирование дома
             *  Используется для:
             *   - 2.3.2.8    ВИ_РП_026: Создание сведений о доме
             *
             *  @param {Object} place - дом ru.lanit.hcs.inspection.api.dto.ExaminationPlaceDTO
             *  @param {Function} successcb - Callback Deferred.then
             *  @param {Function} errorcb - Callback Deferred.then.fail
             *  @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             */
            updatePlace: function (place, successcb, errorcb) {
                return self.placeResource.update(place, successcb, errorcb);
            },
            /**
             * Удаление дома
             *
             * @param {String} guid - дома
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws NotFoundException - объект не найден
             */
            deletePlace: function (guid, successcb, errorcb) {
                return self.placeDeleteResource.remove(guid, successcb, errorcb);
            },
            /**
             * Поиск домов по guid проверки
             *
             * @param {String} examinationGuid - guid проверки
             * @param {Number} pageIndex - Номер страницы, для которой необходимо сформировать данные
             * @param {Number} elementsPerPage - Количество элементов на странице
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             * @return {Array} список домов ru.lanit.hcs.inspection.api.dto.ExaminationPlaceDTO
             */
            findPlaceByCriteria: function (examinationGuid, pageIndex, elementsPerPage, successcb, errorcb) {
                return self.placeSearchResource.queryPost({
                    page: pageIndex ? pageIndex : 1,
                    itemsPerPage: elementsPerPage
                }, examinationGuid, successcb, errorcb);
            },
            /**
             * Получить максимальный номер
             * места для проверки
             * Метод используется для:
             * ЭФ_РП_СП_СМ_СВДом.02
             *
             * @param {String} guid - guid проверки
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @return {Number} максимальный номер мероприятия
             */
            getMaxNumber: function (guid, successcb, errorcb) {
                return self.placeMaxNumberResource.get({guid: guid}, successcb, errorcb);
            }
        };
    }])
    .factory('$InspectionPreceptService', ['InspectionResource', function (InspectionResource) {
        var self = this,
            toUrl = function (part) {
                return '/examinations/precepts' + part;
            };

        self.preceptResource = new InspectionResource(toUrl('/'));
        self.preceptDeleteResource = new InspectionResource(toUrl(''));
        self.preceptSearchResource = new InspectionResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.preceptCancelResource = new InspectionResource(toUrl('/cancel'));

        return {
            /**
             * Добавление нового предписания
             * Используется для:
             *  - 2.3.2.5    ВИ_РП_026: Создание сведений о предписании
             *
             * @param {Object} precept - предписание ru.lanit.hcs.inspection.api.dto.ExaminationPreceptDTO
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             * @throws SuchNumberAlreadyExistsException - такой номер документа уже сущес
             */
            addPrecept: function (precept, successcb, errorcb) {
                return self.preceptResource.create(precept, successcb, errorcb);
            },
            /**
             * Редактирование предписания
             *
             * @param {Object} precept - предписание ru.lanit.hcs.inspection.api.dto.ExaminationPreceptDTO
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             * @throws SuchNumberAlreadyExistsException - такой номер документа уже сущес
             */
            updatePrecept: function (precept, successcb, errorcb) {
                return self.preceptResource.update(precept, successcb, errorcb);
            },
            /**
             * Удаление предписания
             *
             * @param {String} guid - предписания
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws NotFoundException - объект не найден
             */
            deletePrecept: function (guid, successcb, errorcb) {
                return self.preceptDeleteResource.remove(guid, successcb, errorcb);
            },
            /**
             * Отмена предписания
             *
             * @param {String} data - отмена
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws NotFoundException - объект не найден
             */
            cancelPrecept: function (data, successcb, errorcb) {
                successcb = successcb || angular.noop;
                errorcb = errorcb || angular.noop;
                return self.preceptCancelResource.queryPost({}, data, successcb, errorcb);
            },
            /**
             * Поиск предписаний по guid проверки
             *
             * @param {String} examinationGuid - guid проверки
             * @param {Number} pageIndex - Номер страницы, для которой необходимо сформировать данные
             * @param {Number} elementsPerPage - Количество элементов на странице
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             * @return {Array} список предписаний ru.lanit.hcs.inspection.api.dto.ExaminationPreceptDTO
             */
            findPreceptByCriteria: function (examinationGuid, pageIndex, elementsPerPage, successcb, errorcb) {
                return self.preceptSearchResource.queryPost({
                    page: pageIndex ? pageIndex : 1,
                    itemsPerPage: elementsPerPage
                }, examinationGuid, successcb, errorcb);
            }
        };
    }])
    .factory('$InspectionEventService', ['InspectionResource', function (InspectionResource) {
        var self = this,
            toUrl = function (part) {
                return '/examinations/events' + part;
            };

        self.eventSearchResource = new InspectionResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.eventResource = new InspectionResource(toUrl('/'));
        self.eventMaxNumberResource = new InspectionResource(toUrl('/:guid'));
        self.deleteEventResource = new InspectionResource(toUrl(''));

        return {
            /**
             * Добавление нового мероприятия
             * Используется для:
             *  - 2.3.2.7    ВИ_РП_026: Создание сведений о мероприятии
             *
             * @param {Object} event - мероприятие ru.lanit.hcs.inspection.api.dto.ExaminationEventDTO
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             */
            addEvent: function (event, successcb, errorcb) {
                return self.eventResource.create(event, successcb, errorcb);
            },
            /**
             * Редактирование мероприятия
             * Используется для:
             *  - 2.3.2.7    ВИ_РП_026: Создание сведений о мероприятии
             *
             * @param {Object} event - мероприятие ru.lanit.hcs.inspection.api.dto.ExaminationEventDTO
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             */
            updateEvent: function (event, successcb, errorcb) {
                return self.eventResource.update(event, successcb, errorcb);
            },
            /**
             * Удаление мероприятия
             *
             * @param {String} guid - мероприятия
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws NotFoundException - объект не найден
             */
            deleteEvent: function (guid, successcb, errorcb) {
                return self.deleteEventResource.remove(guid, successcb, errorcb);
            },
            /**
             * Поиск мероприятий по guid проверки
             *
             * @param {String} examinationGuid - guid проверки
             * @param {Number} pageIndex - Номер страницы, для которой необходимо сформировать данные
             * @param {Number} elementsPerPage - Количество элементов на странице
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             * @return {Array} список мероприятий ru.lanit.hcs.inspection.api.dto.ExaminationEventDTO
             */
            findEventByCriteria: function (examinationGuid, pageIndex, elementsPerPage, successcb, errorcb) {
                return self.eventSearchResource.queryPost({
                    page: pageIndex ? pageIndex : 1,
                    itemsPerPage: elementsPerPage
                }, examinationGuid, successcb, errorcb);
            },
            /**
             * Получить максимальный номер
             * мероприятия для проверки
             * Метод используется для:
             * ЭФ_РП_СП_СМ_СВМ.02
             *
             * @param {String} guid - guid проверки
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @return {Number} максимальный номер мероприятия
             */
            getMaxNumber: function (guid, successcb, errorcb) {
                return self.eventMaxNumberResource.get({guid: guid}, successcb, errorcb);
            }
        };
    }])
    .factory('$InspectionResultService', ['InspectionResource', function (InspectionResource) {
        var self = this,
            toUrl = function (part) {
                return '/examinations/results' + part;
            };

        self.examinationResultFindResource = new InspectionResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.examinationResultResource = new InspectionResource(toUrl('/'));
        return {
            /**
             *  Поиск результат проверки
             *  @param guid -- guid проверки
             *  @param successcb
             *  @param errorcb
             */
            getExaminationResult: function (guid, successcb, errorcb) {
                return self.examinationResultFindResource.queryPost({
                    page: 1,
                    itemsPerPage: 100
                }, guid, successcb, errorcb);
            },
            /**
             *  Создание результата проверки
             *  @param result -- результат проверки
             *  @param successcb
             *  @param errorcb
             */
            createExaminationResult: function (result, successcb, errorcb) {
                return self.examinationResultResource.create(result, successcb, errorcb);
            },
            /**
             *  Редактирование результата проверки
             *  @param result -- результат проверки
             *  @param successcb
             *  @param errorcb
             */
            updateExaminationResult: function (result, successcb, errorcb) {
                return self.examinationResultResource.update(result, successcb, errorcb);
            }
        };
    }])
;