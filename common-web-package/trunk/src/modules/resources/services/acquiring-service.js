angular
    .module('ru.lanit.hcs.bills.rest.AcquiringService', ['acquiring-resource'])
    .factory('$AcquiringService', ['AcquiringResource', function (AcquiringResource) {
        var self = this;

        self.resources = {};
        self.pathPrefix = '/acquiring';

        self.getResource = function (address) {
            var resource = null;
            if (self.resources[address]) {
                resource = self.resources[address];
            } else {
                resource = new AcquiringResource(self.pathPrefix + address);
                self.resources[address] = resource;
            }

            return resource;
        };

        return {
            findPaymentDocumentsWithRemainders: function (request, queryParams) {
                return self.getResource('/payment/documents;page=:page;itemsPerPage=:itemsPerPage')
                    .create(request, angular.noop, angular.noop, queryParams);
            }
        };
    }]);
