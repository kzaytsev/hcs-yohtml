angular.module('ru.lanit.hcs.ppa.rest.PpaService', [
    'ppa-resource'
])

    .factory('$PpaService', ['PpaResource', '$q', function (PpaResource, $q) {

        var self = this;

        var toUrl = function (part) {
            return '/ppa' + part;
        };

        self.managementCitizensResource = new PpaResource(toUrl('/citizens/'));
        self.citizenPermissionsResource = new PpaResource(toUrl('/citizens/permissions'));
        self.findOrganizationListResource = new PpaResource(toUrl('/organizations/search'));
        self.managementOrganizationResource = new PpaResource(toUrl('/organizations'));
        self.userPermissionsResource = new PpaResource(toUrl('/organizations/users/permissions'));
        self.organizationDetailsResource = new PpaResource(toUrl('/organizations/details'));
        self.organizationsForChoosingComponentResource = new PpaResource(toUrl('/organizations/chooser/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.managementUsersResource = new PpaResource(toUrl('/users'));
        self.findEmployeeResource = new PpaResource(toUrl('/employee/info'));
        self.findCitizenResource = new PpaResource(toUrl('/citizen/info'));
        self.omsTerritoryResource = new PpaResource(toUrl('/omsTerritory/'));
        self.omsTerritoryUpdateResource = new PpaResource(toUrl('/omsTerritory/update'));
        self.systemSettingsResource = new PpaResource(toUrl('/systemsettings'));
        self.esiaResource = new PpaResource(toUrl('/esia'));
        self.userPremises = new PpaResource(toUrl('/citizens/cabinet/premises'));
        self.regionalSettingsAllResource = new PpaResource(toUrl('/regionalsettings/all'));
        self.regionalSettingsDetailResource = new PpaResource(toUrl('/regionalsettings/:guid'));
        self.removalRegionalSettingsResource = new PpaResource(toUrl('/regionalsettings/removal/all'));
        self.removeRegionalSettingsResource = new PpaResource(toUrl('/regionalsettings/removal'));
        self.organizationRegionalSettingsAllResource = new PpaResource(toUrl('/organizations/regionalsettings/all'));
        self.organizationRegionalSettingsResource = new PpaResource(toUrl('/organizations/regionalsettings'));
        self.removalOrganizationRegionalSettingsResource = new PpaResource(toUrl('/organizations/regionalsettings/removal'));
        self.removalAllOrganizationRegionalSettingsResource = new PpaResource(toUrl('/organizations/regionalsettings/removal/all?organizationGuid=:organizationGuid'));
        self.organizationRolesResource = new PpaResource(toUrl('/organizations/roles'));
        self.unblockLinkBetweenCitizenAndAccountResource = new PpaResource(toUrl('/citizens/account/unblock'));
        self.cancelLinkBetweenCitizenAndAccountResource = new PpaResource(toUrl('/citizens/account/cancel'));
        self.cancelLinkBetwenOrganizationAndAccountResource = new PpaResource(toUrl('/organization/account/cancel'));
        self.lLinkBetwenOrganizationAndAccountResource = new PpaResource(toUrl('/organization/account/unblock'));
        self.blockLinkBetweenCitizenAndAccountResource = new PpaResource(toUrl('/citizens/account/block'));
        self.createLinkBetweenCitizenAndAccountResource = new PpaResource(toUrl('/citizens/account'));
        self.countLinkBetweenCitizenAndAccountResource = new PpaResource(toUrl('/citizens/account/count'));
        self.findConnectedHousesForCitizenResource = new PpaResource(toUrl('/citizens/houses'));
        self.findCitizenRefsResource = new PpaResource(toUrl('/citizens/chooser/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.findCitizenShortInfosResource = new PpaResource(toUrl('/citizens/info/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.findEmployeeShortInfoResource = new PpaResource(toUrl('/employees/chooser/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.findOrganizationRegionalSettingsForServicedHousesResource = new PpaResource(toUrl('/organizations/regionalsettings/all/for/serviced/houses'));
        self.findEmployeesWithPrivilegesResource = new PpaResource(toUrl('/employees/privileges/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.updateEmployeePrivilegesResource = new PpaResource(toUrl('/employees/privileges'));
        self.findPositionsInOrganizationResource = new PpaResource(toUrl('/organization/positions'));
        self.getCurrentUserResource = new PpaResource(toUrl('/current/user/'));
        self.paymentInfoSearchResource = new PpaResource(toUrl('/payments/info/search;page=:pageIndex;itemsPerPage=:elementsPerPage'));
        self.addPaymentInformationResource = new PpaResource(toUrl('/payments/info/add'));
        self.updatePaymentInformationResource = new PpaResource(toUrl('/payments/info'));
        self.deletePaymentInformationResource = new PpaResource(toUrl('/payments/info/close'));
        self.refreshOrganizationRolesResource = new PpaResource(toUrl('/organizations/roles/refresh'));
        self.findOrganizationRolesResource = new PpaResource(toUrl('/organizations/roles/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.adminRolesCreateResource = new PpaResource(toUrl('/roles/admin/request'));
        self.resendAdminOfGeneralMeetingRoleForApprovalResource = new PpaResource(toUrl('/roles/admin/approval'));
        self.blockOrDiscardAdminOfGeneralMeetingRoleResource = new PpaResource(toUrl('/roles/admin/block'));
        self.registerAdminOfGeneralMeetingOrganizationRoleResource = new PpaResource(toUrl('/organizations/role/admin'));
        self.registerAdminOfGeneralMeetingCitizenRoleResource = new PpaResource(toUrl('/citizens/role/admin'));
        self.considerAdminOfGeneralMeetingCitizenRoleApprovalResource = new PpaResource(toUrl('/citizens/role/admin/approval'));
        self.findAdminsOfGeneralMeetingResource = new PpaResource(toUrl('/citizens/admin/search'));
        self.unblockAdminOfGeneralMeetingRoleResource = new PpaResource(toUrl('/roles/admin/unblock'));
        self.organizationRolesOktmosResource = new PpaResource(toUrl('/organizations/roles/oktmos'));
        self.organizationRolesRegionResource = new PpaResource(toUrl('/organizations/roles/region'));
        self.organizationsRoleListSearch = new PpaResource(toUrl('/organizations/rolelist/search'));
        self.localGovAuthoritiesOfExecutiveAuthorityInChargeOfGovResidentialControlResource = new PpaResource(toUrl('/executiveauthorityinchargeofgovresidentialcontrol/localgovauthorities'));
        self.findRegionalSettingsOfRegionalGovAuthorityResource= new PpaResource(toUrl('/regionalsettings/of/gov/auth'));
        self.getDefaultSystemSettingsResource= new PpaResource(toUrl('/systemsettings/default'));
        self.connectedHousesInRegistryResource= new PpaResource(toUrl('/houses/connected/registry/search'));
        self.findAdminOfGeneralMeetingInfoResource= new PpaResource(toUrl('/admin/search'));
        self.findUserDefaultPremiseWithAlertsResource= new PpaResource(toUrl('/citizens/cabinet/info'));
        self.findOrganizationAdminOfGeneralMeetingInApartmentHouseInfoResource = new PpaResource(toUrl('/admininfo/organization/:guid'));
        self.findCitizenAdminOfGeneralMeetingInApartmentHouseInfoResource = new PpaResource(toUrl('/admininfo/citizen/:guid'));
        self.findLinkBetweenOrganizationAndAccountResource = new PpaResource(toUrl('/organization/account'));
        self.blockOrganizationResource = new PpaResource(toUrl('/organizations/block'));
        self.unblockOrganizationResource = new PpaResource(toUrl('/organizations/unblock'));
        self.blockCitizenResource = new PpaResource(toUrl('/citizens/block'));
        self.unblockCitizenResource = new PpaResource(toUrl('/citizens/unblock'));
        self.blockEmployeeResource = new PpaResource(toUrl('/employees/block'));
        self.unblockEmployeeResource = new PpaResource(toUrl('/employees/unblock'));
        self.findAccountForCitizenResource = new PpaResource(toUrl('/citizen/account/search'));
        self.findAccountsForUserResource = new PpaResource(toUrl('/accounts/for/user'));
        self.invalidateSession = new PpaResource(toUrl('/current/user/logout'));
        self.findUserSettingsResource = new PpaResource(toUrl('/user/settings'));
        self.userSettingsResource = new PpaResource(toUrl('/user/settings'));
        self.organizationAccountRefreshResource = new PpaResource(toUrl('/organization/account/refresh'));

        return {
            findConnectedHousesInRegistry: function (searchCriteria, successCallback, errorCallback) {
                return self.connectedHousesInRegistryResource.queryPost(null, searchCriteria, successCallback, errorCallback);
            },
            findLocalGovAuthoritiesOfExecutiveAuthorityInChargeOfGovResidentialControl: function (queryJson, successcb, errorcb) {
                return self.localGovAuthoritiesOfExecutiveAuthorityInChargeOfGovResidentialControlResource.query(null, queryJson, successcb, errorcb);
            },
            findOrganizationList: function (queryJson, successcb, errorcb) {
                return self.findOrganizationListResource.query(null, queryJson, successcb, errorcb);
            },
            findOrganizationsForChooserComponent: function (pathParams, searchCriteria, successCallback, errorCallback) {
                return self.organizationsForChoosingComponentResource.queryPost(pathParams, searchCriteria, successCallback, errorCallback);
            },
            registerOrgAndUser: function (organizationAndUserRegistrationRequest, successcb, errorcb) {
                return self.managementOrganizationResource.create(organizationAndUserRegistrationRequest, successcb, errorcb);
            },
            registerUser: function (employeeRegistrationRequest, successcb, errorcb) {
                return self.managementUsersResource.create(employeeRegistrationRequest, successcb, errorcb);
            },
            updateUserOrg: function (employeeUpdateRequest, successcb, errorcb) {
                return self.managementUsersResource.update(employeeUpdateRequest, successcb, errorcb);
            },
            findUserSummary: function (successcb, errorcb) {
                return self.managementCitizensResource.get(null, successcb, errorcb);
            },
            updateCitizen: function (citizenUpdateRequest, successcb, errorcb) {
                return self.managementCitizensResource.update(citizenUpdateRequest, successcb, errorcb);
            },
            findOrganizationDetails: function (successcb, errorcb) {
                return self.organizationDetailsResource.get(null, successcb, errorcb);
            },
            findOrganizationDetailsByGuid: function($guid, successcb, errorcb){
                return self.organizationDetailsResource.query(null, {guid: $guid}, successcb || angular.noop, errorcb || angular.noop);
            },
            findOmsTerritories: function (omsTerritorySearchCriteria, successcb, errorcb) {
                return self.omsTerritoryResource.get(omsTerritorySearchCriteria, successcb, errorcb);
            },
            updateOmsTerritories: function (updateOmsTerritoryRequest, successcb, errorcb) {
                return self.omsTerritoryUpdateResource.create(updateOmsTerritoryRequest, successcb, errorcb);
            },
            updateOrganization: function (organizationUpdateRequest, successcb, errorcd) {
                return self.managementOrganizationResource.update(organizationUpdateRequest, successcb, errorcd);
            },
            findEmployee: function (successcb, errorcb) {
                return self.findEmployeeResource.get(null, successcb, errorcb);
            },
            findCitizen: function (successcb, errorcb) {
                return self.findCitizenResource.get(null, successcb, errorcb);
            },
            registerCitizen: function (сitizenRegistrationWithLinkAccountRequest, successcb, errorcb) {
                return self.managementCitizensResource.create(сitizenRegistrationWithLinkAccountRequest, successcb, errorcb);
            },
            findOrgUserPermissions: function (successcb, errorcb) {
                return self.userPermissionsResource.get(null, successcb, errorcb);
            },
            findCitizenPermissions: function (successcb, errorcb) {
                return self.citizenPermissionsResource.get(null, successcb, errorcb);
            },
            getLasSyncEventDate: function (organizationGuid, successcb, errorcb) {
                return self.managementOrganizationResource.queryObject('/:organizationGuid/last/sync/date', {organizationGuid: organizationGuid}, successcb, errorcb);
            },
            getEsiaData: function (successcb, errorcb) {
                return self.esiaResource.get({}, successcb, errorcb);
            },
            getSystemSettings: function (successcb, errorcb) {
                return self.systemSettingsResource.get({}, successcb, errorcb);
            },
            updateSystemSettings: function (systemSettings, successcb, errorcb) {
                return self.systemSettingsResource.create(systemSettings, successcb, errorcb);
            },
            findLinkBetweenCitizenAndAccount: function (queryJson, successcb, errorcb) {
                return self.createLinkBetweenCitizenAndAccountResource.queryObject(angular.noop, queryJson, successcb, errorcb);
            },
            unblockLinkBetweenCitizenAndAccount: function (linkBetweenCitizenAndAccountGuid, successcb, errorcb) {
                return self.unblockLinkBetweenCitizenAndAccountResource.update(linkBetweenCitizenAndAccountGuid, successcb, errorcb);
            },
            cancelLinkBetweenCitizenAndAccount: function (linkBetweenCitizenAndAccountGuid, successcb, errorcb) {
                return self.cancelLinkBetweenCitizenAndAccountResource.update(linkBetweenCitizenAndAccountGuid, successcb, errorcb);
            },
            cancelLinkBetwenOrganizationAndAccount: function (LinkBetweenOrganizationAndAccountCancelRequest, successcb, errorcb) {
                return self.cancelLinkBetwenOrganizationAndAccountResource.create(LinkBetweenOrganizationAndAccountCancelRequest, successcb, errorcb);
            },
            lLinkBetwenOrganizationAndAccount: function (LinkBetweenOrganizationAndAccountUnblockRequest, successcb, errorcb) {
                return self.lLinkBetwenOrganizationAndAccountResource.create(LinkBetweenOrganizationAndAccountUnblockRequest, successcb, errorcb);
            },
            blockLinkBetweenCitizenAndAccount: function (linkBetweenCitizenAndAccountGuid, successcb, errorcb) {
                return self.blockLinkBetweenCitizenAndAccountResource.update(linkBetweenCitizenAndAccountGuid, successcb, errorcb);
            },
            findRegionalSettingsList: function (queryJson, successcb, errorcb) {
                return self.regionalSettingsAllResource.query(null, queryJson, successcb, errorcb);
            },
            updateRegionalSettingsAll: function (queryJson, successcb, errorcb) {
                return self.regionalSettingsAllResource.update(queryJson, successcb, errorcb);
            },
            removeAllRegionalSettings: function (successcb, errorcb) {
                return self.removalRegionalSettingsResource.update(null, successcb, errorcb);
            },
            removeRegionalSettings: function (regionalSettings, successcb, errorcb) {
                return self.removeRegionalSettingsResource.update(regionalSettings, successcb, errorcb);
            },
            findRegionalSettingsDetail: function (guid, successcb, errorcb) {
                return self.regionalSettingsDetailResource.queryObject({'guid': guid}, null, successcb, errorcb);
            },
            createLinkBetweenCitizenAndAccount: function (account, successcb, errorcb) {
                return self.createLinkBetweenCitizenAndAccountResource.create(account, successcb, errorcb);
            },
            findOrganizationRegionalSettings: function (queryJson, successcb, errorcb) {
                return self.organizationRegionalSettingsAllResource.query(angular.noop, queryJson, successcb, errorcb);
            },
            findOrganizationRegionalSettingsForServicedHouses: function (queryJson, successcb, errorcb) {
                return self.findOrganizationRegionalSettingsForServicedHousesResource.query(angular.noop, queryJson, successcb, errorcb);
            },
            countLinkBetweenCitizenAndAccount: function (queryJson, successcb, errorcb) {
                return self.countLinkBetweenCitizenAndAccountResource.queryObject(angular.noop, queryJson, successcb, errorcb);
            },
            findConnectedHousesForCitizen: function (successcb, errorcb) {
                return self.findConnectedHousesForCitizenResource.queryObject(angular.noop, {}, successcb, errorcb);
            },
            findCitizenRefs: function (pathParams, searchCriteria, successCallback, errorCallback) {
                return self.findCitizenRefsResource.queryPost(pathParams, searchCriteria, successCallback, errorCallback);
            },
            findCitizenShortInfos: function (pathParams, searchCriteria, successCallback, errorCallback) {
                return self.findCitizenShortInfosResource.queryPost(pathParams, searchCriteria, successCallback, errorCallback);
            },
            findEmployeeShortInfo: function (pathParams, searchCriteria, successCallback, errorCallback) {
                return self.findEmployeeShortInfoResource.queryPost(pathParams, searchCriteria, successCallback, errorCallback);
            },
            findOrganizationRegionalSettingsDetail: function (queryJson, successcb, errorcb) {
                return self.organizationRegionalSettingsResource.queryObject(null, queryJson, successcb, errorcb);
            },
            updateOrganizationRegionSettings: function (regionalSettings, successcb, errorcb) {
                return self.organizationRegionalSettingsResource.update(regionalSettings, successcb, errorcb);
            },
            updateOrganizationRegionSettingsAll: function (regionalSettingsList, successcb, errorcb) {
                return self.organizationRegionalSettingsAllResource.update(regionalSettingsList, successcb, errorcb);
            },
            removeOrganizationRegionSettings: function (regionalSettingsList, successcb, errorcb) {
                return self.removalOrganizationRegionalSettingsResource.update(regionalSettingsList, successcb, errorcb);
            },
            removeAllOrganizationRegionSettings: function (organizationGuid, successcb, errorcb) {
                return self.removalAllOrganizationRegionalSettingsResource.update(undefined, successcb, errorcb, {organizationGuid: organizationGuid});
            },
            findEmployeesWithPrivileges: function (pathParams, searchCriteria, successCallback, errorCallbac) {
                return self.findEmployeesWithPrivilegesResource.queryPost(pathParams, searchCriteria, successCallback, errorCallbac);
            },
            updateEmployeePrivileges: function (updateEmployeePrivilegesRequest, successcb, errorcb) {
                return self.updateEmployeePrivilegesResource.update(updateEmployeePrivilegesRequest, successcb, errorcb);
            },
            findPositionsInOrganization: function (successcb, errorcb) {
                return self.findPositionsInOrganizationResource.get(null, successcb, errorcb);
            },
            getCurrentUser: function (successcb, errorcb) {
                return self.getCurrentUserResource.get(null, successcb, errorcb);
            },
            findRegionalSettingsOfRegionalGovAuthority: function (successcb, errorcb) {
                return self.findRegionalSettingsOfRegionalGovAuthorityResource.get(null, successcb, errorcb);
            },
            getDefaultSystemSettings: function (successcb, errorcb) {
                return self.getDefaultSystemSettingsResource.get(null, successcb, errorcb);
            },
            refreshOrganizationRoles: function (refreshOrganizationRolesRequest) {
                return self.refreshOrganizationRolesResource.update(refreshOrganizationRolesRequest);
            },
            findOrganizationRoles: function (pathParams, searchCriteria, successCallback, errorCallbac) {
                return self.findOrganizationRolesResource.queryPost(pathParams, searchCriteria, successCallback, errorCallbac);
            },
            findPaymentInformation: function (paymentInformationSearchCriteria, pageIndex, elementsPerPage) {
                var pathParams = {
                    pageIndex: pageIndex,
                    elementsPerPage: elementsPerPage
                };
                return self.paymentInfoSearchResource.queryPost(pathParams, paymentInformationSearchCriteria);
            },
            addPaymentInformation: function (addPaymentInformationRequest, successcb, errorcb) {
                return self.addPaymentInformationResource.create(addPaymentInformationRequest, successcb, errorcb);
            },
            updatePaymentInformation: function (updatePaymentInformationRequest, successcb, errorcb) {
                return self.updatePaymentInformationResource.update(updatePaymentInformationRequest, successcb, errorcb);
            },
            deletePaymentInformation: function (deletePaymentInformationRequest, successcb, errorcb) {
                return self.deletePaymentInformationResource.update(deletePaymentInformationRequest, successcb, errorcb);
            },
            createRequestForAdminOfGeneralMeetingRole: function (createRequestForAdminOfGeneralMeetingRoleRequest) {
                return self.adminRolesCreateResource.create(createRequestForAdminOfGeneralMeetingRoleRequest);
            },
            resendAdminOfGeneralMeetingRoleForApproval: function (resendAdminOfGeneralMeetingRoleForApprovalRequest, successcb, errorcb) {
                return self.resendAdminOfGeneralMeetingRoleForApprovalResource.create(resendAdminOfGeneralMeetingRoleForApprovalRequest, successcb, errorcb);
            },
            blockOrDiscardAdminOfGeneralMeetingRole: function (blockOrDiscardAdminOfGeneralMeetingRoleRequest, successcb, errorcb) {
                return self.blockOrDiscardAdminOfGeneralMeetingRoleResource.update(blockOrDiscardAdminOfGeneralMeetingRoleRequest, successcb, errorcb);
            },
            unblockAdminOfGeneralMeetingRole: function (unblockAdminOfGeneralMeetingRoleRequest, successcb, errorcb) {
                return self.unblockAdminOfGeneralMeetingRoleResource.update(unblockAdminOfGeneralMeetingRoleRequest, successcb, errorcb);
            },
            registerAdminOfGeneralMeetingOrganizationRole: function (registerAdminOfGeneralMeetingOrganizationRoleRequest, successcb, errorcb) {
                return self.registerAdminOfGeneralMeetingOrganizationRoleResource.create(registerAdminOfGeneralMeetingOrganizationRoleRequest, successcb, errorcb);
            },
            registerAdminOfGeneralMeetingCitizenRole: function (registerAdminOfGeneralMeetingCitizenRoleRequest, successcb, errorcb) {
                return self.registerAdminOfGeneralMeetingCitizenRoleResource.create(registerAdminOfGeneralMeetingCitizenRoleRequest, successcb, errorcb);
            },
            updateAdminOfGeneralMeetingInApartmentHouseCitizenInfo: function (updateAdminOfGeneralMeetingInApartmentHouseCitizenInfoRequest, successcb, errorcb) {
                return self.registerAdminOfGeneralMeetingCitizenRoleResource.update(updateAdminOfGeneralMeetingInApartmentHouseCitizenInfoRequest, successcb, errorcb);
            },
            considerAdminOfGeneralMeetingCitizenRoleApproval: function (considerAdminOfGeneralMeetingCitizenRoleApprovalRequest, successcb, errorcb) {
                return self.considerAdminOfGeneralMeetingCitizenRoleApprovalResource.create(considerAdminOfGeneralMeetingCitizenRoleApprovalRequest, successcb, errorcb);
            },
            findAdminsOfGeneralMeeting: function (searchCriteria, success, error) {
                return self.findAdminsOfGeneralMeetingResource.queryPost(null, searchCriteria, success, error);
            },
            updateOrganizationRoleTerritoryOktmos: function (organizationRoleOktmosUpdateRequest) {
                return self.organizationRolesOktmosResource.update(organizationRoleOktmosUpdateRequest);
            },
            updateOrganizationRoleTerritoryRegion: function (organizationRoleOktmoUpdateRequest) {
                return self.organizationRolesRegionResource.update(organizationRoleOktmoUpdateRequest);
            },
            findPresetOrganizationRoles: function (organizationRolesSearchCriteria) {
                return self.organizationsRoleListSearch.queryPost(null, organizationRolesSearchCriteria);
            },
            findAdminOfGeneralMeetingInfo: function (adminOfGeneralMeetingInfoSearchCriteria, successcb, errorcb) {
                return self.findAdminOfGeneralMeetingInfoResource.create(adminOfGeneralMeetingInfoSearchCriteria, successcb, errorcb);
            },
            findUserDefaultPremiseWithAlerts: function (findUserDefaultPremiseWithAlertsSearchCriteria, successcb, errorcb) {
                return self.findUserDefaultPremiseWithAlertsResource.create(findUserDefaultPremiseWithAlertsSearchCriteria, successcb, errorcb);
            },
            findOrganizationAdminOfGeneralMeetingInApartmentHouseInfo: function (guid, successcb, errorcb) {
                return self.findOrganizationAdminOfGeneralMeetingInApartmentHouseInfoResource.queryObject({'guid': guid}, null, successcb, errorcb);
            },
            findCitizenAdminOfGeneralMeetingInApartmentHouseInfo: function (guid, successcb, errorcb) {
                return self.findCitizenAdminOfGeneralMeetingInApartmentHouseInfoResource.queryObject({'guid': guid}, null, successcb, errorcb);
            },
            findLinkBetweenOrganizationAndAccount: function (queryJson, successcb, errorcb) {
                return self.findLinkBetweenOrganizationAndAccountResource.queryObject(angular.noop, queryJson, successcb, errorcb);
            },
            findUserPremises: function (successcb, errorcb) {
                return self.userPremises.get(null, successcb, errorcb);
            },
            setUserDefaultPremise: function (userDefaultPremiseSetRequest, successcb, errorcb) {
                return self.userPremises.create(userDefaultPremiseSetRequest, successcb, errorcb);
            },
            blockOrganization: function (blockOrganizationRequest) {
                return self.blockOrganizationResource.update(blockOrganizationRequest);
            },
            unblockOrganization: function (unblockOrganizationRequest) {
                return self.unblockOrganizationResource.update(unblockOrganizationRequest);
            },
            blockEmployee: function (blockEmployeeRequest) {
                return self.blockEmployeeResource.update(blockEmployeeRequest);
            },
            unblockEmployee: function (unblockEmployeeRequest) {
                return self.unblockEmployeeResource.update(unblockEmployeeRequest);
            },
            blockCitizen: function (blockCitizenRequest) {
                return self.blockCitizenResource.update(blockCitizenRequest);
            },
            unblockCitizen: function (unblockCitizenRequest) {
                return self.unblockCitizenResource.update(unblockCitizenRequest);
            },
            findAccountForCitizen: function (request) {
                return self.findAccountForCitizenResource.queryPost(null, request);
            },
            findAccountsForUser: function (successcb, errorcb) {
                return self.findAccountsForUserResource.create(null, successcb, errorcb);
            },
            invalidateSession: function (successCallback, errorCallback) {
                return self.invalidateSession.create(null, successCallback, errorCallback);
            },
            findUserSettings: function () {
                return self.userSettingsResource.get();
            },
            updateUserSettings: function (updateUserSettingsRequest) {
                return self.userSettingsResource.update(updateUserSettingsRequest);
            },
            refreshLinksBetweenOrganizationAndAccount: function (refreshLinksBetweenOrganizationAndAccountRequest) {
                return self.organizationAccountRefreshResource.update(refreshLinksBetweenOrganizationAndAccountRequest);
            }
        };
    }])

;