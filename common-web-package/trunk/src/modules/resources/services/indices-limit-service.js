angular
    .module('ru.lanit.hcs.indices.rest.LimitIndicesService', [
        'indices-resource'
    ])
    .factory('$LimitIndicesService', function (IndicesResource) {
        var self = this,
            toUrl = function (part) {
                return '/indices/limit' + part;
            };
        self.searchResource = new IndicesResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.rootResource = new IndicesResource(toUrl('/'));
        self.publishResource = new IndicesResource(toUrl('/:guid/publishing'));
        self.removeResource = new IndicesResource(toUrl('/:guid/removal'));
        self.cancelPublishResource = new IndicesResource(toUrl('/:guid/publishing/cancellation'));
        self.addFull = new IndicesResource(toUrl('/addFull'));

        return {
            /**
             *  Добавить индекс
             *  Используется для:
             *   - ВИ_СООРИ_05: Добавление индекса по МО
             *  @param request - тело индекса
             *  @return guid индекса
             *  @throws FieldValidationException - исключение валидации полей, содержит список проблемных полей. Используется в:
             *   - Альтернативный сценарий 5. Не указано значение поля предельный индекс и нет установленной отметки «Указать дифференцированный индекс»
             *   - Альтернативный сценарий 2. Данные введены некорректно
             *  Первичная валидация должна быть также проведена на клиенте.
             */
            create: self.rootResource.create,

            /**
             * Получить индекс по guid
             * Метод используется для:
             *  - ВИ_СООРИ_13: Просмотр предельного индекса
             *  Метод вызывает
             * - получение названия МО {@link }
             *  @param guid - guid индекса.
             *  @return индекс
             */
            getById: self.rootResource.getById,

            /**
             * Поиск индексов по критериям
             * Метод используется для:
             *  - ВИ_СООРИ_17: Поиск индексов по муниципальному образованию
             * Метод вызывает
             * - получение названия МО  {@link ru.lanit.hcs.nsi.api.ClassifierService#findOktmo(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
             * - проверка наличия диф. индексов {@link ru.lanit.hcs.indices.api.DifferentiatedIndicesService#exists(java.util.List<java.lang.String>))}
             * @param searchCriteria - критерии поиска
             * @return список индексов
             */
            findLimitIndices: self.searchResource.queryPost,

            /**
             *  Редактировать индексы
             *  Используется для:
             *   - ВИ_СООРИ_06: Изменение индексов по МО
             *  @param request - тело индекса
             *  @throws FieldValidationException - исключение валидации полей, содержит список проблемных полей. Используется в:
             *   - Не указано значение поля предельный индекс и нет установленной отметки «Указать дифференцированный индекс»
             *  Первичная валидация должна быть также проведена на клиенте.
             */
            update: self.rootResource.update,

            /**
             *  Удалить предельный индекс.
             *  Используется для:
             *   - ВИ_СООРИ_07: Удаление предельных индексов
             *  @param request - guid индекса (required)
             */
            remove: function (guid, successCalback, errorCallback) {
                return self.removeResource.update(null, successCalback, errorCallback, {guid: guid});
            },

            /**
             * Массовое добавление индексов
             * Используется для:
             * @param request - тело индекса
             */
            addFull: self.addFull.create
        };
    });