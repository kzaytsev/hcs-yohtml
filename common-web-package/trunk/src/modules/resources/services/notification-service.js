angular.module('ru.lanit.hcs.notification.rest.NotificationService', [
    'notification-resource'
])
    .factory('$NotificationService', ['NotificationResource', '$timeout', function (NotificationResource) {
        function toUrl(part) {
            return '/notifications' + part;
        }

        var self = this;
        self.searchResource = new NotificationResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.userSearchResource = new NotificationResource(toUrl('/usersearch;page=:page;itemsPerPage=:itemsPerPage'));
        self.userSearchModalResource = new NotificationResource(toUrl('/usersearchmodal;page=:page;itemsPerPage=:itemsPerPage'));
        self.notificationResource = new NotificationResource(toUrl('/'));
        self.deleteNotificationResource = new NotificationResource(toUrl(''));
        self.actionNotificationResource = new NotificationResource(toUrl('/:action'));
        self.findNotificationResource = new NotificationResource(toUrl('/:guid'));
        self.markAsViewedNotificationsResource = new NotificationResource(toUrl('/viewed'));
        self.countNotificationsResource = new NotificationResource(toUrl('/user/notifications/count'));

        return {
            /**
             * Метод используется для поиска оповещений (для пользователь с ролью «Должностное лицо организации»,
             * полномочием пользователя «Уполномоченный специалист», полномочием организации «Оператор ГИС ЖКХ»
             * и привилегией «Настройка оповещений пользователей».)
             *
             * @param {Object} criteria - критерии поиска оповещений ru.lanit.hcs.notification.dto.NotificationShortInfoSearchCriteria
             * @param {Number} pageIndex - Номер страницы, для которой необходимо сформировать данные
             * @param {Number} itemsPerPage - Количество элементов на странице
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @return {Array} Список кратких сведений о оповещениях, удовлетворяющих критериям поиска ru.lanit.hcs.notification.dto.NotificationShortInfo
             */
            findNotifications: function (criteria, pageIndex, itemsPerPage, successcb, errorcb) {
                return self.searchResource.queryPost({
                    page: pageIndex ? pageIndex : 1,
                    itemsPerPage: itemsPerPage
                }, criteria, successcb, errorcb);
            },
            /**
             * Метод используется для поиска оповещений этого пользователя
             *
             * @param {Object} criteria - критерии поиска оповещений ru.lanit.hcs.notification.dto.NotificationsForUserSearchCriteria
             * @param {Number} pageIndex - Номер страницы, для которой необходимо сформировать данные
             * @param {Number} itemsPerPage - Количество элементов на странице
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @return {Array} Список кратких сведений о оповещениях, удовлетворяющих критериям поиска ru.lanit.hcs.notification.dto.NotificationShortInfoForUser
             */
            findNotificationsForUser: function (criteria, pageIndex, itemsPerPage, successcb, errorcb) {
                return self.userSearchResource.queryPost({
                    page: pageIndex ? pageIndex : 1,
                    itemsPerPage: itemsPerPage
                }, criteria, successcb, errorcb);
            },
            /**
             * Метод используется для поиска оповещений этого пользователя для отображения в модальном окне
             *
             * @param {Object} criteria - критерии поиска оповещений ru.lanit.hcs.notification.dto.NotificationsForUserSearchCriteria
             * @param {Number} pageIndex - Номер страницы, для которой необходимо сформировать данные
             * @param {Number} itemsPerPage - Количество элементов на странице
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @return {Array} Список кратких сведений о оповещениях, удовлетворяющих критериям поиска ru.lanit.hcs.notification.dto.NotificationShortInfoForUser
             */
            findNotificationsForUserModal: function (criteria, pageIndex, itemsPerPage, successcb, errorcb) {
                return self.userSearchModalResource.queryPost({
                    page: pageIndex ? pageIndex : 1,
                    itemsPerPage: itemsPerPage
                }, criteria, successcb, errorcb);
            },
            /**
             * Метод используется для удаления оповещения
             *
             * @param {String} guid - идентификатор оповещения
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            deleteNotification: function (guid, successcb, errorcb) {
                self.deleteNotificationResource.remove(guid, successcb, errorcb);
            },
            /**
             * Метод используется для сохранения оповещения
             *
             * @param {Object} createNotificationRequest - оповещение
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            saveNotification: function (createNotificationRequest, successcb, errorcb) {
                return self.notificationResource.create(createNotificationRequest, successcb, errorcb);
            },

            /**
             * Метод используется для обновления оповещения
             *
             * @param {Object} updateNotification - оповещение
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            updateNotification: function (updateNotification, successcb, errorcb) {
                return self.notificationResource.update(updateNotification, successcb, errorcb);
            },
            /**
             * Метод используется для отправки оповещения
             *
             * @param {String} guid - идентификатор оповещения
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            sendNotification: function (guid, successcb, errorcb) {
                self.actionNotificationResource.update({guid: guid}, successcb, errorcb, {action: "sending"});
            },
            /**
             * Метод используется для активации оповещения
             *
             * @param {String} guid - идентификатор оповещения
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            activateNotification: function (guid, successcb, errorcb) {
                self.actionNotificationResource.update({guid: guid}, successcb, errorcb, {action: "activation"});
            },
            /**
             * Метод используется для деактивации оповещения
             *
             * @param {String} guid - идентификатор оповещения
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            deactivateNotification: function (guid, successcb, errorcb) {
                self.actionNotificationResource.update({guid: guid}, successcb, errorcb, {action: "deactivation"});
            },
            /**
             * Метод находит оповещение
             *
             * @param guid - идентификатор оповещения
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @returns {Object} информация об оповещении
             */
            findNotification: function (guid, successcb, errorcb) {
                return self.findNotificationResource.get({guid: guid}, successcb, errorcb);
            },
            /**
             * Метод используется для отображения оповещения, как просмотренное
             *
             * @param {Object} markNotificationsAsViewedRequest - ru.lanit.hcs.notification.dto.MarkNotificationsAsViewedRequest
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            markNotificationsAsViewed: function (markNotificationsAsViewedRequest, successcb, errorcb) {
                self.markAsViewedNotificationsResource.queryPost({}, markNotificationsAsViewedRequest, successcb, errorcb);
            },
            /**
             * Для отображения индексов в ЛК
             *
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            countNotificationsForUser: function (successcb, errorcb) {
                return self.countNotificationsResource.get({}, successcb, errorcb);
            }
        };
    }])
;