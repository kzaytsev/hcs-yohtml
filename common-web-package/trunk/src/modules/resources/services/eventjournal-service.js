angular
    .module('ru.lanit.hcs.eventjournal.rest.JournalRestService', [
        'eventjournal-resource'
    ])
    .factory('$EventJournalService', function (EventJournalResource) {
        var self = this,
            toUrl = function (part) {
                return '/journal' + part;
            };
        self.eventTypeResource = new EventJournalResource(toUrl('/event/typelist;groups=:groups'));
        self.eventJournalResource = new EventJournalResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.findEventsResource = new EventJournalResource(toUrl('/search/:eventType?description=:description'));

        return {
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.eventjournal.rest.JournalRestService#getEventTypeList}
            // Используется для
            // - поиска доступных типов событий
            // @param searchCriteria - критерии поиска (пока не используется);
            // @return Список типов событий [{name: '<hw_id>', label: '<human_name>'}]
            getEventTypeList: function (params, successCalback, errorCallback) {
                return self.eventTypeResource.get(params, successCalback, errorCallback);
            },
            findEventsGet: function (params, successCallback, errorCallback) {
                return self.findEventsResource.get(params, successCallback, errorCallback);
            },
            findEvents: function(pathParams, searchCriteria, successCallback, errorCallback) {
                return self.eventJournalResource.queryPost(pathParams, searchCriteria, successCallback, errorCallback);
            }
        };
    })

;