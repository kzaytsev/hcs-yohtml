angular
    .module('ru.lanit.hcs.indices.rest.DifferentiatedIndicesService', [
        'indices-resource'
    ])
    .factory('$DifferentiatedIndicesService', function (IndicesResource) {
        var self = this,
            toUrl = function (part) {
                return '/indices/diff' + part;
            };
        self.createResource = new IndicesResource(toUrl('/'));
        self.readResource = new IndicesResource(toUrl('/:guid'));
        self.updateResource = new IndicesResource(toUrl('/'));
        self.removeResource = new IndicesResource(toUrl('/:guid/removal'));
        self.searchResource = new IndicesResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.getAllAttributesResource = new IndicesResource(toUrl('/attributes'));

        return {
            /**
             *  Добавить индекс
             *  Используется для:
             *   - ВИ_СООРИ_08: Добавление дифференцированного индекса
             *  @param request - тело индекса
             *  @return guid индекса
             *  @throws ru.lanit.hcs.indices.api.exception.FieldValidationException - исключение валидации полей, содержит список проблемных полей. Используется в:
             *   - Альтернативный сценарий 2. Данные введены некорректно
             *  Первичная валидация должна быть также проведена на клиенте.
             */
            create: function (data, successCalback, errorCallback) {
                return self.createResource.create(data, successCalback, errorCallback, null);
            },

            /**
             *  Редактировать индексы
             *  Используется для:
             *   - ВИ_СООРИ_09: Изменение дифференцированного индекса
             *  @param request - тело индекса
             *  @throws ru.lanit.hcs.indices.api.exception.FieldValidationException - исключение валидации полей, содержит список проблемных полей. Используется в:
             *   - Альтернативный сценарий 2. Данные введены некорректно
             *  Первичная валидация должна быть также проведена на клиенте.
             */
            update: function (data, successCalback, errorCallback) {
                return self.updateResource.update(data, successCalback, errorCallback, null);
            },

            /**
             * Получить все атрибуты
             *  Метод используется для:
             *   - ВИ_СООРИ_09: Изменение дифференцированного индекса
             *  @return список атрибутов с возможными значениями
             */
            getAllAttributes: function (successCalback, errorCallback) {
                return  self.getAllAttributesResource.get(null, successCalback, errorCallback);
            },

            /**
             *  Удалить индексы по субъекту.
             *  Используется для:
             *   - ВИ_СООРИ_10: Удаление индексов
             *  @param request - guid индекса
             */
            remove: function (guid, successCalback, errorCallback) {
                return self.removeResource.update(null, successCalback, errorCallback, {guid: guid});
            },

            /**
             * Получить индекс по guid
             *  Метод используется для:
             *   - нет сценариев
             *  @param guid - guid индекса.
             *  @return индекс
             */
            read: function (guid, successCalback, errorCallback) {
                return self.readResource.get({guid: guid}, successCalback, errorCallback);
            },

            /**
             * Поиск индексов по критериям
             * Метод используется для:
             *  - поиск индексов
             * Метод вызывает
             *  - получить названия услуг {@link ru.lanit.hcs.nsi.api.HouseService#findMunicipalService(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
             * @param searchCriteria - критерий поиска
             * @return список индексов
             */
            findDifferentiatedIndices: function(pageParams, searchCriteria, successCalback, errorCallback){
                return self.searchResource.queryPost(pageParams, searchCriteria, successCalback, errorCallback);
            }
        };
    });