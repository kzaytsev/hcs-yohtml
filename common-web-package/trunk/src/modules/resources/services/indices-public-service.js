angular
    .module('ru.lanit.hcs.indices.rest.PublicIndicesService', [
        'indices-resource'
    ])
    .factory('$PublicIndicesService', function (IndicesResource) {
        var self = this,
            toUrl = function (part) {
                return '/indices/public' + part;
            };
        self.searchIndicesResource = new IndicesResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.rootResource = new IndicesResource(toUrl('/'));
        self.searchLimitIndicesResource = new IndicesResource(toUrl('/search/limit;page=:page;itemsPerPage=:itemsPerPage'));
        self.getLimitIndexResource = new IndicesResource(toUrl('/limit'));
        self.searchDiffResource = new IndicesResource(toUrl('/search/diff;page=:page;itemsPerPage=:itemsPerPage'));


        return {

            /**
             * Поиск индексов по критериям
             * Метод используется для:
             *  - ВИ_ПОЧ_18: Просмотр индексов по субъектам РФ
             *  - ВИ_ПОЧ_19: Поиск индексов по субъектам РФ
             * @param searchCriteria - критерии поиска
             * @return список индексов
             */
            findIndices: self.searchIndicesResource.queryPost,

            /**
             * Получить индекс по guid
             *  Метод используется для:
             *   - ВИ_ПОЧ_20: Просмотр предельных индексов в муниципальных образованиях субъекта РФ
             *   - ВИ_ПОЧ_22: Просмотр информации о предельном индексе
             *  @param guid - guid индекса (required).
             *  @return индекс
             */
            getIndexById: self.rootResource.getById,

            /**
             * Поиск индексов по критериям
             * Метод используется для:
             *  - ВИ_ПОЧ_20: Просмотр предельных индексов в муниципальных образованиях субъекта РФ
             *  - ВИ_ПОЧ_21: Поиск предельных индексов по муниципальным образованиям субъекта РФ
             * @param searchCriteria - критерии поиска
             * @return список индексов
             */
            findLimitIndices: self.searchLimitIndicesResource.queryPost,

            /**
             * Получить индекс по guid
             * Метод используется для:
             *  - ВИ_СООРИ_13: Просмотр предельного индекса
             *  @param guid - guid индекса.
             *  @return индекс
             */
            getLimitIndexById: self.getLimitIndexResource.getById,

            /**
             * Поиск индексов по критериям
             * Метод используется для:
             *  - ВИ_ПОЧ_22: Просмотр информации о предельном индексе
             * @param searchCriteria - критерий поиска
             * @return список индексов
             */
            findDifferentiatedIndices: self.searchDiffResource.queryPost

        };
    })

;