angular.module('ru.lanit.hcs.workPlanning.rest.WorkPlanningService', [
    'work-planning-resource'
])

    .factory('$WorkPlanningService', ['WorkPlanningResource', '$q', function (WorkPlanningResource, $q) {

        var self = this;

        var toUrl = function (part) {
            return '/workplanning' + part;
        };

        self.findWorkListsResource = new WorkPlanningResource('/search/worklists;pageIndex=:pageIndex;elementsPerPage=:elementsPerPage');
        self.findWorkListResource = new WorkPlanningResource('/search/worklist/:guid');
        self.findWorkListPeriodsResource = new WorkPlanningResource('/search/worklist/periods');

        self.approveWorkListResource = new WorkPlanningResource('/approve/worklist');
        self.cancelWorkListResource = new WorkPlanningResource('/cancel/worklist');
        self.deleteWorkListResource = new WorkPlanningResource('/delete/worklist');
        self.saveWorkListResource = new WorkPlanningResource('/save/worklist');

        self.findWorkPlansResource = new WorkPlanningResource('/search/workplans');
        self.saveWorkPlanResource = new WorkPlanningResource('/save/workplan');

        self.findWorkReportingPeriodsResource = new WorkPlanningResource('/search/reportingperiods');
        self.findCompletedWorksResource = new WorkPlanningResource('/search/fixedworks');
        self.fixWorksResource = new WorkPlanningResource('/save/fixedworks');

        self.findActTitlesResource = new WorkPlanningResource('/search/acttitles;pageIndex=:pageIndex;elementsPerPage=:elementsPerPage');
        self.findActsResource = new WorkPlanningResource('/search/acts');
        self.findActResource = new WorkPlanningResource('/search/act/:guid');
        self.saveActResource = new WorkPlanningResource('/save/act');

        self.findNotActualResourceProvidersResource = new WorkPlanningResource('/search/providers/notactual');
        self.updateResourceProviderLinksResource = new WorkPlanningResource('/update/providers');

        return {
            findWorkLists: function (criteria, matrixParam, successcb, errorcb) {
                return self.findWorkListsResource.queryPost(matrixParam, criteria, successcb, errorcb);
            },
            findWorkList: function (guid, matrixParam, successcb, errorcb) {
                return self.findWorkListResource.get({guid: guid}, successcb, errorcb);
            },
            findWorkListPeriods: function (findWorkListPeriodsSearchCriteria, successcb, errorcb) {
                return self.findWorkListPeriodsResource.create(findWorkListPeriodsSearchCriteria, successcb, errorcb);
            },
            approveWorkList: function (approveWorkListRequest, successcb, errorcb) {
                return self.approveWorkListResource.update(approveWorkListRequest, successcb, errorcb);
            },
            cancelWorkList: function (cancelWorkListRequest, successcb, errorcb) {
                return self.cancelWorkListResource.update(cancelWorkListRequest, successcb, errorcb);
            },
            deleteWorkList: function (deleteWorkListRequest, successcb, errorcb) {
                return self.deleteWorkListResource.update(deleteWorkListRequest, successcb, errorcb);
            },
            saveWorkList:  function (saveWorkListRequest, successcb, errorcb) {
                return self.saveWorkListResource.create(saveWorkListRequest, successcb, errorcb);
            },
            findWorkPlans: function (findWorkPlansSearchCriteria, matrixParam, successcb, errorcb) {
                return self.findWorkPlansResource.queryPost(matrixParam, findWorkPlansSearchCriteria, successcb, errorcb);
            },
            saveWorkPlan: function (saveWorkPlanRequest, successcb, errorcb) {
                return self.saveWorkPlanResource.update(saveWorkPlanRequest, successcb, errorcb);
            },
            findWorkReportingPeriods: function (findWorkReportingPeriodsSearchCriteria, successcb, errorcb) {
                return self.findWorkReportingPeriodsResource.queryPost('', findWorkReportingPeriodsSearchCriteria, successcb, errorcb);
            },
            fixWorks: function (fixWorksRequest, successcb, errorcb) {
                return self.fixWorksResource.update(fixWorksRequest, successcb, errorcb);
            },
            findCompletedWorks: function (findCompletedWorksSearchCriteria, successcb, errorcb) {
                return self.findCompletedWorksResource.queryPost('', findCompletedWorksSearchCriteria, successcb, errorcb);
            },

            findActTitles: function (findActTitlesSearchCriteria, matrixParam, successcb, errorcb) {
                return self.findActTitlesResource.queryPost(matrixParam, findActTitlesSearchCriteria, successcb, errorcb);
            },
            findActs: function (findActsSearchCriteria, successcb, errorcb) {
                return self.findActsResource.queryPost('', findActsSearchCriteria, successcb, errorcb);
            },
            findAct: function (guid, successcb, errorcb) {
                return self.findActResource.get({guid: guid}, successcb, errorcb);
            },
            saveAct: function (saveActRequest, successcb, errorcb) {
                return self.saveActResource.create(saveActRequest, successcb, errorcb);
            },
            findNotActualResourceProviders: function (notActualResourceProvidersSearchCriteria, successcb, errorcb) {
                return self.findNotActualResourceProvidersResource.queryPost('', notActualResourceProvidersSearchCriteria, successcb, errorcb);
            },
            updateResourceProviderLinks: function (updateResourceProviderLinksRequest, successcb, errorcb) {
                return self.updateResourceProviderLinksResource.create(updateResourceProviderLinksRequest, successcb, errorcb);
            }
        };
    }]);