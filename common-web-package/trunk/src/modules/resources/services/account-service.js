angular
    .module('ru.lanit.hcs.homemanagement.rest.AccountService', ['home-mgmt-resource'])
    .factory('$AccountService', ['HomeManagementResource', function (HomeManagementResource) {
        var self = this;

        self.resources = {};
        self.pathPrefix = '/accounts';

        self.getResource = function (address) {
            var resource = null;
            if (self.resources[address]) {
                resource = self.resources[address];
            } else {
                resource = new HomeManagementResource(self.pathPrefix + address);
                self.resources[address] = resource;
            }

            return resource;
        };

        return {
            findAccountsWithNsi: function (request, queryParams) {
                return self.getResource('/search;page=:page;itemsPerPage=:itemsPerPage')
                    .create(request, angular.noop, angular.noop, queryParams);
            },
            findMunicipalityAccountsWithNsi: function (request, queryParams) {
                return self.getResource('/municipality/search;page=:page;itemsPerPage=:itemsPerPage')
                    .create(request, angular.noop, angular.noop, queryParams);
            },
            findAccount: function (request) {
                return self.getResource('').getById(request);
            },
            findAccounts: function (request) {
                return self.getResource('/references').create(request);
            },
            createAccount: function (request) {
                return self.getResource('').create(request);
            },
            updateAccount: function (request) {
                return self.getResource('').update(request);
            },
            closeAccount: function (request) {
                return self.getResource('/removal').removeObject(request);
            },
            findAccountsForCitizen: function (request) {
                return self.getResource('/for/citizen').queryPost(null, request);
            }
        };
    }]);