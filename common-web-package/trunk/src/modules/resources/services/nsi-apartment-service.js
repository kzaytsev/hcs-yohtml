angular.module('ru.lanit.hcs.nsi.rest.NsiApartmentService', [
    'nsi-resource'
]).factory('$NsiApartmentService', ['NsiResource',
    function (NsiResource) {
        var self = this;

        var toUrl = function (part) {
            return '/nsi/apartments' + part;
        };

        self.CategoriesResource = new NsiResource(toUrl('/categories'));
        self.ResidentPermiseTypeResource = new NsiResource(toUrl('/resident/premise/types'));
        self.PropertyTypes = new NsiResource(toUrl('/property/types'));
        self.CooperativePremisePurposeResource = new NsiResource(toUrl('/cooperative/premise/purposes'));
        self.NumberOfRoomsResource = new NsiResource(toUrl('/number/of/rooms'));
        self.LivingPersonTypeResource = new NsiResource(toUrl('/living/person/type'));

        return {
            // Метод выполняет поиск категорий помещения, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.ApartmentService#findApartmentCategory(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных категорий помещения при создании, редактировании сведений о помещении
            // - поиска актуальных категорий помещения при поиске, создании, редактировании сведений о приборе учета
            // - поиска актуальных причин закрытия лицевого счета при поиске, создании, редактировании сведений о лицевом счете
            // - поиска категорий помещения для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список категорий помещения, удовлетворяющих критертям поиска
            findApartmentCategory: function (code, name, guid, successcb, errorcb) {
                return self.CategoriesResource.get({code: code, name: name, guid: guid}, successcb, errorcb);
            },
            // Метод выполняет поиск характеристик помещения, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.ApartmentService#findResidentPremiseType(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных характеристик помещения при создании, редактировании сведений о помещении
            // - поиска характеристик помещения для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список характеристик помещения, удовлетворяющих критертям поиска
            findResidentPremiseType: function (code, name, guid, successcb, errorcb) {
                return self.ResidentPermiseTypeResource.get({code: code, name: name, guid: guid}, successcb, errorcb);
            },
            // Метод выполняет поиск видов собственности, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.ApartmentService#findPropertyType(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных видов собственности при создании, редактировании сведений о помещении
            // - поиска видов собственности для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список видов собственности, удовлетворяющих критертям поиска
            findPropertyType: function (code, name, guid, successcb, errorcb) {
                return self.PropertyTypes.get({code: code, name: name, guid: guid}, successcb, errorcb);
            },
            // Метод выполняет поиск назначений помещения, относящегося к общему долевому имуществу собственников помещений, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.ApartmentService#findCooperativePremisePurpose(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных назначений помещения, относящегося к общему долевому имуществу собственников помещений, при создании, редактировании электронного паспорта МКД
            // - поиска назначений помещения, относящегося к общему долевому имуществу собственников помещений, для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список назначений помещения, относящегося к общему долевому имуществу собственников помещений, удовлетворяющих критертям поиска
            findCooperativePremisePurpose: function (code, name, guid, successcb, errorcb) {
                return self.CooperativePremisePurposeResource.get({code: code, name: name, guid: guid}, successcb, errorcb);
            },
            // Метод выполняет поиск количества комнат в помещении, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.ApartmentService#findApartmentNumberOfRooms(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных количеств комнат в помещении при создании, редактировании сведений о помещении
            // - поиска количеств комнат в помещении для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список количества комнат в помещении, удовлетворяющих критертям поиска
            findApartmentNumberOfRooms: function (code, name, guid, successcb, errorcb) {
                return self.NumberOfRoomsResource.get({code: code, name: name, guid: guid}, successcb, errorcb);
            },
            // Метод выполняет поиск видов лиц, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.ApartmentService#findLivingPersonType(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных видов лиц при создании, редактировании электронного паспорта МКД
            // - поиска видов лиц для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список видов лиц, удовлетворяющих критертям поиска
            findLivingPersonType: function (code, name, guid, successcb, errorcb) {
                return self.LivingPersonTypeResource.get({code: code, name: name, guid: guid}, successcb, errorcb);
            }
        };

}]);

