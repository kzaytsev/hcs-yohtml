angular.module('ru.lanit.hcs.nsi.rest.NsiDeviceService', [
    'nsi-resource'
]).factory('$NsiDeviceService', ['NsiResource',
    function (NsiResource) {
        var self = this;

        var toUrl =  function(part) {
            return '/nsi/devices' + part;
        };

        self.MunicipalResource = new NsiResource(toUrl('/municipal/resources'));
        self.MunicipalServices = new NsiResource(toUrl('/municipal/services'));
        self.DeviceTypesResource = new NsiResource(toUrl('/types'));
        self.DeviceCalibrationIntervalResource = new NsiResource(toUrl('/calibration/intervals'));
        self.ActualDeviceOperationPrincipleTypeResource = new NsiResource(toUrl('/operation/principle/actual/types'));
        self.DeviceOperationPrincipleTypeResource = new NsiResource(toUrl('/operation/principle/types'));
        self.DeviceMountTypeResource = new NsiResource(toUrl('/mount/types'));
        self.DeviceConnectionTypeResource = new NsiResource(toUrl('/connection/types'));
        self.DeviceTariffTypeResource = new NsiResource(toUrl('/tariff/types'));
        self.ElectricityMeteringType = new NsiResource(toUrl('/energy/metering/types'));

        return {
            /**
             * Метод выполняет поиск типа учета электроэнергии, удовлетворяющих критериям поиска
             * @param queryJson - критерий поиска
             * @return Список типов учета электроэнергии, удовлетворяющих критериям поиска
             */
            findElectricityMeteringTypes: function (queryJson, successcb, errorcb) {
                return self.ElectricityMeteringType.query([], queryJson, successcb, errorcb);
            },
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.HouseService#findMunicipalResource(ru.lanit.hcs.nsi.api.dto.search.MunicipalResourceSearchCriteria)}
            // Используется для
            // - фильтрации списка значений ресурса для выпадающего списка при добавлении коммунальной услуги в РАО
            // - поиска актуальных видов коммунального ресурса при поиске, создании, редактировании сведений о приборе учета
            // @param municipalResourceSearchCriteria - критерий поиска
            // @return Список видов коммунальных ресурсов, удовлетворяющих критериям поиска
            findMunicipalResource: function (queryJson, successcb, errorcb) {
                return self.MunicipalResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск видов коммунальных услуг, удовлетворяющих критериям поиска
            // Используется для
            // - поиска актуальных видов коммунального ресурса при создании, редактировании электронного паспорта
            // - поиска видов коммунальных услуг для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список видов коммунальных услуг, удовлетворяющих критертям поиска
            findMunicipalServices: function (queryJson, successcb, errorcb) {
                return self.MunicipalServices.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск типов приборов учета, удовлетворяющих критериям поиска
            // Используется для
            // - поиска актуальных типов приборов учета при поиске, создании, редактировании сведений о приборе учета
            // - поиска типов приборов учета для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список типов приборов учета, удовлетворяющих критертям поиска
            findDeviceTypes: function (queryJson, successcb, errorcb) {
                return self.DeviceTypesResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск межповерочных интервалов, удовлетворяющих критериям поиска
            // Используется для
            // - поиска актуальных межповерочных интервалов при создании, редактировании сведений о приборе учета
            // - поиска межповерочных интервалов для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список межповерочных интервалов, удовлетворяющих критертям поиска
            findDeviceCalibrationInterval: function (queryJson, successcb, errorcb) {
                return self.DeviceCalibrationIntervalResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск видов приборов учета по принципу действия, удовлетворяющих критериям поиска
            // Используется для
            // - поиска актуальных видов приборов учета по принципу действия при создании, редактировании сведений о приборе учета
            // @param municipalResource - Вид коммунального ресурса
            // @return Список видов приборов учета по принципу действия, удовлетворяющих критертям поиска
            findActualDeviceOperationPrincipleType: function (queryJson, successcb, errorcb) {
                return self.ActualDeviceOperationPrincipleTypeResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск видов приборов учета по принципу действия, удовлетворяющих критериям поиска
            // Используется для
            // - поиска актуальных видов приборов учета по принципу действия при создании, редактировании сведений о приборе учета
            // @param municipalResource - Вид коммунального ресурса
            // @return Список видов приборов учета по принципу действия, удовлетворяющих критертям поиска
            findDeviceOperationPrincipleType: function (queryJson, successcb, errorcb) {
                return self.DeviceOperationPrincipleTypeResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск видов прибора учета по типу подключения, удовлетворяющих критериям поиска
            // Используется для
            // - поиска актуальных видов прибора учета по типу подключения при создании, редактировании сведений о приборе учета
            // - поиска видов прибора учета по типу подключения для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список видов прибора учета по типу подключения, удовлетворяющих критертям поиска
            findDeviceMountType: function (queryJson, successcb, errorcb) {
                return self.DeviceMountTypeResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск видов прибора учета по типу интерфейса связи, удовлетворяющих критериям поиска
            // Используется для
            // - поиска актуальных видов прибора учета по типу интерфейса связи при создании, редактировании сведений о приборе учета
            // - поиска видов прибора учета по типу интерфейса связи для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список видов прибора учета по типу интерфейса связи, удовлетворяющих критертям поиска
            findDeviceConnectionType: function (queryJson, successcb, errorcb) {
                return self.DeviceConnectionTypeResource.query([], queryJson, successcb, errorcb);
            },
             // Метод выполняет поиск видов прибора учета по количеству тарифов, удовлетворяющих критериям поиска
             // Используется для
             // - поиска актуальных видов прибора учета по количеству тарифов при создании, редактировании сведений о приборе учета
             // - поиска видов прибора учета по количеству тарифов для отображения на экранных формах при просмотре
             // @param nsiSearchCriteria - криткерий поиска
             // @return Список видов прибора учета по количеству тарифов, удовлетворяющих критертям поиска
            findDeviceTariffType: function (queryJson, successcb, errorcb) {
                return self.DeviceTariffTypeResource.query([], queryJson, successcb, errorcb);
            }

        };
    }
]);
