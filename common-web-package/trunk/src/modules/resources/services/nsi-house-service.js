angular.module('ru.lanit.hcs.nsi.rest.NsiHouseService', [
    'nsi-resource'
]).factory('$NsiHouseService', ['NsiResource',
    function (NsiResource) {
        var self = this;

        var toUrl = function (part) {
            return '/nsi/houses' + part;
        };

        self.HousesTypeResource = new NsiResource(toUrl('/types'));
        self.HouseConditionResource = new NsiResource(toUrl('/conditions'));
        self.ManagementTypeResource = new NsiResource(toUrl('/management/types'));
        self.EnergyEfficiencyResource = new NsiResource(toUrl('/energy/efficiency'));
        self.PlanTypesResource = new NsiResource(toUrl('/plan/types'));
        self.CooperativeSupportServicesResource = new NsiResource(toUrl('/cooperative/support/services'));
        self.StructuralComponentsResource = new NsiResource(toUrl('/structural/components'));
        self.MunicipalResources = new NsiResource(toUrl('/municipal/resources'));
        self.InspectionObjectsResource = new NsiResource(toUrl('/inspection/objects'));

        self.WallMaterialsResource = new NsiResource(toUrl('/wall/materials'));
        self.HouseIrrelevanceReasonsResource = new NsiResource(toUrl('/irrelevance/reasons'));
        self.AccidentObjectsResource = new NsiResource(toUrl('/accident/objects'));

        self.OverhaulFundFormingMethodsResource = new NsiResource(toUrl('/overhaul/fund/forming/methods'));
        self.ResidentialHouseTypesResource = new NsiResource(toUrl('/residential/house/types'));

        return {
            /**
             * Метод выполняет поиск по справочнику "Тип жилого помещения", удовлетворяющих критериям поиска
             * @param nsiSearchCriteria - критерий поиска
             * @return Список типов жилых помещений
             */
            findResidentialHouseTypes: function (queryJson, successcb, errorcb) {
                return self.ResidentialHouseTypesResource.query([], queryJson, successcb, errorcb);
            },
            /**
             * Метод выполняет поиск по справочнику "Способ формирования фонда капитального ремонта", удовлетворяющих критериям поиска
             * Метод вызывает
             * - метод {@link ru.lanit.hcs.nsi.api.HouseService#findOverhaulFundFormingMethod(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
             * Используется для
             * - поиска способов формирования фонда капитального ремонта для отображения на экранных формах при просмотре
             *
             * @param nsiSearchCriteria - критерий поиска
             * @return Список способов формирования фонда капитального ремонта, удовлетворяющих критериям поиска
             */
            findOverhaulFundFormingMethod: function (queryJson, successcb, errorcb) {
                return self.OverhaulFundFormingMethodsResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск по справочнику "Тип дома", удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.HouseService#findHouseType(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных типов домов при поиске, создании, редактировании сведений о доме
            // - поиска актуальных типов домов при создании, редактировании электронного паспорта МКД
            // - поиска типов домов для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список типов домов, удовлетворяющих критертям поиска
            findHouseType: function (queryJson, successcb, errorcb) {
                return self.HousesTypeResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск по справочнику "Состояние дома", удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.HouseService#findHouseCondition(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных состояний домов при поиске, создании, редактировании сведений о доме
            // - поиска состояний домов для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список состояний дома, удовлетворяющих критериям поиска
            findHouseCondition: function (queryJson, successcb, errorcb) {
                return self.HouseConditionResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск по справочнику "Способ управления", удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.HouseService#findHouseManagementType(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных способов управления домом при создании, редактировании сведений о доме
            // - поиска способов управления домом для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список способов управления домом, удовлетворяющих критериям поиска
            findHouseManagementType: function (queryJson, successcb, errorcb) {
                return self.ManagementTypeResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск по Классам энергетической эффективности зданий, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.HouseService#findHouseEnergyEfficiency(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных классов энергетической эффективности зданий при создании, редактировании электронного паспорта МКД
            // - поиска классов энергетической эффективности зданий для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список классов энергетической эффективности зданий, удовлетворяющих критериям поиска
            findHouseEnergyEfficiency: function (queryJson, successcb, errorcb) {
                return self.EnergyEfficiencyResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск типов проектов зданий, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.HouseService#findHousePlanType(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных типов проекта зданий при создании, редактировании сведений о доме
            // - поиска типов проекта здания для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список типов проекта зданий, удовлетворяющих критериям поиска
            findHousePlanType: function (queryJson, successcb, errorcb) {
                return self.PlanTypesResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск по классификатору работ (услуг), удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.HouseService#findCooperativeSupportService(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных работ (услуг) при создании, редактировании электронного паспорта МКД
            // - поиска работ (услуг) для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список работ (услуг) по содержанию общего имущества, удовлетворяющих критериям поиска
            findCooperativeSupportService: function (queryJson, successcb, errorcb) {
                return self.CooperativeSupportServicesResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск конструктивных элементов, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.HouseService#findHouseStructuralComponent(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска конструктивных элементов для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - критерий поиска
            // @return Список конструктивных элементов, удовлетворяющих критериям поиска
            findStructuralComponents: function (queryJson, successcb, errorcb) {
                return self.StructuralComponentsResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск видов коммунальных ресурсов, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.HouseService#findMunicipalResource(ru.lanit.hcs.nsi.api.dto.search.MunicipalResourceSearchCriteria)}
            // Используется для
            // - фильтрации списка значений ресурса для выпадающего списка при добавлении коммунальной услуги в РАО
            // - поиска актуальных видов коммунального ресурса при поиске, создании, редактировании сведений о приборе учета
            // @param municipalResourceSearchCriteria - критерий поиска
            // @return Список видов коммунальных ресурсов, удовлетворяющих критериям поиска
            findMunicipalResources: function (queryJson, successcb, errorcb) {
                return self.MunicipalResources.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск объектов осмотра, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.HouseService#findInspectionObject(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска объектов осмотра для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - критерий поиска
            // @return Список объектов осмотра, удовлетворяющих критериям поиска
            findInspectionObjects: function (queryJson, successcb, errorcb) {
                return self.InspectionObjectsResource.query([], queryJson, successcb, errorcb);
            },
            /**
             * Метод выполняет поиск причин неактуальности домаа, удовлетворяющих критериям поиска
             * @param queryJson - критерий поиска
             * @return Список видов потребителя коммунального ресурса, удовлетворяющих критериям поиска
             */
            findHouseIrrelevanceReason: function(queryJson, successcb, errorcb){
                return self.HouseIrrelevanceReasonsResource.query([], queryJson, successcb, errorcb);
            },
            /**
             * Метод выполняет поиск по справочнику "Материал стен", удовлетворяющих критериям поиска
             * @param queryJson - критерий поиска
             * @return Список материалов стен, удовлетворяющих критериям поиска
             */
            findWallMaterial: function(queryJson, successcb, errorcb){
                return self.WallMaterialsResource.query([], queryJson, successcb, errorcb);
            },
            /**
             * Метод выполняет поиск по справочнику "Объекты аварии", удовлетворяющих критериям поиска
             * @param queryJson - критерий поиска
             * @return Список Объекты аварии, удовлетворяющих критериям поиска
             */
            findAccidentObject: function(queryJson, successcb, errorcb) {
                return self.AccidentObjectsResource.query([], queryJson, successcb, errorcb);
            }

        };

}]);