angular.module('ru.lanit.hcs.ppa.rest.PassportService', [
    'backend-resource'
])

    .factory('$PassportService', ['BackendResource', function (BackendResource) {

        var self = this;
        var config = {baseUrl: '/passport/api/rest/services'};

        var toUrl = function (part) {
            return '/passport' + part;
        };

        self.PassportsSearchResource = new BackendResource(config, toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.PassportFindResource = new BackendResource(config, toUrl('/:type/:passportGuid/:partition'));
        self.PassportUpdateResource = new BackendResource(config, toUrl('/:type/:partition/update'));
        self.PassportRemovalResource = new BackendResource(config, toUrl('/:type/removal'));
        self.PassportApprovalResource = new BackendResource(config, toUrl('/:type/approval'));
        self.PassportSendResource = new BackendResource(config, toUrl('/:type/send'));
        self.PassportCopyResource = new BackendResource(config, toUrl('/:type/copy'));
        self.PassportPublishingResource = new BackendResource(config, toUrl('/:type/publishing'));
        self.PassportApprovalWithSycnCheckResource = new BackendResource(config, toUrl('/:type/approvalWithSyncCheck'));
        self.HouseEngineeringResource = new BackendResource(config, toUrl('/:houseGuid/engineering'));
        self.PassportsResource = new BackendResource(config, toUrl('/'));

        return {
            // Метод используется для
            // - формирования электронного паспорта многоквартирного дома (пользователь с кодом привилегии П_ЭП_2, ВИ_ЭПМКД_14)
            // - формирования электронного паспорта жилого дома (пользователь с кодом привилегии П_ЭП_2, ВИ_ЭПЖД_08)
            // @param passportCreateRequest - Запрос на создание сведений электронного паспорта многоквартирного дома
            // @return Идентификационный код сведений электронного паспорта
            // @throws PassportConfigNotFoundException - настройкла структуры не найдена в реестре настроек структур
            // @throws HouseManagementException - несоответствие организации, выполняющей действие, обслуживающей организации по данному адресу
            // @throws PassportIntervalAlreadyExistException - уже существует ЭП с указанным отчетным периодом
            // @throws FieldValidationException - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            createPassport: function (createRequest, successcb, errorcb) {
                return self.PassportsResource.create(createRequest, successcb, errorcb);
            },
            // Метод выполняет поиск кратких сведений электронного паспорта, удовлетворяющих критериям поиска
            // Используется для
            // - поиска электронных паспортов в реестре ЭП (пользователь с кодом привилегии П_ЭП_2, ВИ_ЭПМКД_09, ВИ_ЭПЖД_06)
            // @param passportSummarySearchCriteria - критерии поиска
            // @param pageIndex - Номер страницы, для которой необходимо сформировать данные
            // @param elementsPerPage - Количество элементов на странице
            // @return Список электронных паспортов, удовлетворяющих критериям поиска
            search: function (searchCriteria, successcb, errorcb, matrixParams) {
                return self.PassportsSearchResource.create(searchCriteria, successcb, errorcb, matrixParams);
            },
            // Метод выполняет поиск сведений раздела "Общие сведения о ЖД" электронного паспорта жилого дома по его идентификационному коду
            // Используется для
            // - поиска ЭП для изменения данных в карточке электронного паспорта жилого дома со статусом Черновик (пользователь с кодом привилегии П_ЭП_2, П_ЭП_3; ВИ_ЭПЖД_09, ВИ_ЭПЖД_10, ВИ_ЭПЖД_12)
            // - поиска ЭП при переходе с вкладки на вкладку в карточке электронного паспорта жилого дома (пользователь с привилегией П_ЭП_2, ВИ_ЭПЖД_11)
            // - поиска ЭП для просмотра карточки электронного паспорта жилого дома (пользователь с привилегией П_ЭП_2, ВИ_ЭПЖД_16)
            // @param passportDetailSearchCriteria - критерии поиска
            // @return Сведения раздела "Общие сведения о ЖД" электронного паспорта жилого дома
            findPassport: function (type, passportGuid, partition, successcb, errorcb) {
                return self.PassportFindResource.get({type: type, passportGuid: passportGuid, partition: partition}, successcb, errorcb);
            },
            updatePassport: function (passportUpdateRequest, type, partition, successcb, errorcb) {
                return self.PassportUpdateResource.update(passportUpdateRequest, successcb, errorcb, {type: type, partition: partition});
            },
            // Используется для
            // - удаления электронного паспорта жилого дома (пользователь с кодом привилегии П_ЭП_2, ВИ_ЭПЖД_07)
            // - удаления электронного паспорта многоквартирного дома (пользователь с кодом привилегии П_ЭП_2, ВИ_ЭПМКД_12)
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.passport.api.PassportService#deleteDwellingHousePassport(ru.lanit.hcs.passport.api.dto.request.DwellingHousePassportDeleteRequest)}
            // @param passportDeleteRequest - Запрос на удаление сведений электронного паспорта жилого дома
            // @throws PassportNotFoundException - ЭП не найден в реестре ЭП
            // @throws HouseManagementException - несоответствие организации, выполняющей действие, обслуживающей организации по данному адресу
            // @throws InvalidStatusException - статус ЭП не черновик
            deletePassport: function (passportRemoveRequest, type, successcb, errorcb) {
                return self.PassportRemovalResource.remove(passportRemoveRequest, successcb, errorcb, {type: type});
            },
            // Используется для
            // - утверждения электронного паспорта жилого дома (пользователь с кодом привилегии П_ЭП_3, ВИ_ЭПЖД_13)
            // - утверждения электронного паспорта многоквартирного дома (пользователь с кодом привилегии П_ЭП_3, ВИ_ЭПМКД_10)
            // @param passportApproveRequest - Запрос на утверждение сведений электронного паспорта многоквартирного дома
            // @throws PassportNotFoundException - ЭП не найден в реестре ЭП
            // @throws HouseManagementException - несоответствие организации, выполняющей действие, обслуживающей организации по данному адресу
            // @throws InvalidStatusException - статус ЭП не черновик
            // @throws FieldValidationException - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            approvePassportWithSyncCheck: function (passportApproveRequest, type, successcb, errorcb) {
                return self.PassportApprovalWithSycnCheckResource.update(passportApproveRequest, successcb, errorcb, {type: type});
            },
            // Используется для
            // - утверждения электронного паспорта многоквартирного дома (пользователь с кодом привилегии П_ЭП_3, ВИ_ЭПМКД_10)
            // - утверждения электронного паспорта жилого дома (пользователь с кодом привилегии П_ЭП_3, ВИ_ЭПЖД_13)
            // @param passportApproveRequest - Запрос на утверждение сведений электронного паспорта жилого дома
            // @throws PassportNotFoundException - ЭП не найден в реестре ЭП
            // @throws HouseManagementException - несоответствие организации, выполняющей действие, обслуживающей организации по данному адресу
            // @throws InvalidStatusException - статус ЭП не черновик
            // @throws FieldValidationException - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            approvePassport: function (passportApproveRequest, successcb, errorcb) {
                return self.PassportApprovalResource.update(passportApproveRequest, successcb, errorcb, {type: type});
            },
            // Метод выполняет поиск мест ввода инженерных систем в доме, удовлетворяющих критериям поиска
            // Используется для
            // - поиска мест ввода инженерных систем в доме для изменения данных в карточке электронного паспорта многоквартирного дома со статусом Черновик (пользователь с кодом привилегии П_ЭП_2, П_ЭП_3; ВИ_ЭПМКД_15, ВИ_ЭПМКД_16, ВИ_ЭПМКД_46)
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.passport.api.PassportService#findHouseEngineeringSystemPlaces(ru.lanit.hcs.passport.api.dto.search.HouseEngineeringSystemPlacesSearchCriteria)}
            // @param houseEngineeringSystemPlacesSearchCriteria - критерии поиска;
            // @return Места ввода инженерных систем в доме, удовлетворяющих критериям поиска
            findHouseEngineeringSystemPlaces: function (houseGuid, successcb, errorcb) {
                return self.HouseEngineeringResource.get({houseGuid: houseGuid}, successcb, errorcb);
            },
            // Используется для
            // - публикации электронного паспорта многоквартирного дома (пользователь с кодом привилегии П_ЭП_4, ВИ_ЭПЖД_15)
            // - публикации электронного паспорта многоквартирного дома (пользователь с кодом привилегии П_ЭП_4, ВИ_ЭПМКД_11)
            // @param passportPublishRequest - Запрос на публикацию сведений электронного паспорта жилого дома
            // @throws PassportNotFoundException - ЭП не найден в реестре ЭП
            // @throws HouseManagementException - несоответствие организации, выполняющей действие, муниципальному образованию по данному адресу
            // @throws InvalidStatusException - некорректный статус ЭП
            // @throws FieldValidationException - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
            publish: function (passportPublishRequest, successcb, errorcb) {
                return self.PassportPublishingResource.update(passportPublishRequest, successcb, errorcb, {type: type});
            },
            send: function (passportGuid, type, successcb, errorcb) {
                return self.PassportSendResource.create({passportGuid: passportGuid}, successcb, errorcb, {type: type});
            },
            copy: function (passportGuid, type, successcb, errorcb) {
                return self.PassportSendResource.create({passportGuid: passportGuid}, successcb, errorcb, {type: type});
            }
        };
    }]);