angular.module('ru.lanit.hcs.nsi.rest.NsiInspectionService', [
        'nsi-resource'
    ]).factory('$NsiInspectionService', ['NsiResource',
        function (NsiResource) {
            var self = this;

            var toUrl =  function(part) {
                return '/nsi/inspections' + part;
            };

            self.findInspectionForm = new NsiResource(toUrl('/inspection/forms'));
            self.inspectionSubjectsResource = new NsiResource(toUrl('/inspection/subjects'));
            self.inspectionResultDocumentTypeResource = new NsiResource(toUrl('/inspection/document/types'));
            self.inspectionBaseResource = new NsiResource(toUrl('/inspection/bases'));

            return {

                findInspectionForms: function(successcb, errorcb){
                    var scb = successcb || angular.noop,
                        ecb = errorcb || angular.noop;
                    self.findInspectionForm.get(null, scb, ecb);
                },

                findInspectionSubject: function(queryJson, successcb, errorcb) {
                    return self.inspectionSubjectsResource.query([], queryJson, successcb, errorcb);
                },

                findInspectionResultDocumentType: function(queryJson, successcb, errorcb) {
                    return self.inspectionResultDocumentTypeResource.query([], queryJson, successcb, errorcb);
                },

                findInspectionBase: function(queryJson, successcb, errorcb) {
                    return self.inspectionBaseResource.query([], queryJson, successcb, errorcb);
                }
            };
        }
    ]);
