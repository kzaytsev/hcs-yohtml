angular.module('ru.lanit.hcs.notification.rest.InformingService', [
    'notification-resource'
])
    .factory('$InformingService', ['NotificationResource', '$q', '$timeout', function (NotificationResource, $q, $timeout) {
        function toUrl(part) {
            return '/informings' + part;
        }

        var self = this;
        self.searchResource = new NotificationResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.userSearchResource = new NotificationResource(toUrl('/usersearch;page=:page;itemsPerPage=:itemsPerPage'));
        self.newsForModalSearchResource = new NotificationResource(toUrl('/usersearchmodal;page=:page;itemsPerPage=:itemsPerPage'));
        self.markNewsResource = new NotificationResource(toUrl('/viewed'));
        self.findNewsResource = new NotificationResource(toUrl('/:guid'));
        self.actionNewsResource = new NotificationResource(toUrl('/'));
        self.deleteNewsResource = new NotificationResource(toUrl(''));
        self.sendNewsResource = new NotificationResource(toUrl('/sending'));
        self.settingsResource = new NotificationResource(toUrl('/settings/presentation'));
        self.countNotificationsResource = new NotificationResource(toUrl('/user/notifications/count'));

        return {
            /**
             * Метод используется для поиска новостей (Пользователь с ролью «Должностное лицо организации», полномочием
             * пользователя «Уполномоченный специалист», полномочием организации «Управляющая организация» и привилегией «Настройка новостной ленты»;
             * Пользователь с ролью «Должностное лицо организации», полномочием пользователя «Уполномоченный специалист»,
             * полномочием организации «Ресурсоснабжающая организация» и привилегией «Настройка новостной ленты»;)
             *
             * @param {Object} criteria - критерии поиска новостей
             * @param {Number} pageIndex - Номер страницы, для которой необходимо сформировать данные
             * @param {Number} itemsPerPage - Количество элементов на странице
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @return {Array} Список кратких сведений о новостях, удовлетворяющих критериям поиска
             */
            findNews: function (criteria, pageIndex, itemsPerPage, successcb, errorcb) {
                return self.searchResource.queryPost({
                    page: pageIndex ? pageIndex : 1,
                    itemsPerPage: itemsPerPage
                }, criteria, successcb, errorcb);
            },
            /**
             * Метод используется для поиска новостей этого пользователя (Плательщик)
             *
             * @param {Object} criteria - критерии поиска новостей
             * @param {Number} pageIndex - Номер страницы, для которой необходимо сформировать данные
             * @param {Number} itemsPerPage - Количество элементов на странице
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @return {Array} Список кратких сведений о новостях, удовлетворяющих критериям поиска
             */
            findNewsForUser: function (criteria, pageIndex, itemsPerPage, successcb, errorcb) {
                return self.userSearchResource.queryPost({
                    page: pageIndex ? pageIndex : 1,
                    itemsPerPage: itemsPerPage
                }, criteria, successcb, errorcb);
            },
            findNewsForUserModal: function (criteria, pageIndex, itemsPerPage, successcb, errorcb) {
                return self.newsForModalSearchResource.queryPost({
                    page: pageIndex ? pageIndex : 1,
                    itemsPerPage: itemsPerPage
                }, criteria, successcb, errorcb);
            },
            /**
             * Метод используется для отображения новости, как просмотренное
             *
             * @param {Object} notificationsGuids - идентификаторы новостей
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            markNewsAsViewed: function (notificationsGuids, successcb, errorcb) {
                self.markNewsResource.queryPost({}, {notificationsGuids: notificationsGuids, isViewed: true}, successcb, errorcb);
            },
            /**
             * Метод используется для удаления новости
             *
             * @param {String} guid - идентификатор новости
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            deletePartNews: function (guid, successcb, errorcb) {
                self.deleteNewsResource.remove(guid, successcb, errorcb);
            },
            /**
             * Метод используется для публикации новости
             *
             * @param {String} guid - идентификатор новости
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            sendPartNews: function (guid, successcb, errorcb) {
                self.sendNewsResource.update({guid: guid}, successcb, errorcb);
            },
            /**
             * Метод находит новости
             *
             * @param guid - идентификатор новости
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @returns {Object} информация об новости
             */
            findPartNews: function (guid, successcb, errorcb) {
                return self.findNewsResource.get({guid: guid}, successcb, errorcb);
            },
            /**
             * Метод используется для сохранения новости
             *
             * @param {Object} partNews - новости
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            savePartNews: function (partNews, successcb, errorcb) {
                return self.actionNewsResource.create({
                    notification: partNews
                }, successcb, errorcb);
            },
            /**
             * Метод используется для обновления новости
             *
             * @param {Object} partNews - новости
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            updatePartNews: function (partNews, successcb, errorcb) {
                return self.actionNewsResource.update({
                    updatedNotification: partNews
                }, successcb, errorcb);
            },
            /**
             * Метод сохраняет настройки отображений новостей в модальном окне
             *
             * @param {Object} newsPresentationSettings - настройки ru.lanit.hcs.notification.dto.NewsPresentationSettings
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            saveNewsPresentationSettings: function (newsPresentationSettings, successcb, errorcb) {
                self.settingsResource.queryPost({}, {
                    newsPresentationSettings: newsPresentationSettings
                }, successcb, errorcb);
            },
            /**
             * Для отображения индексов в ЛК
             *
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             */
            countNotificationsForUser: function (successcb, errorcb) {
                return self.countNotificationsResource.get({}, successcb, errorcb);
            }
        };
    }])
;