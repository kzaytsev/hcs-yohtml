/**
 * Created by vkorzhov on 11/7/14.
 */
angular
    .module('ru.lanit.hcs.inspection.rest.InspectionPlanService', [
        'inspection-resource'
    ])
    .factory('$InspectionPlanService', ['$q', 'InspectionResource', function($q, InspectionResource){
        var self = this,
            toUrl = function(part){
                return '/inspection/plans' + part;
            };
        self.inspectionPlansSearchResource = new InspectionResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.inspectionPlanFindResource = new InspectionResource(toUrl('/:guid'));
        self.searchPlannedExaminationsResource = new InspectionResource(toUrl('/examinations;page=:page;itemsPerPage=:itemsPerPage'));
        self.inspectionPlanCreateResource = new InspectionResource(toUrl("/"));
        self.inspectionPlanDeleteResource = new InspectionResource(toUrl("/"));
        self.inspectionPlanSignResource = new InspectionResource(toUrl("/signing/:guid"));
        self.inspectionPlanGetExaminationMaxOrderNumberResource = new InspectionResource(toUrl("/examination/max/number/:guid"));

        return {
            /**
             * поиск планов проверок для ЭФ_РПП_ПР
             * @param pathParams
             * @param searchParams
             * @param successcb
             * @param errorcb
             * @returns {Promise}
             */
            search: function(pathParams, searchParams, successcb, errorcb){
                var scb = successcb || angular.noop,
                    ecb = errorcb || angular.noop;
                return self.inspectionPlansSearchResource.queryPost(pathParams, searchParams, scb, ecb);
            },
            getInspectionPlan: function(guid, successcb, errorcb){
                return self.inspectionPlanFindResource.get({guid: guid}, successcb, errorcb);
            },
            createInspectionPlan: function(inspectionPlan, scb, ecb){
                scb = scb || angular.noop;
                ecb = ecb || angular.noop;
                return self.inspectionPlanCreateResource.create(inspectionPlan, scb, ecb);
            },
            updateInspectionPlan: function(inspectionPlan, scb, ecb){
                scb = scb || angular.noop;
                ecb = ecb || angular.noop;
                return self.inspectionPlanCreateResource.update(inspectionPlan, scb, ecb);
            },
            searchPlannedExaminations: function(pathParams, searchParams, successcb, errorcb){
                var scb = successcb || angular.noop,
                    ecb = errorcb || angular.noop;
                return self.searchPlannedExaminationsResource.queryPost(pathParams, searchParams, scb, ecb);
            },
            deleteInspectionPlan: function (guid, successcb, errorcb) {
                return self.inspectionPlanDeleteResource.remove(guid, successcb, errorcb);
            },
            getExaminationMaxOrderNumber: function (guid, successcb, errorcb) {
                return self.inspectionPlanGetExaminationMaxOrderNumberResource.get({guid: guid}, successcb, errorcb);
            },
            /**
             * Подписание документа
             *  Используется для:
             *   - РПП. 2.1.6 ВИ_ПИЖФ_08 Подписание электронного документа
             *
             * @param {String} guid - guid документа (required)
             * @param {Function} successcb - Callback Deferred.then
             * @param {Function} errorcb - Callback Deferred.then.fail
             * @throws NotFoundException - документа не найден
             * @throws FieldValidationException - не заполнены обязательные поля, неправильный формат ввода
             * @throws SuchNumberAlreadyExistsException - такой номер документа уже существует
             * @throws DocumentAlreadyPublished - документ уже опубликован
             */
            signInspectionPlan: function (guid, successcb, errorcb) {
                return self.inspectionPlanSignResource.queryPost({guid: guid}, {}, successcb, errorcb);
            }
        };
    }]);