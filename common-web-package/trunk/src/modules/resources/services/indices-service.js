angular
    .module('ru.lanit.hcs.indices.rest.IndicesService', [
        'indices-resource'
    ])
    .factory('$IndicesService', function (IndicesResource) {
        var self = this,
            toUrl = function (part) {
                return '/indices' + part;
            };
        self.searchResource = new IndicesResource(toUrl('/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.removeResource = new IndicesResource(toUrl('/:guid/removal'));
        self.publishResource = new IndicesResource(toUrl('/:guid/publishing'));
        self.rootResource = new IndicesResource(toUrl('/'));
        self.cancelPublishResource = new IndicesResource(toUrl('/:guid/publishing/cancellation'));
        self.findPeriodsResourse = new IndicesResource(toUrl('/periods'));

        return {
            /**
             *  Добавить индекс
             *  Используется для:
             *   - ВИ_СООРИ_01: Добавление индекса по субъектам
             *  @param request - тело индекса
             *  @return guid индекса
             *  @throws FieldValidationException - исключение валидации полей, содержит список проблемных полей. Используется в:
             *   - Альтернативный сценарий 2. Данные введены некорректно.
             *   - Альтернативный сценарий 5. Периоды заданы не корректно.
             *  @throws SuchIndexAlreadyExistsException - Используется в:
             *   - Альтернативный сценарий 6. По указанному периоду найдена запись в реестре.
             *  Первичная валидация должна быть также проведена на клиенте.
             */
            create: self.rootResource.create,

            /**
             *  Редактировать индексы
             *  Используется для:
             *   - ВИ_СООРИ_02: Изменение индексов по субъекту
             *  @param request - тело индекса
             *  @throws FieldValidationException - исключение валидации полей, содержит список проблемных полей. Используется в:
             *   - Альтернативный сценарий 2. Данные введены некорректно.
             *   - Альтернативный сценарий 5. Периоды заданы не корректно.
             *  @throws SuchIndexAlreadyExistsException - Используется в:
             *   - Альтернативный сценарий 6. По указанному периоду найдена запись в реестре.
             *  Первичная валидация должна быть также проведена на клиенте.
             */
            update: self.rootResource.update,

            /**
             * Поиск индексов по критериям
             * Метод используется для:
             *  - Поиск индексов
             * Метод вызывает
             * - получение названия субъекта по коду {@link ru.lanit.hcs.nsi.api.FiasService#findFiasRegion(ru.lanit.hcs.nsi.api.dto.search.FiasRegionSearchCriteria)}
             * - расчет количества предельных индексов {@link ru.lanit.hcs.indices.api.LimitIndicesService#count(java.util.List<java.lang.String>)}
             * - подсчет количества МО по субъекту {@link ru.lanit.hcs.nsi.api.ClassifierService)}
             * @param searchCriteria - критерии поиска
             * @return список индексов
             */
            findIndicesBySubject: self.searchResource.queryPost,

            /**
             * Получить индекс по guid
             *  Метод используется для:
             *   - отображение в реестре предельных индексов по МО
             *  @param guid - guid индекса (required).
             *  @return индекс
             */
            getById: self.rootResource.getById,

            /**
             *  Удалить индексы по субъекту.
             *  Используется для:
             *   - ВИ_СООРИ_03: Удаление индексов по субъекту
             *  @param request - guid индекса (required)
             */
            remove: function (guid, successCalback, errorCallback) {
                return self.removeResource.update(null, successCalback, errorCallback, {guid: guid});
            },

            /**
             *  Публикация индексов
             *  Используется для:
             *   - ВИ_СООРИ_15: Публикация индексов
             *  @param request - guid индекса
             */
            publish: function (guid, successCalback, errorCallback) {
                return self.publishResource.update(null, successCalback, errorCallback, {guid: guid});
            },

            /**
             *  Отмена публикации индексов
             *  Используется для:
             *   - ВИ_СООРИ_16: Отмена публикации индексов
             *  @param request - guid индекса
             */
            cancelPublishing: function (guid, successCalback, errorCallback) {
                return self.cancelPublishResource.update(null, successCalback, errorCallback, {guid: guid});
            },

            /**
             *  Получить список периодов заведенных индексов
             *  Используется для:
             *   - ВИ_СООРИ_15: Публикация индексов
             *  @param searchCriteria -
             *  @return список периодов заведенных индексов индексов
             */
            findPeriods: function (params, data, successcb, errorcb) {
                return self.findPeriodsResourse.queryPost(null, params, successcb, errorcb);
            }

        };
    })

;