angular.module('ru.lanit.hcs.nsi.rest.ClassifierService', [
    'nsi-resource'
]).factory('$ClassifierService', ['NsiResource',
    function (NsiResource) {
        var self = this;

        var toUrl = function (part) {
            return '/nsi/classifiers' + part;
        };

        self.AllClassifiersResource = new NsiResource(toUrl(''));
        self.OkfsResource = new NsiResource(toUrl('/okfs'));
        self.OktmoResource = new NsiResource(toUrl('/oktmo'));
        self.OktmoSearchResource = new NsiResource(toUrl('/oktmo/search;page=:page;itemsPerPage=:itemsPerPage'));
        self.OkoguResource = new NsiResource(toUrl('/okogu'));
        self.OkopfResource = new NsiResource(toUrl('/okopf'));
        self.OkeiResource = new NsiResource(toUrl('/okei'));
        self.OktmoRegionRootsResource = new NsiResource(toUrl('/oktmo/region/roots'));

        return {
            // Метод выполняет поиск всех общероссийских классификаторов
            // @return Список всех общероссийских классификаторов
            findAllClassifiers: function (queryJson, successcb, errorcb) {
                return self.AllClassifiersResource.query([], queryJson, successcb, errorcb);
            },
            
            // Метод выполняет поиск форм собственности, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.ClassifierService#findOkfs(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных форм собственности при регистрации организации
            // - поиска форм собственности для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список форм собственности, удовлетворяющих критертям поиска
            findOkfs: function (queryJson, successcb, errorcb) {
                return self.OkfsResource.query([], queryJson, successcb, errorcb);
            },
            /**
             * Метод выполняет поиск "Субъектов РФ" для формы расширенного поиска по ОКТМО
             * @param successcb
             * @param errorcb
             * @returns {*}
             */
            findOktmoRegionRoots: function(successcb, errorcb){
                return self.OktmoRegionRootsResource.queryPost([], {}, successcb, errorcb);
            },
            /**
             * Метод выполняет поиск "Субъектов РФ" для формы расширенного поиска по ОКТМО
             * @param successcb
             * @param errorcb
             *  @param queryJson
             * @returns {*}
             */
            findOktmoRegionRootsWithParams: function(queryJson,successcb, errorcb){
                return self.OktmoRegionRootsResource.queryPost([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск территорий муниципальных образований, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.ClassifierService#findOktmo(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных территорий муниципальных образований при регистрации организации
            // - поиска территорий муниципальных образований для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список территорий муниципальных образований, удовлетворяющих критертям поиска
            findOktmo: function (queryJson, successcb, errorcb) {
                return self.OktmoResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск органов государственного управления, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.ClassifierService#findOkogu(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных органов государственного управления при регистрации организации
            // - поиска органов государственного управления для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список органов государственного управления, удовлетворяющих критертям поиска
            findOkogu: function (queryJson, successcb, errorcb) {
                return self.OkoguResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск организационно-правовых форм, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.ClassifierService#findOkopf(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных организационно-правовых форм при регистрации организации
            // - поиска организационно-правовых форм для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список организационно-правовых форм, удовлетворяющих критертям поиска
            findOkopf: function (queryJson, successcb, errorcb) {
                return self.OkopfResource.query([], queryJson, successcb, errorcb);
            },
            // Метод выполняет поиск единиц измерения, удовлетворяющих критериям поиска
            // Метод вызывает
            // - метод {@link ru.lanit.hcs.nsi.api.ClassifierService#findOkei(ru.lanit.hcs.nsi.api.dto.search.NsiSearchCriteria)}
            // Используется для
            // - поиска актуальных единиц измерения при созздании ПУ
            // - поиска единиц измерения для отображения на экранных формах при просмотре
            // @param nsiSearchCriteria - криткерий поиска
            // @return Список единиц измерения, удовлетворяющих критертям поиска
            findOkei: function (queryJson, successcb, errorcb) {
                return self.OkeiResource.query([], queryJson, successcb, errorcb);
            },

            /**
             * Метод выполняет поиск территорий муниципальных образований, удовлетворяющих критериям поиска
             * Используется для (ЭФ_НСИ_ПРСМ_КЛСФ)
             * - поиска муниципальных образований для просмотра ОК(ВИ_ВОКТМО)
             * Метод вызывает
             * - метод {@link ru.lanit.hcs.nsi.api.ClassifierService#findOktmo(ru.lanit.hcs.nsi.api.dto.search.OktmoSearchCriteria, Integer, Integer)}
             * @param oktmoSearchCriteria - критерий поиска
             * @param pageIndex - Номер страницы, для которой необходимо сформировать данные
             * @param elementsPerPage - Количество элементов на странице
             * @return Список территорий муниципальных образований, удовлетворяющих критертям поиска
             */
            findOktmoExtendedSearch: function(queryJson, successcb, errorcb) {
                var pageParamsForUrl = _.pick(queryJson, 'page', 'itemsPerPage');
                var searchCriteria = _.omit(queryJson, 'page', 'itemsPerPage');
                return self.OktmoSearchResource.queryPost(pageParamsForUrl, searchCriteria, successcb, errorcb);
            }
            
        };
    }]);