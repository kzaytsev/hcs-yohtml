angular.module('ru.lanit.hcs.voting.rest.VotingService', [
    'voting-resource'
])

    .factory('$VotingService', ['VotingResource', function (VotingResource) {

        var self = this;

        var toUrl = function (part) {
            return '/voting' + part;
        };

        self.findVotingProtocolResource = new VotingResource(toUrl('/search/protocol;pageIndex=:pageIndex;elementsPerPage=:elementsPerPage'));
        self.findVotingMessageResource = new VotingResource(toUrl('/search/message;pageIndex=:pageIndex;elementsPerPage=:elementsPerPage'));

        self.findVotingVersionResource = new VotingResource(toUrl('/search/version/:votingVersionGuid'));
        self.findVotingResource = new VotingResource(toUrl('/search/voting/:votingGuid'));
        self.findVotingVersionsResource = new VotingResource(toUrl('/search/versions'));

        self.deleteVotingProtocolResource = new VotingResource(toUrl('/delete/protocol/:votingProtocolGuid'));
        self.signVotingProtocolResource = new VotingResource(toUrl('/sign/protocol'));
        self.cancelVotingProtocolChangeResource = new VotingResource(toUrl('/cancel/protocol/:votingProtocolVersionGuid'));
        self.addVotingProtocolResource = new VotingResource(toUrl('/create/protocol'));
        self.updateVotingProtocolResource = new VotingResource(toUrl('/update/protocol'));
        self.changeVotingProtocolResource = new VotingResource(toUrl('/change/protocol'));

        self.deleteVotingMessageResource = new VotingResource(toUrl('/delete/message/:votingMessageGuid'));
        self.signVotingMessageResource = new VotingResource(toUrl('/sign/message'));
        self.addVotingMessageResource = new VotingResource(toUrl('/create/message'));
        self.updateVotingMessageResource = new VotingResource(toUrl('/update/message'));
        self.getVotingMessageResource = new VotingResource(toUrl('/search/message/:votingMessageGuid'));

        self.ownerDecisionResource = new VotingResource(toUrl('/owner/decisions'));
        self.ownerWritingDecisionResource = new VotingResource(toUrl('/owner/decisions'));
        self.findOwnerWritingDecisionsResource = new VotingResource(toUrl('/search/owner/writing/decisions'));
        self.findOwnerMadeDecisionsResource = new VotingResource(toUrl('/search/owner/decisions'));

        self.findWritingDecisionsResource = new VotingResource(toUrl('/search/writing/decisions'));

        return {
            deleteVotingProtocol: function (votingProtocolGuid, successcb, errorcb) {
                return self.deleteVotingProtocolResource.update(null, successcb, errorcb, {votingProtocolGuid: votingProtocolGuid});
            },
            signVotingProtocol: function (votingProtocolGuid, successcb, errorcb) {
                return self.signVotingProtocolResource.update({protocolGuid: votingProtocolGuid}, successcb, errorcb);
            },
            cancelVotingProtocolChange: function (votingProtocolVersionGuid, successcb, errorcb) {
                return self.cancelVotingProtocolChangeResource.update(null, successcb, errorcb, {votingProtocolVersionGuid: votingProtocolVersionGuid});
            },
            addVotingProtocol: function (votingProtocolCreateRequest, successcb, errorcb) {
                return self.addVotingProtocolResource.create(votingProtocolCreateRequest, successcb, errorcb);
            },
            updateVotingProtocol: function (votingProtocolUpdateRequest, successcb, errorcb) {
                return self.updateVotingProtocolResource.update(votingProtocolUpdateRequest, successcb, errorcb);
            },
            changeVotingProtocol: function (votingProtocolUpdateRequest, successcb, errorcb) {
                return self.changeVotingProtocolResource.update(votingProtocolUpdateRequest, successcb, errorcb);
            },

            deleteVotingMessage: function (votingMessageGuid, successcb, errorcb) {
                return self.deleteVotingMessageResource.update(null, successcb, errorcb, {votingMessageGuid: votingMessageGuid});
            },
            signVotingMessage: function (votingMessageGuid, successcb, errorcb) {
                return self.signVotingMessageResource.update({messageGuid: votingMessageGuid}, successcb, errorcb);
            },
            addVotingMessage: function (votingMessageCreateRequest, successcb, errorcb) {
                return self.addVotingMessageResource.create(votingMessageCreateRequest, successcb, errorcb);
            },
            /**
             * Метод выполняет поиск решений собсвенников переданых в письменной форме по конретному вопросу  по его идентификационному коду
             * Используется для (ЭФ_ЖКХГ_СПРС)
             * - просмотра решений собсвенников переданых в письменной форме по конретному вопросу (ВИ_ЖКХГ_ПСРС)
             * @param ownerWritingDecisionsSearchCriteria - критерии поиска
             * @param pageIndex - Номер страницы, для которой необходимо сформировать данные
             * @param elementsPerPage - Количество элементов на странице
             * @return Инофрмация о результатах голосования собсвенника
             */
            findOwnerWritingDecisions: function (ownerWritingDecisionsSearchCriteria, successcb, errorcb) {
                return self.findOwnerWritingDecisionsResource.queryPost(angular.noop, ownerWritingDecisionsSearchCriteria, successcb, errorcb);
            },

            /**
             * Метод выполняет поиск сведений о результатах голосования собсвенника по его идентификационному коду
             * Используется для (ЭФ_ЖКХГ_РГУ)
             * - просмотра информации о результатах голосования собсвенника (ВИ_ЖКХГ_ПРГУ)
             * @param ownerMadeDecisionSearchCriteria - критерии поиска
             * @param pageIndex - Номер страницы, для которой необходимо сформировать данные
             * @param elementsPerPage - Количество элементов на странице
             * @return Инофрмация о результатах голосования собсвенника
             */
            findOwnerMadeDecision: function (ownerMadeDecisionSearchCriteria, successcb, errorcb) {
                return  self.findOwnerMadeDecisionsResource.queryPost(angular.noop, ownerMadeDecisionSearchCriteria, successcb, errorcb);
            },
           /////////////////////
            findVotingVersion: function (votingVersionGuid, successcb, errorcb) {
                return self.findVotingVersionResource.get({votingVersionGuid: votingVersionGuid}, successcb, errorcb);
            },
            findVotingProtocol: function (votingSummarySearchCriteria, matrixParam, successcb, errorcb) {
                return self.findVotingProtocolResource.queryPost(matrixParam, votingSummarySearchCriteria, successcb, errorcb);
            },
            findVotingMessage: function (votingSummarySearchCriteria, matrixParam, successcb, errorcb) {
                return self.findVotingMessageResource.queryPost(matrixParam, votingSummarySearchCriteria, successcb, errorcb);
            },
            findVoting: function (votingGuid, successcb, errorcb) {
                return self.findVotingResource.get({votingGuid: votingGuid}, successcb, errorcb);
            },
            findVotingVersions: function(votingRefSearchCriteria, successcb, errorcb) {
                return self.findVotingVersionsResource.queryPost(angular.noop, votingRefSearchCriteria, successcb, errorcb);
            },
            getVotingMessage: function (votingMessageGuid, successcb, errorcb) {
                return self.getVotingMessageResource.get({votingMessageGuid: votingMessageGuid}, successcb, errorcb);
            },
            updateVotingMessage: function (votingMessageUpdateRequest, successcb, errorcb) {
                return self.updateVotingMessageResource.update(votingMessageUpdateRequest, successcb, errorcb);
            },

            /**
             * Участие собственника в голосовании
             * @param addOwnerDecisionCreateRequest  - Запрос на внесение решения собственника
             * @throws MessageNotFoundException - сообщение о голосовании не найдено в реестре
             * @throws DecisionAlreadyExistException - по данному вопросу уже есть решение собственника
             */
            addOwnerDecision: function (addOwnerDecisionCreateRequest, successcb, errorcb) {
                return self.ownerDecisionResource.create(addOwnerDecisionCreateRequest, successcb, errorcb);
            },
            /**
             * Внесение решения собственника, переданного в письменной форме, к сообщению о проведении ОСС (ВИ_ЖКХГ_ВРС,ВИ_ЖКХГ_СХР)
             * @param ownerWritingDecisionCreateRequest  - Запрос на внесение решения собственника
             * @throws FieldValidationException - не пройдены проверки на корректность заполнения полей (обязательность, формат и т.п.)
             * @throws MessageNotFoundException - сообщение о голосовании не найдено в реестре
             * @throws DecisionAlreadyExistException - по данному вопросу уже есть решение собственника
             */
            addOwnerWritingDecision: function (addOwnerWritingDecisionCreateRequest, successcb, errorcb) {
                return self.ownerWritingDecisionResource.update(addOwnerWritingDecisionCreateRequest, successcb, errorcb);
            },

            findWritingDecisions: function (writingDecisionsSearchCriteria, matrixParam, successcb, errorcb) {
                return self.findWritingDecisionsResource.queryPost(matrixParam, writingDecisionsSearchCriteria, successcb, errorcb);
            }
       };
    }])

;