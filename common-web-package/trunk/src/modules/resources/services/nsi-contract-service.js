angular.module('ru.lanit.hcs.nsi.rest.NsiContractService', [
    'nsi-resource'
]).factory('$NsiContractService',
    function (NsiResource) {
        var self = this;

        var toUrl =  function(part) {
            return '/nsi/contracts' + part;
        };

        self.ContractConclusionReasonResource = new NsiResource(toUrl('/contract/reasons'));
        self.ContractStatusResource = new NsiResource(toUrl('/contract/statuses'));
        self.EncumbranceTypeResource = new NsiResource(toUrl('/encumbrance/types'));
        self.ContractTerminationReasonResource = new NsiResource(toUrl('/contract/termination/reasons'));
        self.ContractApprovalStatusResource = new NsiResource(toUrl('/contract/approval/statuses'));

        return {

            /**
             * Метод выполняет поиск по справочнику "Основание заключения договора", удовлетворяющих критериям поиска
             * Используется для
             * - поиска Основание заключения договора для отображения на экранных формах при просмотре
             * @param nsiSearchCriteria - критерий поиска
             * @return Список Основание заключения договора, удовлетворяющих критериям поиска
             */
            findContractConclusionReason: function (queryJson, successcb, errorcb) {
                return self.ContractConclusionReasonResource.query([], queryJson, successcb, errorcb);
            },

            /**
             * Метод выполняет поиск по справочнику "Состояние электронного документа", удовлетворяющих критериям поиска
             * Используется для
             * - поиска Состояние электронного документа для отображения на экранных формах при просмотре
             * @param nsiSearchCriteria - критерий поиска
             * @return Список Состояние электронного документа, удовлетворяющих критериям поиска
             */
            findContractStatus: function (queryJson, successcb, errorcb) {
                return self.ContractStatusResource.query([], queryJson, successcb, errorcb);
            },

            /**
             * Метод выполняет поиск по справочнику "Тип обременения", удовлетворяющих критериям поиска
             * Используется для
             * - поиска типов обременения для отображения на экранных формах при просмотре
             * @param nsiSearchCriteria - критерий поиска
             * @return Список типов обременения, удовлетворяющих критериям поиска
             */
            findEncumbranceType: function (queryJson, successcb, errorcb) {
                return self.EncumbranceTypeResource.query([], queryJson, successcb, errorcb);
            },

            /**
             * Метод выполняет поиск по справочнику "Причина расторжения договора", удовлетворяющих критериям поиска
             * Используется для
             * - поиска типов обременения для отображения на экранных формах при просмотре
             * @param nsiSearchCriteria - критерий поиска
             * @return Список типов обременения, удовлетворяющих критериям поиска
             */
            findContractTerminationReason: function (queryJson, successcb, errorcb) {
                return self.ContractTerminationReasonResource.query([], queryJson, successcb, errorcb);
            },

            /**
             * Метод выполняет поиск по справочнику "Статус утверждения", удовлетворяющих критериям поиска
             * Используется для
             * - поиска Статус утверждения для отображения на экранных формах при просмотре
             * @param nsiSearchCriteria - критерий поиска
             * @return Список Статус утверждения, удовлетворяющих критериям поиска
             */
            findContractApprovalStatus: function (queryJson, successcb, errorcb) {
                return self.ContractApprovalStatusResource.query([], queryJson, successcb, errorcb);
            }

        };
    }
);
