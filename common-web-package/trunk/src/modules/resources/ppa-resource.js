angular.module('ppa-resource', ['backend-resource'])

    .factory('PpaResource', ['BackendResource', 'PPA_BACKEND_CONFIG', function (BackendResource, PPA_BACKEND_CONFIG) {
        function PpaResourceFactory(resourceUrl) {
            return new BackendResource(PPA_BACKEND_CONFIG, resourceUrl);
        }
        return PpaResourceFactory;
    }])
;