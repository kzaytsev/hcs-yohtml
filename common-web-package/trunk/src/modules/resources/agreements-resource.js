angular
    .module('agreements-resource', ['backend-resource'])
    .factory('AgreementResource', [
        'BackendResource',
        'AGREEMENTS_BACKEND_CONFIG',
        function (BackendResource, AGREEMENTS_BACKEND_CONFIG) {
            return function (resourceUrl) {
                return new BackendResource(
                    AGREEMENTS_BACKEND_CONFIG, resourceUrl);
            };
        }
    ]);