angular.module('home-mgmt-resource', ['backend-resource'])
    .factory('HomeManagementResource', ['BackendResource', 'HOME_MGMT_BACKEND_CONFIG', function (BackendResource, HOME_MGMT_BACKEND_CONFIG) {
        function HomeManagementResourceFactory(resourceUrl) {
            return new BackendResource(HOME_MGMT_BACKEND_CONFIG, resourceUrl);
        }
        return HomeManagementResourceFactory;
    }])
;