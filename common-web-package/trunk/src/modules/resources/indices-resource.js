angular.module('indices-resource', ['backend-resource'])

    .factory('IndicesResource', function (BackendResource, INDICES_BACKEND_CONFIG) {
        return function (resourceUrl) {
            return new BackendResource(INDICES_BACKEND_CONFIG, resourceUrl);
        };
    });
