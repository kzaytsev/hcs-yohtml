angular
    .module('bills-resource', ['backend-resource'])
    .factory('BillsResource', ['BackendResource', 'BILLS_BACKEND_CONFIG',
        function (BackendResource, BILLS_BACKEND_CONFIG) {
            function BillsResourceFactory(resourceUrl) {
                return new BackendResource(BILLS_BACKEND_CONFIG, resourceUrl);
            }

            return BillsResourceFactory;
        }
    ]);
