angular
    .module('acquiring-resource', ['backend-resource'])
    .factory('AcquiringResource', ['BackendResource', 'ACQUIRING_BACKEND_CONFIG',
        function (BackendResource, ACQUIRING_BACKEND_CONFIG) {
            function AcquiringResourceFactory(resourceUrl) {
                return new BackendResource(ACQUIRING_BACKEND_CONFIG, resourceUrl);
            }

            return AcquiringResourceFactory;
        }
    ]);
