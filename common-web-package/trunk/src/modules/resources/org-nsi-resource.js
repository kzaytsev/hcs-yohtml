angular
    .module('org-nsi-resource', ['backend-resource'])
    .constant('ORG_NSI_BACKEND_CONFIG', {
        baseUrl: '/orgnsi/api/rest/services'
    })
    .factory('OrgNsiResource', ['BackendResource', 'ORG_NSI_BACKEND_CONFIG', function (BackendResource, ORG_NSI_BACKEND_CONFIG) {
        return function (resourceUrl) {
            return new BackendResource(ORG_NSI_BACKEND_CONFIG, resourceUrl);
        };
    }]);