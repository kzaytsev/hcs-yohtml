angular.module('eventjournal-resource', ['backend-resource'])

    .factory('EventJournalResource', function (BackendResource, EVENT_JOURNAL_BACKEND_CONFIG) {
        return function EventJournalResourceFactory(resourceUrl) {
            return new BackendResource(EVENT_JOURNAL_BACKEND_CONFIG, resourceUrl);
        };
    });
