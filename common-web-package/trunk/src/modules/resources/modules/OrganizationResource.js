angular.module('resources.organizationCommonResource', ['nsi-resource'])

.factory('OrganizationCommonResource', ['PpaResource',
    function (NSIResource) {
        return new NSIResource("/ppa/organizations/search");
    }
])

;