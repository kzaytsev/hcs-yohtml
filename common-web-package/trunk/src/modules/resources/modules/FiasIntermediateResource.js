/**
 * Created by mputov on 20.01.2015.
 */
angular.module('resources.fiasIntermediateResource', ['nsi-resource'])

    .factory('FiasIntermediateResource', ['NsiResource',
        function (NsiResource) {
            return {
                region          : function() { return new NsiResource("/nsi/fias/intermediate/regions")          ; },
                area            : function() { return new NsiResource("/nsi/fias/intermediate/areas")            ; },
                city            : function() { return new NsiResource("/nsi/fias/intermediate/cities")           ; },
                settlement      : function() { return new NsiResource("/nsi/fias/intermediate/settlements")      ; },
                street          : function() { return new NsiResource("/nsi/fias/intermediate/streets")          ; },
                houseNumber     : function() { return new NsiResource("/nsi/fias/intermediate/house/numbers")    ; },
                building        : function() { return new NsiResource("/nsi/fias/intermediate/buildings")        ; },
                struct          : function() { return new NsiResource("/nsi/fias/intermediate/structs")          ; },
                house           : function() { return new NsiResource("/nsi/fias/intermediate/houses")           ; }
            };
        }
    ])
;