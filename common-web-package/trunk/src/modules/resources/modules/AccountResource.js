angular.module('resources.accountCommonResource', ['home-mgmt-resource', 'nsi-resource'])

    .factory('AccountsCloseReasonsResource', ['NsiResource',
        function (NsiResource) {
            return new NsiResource('/nsi/accounts/close/reasons');
        }
    ])

    .factory('AccountsCountResource', ['HomeManagementResource',
        function (HomeManagementResource) {
            return new HomeManagementResource('/accounts/count');
        }
    ])
;
