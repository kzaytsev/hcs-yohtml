angular.module('resources.fiasResource', ['nsi-resource'])

.factory('FiasResource', ['NsiResource',
    function (NsiResource) {
        return {
            region          : function() { return new NsiResource("/nsi/fias/regions")          ; },
            area            : function() { return new NsiResource("/nsi/fias/areas")            ; },
            city            : function() { return new NsiResource("/nsi/fias/cities")           ; },
            settlement      : function() { return new NsiResource("/nsi/fias/settlements")      ; },
            street          : function() { return new NsiResource("/nsi/fias/streets")          ; },
            houseNumber     : function() { return new NsiResource("/nsi/fias/house/numbers")    ; },
            building        : function() { return new NsiResource("/nsi/fias/buildings")        ; },
            struct          : function() { return new NsiResource("/nsi/fias/structs")          ; },
            house           : function() { return new NsiResource("/nsi/fias/houses")           ; }
        };
    }
])
;