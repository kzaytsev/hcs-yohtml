angular.module('resources.houseCommonResource', ['home-mgmt-resource', 'nsi-resource'])

    .factory('AppartmentSearchCommonResource', ['HomeManagementResource',
        function (HomeManagementResource) {
            return new HomeManagementResource("/houses/apartments/search;page=:page;itemsPerPage=:itemsPerPage");
        }
    ])

    .factory('HouseSearchCommonResource', ['HomeManagementResource',
        function (HomeManagementResource) {
            return new HomeManagementResource("/houses/search;page=:page;itemsPerPage=:itemsPerPage");
        }
    ])

    .factory('RoomSearchCommonResource', ['HomeManagementResource',
        function (HomeManagementResource) {
            return new HomeManagementResource("/houses/rooms/select/search;page=:page;itemsPerPage=:itemsPerPage");
        }
    ])

    .factory('HouseByOrgSearchCommonResource', ['HomeManagementResource',
        function (HomeManagementResource) {
            return new HomeManagementResource("/houses/search-by-org;page=:page;itemsPerPage=:itemsPerPage");
        }
    ])

    .factory('HouseTypeCommonResource', ['NsiResource',
        function (NsiResource) {
            return new NsiResource("/nsi/houses/types");
        }
    ])

    .factory('AppartmentCategoryCommonResource', ['NsiResource',
        function (NsiResource) {
            return new NsiResource("/nsi/apartments/categories");
        }
    ])

    .factory('FlatCommonResource', ['NsiResource',
        function (NsiResource) {
            return new NsiResource("/nsi/flat/search");
        }
    ])

    .factory('HousesCountResource', ['HomeManagementResource',
        function (HomeManagementResource) {
            return new HomeManagementResource('/houses/count');
        }
    ])

    .factory('GetHouseResource', ['HomeManagementResource',
        function (HomeManagementResource) {
            return new HomeManagementResource('/houses/:houseTypeCode/:houseGuid');
        }
    ])
    .factory('ServiceTypeResource', ['NsiResource',
        function (NsiResource) {
            return new NsiResource("/nsi/houses/service/types?actual=:actual");
        }
    ])
;
