angular.module('resources.classifierResource', ['nsi-resource'])

    .factory('ClassifierResource', ['NsiResource',
        function (NsiResource) {
            var rootResource = new NsiResource("/nsi/hierarchical/dictionaries/nodes/roots");
            var searchResource = new NsiResource("/nsi/hierarchical/dictionaries/nodes/search");
            var childrenResource = new NsiResource("/nsi/hierarchical/dictionaries/nodes/children");
            var headerResource = new NsiResource("/nsi/hierarchical/dictionaries/headers");


            return {
                roots : function() {
                    return rootResource;
                },
                search: function() {
                    return searchResource;
                },
                children : function() {
                    return childrenResource;
                },
                header :function(){
                    return headerResource;
                }
            };
        }
    ])

    .factory('CommonReferenceResource', ['NsiResource',
        function (NsiResource) {
            return new NsiResource("/nsi/dictionaries/table/data" );
        }
    ])
;