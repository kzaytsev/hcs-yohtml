angular
    .module('resources.eventResource', ['backend-resource'])
    .factory('EventResource', function (BackendResource, EVENT_JOURNAL_BACKEND_CONFIG) {
        return {
            typeList: function () {
                return new BackendResource(EVENT_JOURNAL_BACKEND_CONFIG, '/journal/event/typelist');
            },
            search: function(){
                return new BackendResource(EVENT_JOURNAL_BACKEND_CONFIG, '/journal/search;page=:page;itemsPerPage=:itemsPerPage');
            }
        };
    });