/**
 * Блок сведений о физическом лице (ЭФ_СВФЛ)
 */
angular
    .module('common.ef-svfl', [
        'common.required-field',
        'common.hcs-datepicker',
        'lodash'
    ])

    /**
     * disabledMode - признак того, используется ли директива в режиме просмотра
     * */
    .directive('efSvflForm', function () {
        return {
            restrict: 'E',
            scope: {
                person: '=',
                disabledMode: '=',
                editMode: '='
            },
            templateUrl: 'ef-svfl/ef-svfl.tpl.html',
            controller: 'efSvflController'
        };
    })

    .controller('efSvflController', [
        '$scope',
        '_',
        function($scope,
                 _){

            $scope.$watch('person', function(newValue, oldValue){
                $scope.snilsDisabled = $scope.editMode && !_.isEmpty($scope.person.snils);
                $scope.fioDisabled = $scope.editMode && _.isEmpty($scope.person.snils);
                $scope.birthDateDisabled = $scope.editMode && _.isEmpty($scope.person.snils);
            });

            $scope.birthDateOptions = {
                date: null,
                format: 'dd.MM.yyyy',
                appendToBody: false,
                required: true
            };

        }
    ])
;
