/**
 * Блок сведений о юридическом лице (ЭФ_СВЮЛ)
 */
angular
    .module('common.ef-svul', [
        'common.required-field',
        'common.hcs-datepicker',
        'common.ef-spr',
        'common.ef-vva',
        'common.validate',
        'lodash'
    ])

    /**
     * disabledMode - признак того, используется ли директива в режиме просмотра
     * editMode - режим редактирования?
     * */
    .directive('efSvulForm', function () {
        return {
            restrict: 'E',
            scope: {
                organization: '=',
                disabledMode: '=',
                editMode: '=',
                showErrors: '@'
            },
            templateUrl: 'ef-svul/ef-svul.tpl.html',
            controller: 'efSvulController'
        };
    })

    .controller('efSvulController', [
        '$scope',
        'referenceChooserDialog',
        'addressChooserDialog',
        '_',
        function($scope,
                 referenceChooserDialog,
                 addressChooserDialog,
                 _){

            if (!_.isUndefined($scope.$parent.$eval($scope.showErrors))) {
                $scope.$parent.$watch($scope.showErrors, function(newValue, oldValue) {
                    $scope.showError = newValue;
                });
            }

            $scope.selectOkopf = function () {
                referenceChooserDialog
                    .setReferenceName('ОКОПФ')
                    .setType('Okopf')
                    .hasRadio(true)
                    .show().then(function (result) {
                        $scope.organization.okopf = {guid : result.guid , code: result.code, name: result.fieldValue2};
                    },
                    function () {});
            };

            $scope.selectAddress = function() {
                addressChooserDialog.show().then(function(result) {
                    $scope.organization.mainAddress = result;

                });
            };

            $scope.stateRegistrationDateOptions = {
                date: null,
                format: 'dd.MM.yyyy',
                appendToBody: false,
                required: false
            };

        }
    ])

    .directive('okopf', function () {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                ngModel.$formatters.push(function(value) {
                    if (value) {
                        return value.code + ' - ' + value.name;
                    }
                    return null;
                });

            }
        };
    })

    .directive('factualAddress', function () {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                ngModel.$formatters.push(function(value) {
                    if (value) {
                        return value.address || value.formattedAddress;
                    }
                    return null;
                });

            }
        };
    })

;
