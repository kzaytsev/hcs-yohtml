/**
 * 2.4    Блок поиска адреса по ФИАС (ЭФ_ПА)
 */

angular.module('common.ef-pa', [
    'lodash',
    'ui.select2',
    'resources.fiasResource',
    'common.auth',
    'common.required-field',
    'common.utils',
    'common.dialogs'
])
    .directive('efPaForm', function () {
        return {
            restrict: 'E',
            scope: {
                searchParameters: '=filter',
                required: '@useRequired',
                splitable: '=?',
                meta: '=?',
                //для того, что-бы можно было указывать коды октмо, откуда брать адреса
                //должен быть массив строк.
                oktmoCodes: '=?',
                fiasSearchResource:'=?',
                /**
                 * Позволяет устанавливать флаги видимости и задисейбливания всех полей, а так же фильтрацию значений
                 * поля Субъект РФ. Настройки полей по умолчанию - объект defaultFieldParams. Достаточно
                 * передавать настройку только нужных полей. Для активации фильтра поля Субъект РФ нужно передавать в
                 * fieldParams.region.filter массив с резрешёнными guid-ами.
                 *
                 * Пример передаваемого fieldParams:
                 *
                 * $scope.fieldParams ={
                 *     region: {
                       disabled: false,
                       hidden: false,
                       filter: ['fc5cde20-c0cd-4c17-b250-2b9e32bd82dd','1ae9b068-1c65-4ab5-8b67-7b8fef389537']
                    },
                    area: {
                        disabled: false,
                        hidden: true
                    }
                  };
                 *
                 * */
                fieldParams: '=?'

            },
            templateUrl: 'ef-pa/ef-pa-form.tpl.html',
            transclude: true,
            controller: 'efPaController'
        };
    })
    .controller('efPaController', ['_', '$scope', 'FiasResource', 'Auth', 'commonDialogs', '$filter','$browser',
    function (_, $scope, FiasResource, Auth, commonDialogs, $filter, $browser) {
        if (!$scope.meta) {
            $scope.meta = {};
        }
        if (!$scope.oktmoCodes) {
            $scope.oktmoCodes = [];
        }
        if(!angular.isDefined($scope.required)) {
            $scope.required = false;
        }
        //сделано для того, что-бы переопределить способ, которым заполняются поля на форме ввода
        //т.е. использовать другой рест
        $scope.fiasSearchLocalService = $scope.fiasSearchResource?$scope.fiasSearchResource:FiasResource;


        var defaultFieldParams = {
            region: {
                disabled: false,
                hidden: false,
                filter: null
            },

            area: {
                disabled: false,
                hidden: false
            },

            city: {
                disabled: false,
                hidden: false
            },

            settlement: {
                disabled: false,
                hidden: false
            },

            street: {
                disabled: false,
                hidden: false
            },
            house: {
                disabled: false,
                hidden: false
            },
            building: {
                disabled: false,
                hidden: false
            },
            struct: {
                disabled: false,
                hidden: false
            }
        };

        if($scope.fieldParams != null){
            $scope.fieldParams = angular.extend(defaultFieldParams,  $scope.fieldParams);
        }else{
            $scope.fieldParams = defaultFieldParams;
        }


        function resetComplexValues(deletedNames, complexName) {
            if (_.indexOf(deletedNames, complexName + 'Number') > -1) {
                delete $scope[complexName];
                delete $scope.meta[complexName];
                delete $scope[complexName + 's'];
            }
        }

        function resetParameters(deletedNames) {
            _.each(deletedNames, function(name){
                delete $scope.searchParameters[name];
            });

            resetComplexValues(deletedNames, 'house');
            resetComplexValues(deletedNames, 'building');
            resetComplexValues(deletedNames, 'struct');
        }

        function checkFederalRegion() {
            if ($scope.searchParameters && $scope.searchParameters.regionGuid) {
                $scope.findAndExecute('region', function (region) {
                    if (region.formalName == 'Москва' ||
                            region.formalName == 'Санкт-Петербург' ||
                            region.formalName == 'Севастополь') {

                        $scope.federalRegion = true;
                    }
                });
            }
        }

        $scope.$watch('searchParameters.regionGuid', function() {
            $scope.federalRegion = false;
            checkFederalRegion();
            $scope.updateParametersWithAddress();
        });

        $scope.regionChange = function() {
            $scope.federalRegion = false;
            resetParameters([
                'areaGuid',
                'settlementGuid',
                'cityGuid',
                'streetGuid',
                'houseNumber',
                'buildingNumber',
                'structNumber',
                'flatNumber'
            ]);

            $scope.getHouseNumbersBuildingsAndStructs();
            checkFederalRegion();
            $scope.updateParametersWithAddress();
        };

        $scope.$watch('searchParameters.houseNumber', function(newValue) {
            if (!newValue) {
                delete $scope.house;
                delete $scope.meta.house;
            }
        });
        $scope.$watchCollection('oktmoCodes', function(newValue) {
            $scope.getRegions();
        });

        $scope.$watch('searchParameters.buildingNumber', function(newValue) {
            if (!newValue) {
                delete $scope.building;
                delete $scope.meta.building;
            }
        });

        $scope.$watch('searchParameters.structNumber', function(newValue) {
            if (!newValue) {
                delete $scope.struct;
                delete $scope.meta.struct;
            }
        });

        $scope.houseChange = function () {
            if ($scope.house) {
                $scope.searchParameters.houseNumber = $scope.house.houseNumber;
                $scope.meta.house = $scope.house;
            } else {
                delete $scope.searchParameters.houseNumber;
                delete $scope.meta.house;
            }

            resetParameters(['buildingNumber', 'structNumber', 'flatNumber']);
            $scope.getBuildings();
            $scope.getStructs();
            $scope.updateParametersWithAddress();
        };

        $scope.buildingChange = function () {
            if ($scope.building) {
                $scope.searchParameters.buildingNumber = $scope.building.buildingNumber;
                $scope.meta.building = $scope.building;
            } else {
                delete $scope.searchParameters.buildingNumber;
                delete $scope.meta.building;
            }

            resetParameters(['structNumber', 'flatNumber']);
            $scope.getStructs();
            $scope.updateParametersWithAddress();
        };

        $scope.structChange = function () {
            if ($scope.struct) {
                $scope.searchParameters.structNumber = $scope.struct.structNumber;
                $scope.meta.struct = $scope.struct;
            } else {
                delete $scope.searchParameters.structNumber;
                delete $scope.meta.struct;
            }

            resetParameters(['flatNumber']);
            $scope.updateParametersWithAddress();
        };

        $scope.getRegions = function () {
            //для того, что-бы можно было указывать коды октмо, откуда брать адреса

            $scope.fiasSearchLocalService.region().query([], {oktmoCodes:$scope.oktmoCodes},
                function (data) {
                    $scope.regions = angular.copy(data);
                    checkFederalRegion();
                },
                $scope.errorCallback
            );
        };
        var watcher = function(property, changeMethod) {
            return function(newVal, oldVal) {
                if ((!newVal && !oldVal) || newVal == oldVal) {
                    return false;
                }

                // FIXME select2 или кто-то еще обрабатывает изменение два раза
                // первый, как и положено в значение приходит id выбранного объекта
                // сразу проходит второе изменение с id на сам объект
                // поэтому мы тут отсекаем данный случай. зная, что нас интересуют только строки
                if(_.isObject(newVal)) {
                    $scope.searchParameters[property] = oldVal;
                    return false;
                }
                if(_.isString(newVal) && !_.isObject(oldVal)) {
                    changeMethod();
                    return true;
                }
                return true;
            };
        };

        $scope.$watch('searchParameters.areaGuid', watcher('areaGuid', areaChange));
        $scope.$watch('searchParameters.cityGuid', watcher('cityGuid', cityChange));
        $scope.$watch('searchParameters.settlementGuid', watcher('settlementGuid', settlementChange));
        $scope.$watch('searchParameters.streetGuid', watcher('streetGuid', streetChange));

        function areaChange() {
            resetParameters([
                'settlementGuid',
                'cityGuid',
                'streetGuid',
                'houseNumber',
                'buildingNumber',
                'structNumber',
                'flatNumber'
            ]);
            $scope.findAndExecute('area', function (area) {
                $scope.searchParameters.zipCode = area.postalCode;
            });
            $scope.getHouseNumbersBuildingsAndStructs();
            $scope.updateParametersWithAddress();
        }

        function cityChange() {
            resetParameters([
                'settlementGuid',
                'streetGuid',
                'houseNumber',
                'buildingNumber',
                'structNumber',
                'flatNumber'
            ]);
            $scope.findAndExecute('city', function (city) {
                $scope.searchParameters.zipCode = city.postalCode;
            });
            $scope.getHouseNumbersBuildingsAndStructs();
            $scope.updateParametersWithAddress();
        }

        function settlementChange() {
            resetParameters(['streetGuid', 'houseNumber', 'buildingNumber', 'structNumber', 'flatNumber']);
            $scope.findAndExecute('settlement', function (settlement) {
                $scope.searchParameters.zipCode = settlement.postalCode;
            });
            $scope.getHouseNumbersBuildingsAndStructs();
            $scope.updateParametersWithAddress();
        }

        function streetChange () {
            resetParameters(['houseNumber', 'buildingNumber', 'structNumber', 'flatNumber']);
            $scope.findAndExecute('street', function (street) {
                $scope.searchParameters.zipCode = street.postalCode;
            });
            $scope.getHouseNumbersBuildingsAndStructs();
            $scope.updateParametersWithAddress();
        }

        var generateSelect2Options = function(resource, params, arrayOfDataProperty) {
            return {
                minimumResultsForSearch: 0,
                initSelection: angular.noop,
                id : function(obj) {
                    return obj.guid;
                },
                formatResult : function(obj, container, query) {
                    var match = obj.formalName.toUpperCase().indexOf(query.term.toUpperCase());

                    var text = obj.formalName;

                    if (match >= 0) {
                        text = obj.formalName.substring(0, match);
                        text += "<span class='select2-match'>";
                        text += obj.formalName.substring(match, match + query.term.length);
                        text += "</span>";
                        text += obj.formalName.substring(match + query.term.length, obj.formalName.length);
                    }


                    var objToFormat = angular.copy(obj);
                    objToFormat.formalName = text;

                    return $scope.name(objToFormat, true);
                },
                formatSelection: function(obj) {
                    return $scope.name(obj, true);
                },
                ajax: {
                    quietMillis: 300,
                    transport: function (query) {
                        return resource.query([], query.data, angular.noop, $scope.errorCallback).then(query.success);
                    },
                    data: function (term, page) {
                        return _.merge(createSearchParams(params), {
                                searchString: term,
                                page: page,
                                itemsPerPage: 20
                            }
                        );
                    },
                    results: function (data, page, query) {
                        return {
                            more: !_.isEmpty(data) && data.length >= 20,
                            results: _.map(data, function (object) {
                                $scope[arrayOfDataProperty] = $scope[arrayOfDataProperty] ?
                                    $scope[arrayOfDataProperty].concat(data) : data;
                                return object;
                            })
                        };

                    }
                }
            };
        };

        $scope.regionSelect2Options = {
            minimumResultsForSearch : -1,
            placeholder: "Выберите субъект РФ"
        };

        $scope.areaSelect2Options =
            generateSelect2Options($scope.fiasSearchLocalService.area(), ['regionGuid'], 'areas');

        $scope.citySelect2Options =
            generateSelect2Options($scope.fiasSearchLocalService.city(), ['regionGuid', 'areaGuid'], 'cities');

        $scope.settlementSelect2Options =
            generateSelect2Options($scope.fiasSearchLocalService.settlement(), ['regionGuid','areaGuid', 'cityGuid'], 'settlements');

        $scope.streetSelect2Options =
            generateSelect2Options($scope.fiasSearchLocalService.street(), ['regionGuid', 'areaGuid', 'cityGuid', 'settlementGuid'], 'streets');

        $scope.getHouseNumbers = function () {
            var params = createSearchParams(['regionGuid', 'areaGuid', 'cityGuid', 'settlementGuid', 'streetGuid']);

            if (params && !angular.equals(params, {})) {
                $scope.fiasSearchLocalService.houseNumber().query([], params,
                    function (data) {
                        $scope.houses = angular.copy(data);
                        $scope.checkNumber('house');
                    }, function() {
                        $scope.housesInit = false;
                        $scope.errorCallback();
                    });
            }
        };

        $scope.getBuildings = function () {
            var params = createSearchParams(['regionGuid', 'areaGuid', 'cityGuid', 'settlementGuid', 'streetGuid', 'houseNumber']);

            if (params && !angular.equals(params, {})) {
                $scope.fiasSearchLocalService.building().query([], params,
                    function (data) {
                        $scope.buildings = angular.copy(data);
                        $scope.checkNumber('building');
                    }, function() {
                        $scope.buildingsInit = false;
                        $scope.errorCallback();
                    });
            }
        };

        $scope.getStructs = function () {
            var params = createSearchParams(['regionGuid', 'areaGuid', 'cityGuid', 'settlementGuid', 'streetGuid', 'houseNumber', 'buildingNumber']);

            if (params && !angular.equals(params, {})) {
                $scope.fiasSearchLocalService.struct().query([], params,
                    function (data) {
                        $scope.structs = angular.copy(data);
                        $scope.checkNumber('struct');
                    }, function() {
                        $scope.structsInit = false;
                        $scope.errorCallback();
                    });
            }
        };

        $scope.getHouseNumbersBuildingsAndStructs = function() {
            $scope.getHouseNumbers();
            $scope.getBuildings();
            $scope.getStructs();
        };

        function createSearchParams(pickArgs) {
            return _($scope.searchParameters).pick(function(val, key) {
                return _.indexOf(pickArgs, key) > -1 && val;
            }).value();
        }

        $scope.updateParametersWithAddress = function () {
            var address = {};

            angular.forEach(['region', 'area', 'city', 'settlement', 'street'], function (obj, key) {
                $scope.findAndExecute(obj, function (realObj) {
                    address[obj] = realObj.formalName;
                });
            });

            $scope.searchParameters.address = angular.copy(address);
        };

        $scope.findAndExecute = function (obj, callback) {
            var multipleObj = obj + 's';
            if (obj == 'city') {
                multipleObj = 'cities';
            }

            if ($scope[multipleObj] && $scope[multipleObj].length > 0) {
                angular.forEach($scope[multipleObj], function (object, key) {
                    if (object.guid == $scope.searchParameters[obj + 'Guid']) {
                        callback(object);
                    }
                });
            }
        };

        $scope.checkNumber = function(prop) {
            if ($scope.searchParameters[prop + 'Number'] && $scope[prop + 'sInit']) {
                var result = _.find($scope[prop + 's'], function(obj) {
                    return obj[prop + 'Number'] == $scope.searchParameters[prop + 'Number'];
                });

                if (result) {
                    $scope[prop] = _.isArray(result) && result.length > 0 ? result[0] : result;
                }
            }
            $scope[prop + 'sInit'] = false;
        };

        $scope.getParams = function () {
            var params = angular.copy($scope.searchParameters);
            delete params.address;
            return params;
        };

        $scope.errorCallback = function (error) {
            commonDialogs.error('Во время работы системы произошла ошибка');
        };

        $scope.name = function (object, reverse) {
            return $filter('geographicalObjectName')(object, reverse);
        };

        $scope.aoName = function(ao, prop) {
            return ao.additionalName ? ao[prop] + ' (' + ao.additionalName + ')' : ao[prop];
        };


        $scope.getRegions();

        // Проверяем если вернулись на форму с сохраненными данными, то подтягиваем справочники
        if ($scope.searchParameters && $scope.searchParameters.regionGuid) {
            var initSelect2 = function(resource, params, prop) {
                if ($scope.searchParameters[prop + 'Guid']) {
                    resource.query([], createSearchParams(params),
                        function (data) {
                            if (data && data.length > 0) {
                                $('#' + prop).select2('data', data[0]);
                            }
                        }, angular.noop
                    );
                }
            };

            //Фикс бага HCS-4952. В данном случае вызовы $browser.defer() позволяют последовательно выбрать значения в 4-х select2
            // Без этого фикса периодически (когда есть тормоза) не вызывается formatSelection. Использовать $browser не желательно, т.к.
            // он является недокументированным функционалом angular, однако тут возможное решение с использование $q.defer не работает.
            //https://github.com/angular/angular.js/blob/master/src/ng/browser.js
            $browser.defer(function(){
                    initSelect2(FiasResource.area(), ['regionGuid', 'areaGuid'], 'area');
                    $browser.defer(function(){
                        initSelect2(FiasResource.city(), ['regionGuid', 'areaGuid', 'cityGuid'], 'city');
                        $browser.defer(function(){
                            initSelect2(FiasResource.settlement(), ['regionGuid', 'areaGuid', 'cityGuid', 'settlementGuid'], 'settlement');
                            $browser.defer(function(){
                                initSelect2(FiasResource.street(), ['regionGuid', 'areaGuid', 'cityGuid', 'settlementGuid', 'streetGuid'], 'street');
                            });
                        });

                    });
                }
            );

            $scope.housesInit = true;
            $scope.buildingsInit = true;
            $scope.structsInit = true;
            $scope.getHouseNumbersBuildingsAndStructs();

        }

        $scope.fieldFilter = function(prop){
            return function(item) {
                if($scope.fieldParams[prop].filter == null){
                    return true;
                }else{
                    var index = $scope.fieldParams[prop].filter.indexOf(item.guid);
                    return index >= 0;
                }
            };
        };
    }])
;