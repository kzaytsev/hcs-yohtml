angular.module('common.component-config', [])

    //runtime конфигурирование компонентов на старте приложения
    .run(['uiSelect2Config', function(uiSelect2Config) {
        uiSelect2Config.minimumResultsForSearch = -1; //select2 - отключение блока поиска
    }])

;

angular.module('common.config', [])

    //runtime конфигурирование компонентов на старте приложения
    .run(['$rootScope', '$modalStack', '$preloader','previousStateService', function($rootScope, $modalStack, $preloader, previousStateService) {
    //previousStateService инжектится для начала записи стейтов, если она ещё не ведётся

        // Скрытие модальных окон при нажатии кнопки back в браузере,
        // см. https://github.com/angular-ui/bootstrap/issues/335
        $rootScope.$on('$stateChangeStart', function() {
            var guard = 0, MAX_NUM_TO_DISMISS = 10;
            var top = $modalStack.getTop();
            while (top) {
                $modalStack.dismiss(top.key);
                top = $modalStack.getTop();

                //To prevent infinite loop, if any
                if (guard++ == MAX_NUM_TO_DISMISS) {break;}
            }

            $preloader.hide();
        });
    }])

    .directive('preloader', ['$http' ,function ($http) {
        return {
            restrict: 'A',
            link: function ($scope, elm, attrs) {
                $scope.isLoading = function () {
                    return $http.pendingRequests.length > 0;
                };

                $scope.$watch($scope.isLoading, function (v) {
                    if (v) {
                        elm.show();
                    } else {
                        elm.hide();
                    }
                });
            }
        };

    }])

    .service('$preloader', function() {
        this.show = function() {
            angular.element('#loading-dialog').show();
        };

        this.hide = function() {
            angular.element('#loading-dialog').hide();
        };
    })
;

angular.module('common.tab-extended', [])
    .config(function($provide) {
        /**
         * Пришлось расширить директиву bootstrap-а 'tab', т.к. не возможно было "вклиниться"
         * в событие смены вкладки (для проверки наличия несохраненных данных)
         *
         * В директиве появился новый атрибут beforeSelect (функция).
         * Если beforeSelect возвращает false, то смена закладок не происходит
         *
         * (если не задавать параметр beforeSelect, поведение будет аналогичное директиве bootstrap)
         * */
        $provide.decorator('tabDirective', ['$parse', '$delegate', function($parse, $delegate) {

            var directive = $delegate[0];

            angular.extend(directive.scope, {
                beforeSelect: '&beforeSelect'
            });

            directive.compile = function(elm, attrs, transclude) {
                return function postLink(scope, elm, attrs, tabsetCtrl) {
                    scope.$watch('active', function(active) {
                        if (active) {
                            if(scope.beforeSelect == null || scope.beforeSelect() !== false){
                                tabsetCtrl.select(scope);
                            }else{
                                scope.active = false;
                            }
                        }
                    });

                    scope.disabled = false;
                    if ( attrs.disabled ) {
                        scope.$parent.$watch($parse(attrs.disabled), function(value) {
                            scope.disabled = !! value;
                        });
                    }

                    scope.select = function() {
                        if ( !scope.disabled ) {
                            scope.active = true;
                        }
                    };

                    tabsetCtrl.addTab(scope);
                    scope.$on('$destroy', function() {
                        tabsetCtrl.removeTab(scope);
                    });

                    //We need to transclude later, once the content container is ready.
                    //when this link happens, we're inside a tab heading.
                    scope.$transcludeFn = transclude;
                };
            };

            return $delegate;
        }]);
    })
;