/**
 * Блок контекстного поиска организации (ЭФ_КПОРГ)
 */
angular
    .module('common.ef-kporg', [
        'lodash',
        'ui.select2',
        'common.ef-vborg',
        'ppa.collection-search',
        'ru.lanit.hcs.organizationregistry.OrganizationRegistryService'
    ])
    .directive('efKporg', function () {
        return {
            restrict: 'E',
            scope: {
                ngModel: '='
            },
            templateUrl: 'ef-kporg/ef-kporg.tpl.html',
            controller: 'efKporgController'
        };
    })
    .controller('efKporgController', ['$scope', '$OrganizationRegistryService', 'efVborgDialog', 'CollectionSearchCriteria',
        function ($scope, $OrganizationRegistryService, efVborgDialog, CollectionSearchCriteria) {

            $scope.select2Options = {
                placeholder: 'Введите полное или сокращенное наименование, ИНН, ОГРН, ФИО, ОГРНИП',
                minimumInputLength: 3,
                minimumResultsForSearch: 0,
                initSelection: angular.noop,
                allowClear: true,
                selectOnBlur: true,
                formatInputTooShort: function (term, minLength) {
                            return '<span class="text-danger">Поисковый запрос должен содержать не менее 3 символов</span>';

                },
                formatLoadMore: function (pageNumber) {
                    return 'Отображены не все результаты. Показать больше...';
                },
                id : function(obj) {
                    return obj.guid;
                },
                //Allow manually entered text in drop down.
                createSearchChoice: function (term, data) {
                    return data;
                },
                formatResult : function(obj, container, query) {

                    if (obj.guid === obj.text){
                        return obj.text;
                    }

                    var match = obj.readableName.toUpperCase().indexOf(query.term.toUpperCase());
                    var text = obj.readableName;
                    if (match >= 0) {
                        text = obj.readableName.substring(0, match);
                        text += "<span class='select2-match'>";
                        text += obj.readableName.substring(match, match + query.term.length);
                        text += "</span>";
                        text += obj.readableName.substring(match + query.term.length, obj.readableName.length);
                    }

                    return text;
                },
                formatSelection: function(obj) {
                    return obj.readableName;
                },
                ajax: {
                    quietMillis: 300,
                    transport: function (query) {
                        return $OrganizationRegistryService.findRegistryOrganizations(query.data.searchCriteria, angular.noop, $scope.errorCallback,query.data.pathParams).then(query.success);
                    },
                    data: function (term, page) {
                        var pathParams = {
                            page: page,
                            itemsPerPage: 5
                        };
                        var searchCriteria = {
                            organizationName: term
                            //,
                            //organizationType: "L"
                        };

                        return {
                            pathParams: pathParams,
                            searchCriteria: searchCriteria
                        };
                    },
                    results: function (data, page, query) {
                        return {
                            more: !_.isEmpty(data.items) && data.items.length >= 5,
                            results: _.map(data.items,
                                function (elem) {
                                    return processResult(elem);
                                }
                            )
                        };

                    }
                }
            };

            $scope.search = function () {
                efVborgDialog.create().setReturnDetailObject(true).show().then(function (result) {
                    console.log(result);
                    $scope.ngModel = processResult(result);
                }, function () {
                });
            };


            $scope.onclose=function(){
              $scope.$apply();
            };

            $scope.errorCallback = function () {
                console.log('Произошла ошибка во время поиска ОКТМО');
            };

            function processResult(result){
                var readableName;

                if (result.registryOrganizationType === 'L'){
                    readableName =  result.fullName ? result.fullName : result.shortName;
                    readableName += " (ИНН ";
                    readableName += result.inn;
                    readableName += ", КПП ";
                    readableName += result.kpp;
                    readableName += ")";

                }else{
                    readableName = "ИП ";
                    readableName +=  (result.chiefLastName ? (result.chiefLastName + " "): "") +
                    (result.chiefFirstName ? (result.chiefFirstName + " "): "")  +
                    (result.chiefMiddleName ? result.chiefMiddleName: "");
                    readableName += " (ОГРНИП ";
                    readableName += result.ogrn;
                    readableName += ")";
                }

                return  {
                    readableName: readableName,
                    guid: result.guid,
                    element: result
                };
            }

        }]).directive('select2Events', function() { //позволяет перехватить события select2
        return {
            scope: {
               select2Close : '&'
            },
            link: function(scope, element) {
                element.on('select2-close', function(e) {
                    scope.select2Close(e);
                });
            }
    };
});
