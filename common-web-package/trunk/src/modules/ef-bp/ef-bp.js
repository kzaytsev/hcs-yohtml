/**
 * 2.1    Блок поиска (ЭФ_БП)
 * Ожидает на вход в виде аттрибутов:
 * searchParameters - объект в котором будет хранится внешнее, изменяющееся состояние фильтра.
 *      Начальное значение этого объекта (при инициализации экземпляра директивы)
 *      будет принято за настройки поиска "по умолчанию"
 * searchService - сервис или функция поиска.
 *      Ожидаем от нее тип возврата boolean или promise
 *          boolean:
 *              true - поиск прошел успешно, отображены новые результаты, нужно показать кнопку Очистить
 *              false - поиск не прошел, например не выполнены условия валидации, ничего не делать
 *          promise:
 *              resolved - см. boolean:true,
 *              rejected - см. boolean:false
 */
angular.module('common.ef-bp', [
        'lodash',
        'ui.select2',
        'ui.bootstrap'
    ])
    .directive('efBpForm', function () {
        return {
            restrict: 'E',
            scope: {
                searchService: '=',
                searchParameters: '=',
                defaultSearchParameters: '=',
                searchField: '=?',
                expanded: '=?',
                notExpandable: '=?',
                noSearchBtn: '=?',
                panelConfig: '=?'
            },
            controller: 'efBpFormController',
            transclude: true,
            templateUrl: 'ef-bp/ef-bp-form.tpl.html'
        };
    })
    .controller('efBpFormController', ['$scope', '_', function ($scope, _) {
        $scope.expanded = true;
        if (!$scope.panelConfig){
            $scope.panelConfig={
                searchBtnDisabled:false
            };
        }

        var defaultSearchParameters;
        if (_.isUndefined($scope.defaultSearchParameters)) {
            defaultSearchParameters = angular.copy($scope.searchParameters);
        } else {
            defaultSearchParameters = $scope.defaultSearchParameters;
            refreshDirtyState();
        }
        $scope.onSelect = function(item){
            if($scope.searchField.onSelect){
                $scope.searchField.onSelect(item);    
            }
        };
        $scope.action = {
            search: function () {
                
                if ($scope.efBpForm.$invalid) {
                    console.log("Форма помечена как не верная. Не заполнены все обязательные поля.");
                    $scope.$parent.$broadcast('showError');
                    //refreshDirtyState();
                    return false;
                }
                $scope.$parent.$broadcast('hideError');
                
                var respond = $scope.searchService($scope.searchParameters);
                // Сервис поиска вернул булево значение и оно true
                if (_.isBoolean(respond)) {
                    if (respond) {
                        refreshDirtyState();
                    } else {
                        // при неудачном поиске не меняем состояния dirty

                        angular.noop();
                    }
                    // сервис поиска вернул что-то похожее на promise
                } else if (!_.isUndefined(respond) && _.isFunction(respond.then)) {
                    // при неудачном поиске не меняем состояния dirty
                    respond.then(refreshDirtyState);
                } else {
                    console.log('ef-bp-form: not supported searchService return type');
                }
                return false;
            },
            reset: function () {
                $scope.$parent.$broadcast('hideError');
                $scope.$parent.$broadcast('resetBpForm');
                angular.copy(defaultSearchParameters, $scope.searchParameters);
                if ($scope.searchField) {
                    $scope.searchField.model = null;
                }

                $scope.action.search();
            }
        };
        function refreshDirtyState() {
            $scope.isDirty = !_.isEqual(_.cleanEmpty($scope.searchParameters), _.cleanEmpty(defaultSearchParameters));
            $scope.isDirtySearchField = $scope.searchField && $scope.searchField.loadData && !$scope.searchField.disabled && $scope.searchField.model;
        }
    }])
;