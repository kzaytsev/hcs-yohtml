/**
 * 2.2	Блок прикрепления файла (ЭФ_ПрФ)
 */

angular.module('common.ef-vva', ['common.ef-pa', 'resources.fiasResource', 'common.filter', 'common.dialogs'])

        .service('addressChooserDialog', ['$modal', '$filter', 'FiasResource', 'commonDialogs',
            function ($modal, $filter, FiasResource, commonDialogs) {

            var modalDefaults = {
                backdrop: 'static',
                keyboard: true,
                modalFade: true,
                templateUrl: 'ef-vva/ef-vva-dialog.tpl.html'
            };

            var modalOptions = {
                closeButtonText: 'Отменить',
                actionButtonText: 'OK',
                headerText: 'Выбрать адрес'
            };

            this.show = function () {
                if (!modalDefaults.controller) {
                    modalDefaults.controller = function ($scope, $modalInstance) {
                        $scope.searchParameters = {};
                        $scope.meta = {};

                        $scope.modalOptions = modalOptions;
                        $scope.modalOptions.ok = function (result) {
                            FiasResource.house().query([], $scope.getParams(),
                                function(data) {
                                    if (data && data.length > 0) {
                                        if (data.length == 1) {
                                            $modalInstance.close($scope.getResult($scope.searchParameters, data[0]));
                                        } else {
                                            var house = $scope.findAppropriateHouse(data);
                                            $modalInstance.close($scope.getResult($scope.searchParameters, house));
                                        }
                                    } else {
                                        commonDialogs.alert('Не найден дом по указанным параметрам');
                                    }
                                }, $scope.errorCallback);
                        };
                        $scope.modalOptions.close = function (result) {
                            $modalInstance.dismiss('cancel');
                        };

                        $scope.getParams = function() {
                            var params = angular.copy($scope.searchParameters);
                            delete params.address;
                            delete params.zipCode;

                            if (params.streetGuid) {
                                params.aoGuid = params.streetGuid;
                            } else if (params.settlementGuid) {
                                params.aoGuid = params.settlementGuid;
                            } else if (params.cityGuid) {
                                params.aoGuid = params.cityGuid;
                            } else if (params.areaGuid) {
                                params.aoGuid = params.areaGuid;
                            } else if (params.regionGuid) {
                                params.aoGuid = params.regionGuid;
                            }

                            delete params.streetGuid;
                            delete params.settlementGuid;
                            delete params.cityGuid;
                            delete params.areaGuid;
                            delete params.regionGuid;

                            if ($scope.meta.house && $scope.meta.house.additionalName) {
                                params.additionalName = $scope.meta.house.additionalName;
                            }
                            if ($scope.meta.building && $scope.meta.building.additionalName) {
                                params.additionalName = $scope.meta.building.additionalName;
                            }
                            if ($scope.meta.struct && $scope.meta.struct.additionalName) {
                                params.additionalName = $scope.meta.struct.additionalName;
                            }

                            return params;
                        };

                        $scope.findAppropriateHouse = function(houses) {
                            var result = null;
                            angular.forEach(houses, function(house) {
                                if (result != null) {return;}

                                var thatOne = $scope.equals($scope.searchParameters.houseNumber, house.houseNumber);
                                thatOne &= $scope.equals($scope.searchParameters.buildingNumber, house.buildingNumber);
                                thatOne &= $scope.equals($scope.searchParameters.structNumber, house.structNumber);

                                if (thatOne) {
                                    result = house;
                                }
                            });

                            if (result == null) {
                                result = houses[0];//fallback
                            }
                            return result;
                        };

                        $scope.equals = function(a, b) {
                            return (!a && !b) || angular.equals(a, b);
                        };

                        $scope.getResult = function (searchParameters, house) {
                            var result = {};

                            if (searchParameters.regionGuid) {
                                result.region = {guid: searchParameters.regionGuid};
                            }

                            if (searchParameters.areaGuid) {
                                result.area = {guid: searchParameters.areaGuid};
                            }

                            if (searchParameters.cityGuid) {
                                result.city = {guid: searchParameters.cityGuid};
                            }

                            if (searchParameters.settlementGuid) {
                                result.settlement = {guid: searchParameters.settlementGuid};
                            }

                            if (searchParameters.streetGuid) {
                                result.street = {guid: searchParameters.streetGuid};
                            }

                            if (searchParameters.houseNumber) {
                                result.houseNumber = searchParameters.houseNumber;
                            }

                            if (searchParameters.buildingNumber) {
                                result.buildingNumber = searchParameters.buildingNumber;
                            }

                            if (searchParameters.structNumber) {
                                result.structNumber = searchParameters.structNumber;
                            }

                            if (searchParameters.zipCode) {
                                result.zipCode = searchParameters.zipCode;
                            }

                            if (house && house.guid) {
                                result.house = {guid: house.guid};
                                if (house.oktmo) {
                                    result.house.oktmo = house.oktmo;
                                }
                            }

                            result.address = $filter('houseAddress')(searchParameters, true);

                            return result;
                        };

                        $scope.errorCallback = function(error) {
                            commonDialogs.error(error);
                        };

                        $scope.addressValid = function() {
                            var p = $scope.searchParameters;
                            return p.houseNumber || p.buildingNumber || p.structNumber;
                        };
                    };
                }

                return $modal.open(modalDefaults).result;
            };
        }])
;