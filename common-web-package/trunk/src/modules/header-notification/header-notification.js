/**
 *
 */
angular
    .module('common.header-notification', [
        'lodash'
    ])
    .directive('headerNotification', function () {
        return {
            restrict: 'E',
            templateUrl: 'header-notification/header-notification.tpl.html',
            scope: {
                notifications: '=?'
            },
            controller: ['$scope', '$window', '_', function ($scope, $window, _) {
                if (_.isUndefined($scope.notifications)) {
                    $scope.notifications = [{
                        notificationMessage: 'В настоящий момент сайт находится в режиме опытной эксплуатации. Некоторые разделы сайта находятся в стадии разработки. Информацию о функционировании разделов сайта можно узнать из публикуемых новостей. Следите за нашими объявлениями.',
                        notificationTitle: 'Уважаемые посетители!',
                        notificationType: 'warning',
                        notificationWithClose: false
                    }];
                }
                $scope.closeAlert = function (index) {
                    $scope.notifications.splice(index, 1);
                };
            }],
            link: function ($scope) {
                $scope.$root.$on('notificationChangeSuccess', function (event, data) {
                    $scope.notifications.push(data);
                    $scope.$apply();
                });
            }
        };
    });