/**
 * 2.8    Компонент межстраничной навигации (ЭФ_КМН)
 */
angular.module('common.hcs-pagination', [
    'lodash'
])
    .directive('hcsPagination', function () {
        return {
            restrict: 'E',
            scope: {
                pageChanged: '=changed',
                inModal: '=modal',

                /**
                 * paginationConfig = {
                 *    itemsPerPage : 10,
                 *    page: 1,
                 *    isLast: false,
                 *    total: 50 - not required,
                 *    isEmpty: false - not required
                 * }
                 */
                paginationConfig: '=?',     //prefer to use


                itemsPerPage: '=',          //deprecated (use paginationConfig)
                fixedItemsPerPage: '@',
                currentPage: '=',           //deprecated (use paginationConfig)
                isLast: '=last'             //deprecated (use paginationConfig)
            },
            replace: true,
            templateUrl: 'hcs-pagination/hcs-pagination.tpl.html',
            controller: function (_, $scope) {

                if (!$scope.paginationConfig) {
                    $scope.paginationConfig = {
                        itemsPerPage : $scope.itemsPerPage,
                        isLast: $scope.isLast,
                        page : $scope.currentPage
                    };
                }

                $scope.maxSize = 5;
                $scope.nextPage = function () {
                    if ($scope.paginationConfig.isLast) {
                        return;
                    }
                    $scope.paginationConfig.page++;
                    $scope.innerPageChanged();
                };
                $scope.prevPage = function () {
                    if ($scope.paginationConfig.page <= 1) {
                        return;
                    }
                    $scope.paginationConfig.page--;
                    $scope.innerPageChanged();
                };

                $scope.$watch('paginationConfig.itemsPerPage', function (newValue, oldValue) {
                    if (newValue == oldValue) {return;}

                    $scope.paginationConfig.page = 1;
                    $scope.innerPageChanged();
                });

                $scope.$watch('paginationConfig.isLast', function (newValue, oldValue) {
                    if (newValue == oldValue) {return;}
                    refreshInnerTotal();
                });

                $scope.$watch('paginationConfig.page', function (newValue, oldValue) {
                    if (newValue == oldValue) {return;}
                    refreshInnerTotal();
                });

                $scope.$watch('paginationConfig.total', function (newValue, oldValue) {
                    if (newValue == oldValue) {return;}
                    refreshInnerTotal();
                });

                $scope.innerPageChanged = function () {
                    var response = $scope.pageChanged($scope.paginationConfig.page);
                    if (response && _.isFunction(response.then)) {
                        if ($scope.paginationConfig.total) {
                            $scope.innerTotal = $scope.paginationConfig.total;
                        }
                        response.then(function () {
                            refreshInnerTotal();
                        });
                    }
                    $('.pagination :focus').blur();
                };

                function calculatePagesCount(){
                    return Math.ceil($scope.innerTotal / $scope.paginationConfig.itemsPerPage);
                }

                function refreshInnerTotal() {
                    var factor = 0;
                    if (!$scope.paginationConfig.page) {
                        return;
                    }
                    if ($scope.paginationConfig.total) {
                        $scope.innerTotal = $scope.paginationConfig.total;
                    } else if (!$scope.paginationConfig.isLast) {
                        if ($scope.paginationConfig.page === 1 || $scope.paginationConfig.page === 2) {
                            // Требование "Если выбранной является кнопка с номером 1, 2 или 3 и не является последней, то отображаются кнопки с номерами от 1 до 5"
                            factor = 5;
                        } else {
                            // Требование "Если выбранной является кнопка с номером > 3 и не является последней, то отображаются кнопки с номерами от <номер выбранной кнопки - 2> до <номер выбранной кнопки + 2>."
                            factor = 2;
                        }
                        $scope.innerTotal = $scope.paginationConfig.itemsPerPage * ($scope.paginationConfig.page + factor);
                    } else {
                        $scope.innerTotal = $scope.paginationConfig.itemsPerPage * ($scope.paginationConfig.page + factor);
                    }

                    var pagesCount = calculatePagesCount();
                    if(pagesCount > 5){
                        if ($scope.paginationConfig.isLast &&
                            $scope.paginationConfig.page != 1 && $scope.paginationConfig.page != 2) {
                            // Требование "Если выбранная кнопка имеет номер отличный от 1 или 2 и является последней, то отображаются кнопки с номерами от <номер выбранной кнопки - 2> до номера выбранной кнопки."
                            $scope.maxSize = 3;
                        } else {
                            $scope.maxSize = 5;
                        }
                    }else{
                        $scope.maxSize = pagesCount;
                    }
                }
            }
        };
    })
;