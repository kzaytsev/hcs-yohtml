/**
 * Горизонтальное меню
 */
angular
    .module('common.horizontal-menu', [
        'ru.lanit.hcs.ppa.rest.PpaService',
        'common.notification.model',
        'common.auth',
        'lodash'
    ])
    .directive('horizontalMenu', function () {
        return {
            restrict: 'E',
            templateUrl: 'horizontal-menu/horizontal-menu.tpl.html',
            controller: [
                '$scope',
                'Auth',
                'User',
                '$PpaService',
                'NotificationModelAmount',
                'InformingModelService',
                '_',
                function ($scope, Auth, User, $PpaService, NotificationModelAmount, InformingModelService, _) {
                    $scope.isCitizen = User.isCitizen();
                    $scope.$on('$destroy', NotificationModelAmount.subscribe(function (data) {
                        $scope.notificationAmount = data;
                    }));

                    if ($scope.isCitizen) {
                        $PpaService.findUserPremises().then(
                            function (data) {
                                $scope.apartmentsAccounts = data;
                                $scope.isShowVoting = Auth.authorize([{organizationRole: 'ADMIN_OF_GENERAL_MEETING_IN_APARTMENT_HOUSE'}]) || $scope.apartmentsAccounts.length > 0;
                                $scope.isShowDevice = $scope.apartmentsAccounts.length > 0;
                                _.each($scope.apartmentsAccounts, function (apartmentsAccount, key) {
                                    if (apartmentsAccount.premiseType == 'APARTMENT') {
                                        $scope.apartmentsAccounts[key].apartmentGuid = apartmentsAccount.guid;
                                    } else if (apartmentsAccount.premiseType == 'DWELLING_HOUSE') {
                                        $scope.apartmentsAccounts[key].houseGuid = apartmentsAccount.guid;
                                    } else if (apartmentsAccount.premiseType == 'ROOM') {
                                        $scope.apartmentsAccounts[key].roomGuid = apartmentsAccount.guid;
                                    }
                                });
                            }
                        );
                    } else {
                        $scope.$on('$destroy', InformingModelService.subscribe(function (data) {
                            $scope.newsAmount = data;
                        }));
                        var queryJson = {
                            linkStatus: 'ACTIVE',
                            linkType: 'OUTGOING_LINKS'
                        };
                        $PpaService.findLinkBetweenOrganizationAndAccount(queryJson).then(
                            function (data) {
                                $scope.isShowOrganizationAccounts = data.linkBetweenOrganizationAndAccountCount;
                            },
                            function (status) {
                            }
                        );
                    }
                }
            ]
        };
    });