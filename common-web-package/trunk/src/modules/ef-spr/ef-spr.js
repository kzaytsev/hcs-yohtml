/**
 * Экранная форма выбора значения из справочника (ЭФ_СПР)
 */

angular.module('common.ef-spr', ['lodash', 'common.ef-bp', 'hcs-table', 'common.ef-kmn', 'resources.classifierResource', 'common.dialogs', 'common.filter', 'common.utils'])

        .service('referenceChooserDialog', ['_', '$modal', '$filter', 'ClassifierResource', 'CommonReferenceResource', 'commonDialogs', '$dateParser',
            function (_, $modal, $filter, ClassifierResource, CommonReferenceResource, commonDialogs, $dateParser) {

            var modalDefaults = {
                backdrop: true,
                keyboard: true,
                modalFade: true,
                referenceName : 'Справочник',
                size: 'lg',
                templateUrl: 'ef-spr/ef-spr-dialog.tpl.html'
            };

            var modalOptions = {
                closeButtonText: 'Отменить',
                actionButtonText: 'Выбрать'
            };

            var defaultParams = {
                structure : 'hierarchy',
                control: 'checkbox',
                onlyLeafs: false,
                parentNsiObjectName : 'nsiObjectName'
            };
            var params = angular.copy(defaultParams);

            var self = this;

            self.setReferenceName = function(name) {
                params.referenceName = name;
                return self;
            };

            self.hasCheckbox = function() {
                params.control = 'checkbox';
                return self;
            };

            self.hasRadio = function() {
                params.control = 'radio';
                return self;
            };

            self.setType = function(type) {
                params.type = type;
                return self;
            };

            self.hierarchy = function() {
                params.structure = 'hierarchy';
                return self;
            };

            self.plane = function() {
                params.structure = 'plane';
                return self;
            };

            self.codeIsHidden = function() {
                params.codeIsHidden = true;
                return self;
            };
            self.excludedGuids = function(guids) {
                //console.log('excluded guid '+guids);
                params.excludedGuids = guids;
                return self;
            };

            self.canSelectOnLevel = function(level) {
                params.level = level;
                return self;
            };
            self.onlyLeafs = function() {
                params.onlyLeafs = true;
                return self;
            };

            self.setParentNsiObjectNameProperty = function(parentNsiObjectName) {
                params.parentNsiObjectName = parentNsiObjectName;
                return self;
            };
                //указываем сущность, которая является корнем (используется только с setRootGuid)
             self.setRootNsiObjectName = function(rootNsiObjectName){
                 params.rootNsiObjectName = rootNsiObjectName;
                 return self;
             };
           /*позволит использовать другой сервис для классификаторов.
             если параметр не задан, используется по умолчанию  ClassifierResource из НСИ
             classifiersResource должен поддерживать минимальный интерфейс для работы м иерархическими справочниками:
             {
            roots : function() { // ресурс для получения корня справочника
                    return rootResource;
            },
            search: function() { // ресурс для поиска в иерархич. справочнике
                    return searchResource;
            },
            children : function() { // для загрузки дочерних элементов узла.
                    return childrenResource;
            }
            };
            где root, search, children - должен быть BackendResource(из общих компонентов).
              */
            self.classifierResource=function(classifiersResource){
                params.classifiersResource = classifiersResource;
                return self;
            };
            /*позволит использовать другой сервис для плоских справочников.
             commonReferenceResource должен быть BackendResource(из общих компонентов).
             ну или поддерживать метод queryObject из BackendResource(из общих компонентов).
             */
            self.commonReferenceResource = function(commonReferenceResource){
                params.commonReferenceResource = commonReferenceResource;
                return self;
            };
            self.setRootGuid = function(rootGuid) {
                params.rootGuid = rootGuid;
                return self;
            };


            self.setupForAgreements = function(document, defaultServiceDateStart, defaultServiceDateEnd) {
                params.agreementsInUse = true;
                params.document = document;
                params.defaultServiceDateStart = defaultServiceDateStart;
                params.defaultServiceDateEnd = defaultServiceDateEnd;
                return self;
            };

            this.show = function () {
                if (!modalDefaults.controller) {
                    modalDefaults.controller = function ($scope, $modalInstance) {
                        var ctrl=this;
                        if (params.classifiersResource){
                            ctrl.ClassifierResource = params.classifiersResource;
                            ctrl.defaultStatus='ACTIVE';
                            console.log('def flag active1 '+ctrl.defaultStatus);
                        } else{
                            ctrl.ClassifierResource = ClassifierResource;

                            ctrl.defaultStatus=true;
                            console.log('def flag active2 '+ctrl.defaultStatus);
                        }
                        console.log('def flag active '+ctrl.defaultStatus);
                        if (params.commonReferenceResource){
                            ctrl.СommonReferenceResource = params.commonReferenceResource;
                        } else{
                            ctrl.СommonReferenceResource =CommonReferenceResource;
                        }
                        $scope.type = $filter('ucwords')(params.type);
                        $scope.onlyLeafs=params.onlyLeafs;
                        $scope.parentNsiObjectName= params.parentNsiObjectName;
                        $scope.rootGuid=params.rootGuid;
                        $scope.rootNsiObjectName = params.rootNsiObjectName?params.rootNsiObjectName:$scope.type;

                        $scope.defaultSearchParameters = {
                            nsiObjectName : $scope.type,
                            actual : ctrl.defaultStatus
                           // status : 'ACTIVE'
                        };
                        $scope.searchParameters = angular.copy($scope.defaultSearchParameters);

                        $scope.agreementsInUse = params.agreementsInUse;
                        if ($scope.agreementsInUse) {
                            var defaultServiceDateStart = _.isEmpty( params.defaultServiceDateStart) ? '' :  params.defaultServiceDateStart;
                            var defaultServiceDateEnd = _.isEmpty(params.defaultServiceDateEnd) ? '' : params.defaultServiceDateEnd;
                            $scope.service = {
                                providingDateStart: defaultServiceDateStart,
                                providingDateEnd: defaultServiceDateEnd
                            };
                            $scope.document = params.document;

                            $scope.dateOptions = {
                                closeOnDateSelection: true,
                                appendToBody: false,
                                showPlaceholder: true,
                                required: true
                            };
                        }

                        $scope.search = function (searchParameters) {
                            $scope.items = [];
                            $scope.checkedItems = {};
                            $scope.radioItem = null;
                            $scope.result = null;

                            $scope.customSearch = !angular.equals(searchParameters, $scope.defaultSearchParameters);
                            var customSearch = angular.copy($scope.customSearch);

                            //console.log('search par '+angular.toJson(params.excludedGuids));
                            if (params.excludedGuids && (params.excludedGuids.length!==0)){
                                searchParameters.excludedGuids=params.excludedGuids;
                            } else{
                                delete searchParameters.excludedGuids;
                            }
                            if ($scope.hierarchy) {
                                if (!$scope.rootGuid) {
                                    var resource = $scope.customSearch ? ctrl.ClassifierResource.search() : ctrl.ClassifierResource.roots();
                                    if (customSearch) {
                                        searchParameters.nsiObjectName = $scope.baseNsiObjectName ? $scope.baseNsiObjectName : $scope.type;
                                    } else{
                                        searchParameters.nsiObjectName = $scope.type;
                                    }
                                    resource.queryPost([], searchParameters, function (data) {
                                        $scope.processHierarchyData(data, customSearch);
                                    }, $scope.errorCallback);
                                } else{
                                    if (!$scope.customSearch) {
                                        var request = {
                                            parentGuid : $scope.rootGuid,
                                            excludedGuids:params.excludedGuids,
                                            actual:ctrl.defaultStatus
                                            //status:'ACTIVE'
                                        };
                                        //request[$scope.parentNsiObjectName] = $scope.rootNsiObjectName;
                                        request['nsiObjectName'] = $scope.rootNsiObjectName;
                                        ctrl.ClassifierResource.children()
                                            .queryPost([], request, function (children) {
                                                $scope.processHierarchyData(children, customSearch);

                                            }, $scope.errorCallback);
                                    } else {
                                        searchParameters.nsiObjectName=$scope.baseNsiObjectName?$scope.baseNsiObjectName:$scope.type;
                                        //searchParameters.parentGuid=$scope.rootGuid;
                                        ctrl.ClassifierResource.search().queryPost([], searchParameters, function (data) {
                                            $scope.processHierarchyData(data, customSearch);
                                        }, $scope.errorCallback);
                                    }
                                }
                            } else {
                                ctrl.СommonReferenceResource.queryObject([], searchParameters, function(data) {
                                    $scope.data = data.data;
                                }, $scope.errorCallback);
                            }

                            return true;
                        };

                        $scope.referenceName = params.referenceName;
                        $scope.hasCheckbox = params.control == 'checkbox';
                        $scope.hasRadio = !$scope.hasCheckbox;

                        $scope.hierarchy = params.structure == 'hierarchy';
                        $scope.plane = !$scope.hierarchy;

                        $scope.codeIsHidden = params.codeIsHidden;

                        if (params.level && $scope.hierarchy) {
                            $scope.selectionLevel = params.level;
                        }


                        $scope.checkedItems = {};
                        $scope.items = [];
                        $scope.radioItem = null;

                        $scope.tryItem = function(item) {
                            if ($scope.selectionLevel && $scope.selectionLevel != item.level + 1) {
                                return;
                            }

                            if ($scope.checkedItems[item.guid]) {
                                $scope.items = $scope.items.filter(function (element) {
                                    return !angular.equals(item, element);
                                });
                                delete $scope.checkedItems[item.guid];
                            } else {
                                $scope.items.push(item);
                                $scope.checkedItems[item.guid] = true;
                            }
                            $scope.radioItem = item.guid;
                            $scope.result = item;
                        };

                        $scope.disabled = function() {
                            return ($scope.hasRadio && !$scope.result) || ($scope.hasCheckbox && $scope.items.length === 0);
                        };

                        $scope.modalOptions = modalOptions;
                        $scope.modalOptions.ok = function (result) {
                            if ($scope.hasCheckbox) {
                                if ($scope.agreementsInUse && !$scope.specialActionsForAgreements()) {
                                    return;
                                }
                                $modalInstance.close($scope.items);
                            } else if ($scope.hasRadio) {
                                $modalInstance.close($scope.result);
                            }
                            params = angular.copy(defaultParams);
                        };
                        $scope.modalOptions.close = function (result) {
                            $modalInstance.dismiss('cancel');
                            params = angular.copy(defaultParams);
                        };

                        $scope.specialActionsForAgreements = function() {
                            $scope.$broadcast('showError');

                            if (!$scope.service.providingDateStart ||
                                    !$scope.service.providingDateEnd ||
                                    !$scope.service.baseDocumentGuid) {
                                return false;
                            }

                            var start  = $dateParser($scope.service.providingDateStart, 'dd.MM.yyyy');
                            var finish = $dateParser($scope.service.providingDateEnd, 'dd.MM.yyyy');

                            if (start.getTime() > finish.getTime()) {
                                commonDialogs.error('Дата начала периода действия услуги больше даты окончания. Введите корректные значения.');
                                return false;
                            }

                            for(var i=0; i < $scope.items.length; i++){
                                $scope.items[i].dateFrom = $scope.service.providingDateStart;
                                $scope.items[i].dateTo = $scope.service.providingDateEnd;
                                $scope.items[i].baseDocumentGuid = $scope.service.baseDocumentGuid;
                            }
                            return true;
                        };


                        $scope.showRow = function(item) {
                            if ($scope.customSearch && !item.showOnSearch) {
                                return false;
                            }

                            var parent = $scope.findParent(item);
                            if (parent == null) {return true;}

                           // var parentRow = document.getElementById(code(parent) + 'expanded');
                            var parentRow = document.getElementById(keyForMap(parent) + 'expanded');
                            return !parentRow || (parentRow.checked && parentRow.parentNode.offsetWidth > 0 && parentRow.parentNode.offsetHeight > 0);
                        };

                        $scope.findParent = function(item) {
                            return parentGuid(item) ? _.find($scope.data, function (element) {
                                return element.guid == parentGuid(item);
                            }) : null;
                        };

                        $scope.processHierarchyData = function (data, customSearch) {
                            /**
                             * data - список записей иерархического справочника в формате NsiEntity, где
                             * fieldValue1 = code
                             * fieldValue2 = name
                             */

                            $scope.isEmpty = data && data.length === 0;
                            angular.forEach($scope.data, function(item) {
                                item.showChildren = false;
                                delete item.showOnSearch;
                                delete item.lastInTree;
                            });

                            if ($scope.customSearch && customSearch) { //concurrent access: check both current and saved flags
                                var i = 0;
                                var root = null;
                                if (data && data.length > 0) {
                                    root = data[0].guid;
                                }

                                angular.forEach(data, function(item) {

                                    var index = _.findIndex($scope.data, function(element) {
                                        return element.guid == item.guid;
                                    });
                                    if (index>=0) {

                                        $scope.data[index].showOnSearch = true;
                                        $scope.data[index].showChildren = true;

                                        if (root != null && !parentGuid(item) && root != item.guid) {
                                            if ($scope.data[index - 1]) {
                                                $scope.data[index - 1].showChildren = false;
                                                $scope.data[index - 1].lastInTree = true;
                                            }
                                            root = item.guid;
                                        }


                                        var nextItem = data[i + 1];
                                        if (nextItem && item.guid == parentGuid(nextItem)) {

                                            //var childrenMap = $scope.childrenLoaded[code(item)];
                                            var childrenMap = $scope.childrenLoaded[keyForMap(item)];

                                            var alreadyExistInTree = childrenMap ? _.find(childrenMap.children, function (child) {
                                                return child.guid == nextItem.guid;
                                            }) : null;

                                            var searchItem = alreadyExistInTree ? alreadyExistInTree : nextItem;
                                            searchItem.level = $scope.data[index].level + 1;
                                            searchItem.showOnSearch = true;

                                            if (!childrenMap || !childrenMap.loaded) {
                                                if (!childrenMap) {
                                                    //$scope.childrenLoaded[code(item)] = { children:[] };
                                                    $scope.childrenLoaded[keyForMap(item)] = { children: [] };
                                                    //childrenMap = $scope.childrenLoaded[code(item)];
                                                    childrenMap = $scope.childrenLoaded[keyForMap(item)];
                                                }

                                                if (!alreadyExistInTree) {
                                                    $scope.data.splice(index + 1, 0, nextItem);
                                                    // $scope.childrenLoaded[code(item)].children.push(nextItem);
                                                    $scope.childrenLoaded[keyForMap(item)].children.push(nextItem);
                                                }
                                            }
                                        } else {
                                            $scope.data[index].showChildren = false;
                                            $scope.data[index].lastInTree = true;
                                        }
                                    }

                                    i++;
                                });

                                $scope.showChildren = true;
                            } else if (!$scope.customSearch && !customSearch) { //concurrent access: check both current and saved flags
                                $scope.data = angular.copy(data);
                                $scope.childrenLoaded = {};

                                angular.forEach($scope.data, function(item) {
                                    if (angular.isUndefined(item.level)) {
                                        item.level = 0;
                                    }

                                });
                            }
                        };

                        $scope.childrenLoaded = {};
                        $scope.loadChildren = function(item, index) {
                           // console.log('load children '+keyForMap(item));
                            //console.log('children map '+angular.toJson($scope.childrenLoaded[keyForMap(item)]));
                            //var childrenMap = $scope.childrenLoaded[code(item)];
                            var childrenMap = $scope.childrenLoaded[keyForMap(item)];
                            if (childrenMap && childrenMap.loaded) {
                                if ($scope.customSearch && item.lastInTree) {
                                    angular.forEach(childrenMap.children, function(child) {
                                        child.showOnSearch = true;
                                    });
                                }
                                return;
                            }

                            var request = {
                                parentGuid : item.guid,
                                excludedGuids:params.excludedGuids,
                                actual:ctrl.defaultStatus
                                //status : 'ACTIVE'
                            };

                            //request[$scope.parentNsiObjectName] = item.nsiObjectName?item.nsiObjectName:$scope.type;
                            request['nsiObjectName'] = item.nsiObjectName?item.nsiObjectName:$scope.type;
                            ctrl.ClassifierResource.children()
                                .queryPost([], request, function(children) {

                                    for (var i = 0; i < children.length; i++) {
                                        children[i].level = item.level + 1;
                                        if ($scope.customSearch) {
                                            children[i].showOnSearch = true;
                                        }

                                        var inTree = false;
                                        if (childrenMap && childrenMap.children) {
                                            for (var key in childrenMap.children) {
                                                if (childrenMap.children[key].guid == children[i].guid) {
                                                    inTree = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (!inTree) {
                                            $scope.data.splice(index + i + 1, 0, children[i]);
                                        }
                                    }

                                   // $scope.childrenLoaded[code(item)] = {
                                    $scope.childrenLoaded[keyForMap(item)] = {
                                        children : children,
                                        loaded : true
                                    };
                                }, $scope.errorCallback);

                        };
                        if ($scope.hierarchy) {
                            ctrl.ClassifierResource
                                .header()
                                .queryObject([], { nsiObjectName: $scope.type}, function ( header) {
                                    if (header.header) {
                                        $scope.nsiObjectName = header.header.nsiObjectName;
                                        $scope.baseNsiObjectName = header.header.baseNsiObjectName;


                                    } else{
                                        if (angular.isArray(header) && header[0]){
                                            $scope.nsiObjectName = header[0].nsiObjectName;
                                            $scope.baseNsiObjectName = header[0].baseNsiObjectName;
                                        }
                                    }

                                    $scope.search($scope.searchParameters);

                                }, $scope.errorCallback);

                        }else{
                            $scope.search($scope.searchParameters);
                        }


                        $scope.errorCallback = function(error) {
                            commonDialogs.error('Во время работы системы произошла ошибка');
                        };

                        $scope.parseBoolean = function(value) {
                            return JSON.parse(value);
                        };

                        var code = function(item) {
                            return item.fieldValue1;
                        };

                        var name = function(item) {
                            /*
                             * TODO : всегда возвращать item.fieldValue2, после изменения ЧТЗ
                             * Текущее поведение это ХАК
                             */
                            var retVal = item.fieldValue2;
                            
                            switch(item.nsiObjectName) {                                    
                            case "AdditionalServiceType" :
                                retVal = item.fieldValue2;
                                break;
                            case "AdditionalService" :
                                retVal = item.fieldValue3;
                                break;
                            case "MunicipalService" :
                                retVal = item.fieldValue2;
                                break;                                    
                            case "MainMunicipalService" :
                                retVal = item.fieldValue3;
                                break;                                    
                            case "TechnicalMunicipalService" :
                                retVal = item.fieldValue4;
                                break;                                    
                            default:    
                                retVal = item.fieldValue2;
                            }
                            
                            return retVal;
                        };
                        
                        $scope.name = function(item) {
                            return name(item);
                        };

                        var parentGuid = function(item) {
                            return item.parentGuid;
                        };

                        var hasChildren = function(item) {
                            return item.hasChildren;
                        };
                        var keyForMap =function(item){
                            //console.log('key '+item.guid);
                            return item.guid;
                        };

                    };
                }

                return $modal.open(modalDefaults).result;
            };
        }])
;