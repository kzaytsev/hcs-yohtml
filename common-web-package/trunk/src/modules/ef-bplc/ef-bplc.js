/**
 * ЭФ_БПЛЦ: Блок поиска организации, физического лица
 */
angular.module('common.ef-bplc', [
    'ui.utils',
    'common.validate',
    'common.ef-spr',
    'common.required-field',
    'common.hcs-datepicker',
    'ru.lanit.hcs.nsi.rest.NsiPpaService'
])
    /**
     * organizationMode - режим работы формы (true - поиск организации, false - поиск ФЛ)
     * orgMode - режим работы Организация (см. описание ВБОРГ)
     * fl - режим работы ФЛ (см. описание ВБФЛ)
     * */
    .directive('efBplcForm', function () {
        return {
            restrict: 'E',
            scope: {
                searchParameters: '=',
                orgMode: '=',
                flMode: '='
            },
            templateUrl: 'ef-bplc/ef-bplc.tpl.html',
            controller: 'efBplcController'
        };
    })

    .constant('personTypesCodes', {
        LEGAL_ENTITY: '1',
        NATURAL_PERSON: '2',
        SOLE_TRADER: '3'
    })

    .factory('efBplcNsiCache', [
        '$NsiPpaService',
        function($NsiPpaService){

            var personTypes = null;

            function getPersonTypes(scb, ecb){
                if(!personTypes){
                    $NsiPpaService.findPersonsType({}, function(result){
                        personTypes = result;
                        if(scb){
                            scb(result);
                        }
                    }, ecb);
                }else{
                    if(scb) {
                        scb(personTypes);
                    }
                }
            }

            function getPersonTypeByCode(personTypeCode){
                var result = null;
                angular.forEach(personTypes, function(personType){
                    if(personType.code === personTypeCode){
                        result = personType;
                    }
                });
                return result;
            }

            getPersonTypes();

            return {
                getPersonTypes: getPersonTypes,
                getPersonTypeByCode: getPersonTypeByCode
            };
        }
    ])

    .controller('efBplcController', [
        '$scope',
        'referenceChooserDialog',
        'personTypesCodes',
        'efBplcNsiCache',
        'commonDialogs',
        function ($scope,
                  referenceChooserDialog,
                  personTypesCodes,
                  efBplcNsiCache,
                  commonDialogs) {

            $scope.isLegalEntity = function(personType){
                return personType && personType.code === personTypesCodes.LEGAL_ENTITY;
            };
            $scope.isSoleTrader = function(personType){
                return personType && personType.code === personTypesCodes.SOLE_TRADER;
            };
            $scope.isNaturalPerson = function(personType){
                return personType && personType.code === personTypesCodes.NATURAL_PERSON;
            };

            $scope.errorCallback = function (error) {
                commonDialogs.error('Во время работы системы произошла ошибка');
            };

            efBplcNsiCache.getPersonTypes(function(result){
                $scope.personTypes = result;
                if($scope.orgMode){
                    $scope.searchParameters.personType = efBplcNsiCache.getPersonTypeByCode(personTypesCodes.LEGAL_ENTITY);
                }
                if($scope.flMode){
                    $scope.searchParameters.personType = efBplcNsiCache.getPersonTypeByCode(personTypesCodes.NATURAL_PERSON);
                }
            }, $scope.errorCallback);


            $scope.personTypesByMode = function(personType){
                return !$scope.orgMode && !$scope.flMode ||
                       $scope.orgMode && ($scope.isLegalEntity(personType) || $scope.isSoleTrader(personType)) ||
                       $scope.flMode && $scope.isNaturalPerson(personType);

            };

            $scope.birthDateStartOptions = {
                date: null,
                format: 'dd.MM.yyyy',
                appendToBody: false,
                required: false
            };
            $scope.birthDateEndOptions = {
                date: null,
                format: 'dd.MM.yyyy',
                appendToBody: false,
                required: false,
                dateIsAfter: function () {
                    return $scope.searchParameters.birthDateFrom;
                }
            };

            $scope.selectOkopfCode = function () {
                referenceChooserDialog
                    .setReferenceName('ОКОПФ')
                    .setType('Okopf')
                    .hasRadio(true)
                    .show().then(function (result) {
                        $scope.searchParameters.okopf = {guid : result.guid , code: result.code};
                    },
                    function () {});
            };

        }
    ])

    .directive('okopfCode', function () {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                ngModel.$formatters.push(function(value) {
                    if (value) {
                         return value.code;
                    }
                    return null;
                });

            }
        };
    })
;