/**
 * Блок контекстного поиска записи в ОКТМО (ЭФ_ПОКТМО)
 */
angular
    .module('common.ef-poktmo', [
        'lodash',
        'ui.select2',
        'common.ef-poktmo-rp',
        'ru.lanit.hcs.nsi.rest.ClassifierService'
    ])
    .directive('efPoktmoForm', function () {
        return {
            restrict: 'E',
            scope: {
                ngModel: '=',
                searchService: '=',
                loadedSearchParams: '=?',
                level: '=?',
                regionGuid: '=?',
                excludedGuids: '=?',
                excludedCodes: '=?',
                useCodes: '=?',
                oktmoSearchService: '=?'
            },
            templateUrl: 'ef-poktmo/ef-poktmo.tpl.html',
            controller: 'efPoktmoController'
        };
    })
    .controller('efPoktmoController', ['$scope', '$ClassifierService','$timeout',
        function ($scope, $ClassifierService,$timeout) {
            $scope.defaultSearchParameters = {};

           // $scope.tooShort = true;
            if ($scope.loadedSearchParams && $scope.loadedSearchParams.region) {
                $scope.searchParameters = angular.copy($scope.loadedSearchParams);
                $scope.expanded = true;
            } else {
                $scope.searchParameters = {};
                $scope.expanded = false;
            }


            $scope.select2Options = {
                placeholder: 'Начните вводить код позиции или наименование муниципального образования или населённого пункта',
                minimumInputLength: 4,
                minimumResultsForSearch: 0,
                initSelection: angular.noop,
                selectOnBlur: true,
                formatInputTooShort: function (term, minLength) {
                            $timeout(function(){$scope.tooShort = true; });
                            return '<span class="text-danger">Поисковый запрос должен содержать не менее 4 символов</span>';

                },
                formatLoadMore: function (pageNumber) {
                    return 'Отображены не все результаты. Показать больше...';
                },
                id: function (obj) {
                    return obj.guid;
                },
                //Allow manually entered text in drop down.
                createSearchChoice: function (term, data) {
                    if ($(data).filter(function () {
                        return this.name.localeCompare(term) === 0;
                    }).length === 0) {
                        $timeout(function(){$scope.tooShort = false;});
                        return {guid: term, text: term};
                    }
                    $timeout(function(){$scope.tooShort = false;});
                    return data;
                },
                formatResult: function (obj, container, query) {
                    if (!obj.hasOwnProperty('code')) {
                        //custom input
                        return obj.text;
                    }
                    var matchByCode = obj.code.toUpperCase().indexOf(query.term.toUpperCase());
                    var matchByName = obj.name.toUpperCase().indexOf(query.term.toUpperCase());

                    var oktmoCode = obj.code;
                    var oktmoName = obj.name;

                    if (matchByCode >= 0) {
                        oktmoCode = obj.code.substring(0, matchByCode);
                        oktmoCode += "<span class='select2-match'>";
                        oktmoCode += obj.code.substring(matchByCode, matchByCode + query.term.length);
                        oktmoCode += "</span>";
                        oktmoCode += obj.code.substring(matchByCode + query.term.length, obj.code.length);
                    }
                    if (matchByName >= 0) {
                        oktmoName = obj.name.substring(0, matchByName);
                        oktmoName += "<span class='select2-match'>";
                        oktmoName += obj.name.substring(matchByName, matchByName + query.term.length);
                        oktmoName += "</span>";
                        oktmoName += obj.name.substring(matchByName + query.term.length, obj.name.length);
                    }

                    var parent = obj.parent;
                    var parentText = '';
                    while (parent) {
                        parentText = parent.name + ' / ' + parentText;
                        parent = parent.parent;
                    }

                    return '<table>' +
                                '<tr>' +
                                    '<td class="poktmo-code-cell">' + oktmoCode + ' - </td>' +
                                    '<td>' + oktmoName + '</td>' +
                                '</tr>' +
                                 '<tr>' +
                                    '<td></td>' +
                                    '<td style="font-size: small;">' + parentText + '</td>' +
                                 '</tr>' +
                            '</table>';
                },
                formatSelection: function (obj) {
                    if (!obj.hasOwnProperty('code')) {
                        //custom input
                        return obj.text;
                    }
                    return obj.code + ' - ' + obj.name;
                },
                ajax: {
                    quietMillis: 500,
                    transport: function (query) {
                        if ($scope.oktmoSearchService) {
                            return $scope.oktmoSearchService.findOktmoExtendedSearch(query.data, angular.noop, $scope.errorCallback).then(query.success);                                                        
                        } else {
                            return $ClassifierService.findOktmoExtendedSearch(query.data, angular.noop, $scope.errorCallback).then(query.success);                            
                        }
                    },
                    data: function (term, page) {
                        var params = {
                            searchString: term,
                            page: page,
                            excludedCodes:$scope.excludedCodes,
                            useCodes:$scope.useCodes,
                            itemsPerPage: 10,
                            useCodesHierarchyScope:'CHILDREN',
                            actual:true
                        };

                        if ($scope.level) {
                            params.level = $scope.level;
                        }

                        if ($scope.regionGuid) {
                            params.regionGuid = $scope.regionGuid;
                        }

                        if ($scope.excludedGuids) {
                            params.excludedGuids = $scope.excludedGuids;
                        }
                        return params;
                    },
                    results: function (data, page, query) {
                        return {
                            more: !_.isEmpty(data.oktmoList) && data.oktmoList.length >= 10,
                            results: data.oktmoList
                        };
                    }
                }
            };


            $scope.search = function () {
                $scope.searchService({
                    oktmo: $scope.ngModel,
                    contextSearch: true,
                    excludedGuids: $scope.excludedGuids,
                    excludedCodes:$scope.excludedCodes,
                    useCodes:$scope.useCodes
                });
            };
            $scope.onclose=function(){
              $scope.tooShort=false;
              $scope.$apply();
            };

            $scope.errorCallback = function () {
                console.log('Произошла ошибка во время поиска ОКТМО');
            };

        }]).directive('select2Events', function() { //позволяет перехватить события select2
        return {
            scope: {
               select2Close : '&'
            },
            link: function(scope, element) {
                element.on('select2-close', function(e) {
                    scope.select2Close(e);
                });
            }
    };
});
