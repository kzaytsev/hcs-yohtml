/**
 * Created by mvorochaev on 27.01.2015.
 */
angular
    .module('common.error404', [])
    .directive('error404',[
        function () {
            return {
                restrict : 'E',
                templateUrl : 'error404/error404.tpl.html',
                controller : function () {

                }
            };
        }]);