/**
 * Группа полей для выбора отчетного периода (ЭФ_ВОП)
 */
angular.module('common.hcs-datepicker', ['ui.bootstrap', 'ui.utils', 'common.utils'])

    .directive('hcsDatepicker', ['$filter', function ($filter) {
        return {
            restrict: 'E',
            require: 'ngModel',
            scope: {
                dateAsString: '=ngModel',
                options: '=',
                disabled: '=?', //наличие этого свойства в директиве вызывает баги на IE (HCS-3272) - начинает глючить ui-mask. Вместо disabled следует использовать isDisabled
                isDisabled: '=?'
            },
            link: function ($scope, elem, attr, ngModel) {
                var validatorCallback = function (value) {
                    ngModel.$setValidity('required', !$scope.options.required || value);
                    if ($scope.date) {
                        ngModel.$setValidity('format', $scope.checkDate($scope.date));
                    }
                    ngModel.$setValidity('format', !$scope.invalidFormat);
                    if ($scope.options.dateIsAfter) {
                        ngModel.$setValidity('period', $scope.dateIsAfterValid);
                    }
                    if ($scope.options.maxDate) {
                        ngModel.$setValidity('maxDate', $scope.maxDateValid);
                    }
                    return value;
                };

                ngModel.$parsers.unshift(validatorCallback);
                ngModel.$formatters.unshift(validatorCallback);

                if ($scope.options.dateIsAfter) {
                    $scope.$watch($scope.options.dateIsAfter, function () {
                        validatorCallback($scope.dateAsString);
                    });
                }
            },
            controller: function ($scope, $dateParser) {
                $scope.options.onDateBlur = $scope.options.onDateBlur || angular.noop;
                $scope.options.onDateChange = $scope.options.onDateChange || angular.noop;
                $scope.checkIsDisabled = function(){
                  return $scope.disabled === true ||  $scope.isDisabled === true;
                };

                $scope.today = function () {
                    $scope.date = new Date();
                };

                $scope.clear = function () {
                    $scope.date = null;
                };

                $scope.disabledSelection = function (date, mode) {
                    if ($scope.options.disabled) {
                        return $scope.options.disabled(date, mode);
                    }
                    return false;
                };

                $scope.showButtonBar = function () {
                    if ($scope.options.hasOwnProperty('showButtonBar')) {
                        return $scope.options.showButtonBar;
                    }
                    return true;
                };

                $scope.closeOnDateSelection = function () {
                    if ($scope.options.hasOwnProperty('closeOnDateSelection')) {
                        return $scope.options.closeOnDateSelection;
                    }
                    return true;
                };

                $scope.appendToBody = function () {
                    if ($scope.options.hasOwnProperty('appendToBody')) {
                        return $scope.options.appendToBody;
                    }
                    return true;
                };

                $scope.showErrors = function () {
                    if ($scope.options.hasOwnProperty('showErrors')) {
                        return $scope.options.showErrors();
                    } else {
                        return $scope.showErrorOnEvent;
                    }
                };

                $scope.showFormatErrors = function () {
                    return !!$scope.options.showFormatErrorsImmediately;
                };

                $scope.validatePeriodImmediately = function () {
                    if ($scope.options.dateIsAfter && $scope.options.hasOwnProperty('validatePeriodImmediately')) {
                        return $scope.options.validatePeriodImmediately;
                    } else {
                        return false;
                    }
                };

                $scope.hideRangeValidationError = function () {
                    if ($scope.options.hasOwnProperty('hideRangeValidationError')) {
                        return $scope.options.hideRangeValidationError;
                    } else {
                        return false;
                    }
                };

                $scope.$on('showError', function () {
                    $scope.showErrorOnEvent = true;
                });
                $scope.$on('hideError', function () {
                    $scope.showErrorOnEvent = false;
                });

                $scope.open = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    if ($scope.options.open) {
                        $scope.options.open();
                    }

                    $scope.opened = true;
                };

                $scope.options.close = function () {
                    $scope.opened = false;
                };

                $scope.dateOptions = {
                    formatYear: 'yyyy',
                    startingDay: 1
                };

                $scope.errorMessages = {
                    fieldRequiredMsg: 'Поле обязательно для заполнения',
                    notBeforeNowMsg: 'Значение не может ранее текущей даты и времени',
                    invalidFormatMsg: 'Неверный формат поля',
                    endDateNotBeforeStartDateMsg: 'Дата окончания не должна быть раньше даты начала'
                };

                if ($scope.options.fieldRequiredMsg) {
                    $scope.errorMessages.fieldRequiredMsg = $scope.options.fieldRequiredMsg;
                }

                if ($scope.options.notBeforeNowMsg) {
                    $scope.errorMessages.notBeforeNowMsg = $scope.options.notBeforeNowMsg;
                }

                if ($scope.options.invalidFormatMsg) {
                    $scope.errorMessages.invalidFormatMsg = $scope.options.invalidFormatMsg;
                }

                if ($scope.options.endDateNotBeforeStartDateMsg) {
                    $scope.errorMessages.endDateNotBeforeStartDateMsg = $scope.options.endDateNotBeforeStartDateMsg;
                }

                if (!$scope.options.classError) {
                    $scope.classError = "text-danger";
                } else {
                    $scope.classError = $scope.options.classError;
                }

                $scope.defaultFormat = 'dd.MM.yyyy';
                $scope.standardFormats = [$scope.defaultFormat, 'MM.yyyy'];
                $scope.format = $scope.options.format ? $scope.options.format : $scope.defaultFormat;

                if (_.indexOf($scope.standardFormats, $scope.format) == -1) {
                    $scope.nonStandard = true;
                }

                $scope.isEmpty = function () {
                    return $scope.options.required && !$scope.dateAsString;
                };

                $scope.dateValid = true;
                $scope.maxDateValid = true;
                $scope.dateIsAfterValid = true;
                $scope.isNotLateThanCurrent = true;

                $scope.getDateFromString = function (dateAsString, format) {
                    var date = null;
                    if (format == $scope.defaultFormat && /^\d{2}\.\d{2}\.\d{4}$/.test(dateAsString)) {
                        date = $dateParser(dateAsString, format);
                    }
                    if (format == 'MM.yyyy' && /^\d{2}\.\d{4}$/.test(dateAsString)) {
                        date = $dateParser(dateAsString, format);
                    }

                    if (date) {
                        date.setHours(0, 0, 0, 0);
                    }
                    return date;
                };

                $scope.convertDateFromString = function () {
                    var date = null;

                    if (typeof $scope.dateAsString == 'string' && $scope.dateAsString.indexOf('.') > -1) {
                        $scope.dateAsString = $scope.dateAsString.replace(/\./g, '');
                    }

                    if ($scope.format == $scope.defaultFormat && /^\d{8}$/.test($scope.dateAsString)) {
                        date = $dateParser($scope.dateAsString, 'ddMMyyyy');
                    }
                    if ($scope.format == 'MM.yyyy' && /^\d{6}$/.test($scope.dateAsString)) {
                        date = $dateParser($scope.dateAsString, 'MMyyyy');
                    }

                    if (date) {
                        date.setHours(0, 0, 0, 0);
                    }
                    return date;
                };
                $scope.fromDate = false;
                $scope.isReset = false;
                $scope.fromDateAsString = false;
                $scope.$watch('dateAsString', function () {
                    if ($scope.options.rpdDateFormat && !$scope.fromDate) {
                        $scope.fromDateAsString = !$scope.fromDate;
                        $scope.date = $dateParser($scope.dateAsString, $scope.defaultFormat);
                        return;
                    }
                    if ($scope.nonStandard) {
                        $scope.fromDate = false;
                        return;
                    }
                    $scope.invalidFormat = false;
                    $scope.dateIsAfterValid = true;
                    $scope.isNotLateThanCurrent = true;
                    var date = $scope.convertDateFromString();
                    if (date != null) {
                        $scope.checkDate(date);
                        $scope.date = date;
                    } else {
                        if ($scope.dateAsString) {
                            $scope.invalidFormat = true;
                            $scope.date = '';
                        }else{
                            $scope.date = new Date();
                            $scope.isReset = true;
                        }
                    }
                });

                $scope.$watch('date', function () {
                    var date = $scope.date;
                    if (date) {
                        $scope.checkDate(date);
                        $scope.invalidFormat = false;
                        if ($scope.options.rpdDateFormat && $scope.fromDateAsString) {
                            $scope.fromDateAsString = false;
                            $scope.fromDate = false;
                        } else {
                            if($scope.isReset){
                                $scope.isReset = false;
                            }else {
                                $scope.fromDate = true;
                                $scope.dateAsString = $filter('date')(date, $scope.nonStandard ? $scope.defaultFormat : $scope.format);
                            }
                        }
                    } else {
                        if (date == null) {
                            $scope.dateAsString = '';
                            if ($scope.options.rpdDateFormat) {
                                $scope.fromDate = false;
                                $scope.fromDateAsString = false;
                            }
                        }
                    }
                });

                $scope.checkDate = function (date) {
                    var greaterMin = true;
                    var lessMax = true;
                    var customValid = true;
                    var isAfterValid = true;

                    date.setHours(0, 0, 0, 0);

                    if ($scope.options.minDate) {
                        var min = new Date($scope.options.minDate);
                        min.setHours(0, 0, 0, 0);
                        greaterMin = min.getTime() <= date.getTime();
                    }
                    if ($scope.options.maxDate) {
                        var max = new Date($scope.options.maxDate);
                        max.setHours(0, 0, 0, 0);
                        lessMax = date.getTime() <= max.getTime();
                    }
                    if ($scope.options.validate) {
                        customValid = $scope.options.validate(date);
                    }
                    if ($scope.options.notLateThanCurrent) {
                        $scope.isNotLateThanCurrent = $scope.options.notLateThanCurrent(date);
                    }
                    if ($scope.options.dateIsAfter) {
                        var beforeDate = $scope.options.dateIsAfter();
                        if (beforeDate) {
                            beforeDate = $scope.getDateFromString(beforeDate, $scope.nonStandard ? $scope.defaultFormat : $scope.format);
                            if (beforeDate) {
                                beforeDate.setHours(0, 0, 0, 0);
                                if ($scope.options.strongComparing) {
                                    $scope.dateIsAfterValid = date.getTime() > beforeDate.getTime();
                                } else {
                                    $scope.dateIsAfterValid = date.getTime() >= beforeDate.getTime();
                                }
                            }
                        }
                    }

                    $scope.maxDateValid = lessMax;
                    $scope.dateValid = greaterMin && lessMax && customValid;

                    $scope.options.valid = $scope.dateValid && $scope.dateIsAfterValid && $scope.isNotLateThanCurrent;
                    return $scope.options.valid;
                };

                $scope.getUTC = function () {
                    if (!$scope.date) {
                        return '';
                    }

                    var utc = -$scope.date.getTimezoneOffset() / 60;
                    return utc >= 0 ? '(UTC+' + utc + ')' : '(UTC-' + utc + ')';

                };

                if ($scope.format == 'MM.yyyy') {
                    $scope.mask = '99.9999';
                    if ($scope.options.showPlaceholder) {
                        $scope.placeholder = 'ММ.ГГГГ';
                    }
                } else {
                    $scope.mask = '99.99.9999';
                    if ($scope.options.showPlaceholder) {
                        $scope.placeholder = 'ДД.ММ.ГГГГ';
                    }
                }

                if ($scope.options.initDate) {
                    $scope.date = $scope.options.initDate;
                }

                if ($scope.options.datepickerMode) {
                    $scope.dateOptions = {
                        minMode : $scope.options.datepickerMode
                    };
                }

            },
            transclude: true,
            templateUrl: 'hcs-datepicker/hcs-datepicker.tpl.html'
        };
    }])
;