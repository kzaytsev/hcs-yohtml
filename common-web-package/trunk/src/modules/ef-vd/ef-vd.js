/**
 * Модальное окно для выбора дома (ЭФ_ВД)
 */
angular.module('common.ef-vd', [
    'common.ef-bp',
    'common.ef-pa',
    'hcs-table',
    'common.ef-kmn',
    'resources.houseCommonResource',
    'common.auth',
    'common.filter',
    'common.dialogs'
])
    .service('houseChooserDialog', function ($modal, $filter, HouseByOrgSearchCommonResource, HouseTypeCommonResource, Auth, commonDialogs) {

        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            size: 'lg',
            templateUrl: 'ef-vd/ef-vd-dialog.tpl.html'
        };

        var modalOptions = {
            closeButtonText: 'Отменить',
            actionButtonText: 'Выбрать'
        };

        var params = {};

        var self = this;

        self.residentialOnly = function () {
            params.houseTypeCode = 2;
            return self;
        };

        self.multiroomOnly = function () {
            params.houseTypeCode = 1;
            return self;
        };

        params.hasCheckbox = false;
        self.selectCheckbox = function () {
            params.hasCheckbox = true;
            return self;
        };

        params.excludedGuids = [];
        self.excludedGuids = function(guids) {
            params.excludedGuids = guids;
            return self;
        };

        params.excludedHouseFiasGuids = [];
        self.excludedHouseFiasGuids = function(guids) {
            params.excludedHouseFiasGuids = guids;
            return self;
        };

        params.showAllHouses = false;
        self.selectShowAllHouses = function () {
            params.showAllHouses = true;
            return self;
        };

        self.returnObject = function () {
            params.returnObject = true;
            return self;
        };

        self.defaultSearchOff = function () {
            params.defaultSearchOff = true;
            return self;
        };
        self.show = function () {
            if (!modalDefaults.controller) {
                modalDefaults.controller = function ($scope, $modalInstance) {
                    $scope.defaultSearchParameters = {};
                    $scope.searchParameters = angular.copy($scope.defaultSearchParameters);

                    $scope.searchService = function () {
                        $scope.radioItem = null;
                        return $scope.searchResultTable.state.refresh();
                    };

                    $scope.houseTypeCode = params.houseTypeCode;
                    $scope.excludedGuids = params.excludedGuids;
                    $scope.excludedHouseFiasGuids = params.excludedHouseFiasGuids;
                    $scope.hasCheckbox = params.hasCheckbox;
                    $scope.showAllHouses = params.showAllHouses;
                    $scope.checkedItems = {};

                    $scope.radioOrSelectButtonDisabled = function (houseType) {
                        return $scope.houseTypeCode && houseType && $scope.houseTypeCode != houseType.code;
                    };

                    $scope.getHouseTypeName = function (guid) {
                        if (!$scope.houseTypes || $scope.houseTypes.length === 0) {
                            return '';
                        }

                        var result = $scope.houseTypes.filter(function (item) {
                            return item.guid == guid;
                        });

                        return result && result.length > 0 ? result[0] : '';
                    };

                    $scope.chooseHouse = function (id) {
                        var result = $scope.searchResults.filter(function (item) {
                            return item.guid == id;
                        });
                        if (result && result.length > 0) {
                            if (!$scope.radioOrSelectButtonDisabled(result[0].houseType)) {
                                $scope.radioItem = id;
                            }
                        }
                        if($scope.hasCheckbox){
                            if ($scope.checkedItems[id]) {
                                delete $scope.checkedItems[id];
                            } else {
                                $scope.checkedItems[id] = result;
                            }
                        }
                    };

                    $scope.modalOptions = modalOptions;
                    $scope.modalOptions.ok = function (result) {
                        var returnResult = null;
                        if($scope.hasCheckbox){
                            returnResult = _.values($scope.checkedItems);
                        }else{
                            returnResult = $scope.radioItem;
                            if (params.returnObject) {
                                var res = $scope.searchResults.filter(function (item) {
                                    return item.guid == $scope.radioItem;
                                });

                                if (res && res.length > 0) {
                                    returnResult = res[0];
                                }
                            }
                        }
                        $modalInstance.close(returnResult);
                        params = {};
                    };
                    $scope.modalOptions.close = function (result) {
                        $modalInstance.dismiss('cancel');
                        params = {};
                    };

                    $scope.errorCallback = function (error) {
                        commonDialogs.error('Во время работы системы произошла ошибка');
                    };

                    $scope.searchResults = [];
                    $scope.searchResultTable = {};
                    $scope.searchResultTable.config = {
                        dataSource: refresh,
                        sortable: false,
                        noRefreshOnInit: true,
                        modal: true
                    };
                    $scope.searchResultTable.state = {};

                    HouseTypeCommonResource.query(
                        [],
                        {},
                        function (types) {
                            $scope.houseTypes = angular.copy(types);

                            if ($scope.houseTypeCode) {
                                var result = $scope.houseTypes.filter(function (item) {
                                    return item.code == $scope.houseTypeCode;
                                });

                                if (result && result.length > 0) {
                                    $scope.searchParameters.houseTypeGuid = result[0].guid;
                                    $scope.defaultSearchParameters.houseTypeGuid = result[0].guid;
                                }
                            }

                            if (!params.defaultSearchOff) {
                                $scope.searchService();
                                params.defaultSearchOff = false;
                            }
                        },
                        angular.noop
                    );

                    function refresh(query) {
                        var params = angular.copy($scope.searchParameters);
                        delete params.address;
                        delete params.zipCode;

                        if (Auth && Auth.user && Auth.user.organizationGuid) {
                            params.organizationGuid = Auth.user.organizationGuid;
                        }
                        if (!_.isEmpty($scope.excludedGuids)) {
                            params.excludedHouseGuid = $scope.excludedGuids;
                        }
                        if (!_.isEmpty($scope.excludedHouseFiasGuids)) {
                            params.excludedHouseFiasGuid = $scope.excludedHouseFiasGuids;
                        }
                        params.showAllHouses = $scope.showAllHouses;

                        angular.forEach(params, function (value, property) {
                            if (!value) {
                                delete params[property];
                            }
                        });
                        return HouseByOrgSearchCommonResource.create(
                            params,
                            angular.noop,
                            angular.noop,
                            query.queryParams
                        ).then(
                            function(responce) {
                                /* оставляем себе ссылку на результаты,
                                нужно для совместимости с фильтрами старой версии реализации*/
                                $scope.searchResults = responce['items'];
                                return responce;
                            },
                            function () {
                                $scope.errorCallback();
                                return {
                                    total: 0,
                                    items: []
                                };
                            }
                        );
                    }
                };
            }
            return $modal.open(modalDefaults).result;
        };
    })
;