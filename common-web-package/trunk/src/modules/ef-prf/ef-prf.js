/**
 * 2.2    Блок прикрепления файла (ЭФ_ПрФ)
 */

angular
    .module('common.ef-prf', ['angularFileUpload', 'common.dialogs', 'common.config', 'flow', 'common.ef-bpp'])
    .config(function (flowFactoryProvider) {
        flowFactoryProvider.defaults = {
            target: '/filestore/fileUploadServlet',
            permanentErrors: [404, 500, 501],
            maxChunkRetries: 1,
            chunkRetryInterval: 5000,
            simultaneousUploads: 1,
            testChunks: false,
            withCredentials: true,
            query: function (flowFile) {
                var headers = {
                    context: flowFile.flowObj.opts.context,
                    description: flowFile.description
                };
                if (flowFile.contentGuid) {
                    headers.contentGuid = flowFile.contentGuid;
                }
                return headers;
            },
            chunkSize: 5 * 1024 * 1024,
            forceChunkSize: true,
            preprocess: function (flowChunk) {
                flowChunk.flowObj.opts.target = '/' + flowChunk.flowObj.opts.contextPath + '/fileUploadServlet';
                if (flowChunk.fileObj.chunks.length > 1) {
                    var firstChunk = flowChunk.fileObj.chunks[0];
                    if (firstChunk.xhr && firstChunk.xhr.status === 200) {
                        var guid;
                        try {
                            var responseJson = angular.fromJson(firstChunk.xhr.responseText);
                            guid = responseJson.fileInfo.contentGuid;
                        } catch (e) {
                            delete flowChunk.fileObj.contentGuid;
                            return commonDialogs.error("Ошибка при загрузке файла");
                        }
                        flowChunk.fileObj.contentGuid = guid;
                    }
                }
                flowChunk.preprocessFinished();
            }
        };
    })
    .constant('FILE_STATUS', {
        EDITING: 'FILE_STATUS.edit',
        UPLOADING: 'FILE_STATUS.upl',
        UPLOADING_ERROR: 'FILE_STATUS.upl_err',
        UPLOADED: 'FILE_STATUS.upldd'
    })
    .constant('MAX_FILE_SIZE', 1 * 1024 * 1024 * 1024)
    .directive('efPrfProgressBar', function () {
        return {
            restrict: 'E',
            scope: {
                file: '=',
                flow: '='
            },
            template:
                '<div class="progress-base">' +
                    '<div class="progress-base__label" ng-if="progress">' +
                        'Идет процесс загрузки. Загружено: ' +
                        '<span class="progress-base__label_bold">'+
                            '{{progress}}%' +
                        '</span>'+
                    '</div>' +
                    '<div class="progress-base__label" ng-if="!progress">' +
                        'Ожидает загрузки' +
                    '</div>' +
                    '<div class="progress">' +
                        '<div class="progress-bar" role="progressbar" aria-valuenow="{{progress}}" aria-valuemin="0" aria-valuemax="100" ng-style="progressStyle">' +
                        '</div>' +
                    '</div>' +
                '</div>',
            controller: function ($scope) {

                $scope.progress = 0;
                $scope.progressStyle = {
                    width: 0
                };
                $scope.flow.on('uploadStart', function () {
                    $scope.progress = 5;
                    $scope.progressStyle = {
                        width: $scope.progress + '%'
                    };
                });
                $scope.flow.on('progress', function () {
                    $scope.progress = Math.floor($scope.file.progress() * 100);
                    $scope.progressStyle = {
                        width: $scope.progress + '%'
                    };
                });
                $scope.flow.on('complete', function () {
                    console.log(arguments);
                });
                $scope.flow.on('error', function () {
                    console.log(arguments);
                });
            }
        };
    })
    .directive('efPrfForm', function () {
        var controllerFallback = function ($scope, $fileUploader, commonDialogs, $preloader) {
            $scope.flowUpload = false;
            $scope.contextPath = $scope.contextPath || 'filestore';

            //https://github.com/nervgh/angular-file-upload/
            var uploader = $scope.uploader = $fileUploader.create({
                scope: $scope,
                url: '/filestore/uploadServlet',
                formData: [
                    { context: $scope.context }
                ],
                filters: [
                    function (item) {
                        return true;
                    }
                ]});

            uploader.filters.splice(0, 1);

            $scope.upload = function() {
                uploader.queue[0].formData.push({description : $scope.customPath});
                uploader.uploadAll();
                $preloader.show();
            };

            $scope.defaultRegexPattern = "\\.(pdf)|(docx)|(doc)|(rtf)|(txt)|(xls)|(xlsx)|(jpeg)|(jpg)|(bmp)|(tif)|(tiff)|(gif)|(zip)|(rar)|(csv)|(odp)|(odf)|(ods)|(odt)|(sxc)|(sxw)$";
            $scope.noUserExtensions = _.isUndefined($scope.extensions) || !_.isArray($scope.extensions);

            if ($scope.noUserExtensions) {
                $scope.regexPattern = $scope.defaultRegexPattern;
            } else {
                var joined = _.map($scope.extensions,
                    function(extension) {
                        return '(' + extension + ')';
                    }).join('|');

                $scope.regexPattern = '\\.' + joined + '$';
            }

            $scope.generateAccepts = function() {
                if ($scope.noUserExtensions) {
                    return $scope.defaultAccept.join(',');
                }
                return $scope.extensions.map(function(e) { return '.' + e; }).join(',');
            };

            $scope.getExtensions = function() {
                if ($scope.noUserExtensions) {
                    return 'pdf, docx, doc, rtf, txt, xls, xlsx, jpeg, jpg, bmp, tif, tiff, gif, zip, rar, csv, odp, odf, ods, odt, sxc, sxw';
                }
                return $scope.extensions.join(', ');
            };

            uploader.bind('afteraddingfile', function (event, item) {
                $scope.extensionNotValid = false;
                if (!new RegExp($scope.regexPattern).test(item.file.name)) {
                    $scope.extensionNotValid = true;
                    return;
                }

                uploader.queue.splice(0, uploader.queue.length - 1);

                $scope.fakePath = item.file.name;
                $scope.customPath = item.file.name;
            });

            uploader.bind('whenaddingfilefailed', function (event, item) {
                //console.info('When adding a file failed', item);
            });

            uploader.bind('afteraddingall', function (event, items) {
                //console.info('After adding all files', items);
            });

            uploader.bind('beforeupload', function (event, item) {
                //console.info('Before upload', item);
            });

            uploader.bind('progress', function (event, item, progress) {
                //console.info('Progress: ' + progress, item);
            });

            uploader.bind('success', function (event, xhr, item, response) {
                $preloader.hide();
                if (xhr.status == 400) {
                    commonDialogs.error('Во время работы системы произошла ошибка');
                } else if ($scope.callback) {
                    $scope.$apply(function() {
                        $scope.callback(response);
                        $scope.fakePath = '';
                        $scope.customPath = '';
                    });
                }
            });

            uploader.bind('cancel', function (event, xhr, item) {
                //console.info('Cancel', xhr, item);
            });

            uploader.bind('error', function (event, xhr, item, response) {
                //console.info('Error', xhr, item, response);
                $preloader.hide();
                commonDialogs.error('Во время работы системы произошла ошибка');
            });

            uploader.bind('complete', function (event, xhr, item, response) {
                //console.info('Complete', xhr, item, response);
            });

            uploader.bind('progressall', function (event, progress) {
                //console.info('Total progress: ' + progress);
            });

            uploader.bind('completeall', function (event, items) {
                //console.info('Complete all', items);
            });

            $scope.$on('showError', function () {
                $scope.showError = true;
            });
            $scope.$on('hideError', function () {
                $scope.showError = false;
            });

            $scope.defaultAccept = [
                'application/pdf',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'application/msword',
                '.rtf',
                'text/plain',
                'application/vnd.ms-excel',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                '.jpeg',
                '.jpg',
                'image/bmp',
                '.tif',
                'image/tiff',
                'image/gif',
                'application/zip',
                'application/x-rar-compressed',
                '.rar',
                '.csv',
                'application/vnd.oasis.opendocument.presentation',
                '.odf',
                'application/vnd.oasis.opendocument.spreadsheet',
                'application/vnd.oasis.opendocument.text',
                '.sxc',
                '.sxw',
                'application/vnd.sun.xml.calc',
                'application/vnd.sun.xml.writer'
            ];
        };

        return {
            restrict: 'AEC',
            scope: {
                context: '@',
                callback: '=?',
                disabled: '=?',
                notRequired: '=?',
                extensions: '=?',
                max: '=?max',
                internalMax: '@?',
                contextPath: '@?'
            },
            templateUrl: 'ef-prf/ef-prf-form.tpl.html',
            controller: (!window.FileReader) ? controllerFallback : function ($scope, $fileUploader, commonDialogs, $preloader, FILE_STATUS, getExtensionByFilename, MAX_FILE_SIZE, getHumanReadableFilesize) {
                $scope.getExtensionByFilename = getExtensionByFilename;
                $scope.getHumanReadableFilesize = getHumanReadableFilesize;
                $scope.MAX_FILE_SIZE = MAX_FILE_SIZE;
                $scope.contextPath = $scope.contextPath || 'filestore';
                $scope.flowUpload = true;
                $scope.max = Math.abs(parseInt($scope.max, 10)) || 99;
                $scope.maxFiles = function(){
                    return ($scope.internalMax === undefined) ? $scope.max : $scope.internalMax;
                };
                $scope.numberOfFilesCouldBeAttached = function(){
                    return $scope.maxFiles() - $scope.$flow.files.length;
                };

                $scope.flow = {}; // there will be flow object
                $scope.cancelFile = function(file) {
                    file.cancel();
                    $scope.errors = {};
                };
                $scope.initFlowObject = function () {
                    $scope.$flow = $scope.flow.flow;
                    $scope.$flow.opts.context = $scope.context;
                    $scope.$flow.opts.contextPath = $scope.contextPath;
                    $scope.$flow.on('uploadStart', function () {
                        $scope.uploading = true;
                    });
                    $scope.$flow.on('fileSuccess', function (file, msg) {
                        if ($scope.callback) {
                            $scope.$apply(function () {
                                $scope.callback(angular.fromJson(msg));
                            });
                        }
                        file.cancel();
                    });
                    $scope.$flow.on('complete', function () {
                        $scope.uploading = false;
                    });
                    $scope.$flow.on('error', function (msg, file) {
                        file.status = FILE_STATUS.UPLOADING_ERROR;
                        try{
                            var error = angular.fromJson(msg);
                            if(error && error.title === 'InvalidSizeException' && error.errorMessage){
                                file.errorMsg = error.errorMessage;
                            }
                        }catch(e){

                        }

                    });
                    $scope.$flow.on('fileRetry', function(file){
                        $scope.uploading = true;
                        file.status = FILE_STATUS.UPLOADING;
                        $scope.$apply();
                    });
                    $scope.$flow.on('fileAdded', function (file, e) {
                        $scope._fileQueueInfo = $scope._fileQueueInfo || {errors: {}, added: 0};
                        file.status = FILE_STATUS.EDITING;
                        file.description = file.name;
                        var extension = file.name.split('.').pop().toLowerCase();

                        if ($scope.imageExtensions.indexOf(extension) !== -1) {
                            file.isImage = true;
                        }
                        if ($scope.$flow.files.length + $scope._fileQueueInfo.added >= $scope.maxFiles()) {
                            $scope._fileQueueInfo.errors.tooMuchFiles = true;
                            return false;
                        }
                        if (file.size > MAX_FILE_SIZE) {
                            $scope._fileQueueInfo.errors.tooBigFile = true;
                            return false;
                        }
                        if (file.size === 0) {
                            $scope._fileQueueInfo.errors.emptyFile = true;
                            return false;
                        }
                        if ($scope.getExtensions().indexOf(extension) === -1) {
                            $scope._fileQueueInfo.errors.extensionNotValid = true;
                            return false;
                        }
                        $scope._fileQueueInfo.added++;
                        return true;
                    });
                    $scope.$flow.on('filesAdded', function (files, e) {
                        $scope.errors = angular.copy($scope._fileQueueInfo.errors);
                        $scope._fileQueueInfo = null;
                        if ($scope.$flow.files.length + files.length > $scope.maxFiles()) {
                            $scope.tooMuchFiles = true;
                            return false;
                        } else {
                            $scope.tooMuchFiles = false;
                            return true;
                        }
                    });
                };
                $scope.startUpload = function () {
                    $scope.$flow.files.forEach(function (f) {
                        f.status = FILE_STATUS.UPLOADING;
                    });
                    $scope.$flow.opts.query.description = $scope.fileDescription;
                    $scope.$flow.upload();
                };


                $scope.defaultRegexPattern = "\\.(pdf)|(docx)|(doc)|(rtf)|(txt)|(xls)|(xlsx)|(jpeg)|(jpg)|(bmp)|(tif)|" +
                "(tiff)|(gif)|(zip)|(rar)|(csv)|(odp)|(odf)|(ods)|(odt)|(sxc)|(sxw)$";
                $scope.noUserExtensions = _.isUndefined($scope.extensions) || !_.isArray($scope.extensions);

                if ($scope.noUserExtensions) {
                    $scope.regexPattern = $scope.defaultRegexPattern;
                } else {
                    var joined = _.map($scope.extensions,
                        function (extension) {
                            return '(' + extension + ')';
                        }).join('|');

                    $scope.regexPattern = '\\.' + joined + '$';
                }

                $scope.generateAccepts = function () {
                    if ($scope.noUserExtensions) {
                        return $scope.defaultAccept.join(',');
                    }
                    return $scope.extensions.map(function (e) {
                        return '.' + e;
                    }).join(',');
                };

                $scope.getExtensions = function () {
                    if ($scope.noUserExtensions) {
                        return 'pdf, docx, doc, rtf, txt, xls, xlsx, jpeg, jpg, bmp, tif, tiff, gif, zip, rar, csv, odp, odf, ods, odt, sxc, sxw';
                    }
                    return $scope.extensions.join(', ');
                };
                $scope.imageExtensions = 'jpeg,jpg,bmp';

                $scope.$on('showError', function () {
                    $scope.showError = true;
                });
                $scope.$on('hideError', function () {
                    $scope.showError = false;
                });

                $scope.defaultAccept = [
                    'application/pdf',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/msword',
                    '.rtf',
                    'text/plain',
                    'application/vnd.ms-excel',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    '.jpeg',
                    '.jpg',
                    'image/bmp',
                    '.tif',
                    'image/tiff',
                    'image/gif',
                    'application/zip',
                    'application/x-rar-compressed',
                    '.rar',
                    '.csv',
                    'application/vnd.oasis.opendocument.presentation',
                    '.odf',
                    'application/vnd.oasis.opendocument.spreadsheet',
                    'application/vnd.oasis.opendocument.text',
                    '.sxc',
                    '.sxw',
                    'application/vnd.sun.xml.calc',
                    'application/vnd.sun.xml.writer'
                ];
            }
        };
    })
;