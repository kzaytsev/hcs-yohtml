/**
 * Модальное окно выбора физичекого лица (ЭФ_ВБФЛ)
 */
angular
    .module('common.ef-vbfl', [
        'common.dialogs',
        'common.ef-bp',
        'ng-simple-grid',
        'common.ef-bplc',
        'ru.lanit.hcs.personregistry.PersonRegistryService',
        'common.ef-dobfl',
        'common.utils'
    ])
    .service('efVbflDialog', [
        '$modal',
        'commonDialogs',
        'efBplcNsiCache',
        'personTypesCodes',
        '$PersonRegistryService',
        'efDobflDialog',
        function ($modal,
                  commonDialogs,
                  efBplcNsiCache,
                  personTypesCodes,
                  $PersonRegistryService,
                  efDobflDialog) {
            var modalDefaults = {
                backdrop: 'static',
                keyboard: true,
                modalFade: true,
                templateUrl: 'ef-vbfl/ef-vbfl.tpl.html',
                size: 'lg'
            };

            function getDialogController(initializationParameters) {
                return function ($scope, $modalInstance) {

                    $scope.fiasHouseCode = initializationParameters.fiasHouseCode;
                    $scope.multipleChoice = initializationParameters.multipleChoice;
                    $scope.checkedItemsGuids = {};
                    $scope.checkedItems = [];

                    $scope.defaultSearchParameters = {
                        personType : efBplcNsiCache.getPersonTypeByCode(personTypesCodes.NATURAL_PERSON)
                    };
                    $scope.searchParameters = angular.copy($scope.defaultSearchParameters);

                    $scope.searchResults = [];
                    $scope.searchResultTable = {};
                    $scope.searchResultTable.config = {
                        dataSource: refresh,
                        sortable: false,
                        modal: true
                    };
                    $scope.searchResultTable.state = {};
                    $scope.searchService = function (searchParameters) {
                        $scope.searchParameters = searchParameters;
                        $scope.selectedPerson = null;
                        $scope.selectedPersonGuid = null;
                        $scope.checkedItemsGuids = {};
                        $scope.checkedItems = [];

                        return $scope.searchResultTable.state.refresh();
                    };

                    $scope.errorCallback = function (error) {
                        commonDialogs.error('Во время работы системы произошла ошибка');
                    };

                    function refresh(pagingConfig) {

                        var params = angular.copy($scope.searchParameters);

                        angular.forEach(params, function (value, property) {
                            if (!value || angular.equals(value, {})) {
                                delete params[property];
                            }
                        });

                        delete params.personType;

                        return $PersonRegistryService.findRegisteredPersons(params,
                                undefined,
                                function(){
                                    $scope.errorCallback();
                                },
                                {
                                    page: pagingConfig.queryParams.page ?  pagingConfig.queryParams.page : 1,
                                    itemsPerPage: pagingConfig.queryParams.itemsPerPage
                                }
                        );
                    }

                    $scope.selectRow = function(person){
                        if($scope.multipleChoice){
                            if ($scope.checkedItemsGuids[person.guid]) {
                                $scope.checkedItems = $scope.checkedItems.filter(function (item) {
                                    return !angular.equals(person, item);
                                });
                                delete $scope.checkedItemsGuids[person.guid];
                            } else {
                                $scope.checkedItemsGuids[person.guid] = true;
                                $scope.checkedItems.push(person);
                            }
                        }else{
                            $scope.selectedPersonGuid = person.guid;
                            $scope.selectedPerson = person;
                        }
                    };

                    $scope.add = function(){
                        efDobflDialog.create()
                                     .setFiasHouseCode(initializationParameters.fiasHouseCode)
                                     .show()
                                     .then(function (person) {
                            var result = person;
                            if($scope.multipleChoice){
                                result = [person];
                            }
                            $modalInstance.close(result);
                        });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.select = function () {
                        $modalInstance.close($scope.multipleChoice ? $scope.checkedItems : $scope.selectedPerson);
                    };

                    $scope.selectBtnIsDisabled = function(){
                        return $scope.multipleChoice ? _.isEmpty($scope.checkedItems) : !$scope.selectedPerson;
                    };
                };
            }

            function DialogInstance() {
                this.initializationParameters = {
                    multipleChoice : false
                };

                this.setMultipleChoice = function(multipleChoice){
                    this.initializationParameters.multipleChoice = angular.copy(multipleChoice);
                    return this;
                };

                this.setFiasHouseCode = function(fiasHouseCode){
                    this.initializationParameters.fiasHouseCode = angular.copy(fiasHouseCode);
                    return this;
                };

                this.show = function () {
                    var modalOptions = _.clone(modalDefaults, true);
                    modalOptions.controller = getDialogController(this.initializationParameters);
                    return $modal.open(modalOptions).result;
                };
            }

            this.create = function () {
                return new DialogInstance();
            };
        }])


;
