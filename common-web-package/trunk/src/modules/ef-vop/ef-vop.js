/**
 * Группа полей для выбора отчетного периода (ЭФ_ВОП)
 */


angular.module('common.ef-vop', ['common.required-field'])

        .directive('efVopForm', function () {
            return {
                restrict: 'AEC',
                scope: {
                    provider : '='
                },
                controller: function ($scope) {
                    $scope.years = [];

                    var currentDate = new Date();
                    var years = [];
                    for (var i = 0; i < 7; i++) {
                        $scope.years.push( { val : currentDate.getFullYear() - i});
                    }

                    $scope.options = {
                        month : currentDate.getMonth() + 1,
                        year : currentDate.getFullYear()
                    };


                    $scope.provider.getOptions = function() {
                        return $scope.options;
                    };

                },
                transclude: true,
                templateUrl: 'ef-vop/ef-vop-form.tpl.html'
            };
        })
;