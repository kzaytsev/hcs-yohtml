angular
    .module('ef-header', [
        'common.auth',
        'common.dialogs',
        'ru.lanit.hcs.ppa.rest.PpaService'
    ])
    .directive('efHeader', ['Auth', '$PpaService', 'commonDialogs', function (Auth, $PpaService, commonDialogs) {
        return {
            restrict: 'AE',
            templateUrl: 'ef-header/ef-header.tpl.html',
            controller: function ($scope) {
                $scope.organizationDetails = {};
                $scope.user = Auth.user;

                $PpaService.findOrganizationDetails().then(function (data) {
                    angular.extend($scope.organizationDetails, data);
                    return $scope.organizationDetails;
                }, function (reason) {
                    console.log("Во время работы Системы произошла ошибка" + reason);
                    commonDialogs.error("Во время работы Системы произошла ошибка");
                });
            }
        };
    }]);