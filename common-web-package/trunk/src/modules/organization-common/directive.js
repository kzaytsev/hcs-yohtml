angular.module('common.organization.directives',
    ['lodash', 'common.organization.enums'])

    .directive('orgRoles', ['_', function (_) {
        return {
            restrict: 'E',
            template: '{{roleNames.join(", ")}}',
            scope: {
                organization: '='
            },
            controller: ['$scope', function ($scope) {
                var roles = $scope.organization.organizationRoles;
                if (roles) {
                    var roleNames = _.map(roles, function (role) {
                        if(!_.isNull(role)&&!_.isNull(role.role)) {
                            return role.role.organizationRoleName;
                        }
                    });

                    $scope.roleNames = _.sortBy(roleNames);
                }
            }]
        };
    }])

    .directive('orgAdmTerritories', [
        '_', 'orgRoleCodeEnum',
        function (_, orgRoleCodeEnum) {
            return {
                restrict: 'E',
                scope: {
                    organization: '='
                },
                template: '<div ng-repeat="territory in territories">' +
                    '<div ng-if="multiple">{{territory.name}}:</div>' +
                    '<div ng-repeat="address in territory.formattedAddresses">{{address}}</div>' +
                    '<br /></div>',
                controller: ['$scope', function ($scope) {
                    // Для роли
                    // Орган местного самоуправления
                    var formatOMSTerritories = function (orgRole) {
                        // коды ОКТМО
                        var codes = _.map(orgRole.oktmos, 'code');
                        var sortedCodes = _.sortBy(codes);
                        return [sortedCodes.join(',')];
                    };

                    // Для роли
                    // Администратор общего собрания собственников
                    // помещений в многоквартирном доме
                    var formatAOSTerritories = function (orgRole) {
                        // <Адрес до улицы>: <перечень домов на улице, через точку с запятой>

                        var formatAddressStart = function (house) {
                            var addressParts = [];

                            if (house.region) {
                                addressParts.push(house.region.formalName);
                            }

                            if (house.area) {
                                addressParts.push(house.area.formalName);
                            }

                            if (house.city) {
                                addressParts.push(house.city.formalName);
                            }

                            if (house.settlement) {
                                addressParts.push(house.settlement.formalName);
                            }

                            if (house.street) {
                                addressParts.push(house.street.formalName);
                            }

                            return addressParts.join(', ');
                        };

                        var streets = {};
                        _.forEach(orgRole.houses, function (houseAddress) {
                            var street = streets[houseAddress.street.guid];
                            var house = houseAddress.house;
                            if (street) {
                                street.houses.push(house.houseNumber);
                            } else {
                                streets[houseAddress.street.guid] = {
                                    address: formatAddressStart(houseAddress),
                                    houses: [house.houseNumber]
                                };
                            }
                        });

                        _.forOwn(streets, function (street) {
                            street.houses = _.sortBy(street.houses);
                        });

                        var addresses = [];
                         _.forOwn(streets, function (street) {
                            var houses = _.map(street.houses, function (house) {
                                return 'д. ' + house;
                            });
                            var address = street.address + ': ' +
                                houses.join(';');
                            addresses.push(address);
                        });

                        return _.sortBy(addresses);
                    };

                    var roleTerritories = _.map(
                        $scope.organization.organizationRoles,
                        function (orgRole) {
                            if(!_.isNull(orgRole)&&!_.isNull(orgRole.role)) {
                                var addresses = [];
                                // Орган местного самоуправления
                                if (orgRole.role.code === orgRoleCodeEnum.OMS) {
                                    addresses = formatOMSTerritories(orgRole);
                                    // Администратор общего собрания собственников
                                    // помещений в многоквартирном доме
                                } else if (orgRole.role.code === orgRoleCodeEnum.AOS) {
                                    addresses = formatAOSTerritories(orgRole);
                                } else if (orgRole.region) {
                                    addresses = [orgRole.region.formalName];
                                }

                                return {
                                    name: orgRole.role.organizationRoleName,
                                    formattedAddresses: addresses
                                };
                            }
                        }
                    );

                    roleTerritories = _.filter(
                        roleTerritories,
                        function (roleTerritory) {
                            if (!_.isUndefined(roleTerritory)) {
                                return roleTerritory.formattedAddresses &&
                                    roleTerritory.formattedAddresses.length !== 0;
                            }
                        }
                    );

                    $scope.territories = roleTerritories;
                    $scope.multiple = $scope.organization.organizationRoles &&
                        $scope.organization.organizationRoles.length > 1;
                }]
            };
        }
    ])

    .directive('orgStatus', ['orgStatusEnum', function (orgStatusEnum) {
        return {
            restrict: 'E',
            template: '{{title}}',
            scope: {
                organization: '='
            },
            controller: ['$scope', function ($scope) {
                var orgStatusCode = $scope.organization.status;
                if (!orgStatusCode ||  !orgStatusEnum[orgStatusCode]) {
                    $scope.title = '';
                } else {
                    $scope.title =  orgStatusEnum[orgStatusCode].title;
                }
            }]
        };
    }]);