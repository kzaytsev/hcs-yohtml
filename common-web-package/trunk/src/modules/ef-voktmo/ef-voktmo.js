/**
 * Экранная форма выбора значений из справочника ОКТМО (ЭФ_ВОКТМО)
 */
angular
    .module('common.ef-voktmo', [
        'lodash',
        'ui.select2',
        'common.dialogs',
        'common.ef-poktmo',
        'common.ef-poktmo-rzp',
        'ru.lanit.hcs.nsi.rest.ClassifierService'
    ])
        .service('oktmoChooserDialog', function ($modal, $filter, $ClassifierService, commonDialogs) {

            var modalDefaults = {
                backdrop: true,
                keyboard: true,
                modalFade: true,
                size: 'lg',
                templateUrl: 'ef-voktmo/ef-voktmo.tpl.html'
            };

            var modalOptions = {
                closeButtonText: 'Отменить',
                actionButtonText: 'Выбрать'
            };

            var params = {};

            var self = this;


            params.hasCheckbox = false;
            self.selectCheckbox = function () {
                params.hasCheckbox = true;
                return self;
            };

            self.selectOnLevel = function (level) {
                params.level = level;
                return self;
            };

            self.restrictRegionGuid = function (regionGuid) {
                params.regionGuid = regionGuid;
                return self;
            };

            self.excludedGuids = function (excludedGuids) {
                params.excludedGuids = excludedGuids;
                return self;
            };
            //октмо коды, исключенные из процесса
            self.excludedCodes = function (excludedCodes) {
                params.excludedCodes = excludedCodes;
                return self;
            };

            //октмо коды, только их разрешено показывать!!!!
            self.useCodes = function (useCodes) {
                params.useCodes = useCodes;
                return self;
            };
            
            self.oktmoSearchService = function (searchService) {
                params.searchService = searchService;
                return self;
            };

            self.show = function () {
                if (!modalDefaults.controller) {
                    modalDefaults.controller = function ($scope, $modalInstance, $q) {
                        $scope.defaultSearchParameters = {};
                        $scope.searchParameters = angular.copy($scope.defaultSearchParameters);

                        $scope.searchService = function (searchParameters, showChildrenView) {
                            $scope.searchParameters = searchParameters;

                            $scope.savedItems = angular.copy($scope.items);
                            $scope.items = [];
                            $scope.checkedItems = {};
                            $scope.radioItem = null;
                            $scope.result = null;

                            $scope.showChildrenView = !!showChildrenView;

                            return $scope.searchResultTable.state.refresh();
                        };

                        $scope.hasCheckbox = params.hasCheckbox;
                        $scope.level = params.level ? params.level : 0;
                        $scope.regionGuid = params.regionGuid;
                        $scope.excludedGuids = params.excludedGuids;
                        $scope.excludedCodes = params.excludedCodes;
                        $scope.useCodes = params.useCodes;

                        $scope.radioItem = null;
                        $scope.checkedItems = {};
                        $scope.items = [];
                        $scope.savedItems = [];

                        $scope.showChildrenView = false;
                        $scope.oktmoService =  params.searchService || $ClassifierService;

                        $scope.tryItem = function (item) {
                            if ($scope.level && $scope.level != item.level) {
                                return;
                            }

                            if ($scope.checkedItems[item.guid]) {
                                $scope.items = $scope.items.filter(function (element) {
                                    return !angular.equals(item, element);
                                });
                                delete $scope.checkedItems[item.guid];
                            } else {
                                $scope.items.push(item);
                                $scope.checkedItems[item.guid] = true;
                            }
                            $scope.radioItem = item.guid;
                            $scope.result = item;
                        };

                        $scope.modalOptions = modalOptions;
                        $scope.modalOptions.ok = function (result) {
                            if ($scope.hasCheckbox) {
                                $modalInstance.close($scope.items);
                            } else {
                                $modalInstance.close($scope.result);
                            }
                            params = {};
                        };
                        $scope.modalOptions.close = function (result) {
                            $modalInstance.dismiss('cancel');
                            params = {};
                        };

                        $scope.resetSelection = function() {
                            $scope.items = [];
                            $scope.checkedItems = {};
                            $scope.radioItem = null;
                            $scope.result = null;
                        };

                        $scope.disabled = function () {
                            return (!$scope.hasCheckbox && !$scope.result) || ($scope.hasCheckbox && $scope.items.length === 0);
                        };

                        $scope.errorCallback = function (error) {
                            commonDialogs.error('Во время работы системы произошла ошибка');
                        };

                        $scope.searchResults = [];
                        $scope.searchResultTable = {};
                        $scope.searchResultTable.config = {
                            dataSource: refresh,
                            sortable: false,
                            noRefreshOnInit: true,
                            modal: true
                        };
                        $scope.searchResultTable.state = {};

                        function refresh(query) {
                            var params = {},
                                searchParams = angular.copy($scope.searchParameters);

                            if (searchParams.contextSearch) {
                                if (searchParams.oktmo) {
                                    if (searchParams.oktmo.hasOwnProperty('code')) {
                                        params.code = searchParams.oktmo.code;
                                    } else {
                                        params.searchString = searchParams.oktmo.text;
                                    }
                                }
                                if ($scope.regionGuid) {
                                    params.regionGuid = $scope.regionGuid;
                                }
                                if(searchParams.excludedGuids){
                                    params.excludedGuids = searchParams.excludedGuids;
                                }
                                if(searchParams.excludedCodes){
                                    params.excludedCodes = searchParams.excludedCodes;
                                }
                                if(searchParams.useCodes){
                                    params.useCodes = searchParams.useCodes;
                                }
                                if(searchParams.useCodesHierarchyScope){
                                    params.useCodesHierarchyScope = searchParams.useCodesHierarchyScope;
                                }

                            } else {
                                if (searchParams.region) {
                                    params.parentCode = searchParams.region.code;
                                }
                                if (searchParams.level3Guid) {
                                    params.parentCode = searchParams.level3Guid.code;
                                    delete params.regionGuid;
                                }
                                if (searchParams.level5Guid) {
                                    params.parentCode = searchParams.level5Guid.code;
                                    delete params.regionGuid;
                                }
                                if (searchParams.level7Guid) {
                                    params.parentCode = searchParams.level7Guid.code;
                                    delete params.regionGuid;
                                }
                                if (searchParams.parentCode) {
                                    params.parentCode = searchParams.parentCode;
                                    delete params.regionGuid;
                                }
                                if (searchParams.showAllLevel) {
                                    params.showAllLevel = searchParams.showAllLevel;
                                }else{
                                    params.showAllLevel = false;
                                }
                                if(searchParams.excludedGuids){
                                    params.excludedGuids = searchParams.excludedGuids;
                                }
                                if(searchParams.excludedCodes){
                                    params.excludedCodes = searchParams.excludedCodes;
                                }
                                if(searchParams.useCodes){
                                    params.useCodes = searchParams.useCodes;
                                    if(searchParams.useCodesHierarchyScope){
                                        params.useCodesHierarchyScope = searchParams.useCodesHierarchyScope;
                                    }
                                }
                                if(searchParams.level>=0){
                                    params.level = searchParams.level;
                                }

                            }
                            //если нажали очистить....
                            if (!params.useCodes && $scope.useCodes){
                                params.useCodesHierarchyScope='CHILDREN';
                                params.useCodes=$scope.useCodes;
                            }

                            angular.forEach(params, function (value, property) {
                                if (!value && property != 'showAllLevel') {
                                    delete params[property];
                                }
                            });

                            if (angular.equals(params, {})) {
                                var defer = $q.defer();
                                defer.resolve({items: []});
                                return defer.promise;
                            }

                            params.page = query.queryParams.page;
                            params.itemsPerPage = query.queryParams.itemsPerPage;
                            
                            return $scope.oktmoService.findOktmoExtendedSearch(
                                    params,
                                    angular.noop,
                                    angular.noop,
                                    query.queryParams
                            ).then(
                                    function(response) {
                                        $scope.searchResults = response['oktmoList'];

                                        //Требование по отображению выбранных элементов в начале списка
                                        if ($scope.hasCheckbox) {
                                            var tail = [];
                                            var head = $scope.searchResults.filter(function (item) {
                                                var toHead = false;

                                                angular.forEach($scope.savedItems, function (savedItem) {
                                                    if (savedItem.guid == item.guid) {
                                                        toHead = true;
                                                    }
                                                });

                                                if (!toHead) {
                                                    tail.push(item);
                                                }
                                                return toHead;
                                            });

                                            $scope.searchResults = head.concat(tail);

                                            if (head.length > 0) {
                                                angular.forEach(head, function (item) {
                                                    if (!$scope.checkedItems[item.guid]) {
                                                        $scope.tryItem(item);
                                                    }
                                                });
                                            }
                                        }


                                        response['items'] = $scope.searchResults;
                                        return response;
                                    },
                                    function () {
                                        $scope.errorCallback();
                                        return {
                                            total: 0,
                                            items: []
                                        };
                                    }
                            );
                        }
                    };
                }
                return $modal.open(modalDefaults).result;
            };
        })
;
