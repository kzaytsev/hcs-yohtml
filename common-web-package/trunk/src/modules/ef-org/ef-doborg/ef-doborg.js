/**
 * ЭФ_ДОБОРГ: Модальное окно добавления организации
 *
 */
angular
    .module('common.ef-doborg', [
        'common.dialogs',
        'common.ef-svul',
        'common.ef-svip',
        'common.ef-org-constants',
        'ru.lanit.hcs.organizationregistry.OrganizationRegistryService'
    ])

    /**
     * Возвращает добавленную организацию
     * */
    .service('efDoborgDialog', [
        '$modal',
        'commonDialogs',
        'organizationTypesCodes',
        '$OrganizationRegistryService',
        'OrganizationAddressConverter',
        function ($modal,
                  commonDialogs,
                  organizationTypesCodes,
                  $OrganizationRegistryService,
                  OrganizationAddressConverter) {
            var modalDefaults = {
                backdrop: 'static',
                keyboard: true,
                modalFade: true,
                templateUrl: 'ef-org/ef-doborg/ef-doborg.tpl.html',
                size: 'lg'
            };

            function getDialogController(initializationParameters) {
                return function ($scope, $modalInstance) {
                    $scope.showErrors = false;
                    $scope.registryOrganizationType = organizationTypesCodes.LEGAL_ENTITY;

                    $scope.organization = {
						registryOrganizationType : organizationTypesCodes.LEGAL_ENTITY
                    };

                    $scope.soleTrader = {
						registryOrganizationType : organizationTypesCodes.SOLE_TRADER
                    };

                    $scope.isLegalEntity = function(organizationType){
                        return organizationType === organizationTypesCodes.LEGAL_ENTITY;
                    };
                    $scope.isSoleTrader = function(organizationType){
                        return organizationType === organizationTypesCodes.SOLE_TRADER;
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.errorCallback = function (error) {
                        commonDialogs.error('Во время работы системы произошла ошибка');
                    };

                    $scope.save = function (formIsValid) {
                        $scope.$broadcast('showError');
                        $scope.showErrors = true;
                        if(formIsValid){
                            var organization = null;
                            switch($scope.registryOrganizationType){
                                case organizationTypesCodes.LEGAL_ENTITY:
                                    organization = $scope.organization;
                                    break;
                                case organizationTypesCodes.SOLE_TRADER:
                                    organization = $scope.soleTrader;
                                    break;

                            }

                            organization.mainAddress = OrganizationAddressConverter.getAddress(organization.mainAddress);

                            $OrganizationRegistryService.addOrganization(
                                organization,
                                function(data, status){
                                    organization.guid = data;
                                    $modalInstance.close(organization);
                                },
                                function(data, status){
                                    if(data && data.title){
                                        switch(data.title){
                                            case 'RegistryOrganizationAlreadyExistsException':
                                                commonDialogs.error('Организация с указанными реквизитами уже присутствует в реестре.');
                                                break;
                                            case 'IllegalNsiReference':
                                                commonDialogs.error('Присутствуют неактуальные справочные данные.');
                                                break;
                                            default:
                                                $scope.errorCallback();
                                                break;
                                        }
                                    }else{
                                        $scope.errorCallback();
                                    }
                                }
                            );
                        }
                    };

                };
            }

            function DialogInstance() {
                this.initializationParameters = {};

                this.show = function () {
                    var modalOptions = _.clone(modalDefaults, true);
                    modalOptions.controller = getDialogController(this.initializationParameters);
                    return $modal.open(modalOptions).result;
                };
            }

            this.create = function () {
                return new DialogInstance();
            };
        }])

;
