/**
 * Модальное окно изменения организации (ЭФ_ИЗМОРГ)
 */
angular
    .module('common.ef-izmorg', [
        'common.dialogs',
        'common.ef-svul',
        'common.ef-svip',
        'common.ef-org-constants',
        'ru.lanit.hcs.organizationregistry.OrganizationRegistryService'
    ])

    /**
     * Возвращает обновленную организацию
     * */
    .service('efIzmorgDialog', [
        '$modal',
        'commonDialogs',
        'organizationTypesCodes',
        '$OrganizationRegistryService',
        'OrganizationAddressConverter',
        function ($modal,
                  commonDialogs,
                  organizationTypesCodes,
                  $OrganizationRegistryService,
                  OrganizationAddressConverter) {
            var modalDefaults = {
                backdrop: 'static',
                keyboard: true,
                modalFade: true,
                templateUrl: 'ef-org/ef-izmorg/ef-izmorg.tpl.html',
                size: 'lg'
            };

            function getDialogController(initializationParameters) {
                return function ($scope, $modalInstance) {
                    $scope.showErrors = false;
                    $OrganizationRegistryService.getRegistryOrganizationDetailByGuid(
                        initializationParameters.organizationGuid,
                        function(data, status){
                            $scope.organization = data;
                        },
                        function(data, status){
                            commonDialogs.error('Во время работы системы произошла ошибка');
                        }
                    );

                    $scope.isLegalEntity = function(organizationType){
                        return organizationType === organizationTypesCodes.LEGAL_ENTITY;
                    };
                    $scope.isSoleTrader = function(organizationType){
                        return organizationType === organizationTypesCodes.SOLE_TRADER;
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.errorCallback = function (error) {
                        commonDialogs.error('Во время работы системы произошла ошибка');
                    };

                    $scope.save = function (formIsValid) {
                        $scope.showErrors = true;
                        $scope.$broadcast('showError');
                        if(formIsValid){

                            var organization = angular.copy($scope.organization);

                            organization.mainAddress = OrganizationAddressConverter.getAddress(organization.mainAddress);

                            $OrganizationRegistryService.updateOrganization(
                                organization,
                                function(data, status){
                                    organization.guid = data;
                                    $modalInstance.close(organization);
                                },
                                $scope.errorCallback
                            );
                        }
                    };

                };
            }

            function DialogInstance() {
                this.initializationParameters = {};

                this.setOrganizationGuid = function(organizationGuid) {
                    this.initializationParameters.organizationGuid = angular.copy(organizationGuid);
                    return this;
                };

                this.show = function () {
                    var modalOptions = _.clone(modalDefaults, true);
                    modalOptions.controller = getDialogController(this.initializationParameters);
                    return $modal.open(modalOptions).result;
                };
            }

            this.create = function () {
                return new DialogInstance();
            };
        }])

;
