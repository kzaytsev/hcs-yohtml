/**
 * Модальное окно просмотра организации (ЭФ_ПРОРГ)
 */
angular
    .module('common.ef-prorg', [
        'common.dialogs',
        'common.ef-svul',
        'common.ef-svip',
        'common.ef-org-constants',
        'ru.lanit.hcs.organizationregistry.OrganizationRegistryService'
    ])

    .service('efProrgDialog', [
        '$modal',
        'commonDialogs',
        'organizationTypesCodes',
        '$OrganizationRegistryService',
        function ($modal,
                  commonDialogs,
                  organizationTypesCodes,
                  $OrganizationRegistryService) {
            var modalDefaults = {
                backdrop: 'static',
                keyboard: true,
                modalFade: true,
                templateUrl: 'ef-org/ef-prorg/ef-prorg.tpl.html',
                size: 'lg'
            };

            function getDialogController(initializationParameters) {
                return function ($scope, $modalInstance) {
                    $scope.formClassName = initializationParameters.formClassName;
                    $OrganizationRegistryService.getRegistryOrganizationDetailByGuid(
                        initializationParameters.organizationGuid,
                        function(data, status){
                            $scope.organization = data;
                        },
                        function(data, status){
                            commonDialogs.error('Во время работы системы произошла ошибка');
                        }
                    );

                    $scope.isLegalEntity = function(organizationType){
                        return organizationType === organizationTypesCodes.LEGAL_ENTITY;
                    };
                    $scope.isSoleTrader = function(organizationType){
                        return organizationType === organizationTypesCodes.SOLE_TRADER;
                    };

                    $scope.close = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.errorCallback = function (error) {
                        commonDialogs.error('Во время работы системы произошла ошибка');
                    };
                };
            }

            function DialogInstance() {
                this.initializationParameters = {};

                this.setOrganizationGuid = function(organizationGuid) {
                    this.initializationParameters.organizationGuid = angular.copy(organizationGuid);
                    return this;
                };

                this.setFormClass = function(formClassName){
                    this.initializationParameters.formClassName = formClassName;
                    return this;
                };

                this.show = function () {
                    var modalOptions = _.clone(modalDefaults, true);
                    modalOptions.controller = getDialogController(this.initializationParameters);
                    return $modal.open(modalOptions).result;
                };
            }

            this.create = function () {
                return new DialogInstance();
            };
        }])

;
