angular
    .module('common.ef-org-constants', [
    ])

    .constant('organizationTypesCodes', {
        LEGAL_ENTITY : 'L',
        SOLE_TRADER : 'B'
    })

    .factory('OrganizationAddressConverter', [function(){

        /**
         * address - FiasHouseAddressWithNsi либо, объект, возвращаемый ЭФ_ВвА
         * */
        function getAddress(address){
            if(address == null){
                return null;
            }

            var result = {};
            if(address.region){
                result.region = {guid: address.region.guid};
            }
            if(address.area){
                result.area = {guid: address.area.guid};
            }
            if(address.city){
                result.city = {guid: address.city.guid};
            }
            if(address.settlement){
                result.settlement = {guid: address.settlement.guid};
            }
            if(address.street){
                result.street = {guid: address.street.guid};
            }
            if(address.house){
                result.house = {guid: address.house.guid};
            }
            result.formattedAddress = address.formattedAddress || address.address;

            return result;
        }

        return {
            getAddress : getAddress
        };
    }])

;