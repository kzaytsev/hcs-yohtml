/**
 * Блок ЭФ_БПП + ЭФ_ПРФ
 */
angular
    .module('common.attachment-element', [
        'common.ef-bpp',
        'common.ef-prf'
    ])
    .directive('attachmentElement', function () {
        return {
            restrict: 'E',
            require: ['ngModel', '^form'],
            scope: {
                ngModel: '=',
                context: '@',
                upload: '=?',
                remove: '=?',
                max: '=?',
                disabled: '=?',
                notRequired: '=?',
                contextPath: '=?'
            },
            controller: function ($scope) {
                $scope.contextPath = $scope.contextPath || 'filestore';
                $scope.max = Math.abs(parseInt($scope.max, 10)) || 99;
                $scope.attachments = [];

                if (!_.isBoolean($scope.disabled)) {
                    $scope.disabled = false;
                }

                $scope.$watch('ngModel', function (newValue) {
                    if (newValue != null && !angular.equals(newValue, {})) {
                        if (_.isArray(newValue)) {
                            $scope.attachments = newValue;
                        } else {
                            $scope.attachments = [];
                            $scope.attachments.push(newValue);
                        }
                        $scope.uploadHiddenGuard = $scope.attachments.length ? 'There is some value' : '';
                    }
                    $scope.maxFiles = $scope.max - $scope.attachments.length;
                });

                $scope.uploadCallback = function (response) {
                    var fileInfo = response.fileInfo;
                    fileInfo.description = response.description;
                    $scope.attachments.push(fileInfo);

                    $scope.assignModel();

                    if ($scope.form && $scope.form.$setDirty) {
                        $scope.form.$setDirty(true);
                    }
                    if ($scope.upload) {
                        $scope.upload(response);
                    }
                };

                $scope.removeCallback = function (contentGuid) {
                    _.remove($scope.attachments, function (item) {
                        return item.contentGuid == contentGuid;
                    });

                    $scope.assignModel();

                    if ($scope.remove) {
                        $scope.remove(contentGuid);
                    }
                };

                $scope.isEmpty = function () {
                    return !$scope.notRequired && !$scope.attachments.length;
                };
                $scope.$on('showError', function () {
                    $scope.showError = true;
                });
                $scope.$on('hideError', function () {
                    $scope.showError = false;
                });

                $scope.assignModel = function () {
                    if ($scope.max == 1) {
                        $scope.ngModel = $scope.attachments.length > 0 ? $scope.attachments[0] : null;
                    } else {
                        $scope.ngModel = $scope.attachments;
                    }
                    $scope.uploadHiddenGuard = $scope.attachments.length ? 'There is some value' : '';
                    $scope.maxFiles = $scope.max - $scope.attachments.length;
                    //$scope.$apply();
                };
                $scope.maxFiles = $scope.max - $scope.attachments.length;

            },
            templateUrl: 'attachment-element/attachment-element.tpl.html',
            link: function (scope, element, attrs, required) {
                scope.form = required[1];
            }
        };
    })
;