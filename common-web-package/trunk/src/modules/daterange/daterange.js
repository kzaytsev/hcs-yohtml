/**
 * Период
 */
angular.module('common.daterange', [])

    .directive('daterange', function () {
        return {
            restrict: 'E',
            scope: {
                dateFromModel: '=fromModel',
                dateToModel: '=toModel',
                fromOptions: '=?',
                toOptions: '=?',
                opts: '=?options'
            },
            require: 'ngModel',
            link: function ($scope, elem, attr, ngModel) {
                var validatorFn = function (value) {
                    ngModel.$setValidity('required', !$scope.options.required || ($scope.oDateFrom && $scope.oDateTo));
                    if ($scope.date) {
                        ngModel.$setValidity('format', $scope.checkDate($scope.date));
                    }
                    if ($scope.oDateFrom && $scope.oDateTo) {
                        if ($scope.options.couldBeEqual) {
                            ngModel.$setValidity('invalidRange', $scope.oDateFrom <= $scope.oDateTo);
                        } else {
                            ngModel.$setValidity('period', $scope.oDateFrom < $scope.oDateTo);
                        }
                    }
                    //if ($scope.fromOptions && $scope.fromOptions.maxDate) {
                    //
                    //}
                    return value;
                };
                $scope.ngModel = ngModel;

                ngModel.$parsers.unshift(validatorFn);
                ngModel.$formatters.unshift(validatorFn);
            },
            controller: function ($scope, $dateParser, $filter, $timeout) {
                var defaultConfig = {
                        format: 'dd.MM.yyyy',
                        mask: '99.99.9999',
                        showPlaceholder: true,
                        placeholder: 'ДД.ММ.ГГГГ',
                        couldBeEqual: true
                    },
                    formatDateRawInput = function (rawInput) {
                        var delimiter = '.';
                        if (!rawInput) {
                            return rawInput;
                        } else if (rawInput.split(delimiter).length == 3) {
                            return rawInput;
                        } else {
                            var aInputChars = rawInput.split('');
                            aInputChars.splice(2, 0, delimiter);
                            aInputChars.splice(5, 0, delimiter);
                            return aInputChars.join('');
                        }
                    },
                    tryToHide = function(){
                        //console.log('wasClicked: ', $scope._wasClicked, ', wasFocused: ', $scope._wasFocused);
                        if ($scope._wasClicked) {
                            return false;
                        } else {
                            $timeout(function(){
                                //console.log('wasClicked: ', $scope._wasClicked, ', wasFocused: ', $scope._wasFocused);
                                if ($scope._wasClicked || $scope._wasFocused) {
                                    return false;
                                } else {
                                    $scope.hide();
                                }
                            }, 300);
                        }
                    },
                    datepickerOptions = {
                        formatYear: 'yyyy',
                        formatDay: 'dd',
                        formatMonth: 'MM',
                        startingDay: 1
                    };

                $scope.options = _.extend(_.clone(defaultConfig), $scope.opts);

                $scope.show = function () {
                    $scope.showPicker = true;
                    $scope.showPanel = true;
                };
                $scope.hide = function () {
                    $scope._wasClicked = false;
                    $scope._wasFocused = false;
                    $scope.showPicker = false;
                    $scope.showPanel = false;
                };
                $scope.hide(); // set flags to false; default

                $scope.close = function(){
                    $timeout($scope.hide);
                };

                $scope.toggle = function () {
                    $scope.showPicker = !$scope.showPicker;
                    $scope.showPanel = !$scope.showPanel;
                };
                $scope.focus = function() {
                    $scope._wasFocused = true;
                    $scope.show();
                };
                $scope.blur = function() {
                    $scope._wasFocused = false;
                    tryToHide();
                };
                $scope.click = function(){
                    $scope._wasClicked = true;
                };

                $scope.datepickerFromOptions = _.extend(_.clone(datepickerOptions), $scope.fromOptions);
                $scope.datepickerToOptions = _.extend(_.clone(datepickerOptions), $scope.toOptions);

                $scope.$watch('sDateFrom', function (sDateVal) {
                    sDateVal = formatDateRawInput(sDateVal);
                    $scope.dateFromModel = sDateVal;
                    if (!sDateVal) {
                        $scope.oDateFrom = null;
                    } else if ($filter('date')($scope.oDateTo, $scope.options.format) !== sDateVal) {
                        $scope.oDateFrom = $dateParser(sDateVal, $scope.options.format);
                    }
                    $scope.ngModel.$setViewValue($scope.sDateFrom + '|' + $scope.sDateTo);
                });
                $scope.$watch('sDateTo', function (sDateVal) {
                    sDateVal = formatDateRawInput(sDateVal);
                    $scope.dateToModel = sDateVal;
                    if (!sDateVal) {
                        $scope.oDateTo = null;
                    } else if ($filter('date')($scope.oDateTo, $scope.options.format) !== sDateVal) {
                        $scope.oDateTo = $dateParser(sDateVal, $scope.options.format);
                    }
                    $scope.ngModel.$setViewValue($scope.sDateFrom + '|' + $scope.sDateTo);
                });

                $scope.sDateFrom = $scope.dateFromModel;
                $scope.$watch('oDateFrom', function (dateVal) {
                    $scope.datepickerToOptions.minDate = dateVal;
                    $scope.sDateFrom = dateVal ? $filter('date')(dateVal, $scope.options.format) : '';
                });

                $scope.sDateTo = $scope.dateToModel;
                $scope.$watch('oDateTo', function (dateVal) {
                    $scope.datepickerFromOptions.maxDate = dateVal;
                    $scope.sDateTo = dateVal ? $filter('date')(dateVal, $scope.options.format) : '';
                });


                $scope.showButtonBar = function () {
                    return true;
                };

                $scope.isEmpty = function () {
                    return !($scope.sDateFrom && $scope.sDateTo);
                };
                $scope.isRangeValid = function () {
                    return $scope.options.couldBeEqual ? $scope.oDateFrom.getTime() <= $scope.oDateTo.getTime() : $scope.oDateFrom.getTime() < $scope.oDateTo.getTime();
                };
            },
            templateUrl: 'daterange/daterange.tpl.html'
        };
    });