/**
 * Период v2
 */
angular.module('common.daterange2', ['common.hcs-datepicker'])
    .directive('daterange2', function () {
        return {
            restrict: 'E',
            scope: {
                startDate: '=',
                endDate: '=',
                options: '=?'
            },
            templateUrl: 'daterange2/daterange2.tpl.html',
            controller: function ($scope) {
                $scope.options = $scope.options || {};

                $scope.periodDateOptionsFrom = {
                    onDateBlur: function(){
                        $scope.periodDateOptionsTo.validatePeriodImmediately = true;
                        $scope.periodDateOptionsFrom.showFormatErrorsImmediately = true;
                    },
                    onDateChange: function(){
                        $scope.periodDateOptionsTo.validatePeriodImmediately = false;
                        $scope.periodDateOptionsFrom.showFormatErrorsImmediately = false;
                    }
                };
                $scope.periodDateOptionsTo = {
                    validatePeriodImmediately: true,
                    onDateBlur: function(){
                        $scope.periodDateOptionsTo.validatePeriodImmediately = true;
                        $scope.periodDateOptionsTo.showFormatErrorsImmediately = true;
                    },
                    onDateChange: function(){
                        $scope.periodDateOptionsTo.validatePeriodImmediately = false;
                        $scope.periodDateOptionsTo.showFormatErrorsImmediately = false;
                    },
                    dateIsAfter: function(){
                        return $scope.startDate;
                    }
                };

                if($scope.options != null) {
                    $scope.periodDateOptionsFrom = angular.extend(angular.copy($scope.options),  $scope.periodDateOptionsFrom);
                    $scope.periodDateOptionsTo = angular.extend(angular.copy($scope.options), $scope.periodDateOptionsTo);
                }

            }
        };
    })
;