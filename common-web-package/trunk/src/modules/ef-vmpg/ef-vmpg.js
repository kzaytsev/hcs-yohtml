/**
 * Группа полей для выбора периода месяц-год (ЭФ_ВПМГ)
 */

angular.module('common.ef-vmpg', ['common.component-config'])

        .directive('efVmpgForm', function () {
            return {
                restrict: 'AEC',
                scope: {
                    startMonth : '=?',
                    startYear : '=?',
                    endMonth : '=?',
                    endYear : '=?',

                    startMonthRequired : '=?',
                    startYearRequired : '=?',
                    endMonthRequired : '=?',
                    endYearRequired : '=?',

                    startMonthDisabled : '=?',
                    startYearDisabled : '=?',
                    endMonthDisabled : '=?',
                    endYearDisabled : '=?'
                },
                controller: function ($scope) {
                    $scope.years = [];

                    var current = new Date().getFullYear();
                    var LIMIT = 15;

                    for (var i = current + LIMIT; i > current - LIMIT; i--) {
                        $scope.years.push({ val : i});
                    }

                    $scope.monthSelect2Options = {
                        placeholder: 'мм'
                    };

                    $scope.yearSelect2Options = {
                        placeholder: 'гггг'
                    };


                },
                templateUrl: 'ef-vmpg/ef-vmpg-form.tpl.html'
            };
        })
;