/**
 * Блок отображения результатов поиска в ОКТМО (ЭФ_ПОКТМО_РЗП)
 */
angular
    .module('common.ef-poktmo-rzp', [
        'lodash',
        'common.filter',
        'common.utils',
        'ru.lanit.hcs.nsi.rest.ClassifierService'
    ])
    .directive('efPoktmoRzpForm', function() {
        return {
            restrict: 'E',
            scope: {
                searchService : '=',
                searchResultTable : '=',
                selectionFunction : '=',
                hasCheckbox : '=',
                checkedItems : '=',
                radioItem : '=',
                showChildrenView : '=',
                selectionLevel : '=?',
                noCheckBoxes: '=?',
                excludedGuids : '=?',
                excludedCodes : '=?', // исключенные коды
                useCodes : '=?' //разрешенные коды
            },
            templateUrl: 'ef-poktmo-rzp/ef-poktmo-rzp.tpl.html',
            controller: 'efPoktmoRzpController'
        };
    })
    .controller('efPoktmoRzpController', ['$scope', '$filter', '$ClassifierService',
        function ($scope, $filter, $ClassifierService) {
            $scope.getParentNames = function(obj) {
                var parent = obj.parent;
                var text = '';
                while (parent) {
                    text = parent.name + ' / ' + text;
                    parent = parent.parent;
                }
                return text;
            };

            $scope.parents = [];

            $scope.goToChildren = function(parent, event) {
                event.stopPropagation();

                if ($scope.parents.length === 0) {
                    var arr = [];
                    var root = parent.parent;
                    while (root) {
                        arr.push(root);
                        root = root.parent;
                    }
                    for (var index = arr.length - 1; index >= 0; index--) {
                        $scope.parents.push(arr[index]);
                    }
                }

                $scope.parents.push(parent);

                var excludedGuids;
                if($scope.excludedGuids){
                    excludedGuids = angular.copy($scope.excludedGuids);
                }else{
                    excludedGuids = [];
                }
                excludedGuids.push(parent.guid);

                return $scope.searchService({
                    parentCode : parent.code,
                    excludedGuids: excludedGuids,
                    excludedCodes:$scope.excludedCodes,
                    useCodes:$scope.useCodes/*,
                    useCodesHierarchyScope:'CHILDREN',
                    level:parent.level+1,
                    showAllLevel:true*/
                }, true);
            };

            $scope.upToParent = function(index) {
                var parent = $scope.parents[index];

                $scope.parents.splice(index + 1);

                var excludedGuids;
                if($scope.excludedGuids){
                    excludedGuids = angular.copy($scope.excludedGuids);
                }else{
                    excludedGuids = [];
                }
                excludedGuids.push(parent.guid);

                return $scope.searchService({
                    parentCode : parent.code,
                    excludedGuids: excludedGuids,
                    excludedCodes:$scope.excludedCodes,
                    useCodes:$scope.useCodes/*,
                    useCodesHierarchyScope:'CHILDREN',
                    level:parent.level+1,
                    showAllLevel:true*/
                }, true);
            };

            $scope.$watch('showChildrenView', function(newValue) {
                if (!newValue) {
                    $scope.parents = [];
                }
            });

            $scope.errorCallback = function() {
                console.log('Произошла ошибка во время поиска ОКТМО');
            };

        }])
;
