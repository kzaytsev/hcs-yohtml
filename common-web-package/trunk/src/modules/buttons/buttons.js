angular.module('common.buttons',
    ['ui.bootstrap']
)
    .directive('ngCloseButton', function () {
        var directiveDefinitionObject = {
            priority: 0,
            template: '<button class="close modal-base__close"><span class="gis-icon-x-thin"></span></button>',
            replace: true,
            restrict: 'E',
            scope: false
        };
        return directiveDefinitionObject;
    });