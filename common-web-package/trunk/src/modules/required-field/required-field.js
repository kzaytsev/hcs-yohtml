/**
 *  Директива для полей с обязательным заполнением
 *  Если required-field = false директива отключена, для отображения форм в режиме просмотра
 *  и в случае когда форма используется как обязательная и как не обязательная в разных местах.
 *  required-error-control-id (необязательный) - ид элемента в который будет встроено сообщение об ошибке.
 *
 * События:
 * showError - показать сообщения об ошибках
 * hideError - скрыть сообщения об ошибках
 *
 */
angular.module('common.required-field', [
    'lodash'
])
    .directive('requiredField', function (_, $compile) {
        return {
            require: '?ngModel',
            controller: 'hcsRequireController',
            scope: {
                requiredField: '@'
            },
            link: function link($scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }

                if ($scope.requiredField === 'false') {
                    return;
                }
                attrs.required = true; // force truthy in case we are on non input element

                var findLabel = function() {
                    if (_.isEmpty(attrs.id)) {
                        return;
                    }
                    var form = element[0].form;
                    if (!form) {
                        return;
                    }
                    var labels = angular.element(form).find("label");
                    if (!labels) {
                        return;
                    }
                    var label = _.find(labels, function(label){
                        return label.getAttribute("for") === attrs.id;
                    });

                    return label ? label : null;
                };

                var isEmpty = function (value) {
                    return (ngModel.$isEmpty(value) || ((value['guid'] !== undefined) && !value['guid']));
                };

                var validator = function (value) {
                    if (attrs.required && isEmpty(value)) {
                        ngModel.$setValidity('required', false);
                        $scope.isValid = false;
                        return;
                    } else {
                        ngModel.$setValidity('required', true);
                        $scope.isValid = true;
                        return value;
                    }
                };

                ngModel.$formatters.push(validator);
                ngModel.$parsers.unshift(validator);

                attrs.$observe('required', function () {
                    validator(ngModel.$viewValue);
                });

                $scope.expressionValue = $scope.$parent.$eval($scope.requiredField);
                if (!_.isUndefined($scope.expressionValue)) {
                    $scope.$parent.$watch($scope.requiredField, function() {
                        var label = findLabel();
                        if ($scope.$parent.$eval($scope.requiredField)) {
                            attrs.required = true;
                            $scope.isValid = !ngModel.$isEmpty(ngModel.$viewValue);
                            if (label) { angular.element(label).addClass('required'); }
                        } else {
                            attrs.required = false;
                            $scope.isValid = true;
                            if (label) { angular.element(label).removeClass('required'); }
                        }
                        ngModel.$setValidity('required', $scope.isValid);
                    });
                }

                (function addErrorMessage() {
                    var tpl = '<span ng-show="showError && !isValid" class="text-danger">Поле обязательно для заполнения</span>';
                    var errorControl = document.getElementById(element.attr("required-error-control-id"));
                    if (errorControl) {
                        errorControl = angular.element(errorControl);
                    } else {
                        errorControl = element.parent();
                    }
                    errorControl.append($compile(tpl)($scope));
                }());
            }
        };
    })
    .controller('hcsRequireController', function ($scope) {
        $scope.$on('showError', function () {
            $scope.showError = true;
        });
        $scope.$on('hideError', function () {
            $scope.showError = false;
        });
    })
;
