angular
    .module('common.share-premises', ['common.houses-state-service'])
    .directive('sharePremises', function () {
        return {
            restrict: 'E',
            scope: {
                share: '='
            },
            templateUrl: 'share-premises/share-premises.tpl.html',
            controller: ['$scope', '$state', '$window', 'Auth', 'HousesStateService', function ($scope, $state, $window, Auth, HousesStateService) {
                $scope.showPremise = function (p) {
                    var state = null, stateName = null, hasPermission = true, params = {},
                        h = $scope.share.house;
                    if (!p.apartment && !p.room) {
                        if (Auth.authorize([{globalRole: 'P'}])) {
                            // ВИ_ЖКХРАО_8
                            stateName = 'house-view';
                            params = {
                                houseTypeCode: h.houseType.code,
                                houseGuid: h.guid
                            };
                        } else {
                            stateName = 'house-card';
                            state = $state.get(stateName);
                            if (state && state.data && state.data.permit) {
                                hasPermission = Auth.authorize(state.data.permit);
                            }

                            if (/* editable && */ hasPermission) {
                                // ВИ_ЖКХРАО_4
                                params = {
                                    houseCardGuid: h.guid,
                                    houseTypeCode: h.houseType.code,
                                    activeTab: 0,
                                    disabled: false
                                };
                            } else {
                                // ВИ_ЖКХРАО_10
                                stateName = 'house-card-view';
                                params = {
                                    houseCardGuid: h.guid,
                                    houseTypeCode: h.houseType.code,
                                    activeTab: 0,
                                    disabled: true
                                };
                            }
                        }
                    } else if (p.apartment && !p.room) {
                        if (Auth.authorize([{globalRole: 'P'}])) {
                            // ВИ_ЖКХРАО_7
                            stateName = 'citizen-apartment-view';
                            params = {
                                apartmentGuid: p.apartment.guid
                            };
                        } else {
                            stateName = 'apartment';
                            state = $state.get(stateName);
                            if (state && state.data && state.data.permit) {
                                hasPermission = Auth.authorize(state.data.permit);
                            }

                            if (/* editable && */ hasPermission) {
                                // ВИ_ЖКХРАО_5
                                params = {
                                    guid: p.apartment.guid,
                                    mode: 'edit'
                                };
                            } else {
                                // ВИ_ЖКХРАО_12, ВИ_ЖКХРАО_13
                                stateName = 'apartment-view';
                                params = {
                                    apartmentGuid: p.apartment.guid
                                };
                            }
                        }
                    } else if (p.room) {
                        if (Auth.authorize([{globalRole: 'P'}])) {
                            // ВИ_ЖКХРАО_47
                            stateName = 'room-view';
                            params = {
                                guid: p.room.guid,
                                houseTypeGuid: h.houseType.guid
                            };
                        } else {
                            stateName = 'room';
                            state = $state.get(stateName);
                            if (state && state.data && state.data.permit) {
                                hasPermission = Auth.authorize(state.data.permit);
                            }

                            if (/* editable && */ hasPermission) {
                                // ВИ_ЖКХРАО_29
                                params = {
                                    mode: 'edit',
                                    roomGuid: p.room.guid,
                                    houseGuid: h.guid,
                                    apartmentGuid: p.apartment ? p.apartment.guid : null,
                                    houseTypeGuid: h.houseType.guid
                                };
                            } else {
                                // ВИ_ЖКХРАО_47
                                stateName = 'room-view';
                                params = {
                                    guid: p.room.guid,
                                    houseTypeGuid: h.houseType.guid
                                };
                            }
                        }
                    }
                    if (stateName) {
                        HousesStateService.goToState(stateName, params);
                    }
                };
            }]
        };
    });
