/**
 * 2.8    Компонент межстраничной навигации (ЭФ_КМН)
 * @deprecated в соответствии с "Общие элементы интерфейсов. Итерация 2"
 * сервер больше не считает общее количество элементов, используйте в работе hcs-pagination
 */
angular.module('common.ef-kmn', [])
    .directive('efKmnForm', function () {
        return {
            restrict: 'AEC',
            scope: {
                totalItems: '=total',
                pageChanged: '=changed',
                inModal: '=modal',
                itemsPerPage: '=',
                fixedItemsPerPage: '@',
                currentPage: '=',
                isLast: '=last'
            },
            replace: true,
            templateUrl: 'ef-kmn/ef-kmn-form.tpl.html',
            controller: function ($scope, $timeout) {
                console.log('Внимание! Вы используете устаревшую версию компонента ' +
                    'межстраничной навигации ef-kmn-form. Пожалуйста перейдите на новое API hcs-pagination.');
                $scope.totalPages = function () {
                    var diff;
                    // Предполагаем, что это биндинг
                    diff = parseInt($scope.totalItems / $scope.itemsPerPage, 10);
                    return $scope.totalItems % $scope.itemsPerPage === 0 ? diff : diff + 1;
                };

                $scope.nextPage = function () {
                    if ($scope.currentPage == $scope.totalPages()) {
                        return;
                    }
                    $scope.currentPage++;
                    $scope.innerPageChanged();
                };
                $scope.prevPage = function () {
                    if ($scope.currentPage <= 1) {
                        return;
                    }
                    $scope.currentPage--;
                    $scope.innerPageChanged();
                };
                $scope.itemsPerPageChanged = function (newValue) {
                    $scope.itemsPerPage = newValue;
                    $scope.currentPage = 1;
                    $scope.innerPageChanged();
                };

                /**
                 * Необходимо чтобы модель успевала обновляться,
                 * иначе в callbacks в родительских областях будут устаревшие значения
                 */
                $scope.innerPageChanged = function () {
                    $timeout($scope.pageChanged, 0);
                };
            }
        };
    })
;