/**
 * Модальное окно для выбора помещения (ЭФ_ВПОМ)
 */
angular.module('common.ef-vpom', [
    'common.ef-bp',
    'common.ef-pa',
    'hcs-table',
    'common.ef-kmn',
    'resources.houseCommonResource',
    'common.auth',
    'common.filter',
    'common.dialogs',
    'ru.lanit.hcs.nsi.rest.NsiApartmentService'
])
    .service('placementChooserDialog', function ($modal, $filter, AppartmentSearchCommonResource, FlatCommonResource, Auth, commonDialogs, $NsiApartmentService) {

        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            modalFade: true,
            size: 'lg',
            templateUrl: 'ef-vpom/ef-vpom-dialog.tpl.html'
        };

        var modalOptions = {
            closeButtonText: 'Отменить',
            actionButtonText: 'Выбрать'
        };

        var settings = {};

        var self = this;
        self.hasCheckbox = function (hasCheckbox) {
            settings.hasCheckbox = hasCheckbox;
            return self;
        };

        self.hasRadio = function (hasRadio) {
            settings.hasRadio = hasRadio;
            return self;
        };

        self.excludedGuids = function(guids) {
            settings.excludedGuids = guids;
            return self;
        };

        self.onlyCommunal = function() {
            settings.onlyCommunal = true;
            return self;
        };

        self.communalDisabled = function() {
            settings.communalDisabled = true;
            return self;
        };

        self.show = function () {
            if (!modalDefaults.controller) {
                modalDefaults.controller = function ($scope, $modalInstance) {
                    $scope.defaultSearchParameters = {
                        addressFias : { address: {}}
                    };
                    $scope.searchParameters = angular.copy($scope.defaultSearchParameters);

                    $scope.searchResults = [];
                    $scope.searchResultTable = {};
                    $scope.searchResultTable.config = {
                        dataSource: refresh,
                        sortable: false,
                        modal: true
                    };
                    $scope.searchResultTable.state = {};
                    $scope.searchService = function (searchParameters) {
                        $scope.searchParameters = searchParameters;

                        $scope.savedItems = angular.copy($scope.items);
                        $scope.items = [];
                        $scope.checkedItems = {};
                        $scope.radioItem = null;
                        $scope.result = null;

                        return $scope.searchResultTable.state.refresh();
                    };

                    $scope.hasCheckbox = settings.hasCheckbox;
                    $scope.hasRadio = settings.hasRadio;
                    $scope.onlyCommunal = settings.onlyCommunal;
                    $scope.communalDisabled = settings.communalDisabled;

                    $scope.searchResultTable.config.noRefreshOnInit = $scope.onlyCommunal;

                    $scope.searchFlatNumbers = function (query) {
                        return FlatCommonResource.query(
                            [],
                            $scope.searchParameters,
                            function (data) {
                                return data;
                            },
                            $scope.errorCallback
                        );
                    };

                    $NsiApartmentService.findApartmentCategory(null, null, null, function (categories) {
                        $scope.categories = angular.copy(categories);
                    }, $scope.errorCallback);

                    $NsiApartmentService.findResidentPremiseType(null, null, null, function(premiseTypes) {
                        $scope.premiseTypes = angular.copy(premiseTypes);
                        if ($scope.onlyCommunal) {
                            var result = $scope.premiseTypes.filter(function (item) {
                                return item.code == '2';
                            });
                            $scope.searchParameters.residentPremiseType = result && result.length > 0 ? result[0].guid : null;
                            $scope.searchService($scope.searchParameters);
                        }
                    }, $scope.errorCallback);

                    $scope.itemDisabled = function(item) {
                        if ($scope.onlyCommunal) {
                            return item.residentialPremiseType && item.residentialPremiseType.code != '2';
                        }
                        if ($scope.communalDisabled) {
                            return item.residentialPremiseType && item.residentialPremiseType.code == '2';
                        }
                        return false;
                    };

                    $scope.radioItem = null;
                    $scope.checkedItems = {};
                    $scope.items = [];
                    $scope.savedItems = [];

                    $scope.chooseAddress = function (apartment) {
                        $scope.radioItem = apartment.guid;
                        $scope.result = apartment;
                    };

                    $scope.tryItem = function (item) {
                        if ($scope.itemDisabled(item)) {
                            return;
                        }

                        if ($scope.checkedItems[item.guid]) {
                            $scope.items = $scope.items.filter(function (element) {
                                return !angular.equals(item, element);
                            });
                            delete $scope.checkedItems[item.guid];
                        } else {
                            $scope.items.push(item);
                            $scope.checkedItems[item.guid] = true;
                        }
                        $scope.chooseAddress(item);
                    };

                    $scope.disabled = function () {
                        return ($scope.hasRadio && !$scope.result) || ($scope.hasCheckbox && $scope.items.length === 0);
                    };

                    $scope.modalOptions = modalOptions;
                    $scope.modalOptions.ok = function () {
                        if ($scope.hasCheckbox) {
                            $modalInstance.close($scope.items);
                        } else if ($scope.hasRadio) {
                            $modalInstance.close($scope.result);
                        }
                        settings = {};
                    };
                    $scope.modalOptions.close = function (result) {
                        $modalInstance.dismiss('cancel');
                        settings = {};
                    };

                    $scope.errorCallback = function (error) {
                        commonDialogs.error('Во время работы системы произошла ошибка');
                    };

                    function refresh(query) {
                        var params = angular.copy($scope.searchParameters);
                        delete params.addressFias.address;
                        delete params.addressFias.zipCode;

                        if (Auth && Auth.user && Auth.user.organizationGuid) {
                            params.organizationGuid = Auth.user.organizationGuid;
                        }

                        if (settings.excludedGuids && settings.excludedGuids.length > 0) {
                            params.excludedApartmentGuid = settings.excludedGuids;
                        }

                        angular.forEach(params, function (value, property) {
                            if (!value || angular.equals(value, {})) {
                                delete params[property];
                            }
                        });

                        return AppartmentSearchCommonResource.create(
                            params,
                            function (data) {
                                var map = angular.copy(data);
                                $scope.searchResults = map['items'];

                                //Требование по отображению выбранных элементов в начале списка
                                if ($scope.hasCheckbox) {
                                    var tail = [];
                                    var head = $scope.searchResults.filter(function (item) {
                                        var toHead = false;

                                        angular.forEach($scope.savedItems, function (savedItem) {
                                            if (savedItem.guid == item.guid) {
                                                toHead = true;
                                            }
                                        });

                                        if (!toHead) {
                                            tail.push(item);
                                        }
                                        return toHead;
                                    });

                                    $scope.searchResults = head.concat(tail);
                                    data.items = $scope.searchResults;

                                    if (head.length > 0) {
                                        angular.forEach(head, function (item) {
                                            if (!$scope.checkedItems[item.guid]) {
                                                $scope.tryItem(item);
                                            }
                                        });
                                    }
                                }

                                //Подгрузка номеров квартир
                                if (!params.flatNumber) {
                                    $scope.flatNumbers = [];
                                    angular.forEach($scope.searchResults, function (apartment) {
                                        $scope.flatNumbers.push(apartment.flatNumber);
                                    });
                                } else {
                                    //Требование по отметке выбранного элемента с поиском по квартире включительно
                                    var result = $scope.searchResults.filter(function (item) {
                                        return item.flatNumber == params.flatNumber;
                                    });

                                    if (result && result.length > 0) {
                                        var item = result[0];
                                        if ($scope.hasCheckbox) {
                                            if (!$scope.checkedItems[item.guid]) {
                                                $scope.tryItem(item);
                                            }
                                        }
                                        if (!$scope.itemDisabled(item)) {
                                            $scope.chooseAddress(item);
                                        }
                                    }
                                }
                            },
                            $scope.errorCallback,
                            query.queryParams
                        );
                    }
                };
            }

            return $modal.open(modalDefaults).result;
        };
    }
)
;