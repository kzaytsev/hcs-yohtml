angular.module('common.hcsInput', ['ui.bootstrap', 'ui.utils', 'common.utils'])

    .directive('hcsInput', function () {
        return {
            restrict: 'E',
            require: 'ngModel',
            replace: true,
            link: function ($scope, elem, attr, ngModel) {
                //console.log('hcs-input link');
            },
            controller: function ($scope, $dateParser) {
                //console.log('hcs-input controller');
            },
            compile: function(tElement, tAttrs){
                //console.log('hcs-input compilation');
                return {
                    pre: function(){
                        //console.log('hcs-input pre-compilation');
                    },
                    post: function(scope, element, attributes, controller, transcludeFn){
                        //console.log('hcs-input post-compilation');
                        var $input = $('<input class="SAMPLE_CLASS"/>');
                        for (var attr in attributes) {
                            if (attr[0] !== '$' &&  attributes.$attr[attr]) {
                                if (attr.toLowerCase() === 'class') {
                                    $input.addClass(attributes[attr]);
                                } else {
                                    $input.attr(attributes.$attr[attr], attributes[attr]);
                                }
                            }
                        }
                        element.replaceWith($input);
                    }
                };
            }
        };
    });