angular.module('ppa.collection-search', ['lodash'])
    .factory('CollectionSearchCriteria', ['_', function (_) {

        var createCriteria = function (items, operand) {
            var coll;
            if (!items){
                coll = [null];
            }

            if (!_.isArray(items)) {
                coll = [items];
            } else {
                coll = items;
            }

            return {
                coll: coll,
                operand: operand
            };
        };

        return {
            create: function (items) {
                return createCriteria(items, null);
            },
            createOr: function(items) {
                return createCriteria(items, 'OR');
            },
            createAnd: function (items) {
                return createCriteria(items, 'AND');
            }
        };
    }]);