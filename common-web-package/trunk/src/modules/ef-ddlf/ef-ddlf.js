/**
 * Предназначен для скачивания нескольких файлов одним архивом .Тестовый модуль, пока нет спецификации
 *
 */

angular.module('common.ef-ddlf', ['common.ef-bpp'])
    .directive('efDdlfForm', function () {
        return {
            restrict: 'AEC',
            scope: {
                context: '@',
                files: '=',//Array
                contextPath: '@?',
                isPublic: '=' // servletUrl = isPublic ? ОЧ : ЗЧ
            },
            templateUrl: 'ef-ddlf/ef-ddlf-form.tpl.html',
            controller: 'efDdlfController'
        };
    })
    .controller('efDdlfController', [
        '$scope','getHumanReadableFilesize','getExtensionByFilename', 'getIconByFileExtension', 'getLabelByContentType',
        function ($scope, getHumanReadableFilesize, getExtensionByFilename, getIconByFileExtension, getLabelByContentType) {
            $scope.ctxPath = $scope.contextPath || 'filestore';
            $scope.servletUrl = $scope.isPublic ? 'publicDownloadServlet' : 'downloadServlet';
            $scope.servletArchiveUrl = $scope.isPublic ? 'publicDownloadAllFilesServlet' : 'downloadAllFilesServlet';
            $scope.getHumanReadableFilesize = getHumanReadableFilesize;
            $scope.getExtensionByFilename = getExtensionByFilename;
            $scope.getLabelByContentType = getLabelByContentType;
            $scope.$watchCollection('files', function(){$scope.uids = _.map($scope.files,'contentGuid').join();});


        }]);
