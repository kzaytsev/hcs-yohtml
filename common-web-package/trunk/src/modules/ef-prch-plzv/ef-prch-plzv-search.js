angular.module('common.ef-prch-plzv.search', [
    'ru.lanit.hcs.nsi.rest.NsiPpaService',
    'common.ef-prch-plzv.service'
]).controller('EfPrchPlzvSearchController', [
    '$scope',
    '$NsiPpaService',
    'EfPrchPlzvRole',
    'EfPrchPlzvRoleService',
    function ($scope, $NsiPpaService, EfPrchPlzvRole, EfPrchPlzvRoleService) {
        var errorCallback = $scope.errorCallback || angular.noop;

        $scope.defaultSearchParameters = {
            roleCode: EfPrchPlzvRole.E.code,
            lastName: null,      // single
            firstName: null,          // single
            middleName: null,         // single
            emploeeRoles: null,      // multiple
            innOrOgrnOrName: null, // single
            plzvStatus: null     // singe
        };

        var initSearchParams = $scope.config.initSearchParameters;
        if(initSearchParams){
            if(initSearchParams.innOrOgrnOrName){
                $scope.defaultSearchParameters.innOrOgrnOrName = initSearchParams.innOrOgrnOrName.value;
                $scope.orgDisbabled = initSearchParams.innOrOgrnOrName.disabled;
            }
        }
        $scope.searchParameters = angular.copy($scope.defaultSearchParameters);
        $scope.searchService = function (searchParameters) {
            $scope.searchParameters = searchParameters;
            $scope.$broadcast('doSearch');
            return true;
        };

        $scope.emploeerRoles = [];
        $NsiPpaService.findEmployeeRole().then(function (roles) {
            $scope.emploeerRoles = roles;
            //$scope.plzvRoles.unshift({"guid":"1","code":"999","actual":null,"userRoleName":"Гражданин"});
        });

        $scope.roles = _.values(EfPrchPlzvRole);
        $scope.isEmployeeRole = function () {
            return EfPrchPlzvRoleService.isEmployeeRole(
                $scope.searchParameters.roleCode);
        };
        $scope.isCitizenRole = function () {
            return EfPrchPlzvRoleService.isCitizenRole(
                $scope.searchParameters.roleCode);
        };

        $scope.citizenRoles = [
            {
                code: 1,
                userRoleName: 'Гражданин'
            },
            {
                code: 2,
                userRoleName: 'Администратор общего собрания собственников помещений'
            }

        ];

        $scope.plzvStatuses = [
            {
                code: 'REGISTERED',
                name: 'Зарегистрирован'
            },
            {
                code: 'BLOCKED',
                name: 'Заблокирован'
            }

        ];

        $scope.plzvStatusesForUi = [
            {
                code: 'REGISTERED',
                name: 'Зарегистрирован'
            }
        ];

        $scope.plzvStatusesDict = _.indexBy($scope.plzvStatuses, 'code');
        $scope.getUserStatus = function (code) {
            return $scope.plzvStatusesDict[code].name;
        };

    }
]);