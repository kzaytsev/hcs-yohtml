angular.module('common.ef-prch-plzv.form', [
    'common.ef-prch-plzv.search',
    'common.ef-prch-plzv.table'
]).directive('efPrchPlzv', function () {
    return {
        restrict: 'E',
        templateUrl: 'ef-prch-plzv/ef-prch-plzv-form.tpl.html',
        controllerAs: 'plzvChooser',
        controller: [
            '$scope',
            'commonDialogs',
            function ($scope, commonDialogs) {
                $scope.config = $scope.externalConfig || {};
                $scope.errorCallback = function () {
                    commonDialogs.error('Во время работы системы произошла ошибка');
                };
            }]
    };
});