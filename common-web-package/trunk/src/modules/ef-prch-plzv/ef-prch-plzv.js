// 2.21	2.21	Блок с перечнем пользователей (ЭФ_ПРЧ_ПОЛ)
angular.module('common.ef-prch-plzv', [
    'common.ef-prch-plzv.form',
    'common.ef-vprch'
]).factory('EfPrchPlzvService', [
    '$modal',
    'EfVprchService',
    function ($modal, EfVprchService) {
        return {
            /**
             * config содержит следующие поля:
             *  - excludeCitizens: для исключение граждан, список guids
             *  - excludeEmployee: для исключение работников организаций, список guids
             *  - selectionMode: single или multiple             *
             */

            chooseCitizens: function (config) {
                return EfVprchService.openChooser({
                    dialogTitle: 'Выбор пользователя',
                    contentDirective: 'ef-prch-plzv',
                    externalConfig: config
                });
            }
        };
    }
]);
