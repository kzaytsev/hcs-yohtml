angular.module('common.ef-prch-plzv.table', [
    'lodash',
    'ru.lanit.hcs.ppa.rest.PpaService',
    'ppa.collection-search',
    'common.ef-prch-plzv.service'
]).controller('EfPrchPlzvTableController', [
    '$scope',
    '$PpaService',
    'CollectionSearchCriteria',
    '$q',
    'EfPrchPlzvRoleService',
    function ($scope, $PpaService, CollectionSearchCriteria, $q,
              EfPrchPlzvRoleService) {
        var config = $scope.config;
        var errorCallback = $scope.errorCallback || angular.noop;
        var currentPageItems = [];
        var selectedItemsResult = $scope.selectedItemsArray || [];

        $scope.selectionMode = config.selectionMode || 'single';
        $scope.isSingleSelectionMode = function () {
            return $scope.selectionMode === 'single';
        };
        $scope.isMultipleSelectionMode = function () {
            return $scope.selectionMode === 'multiple';
        };

        $scope.allSelected = false;
        $scope.checkedItem = null;
        $scope.checkedItems = {};

        $scope.handleRowClick = function (citizen) {
            if ($scope.isSingleSelectionMode()) {
                _.replaceArrayContent(selectedItemsResult, [citizen]);
                $scope.checkedItem = citizen.guid;
            } else if ($scope.isMultipleSelectionMode()) {
                var alreadySelected = $scope.checkedItems[citizen.guid];
                if (alreadySelected) {
                    _.replaceArrayContent(selectedItemsResult, _.filter(selectedItemsResult, function (citizenFromSelected) {
                        return citizenFromSelected.guid != citizen.guid;
                    }));
                    $scope.checkedItems[citizen.guid] = false;
                    $scope.allSelected = false;
                } else {
                    selectedItemsResult.push(citizen);
                    $scope.checkedItems[citizen.guid] = true;

                    var allSelected = _.every(currentPageItems, function (citizen) {
                        return $scope.checkedItems[citizen.guid];
                    });
                    if (allSelected) {
                        $scope.allSelected = true;
                    }
                }
            }
        };
        $scope.selectOrDeselectAll = function () {
            if ($scope.isMultipleSelectionMode()) {
                if ($scope.allSelected) {
                    _.pushAll(selectedItemsResult, _.filter(currentPageItems, function (citizen) {
                        return !$scope.checkedItems[citizen.guid];
                    }));
                    _.forEach(selectedItemsResult, function (citizen) {
                        $scope.checkedItems[citizen.guid] = true;
                    });
                } else {
                    _.forEach(selectedItemsResult, function (citizen) {
                        $scope.checkedItems[citizen.guid] = false;
                    });
                    _.clearArray(selectedItemsResult);
                }
            }
        };

        $scope.$on('clearSelectedItems', function () {
            $scope.allSelected = false;
            $scope.checkedItem = null;
            $scope.checkedItems = {};
        });

        $scope.$on('doSearch', doSearch);

        function doSearch() {
            var roleCode = $scope.searchParameters.roleCode;
            if(EfPrchPlzvRoleService.isCitizenRole(roleCode)) {
                $scope.citizenTable.state.refresh();
            } else {
                $scope.employeeTable.state.refresh();
            }
        }

            var citizensDataSource = function (queryParams) {

                var searchParams = $scope.searchParameters || {};

                var criteria = {};

                if (searchParams.lastName) {
                    criteria.lastNames = CollectionSearchCriteria.create(searchParams.lastName);
                }

                if (searchParams.firstName) {
                    criteria.firstNames = CollectionSearchCriteria.create(searchParams.firstName);
                }

                if (searchParams.middleName) {
                    criteria.middleNames = CollectionSearchCriteria.create(searchParams.middleName);
                }

                if (searchParams.plzvStatus) {
                    criteria.citizenStatuses = CollectionSearchCriteria.create(searchParams.plzvStatus);
                }

                if (searchParams.emploeeRoles) {
                    if (searchParams.emploeeRoles.length == 1){
                        if(searchParams.emploeeRoles == 2) {
                            criteria.isAdminOfGeneralMeetingInApartmentHouse = true;
                        }
                    }
                }

                var excluded = _.union(config.excludeCitizens, _.map(selectedItemsResult, 'guid'));
                if (excluded.length > 0) {
                    criteria.excludeCitizensGuids = excluded;
                }

                var paginationParams = queryParams.queryParams || {};

                return $PpaService.findCitizenShortInfos(
                    paginationParams,
                    criteria,
                    angular.noop,
                    errorCallback).then(function (result) {
                        _.clearArray(currentPageItems);

                        if ($scope.isMultipleSelectionMode()) {
                            _.pushAll(currentPageItems,
                                _.sortBy(selectedItemsResult, 'lastName'));
                            $scope.allSelected = false;
                        } else if ($scope.isSingleSelectionMode()) {
                            _.clearArray(selectedItemsResult);
                            $scope.checkedItem = null;
                        }
                        _.forEach(result.citizenShortInfoList, function (citizen) {
                            citizen.fio = citizen.lastName + " " + citizen.firstName + " " + citizen.middleName;
                            if(citizen.isAdminOfGeneralMeetingInApartmentHouse) {
                                citizen.userRoleNames = 'Администратор общего собрания собственников помещений';
                            }else {
                                citizen.userRoleNames = 'Гражданин';
                            }

                        });

                        _.pushAll(currentPageItems, result.citizenShortInfoList);

                        return {
                            items: currentPageItems
                        };
                    });

                /*return $q.when({items: [
                {guid: "aaa", lastName: "Иванов", firstName: "Иван", middleName: "Иванович"},
                {guid: "bbb", lastName: "Петров", firstName: "Пётр", middleName: "Петрович"},
                {guid: "ccc", lastName: "Сидоров", firstName: "Сидр", middleName: "Сидорович"}
            ]});*/


        };

        var employeesDataSource = function (queryParams) {

            var searchParams = $scope.searchParameters || {};

            var criteria = {};

            if (searchParams.lastName) {
                criteria.lastNames = CollectionSearchCriteria.create(searchParams.lastName);
            }

            if (searchParams.firstName) {
                criteria.firstNames = CollectionSearchCriteria.create(searchParams.firstName);
            }

            if (searchParams.middleName) {
                criteria.middleNames = CollectionSearchCriteria.create(searchParams.middleName);
            }

            if (searchParams.innOrOgrnOrName) {
                criteria.innOrOgrnOrName = CollectionSearchCriteria.create(searchParams.innOrOgrnOrName);
            }

            if (searchParams.plzvStatus) {
                criteria.employeeStatuses = CollectionSearchCriteria.create(searchParams.plzvStatus);
            }

            if (searchParams.emploeeRoles) {
                criteria.employeeRoles = CollectionSearchCriteria.createOr(searchParams.emploeeRoles);
            }

            var excluded = _.union(config.excludeEmployee, _.map(selectedItemsResult, 'guid'));
            if (excluded.length > 0) {
                criteria.excludeEmployeesGuids = excluded;
            }

            var paginationParams = queryParams.queryParams || {};

            return $PpaService.findEmployeeShortInfo(
                paginationParams,
                criteria,
                angular.noop,
                errorCallback).then(function (result) {
                    _.clearArray(currentPageItems);

                    if ($scope.isMultipleSelectionMode()) {
                        _.pushAll(currentPageItems,
                            _.sortBy(selectedItemsResult, 'fio'));
                        $scope.allSelected = false;
                    } else if ($scope.isSingleSelectionMode()) {
                        _.clearArray(selectedItemsResult);
                        $scope.checkedItem = null;
                    }
                    _.forEach(result.employeeShortInfoList, function (employee) {
                        employee.fio = _.filter([
                                employee.lastName,
                                employee.firstName,
                                employee.middleName
                            ]).join(' ');

                        employee.userRoleNames = '';
                        _.forEach(employee.employeeRoles, function (employeeRole) {
                            _.forEach($scope.emploeerRoles, function (allEmployeeRole) {
                                if (employeeRole.guid == allEmployeeRole.guid){
                                    employee.userRoleNames = employee.userRoleNames + ' ' + allEmployeeRole.userRoleName;
                                }
                            });

                        });
                    });

                    _.pushAll(currentPageItems, _.sortBy(result.employeeShortInfoList, 'fio'));

                    return {
                        items: currentPageItems
                    };
                });
        };



        $scope.citizenTable = {
            config: {
                dataSource: citizensDataSource,
                sortable: false,
                noRefreshOnInit: true
            },
            state: {
                page: 1,
                itemsPerPage: 10
            }
        };

        $scope.employeeTable = {
            config: {
                dataSource: employeesDataSource,
                sortable: false,
                noRefreshOnInit: true
            },
            state: {
                page: 1,
                itemsPerPage: 10
            }
        };

        if (config.defaultSearch) {
            _.defer(doSearch);
        }
    }
]);