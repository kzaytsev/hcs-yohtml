angular
    .module('common.ef-prch-plzv.service', [])
    .constant('EfPrchPlzvRole', {
        P: {
            code: 'P',
            name: 'Физическое лицо'
        },
        E: {
            code: 'E',
            name: 'Должностное лицо организации'
        }
    })
    .factory('EfPrchPlzvRoleService', [
        'EfPrchPlzvRole',
        function (EfPrchPlzvRole) {
            return {
                isCitizenRole: function (code) {
                    return code == EfPrchPlzvRole.P.code;
                },
                isEmployeeRole: function (code) {
                    return code == EfPrchPlzvRole.E.code;
                }
            };
        }
    ]);