/**
 * Модальное окно просмотра физического лица (ЭФ_ПРФЛ)
 */
angular
    .module('common.ef-prfl', [
        'common.dialogs',
        'common.ef-svfl',
        'ru.lanit.hcs.personregistry.PersonRegistryService',
        'common.utils'
    ])
    .service('efPrflDialog', [
        '$modal',
        'commonDialogs',
        '$PersonRegistryService',
        function ($modal,
                  commonDialogs,
                  $PersonRegistryService) {
            var modalDefaults = {
                backdrop: 'static',
                keyboard: true,
                modalFade: true,
                templateUrl: 'ef-fl/ef-prfl/ef-prfl.tpl.html'
            };

            function getDialogController(initializationParameters) {
                return function ($scope, $modalInstance) {

                    $PersonRegistryService.getRegistryPersonDetailByGuid(
                        initializationParameters.personGuid,
                        function(data, status){
                            $scope.person = data;
                        },
                        function(data, status){
                            commonDialogs.error('Во время работы системы произошла ошибка');
                        }
                    );

                    $scope.close = function () {
                        $modalInstance.dismiss('cancel');
                    };
                };
            }

            function DialogInstance() {
                this.initializationParameters = {};

                this.setPersonGuid = function(personGuid) {
                    this.initializationParameters.personGuid = angular.copy(personGuid);
                    return this;
                };

                this.show = function () {
                    var modalOptions = _.clone(modalDefaults, true);
                    modalOptions.controller = getDialogController(this.initializationParameters);
                    return $modal.open(modalOptions).result;
                };
            }

            this.create = function () {
                return new DialogInstance();
            };
        }])

;
