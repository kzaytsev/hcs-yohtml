/**
 * Модальное окно добавления физического лица (ЭФ_ДОБФЛ)
 *
 */
angular
    .module('common.ef-dobfl', [
        'common.dialogs',
        'common.ef-svfl',
        'ru.lanit.hcs.personregistry.PersonRegistryService'
    ])
    /**
     * Возвращает добавленное ФЛ
     * {
     *      guid: ''
     *      firstName: '',
     *      lastName: '',
     *      middleName: '',
     *      male: boolean,
     *      birthDate: '',
     *      snils: ''
     * }
     * */
    .service('efDobflDialog', [
        '$modal',
        'commonDialogs',
        '$PersonRegistryService',
        function ($modal,
                  commonDialogs,
                  $PersonRegistryService) {
            var modalDefaults = {
                backdrop: 'static',
                keyboard: true,
                modalFade: true,
                templateUrl: 'ef-fl/ef-dobfl/ef-dobfl.tpl.html'
            };

            function getDialogController(initializationParameters) {
                return function ($scope, $modalInstance) {

                    $scope.person = {
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.save = function (formIsValid) {
                        $scope.$broadcast('showError');
                        if(formIsValid){

                            var request = {
                                fiasHouseCode: initializationParameters.fiasHouseCode,
                                registryPerson : $scope.person
                            };

                            $PersonRegistryService.addPerson(
                                request,
                                function(data, status){
                                    $scope.person.guid = data;
                                    $modalInstance.close($scope.person);
                                },
                                function(data, status){
                                    if(data && data.title === 'RegistryPersonAlreadyExistsException' && data.details.message != null){
                                        switch(data.details.message){
                                            case 'fio':
                                                commonDialogs.error('Физическое лицо с указанными реквизитами уже присутствует в реестре.');
                                                break;
                                            case 'snils':
                                                commonDialogs.error('Физическое лицо с указанным СНИЛС уже присутствует в реестре.');
                                                break;
                                            default:
                                                commonDialogs.error('Во время работы системы произошла ошибка');
                                        }
                                    }else{
                                        commonDialogs.error('Во время работы системы произошла ошибка');
                                    }
                                }
                            );
                        }
                    };
                };
            }

            function DialogInstance() {
                this.initializationParameters = {};

                this.setFiasHouseCode = function(fiasHouseCode){
                    this.initializationParameters.fiasHouseCode = fiasHouseCode;
                    return this;
                };

                this.show = function () {
                    var modalOptions = _.clone(modalDefaults, true);
                    modalOptions.controller = getDialogController(this.initializationParameters);
                    return $modal.open(modalOptions).result;
                };
            }

            this.create = function () {
                return new DialogInstance();
            };
        }])

;
