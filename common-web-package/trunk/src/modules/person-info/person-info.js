angular
    .module('common.person-info', [])
    .directive('soleTraderInfo', function () {
        return {
            restrict: 'E',
            scope: {
                person: '=',
                br: '='
            },
            templateUrl: 'person-info/sole-trader-info.tpl.html',
            controller: ['_', '$scope', function (_, $scope) {
                // ИП <Фамилия> <Имя> <Отчество> (ОГРНИП <ОГРНИП>)
                $scope.name = '';
                if ($scope.person) {
                    $scope.name = _([$scope.person.lastName, $scope.person.firstName, $scope.person.middleName])
                        .filter(function (s) {
                            return _.isString(s) && s.length;
                        })
                        .join(' ');
                }
            }]
        };
    })
    .directive('legalEntityInfo', function () {
        return {
            restrict: 'E',
            scope: {
                person: '=',
                br: '='
            },
            templateUrl: 'person-info/legal-entity-info.tpl.html',
            controller: ['$scope', function ($scope) {
                // <Сокращенное или полное, в случае отсутствия сокращенного, наименование организации> (ИНН <ИНН>, КПП <КПП>)
                if ($scope.person) {
                    $scope.name = $scope.person.shortName ? $scope.person.shortName : $scope.person.fullName;
                }
            }]
        };
    })
    .directive('naturalPersonInfo', function () {
        return {
            restrict: 'E',
            scope: {
                person: '=',
                br: '='
            },
            templateUrl: 'person-info/natural-person-info.tpl.html',
            controller: ['_', '$scope', function (_, $scope) {
                // <Фамилия> <Имя> <Отчество> (<дата рождения>)
                $scope.name = '';
                if ($scope.person) {
                    $scope.name = _([$scope.person.lastName, $scope.person.firstName, $scope.person.middleName])
                        .filter(function (s) {
                            return _.isString(s) && s.length;
                        })
                        .join(' ');
                }
            }]
        };
    });
