angular.module('common.dialogs')

    .service('recoveryConfirmDlg', ['$modal', /*'$rootScope', */  'commonDialogs',
        function($modal, $rootScope,  commonDialogs) {

            var modalDefaults = {
                backdrop: 'static',
                keyboard: true,
                modalFade: true,
                templateUrl: 'dialogs/recovery-confirm/recovery.tpl.html'
            };

            var modalOptions = {};

            var self = this;


            self.recoveryConfirm = function (record) {
                modalDefaults.controller = function ($scope, $modalInstance, $stateParams) {
                    $scope.unchanged = function(){
                        if ($scope.modalOptions.restateOption==null){
                            return true;
                        }
                        return false;
                    };
                    $scope.modalOptions = modalOptions;
                    $scope.modalOptions.restateOption = null;
                    //$scope.modalOptions.restateOption = 'one';
                    $scope.modalOptions.close = function() {

                            $modalInstance.dismiss('cancel');
                    };
                    $scope.modalOptions.restate = function() {
                        $modalInstance.close($scope.modalOptions.restateOption);
                    };
                    $scope.errorCallback = function(error) {
                        console.log(error);
                    };

                };

                return $modal.open(modalDefaults);
            };
        }])
;