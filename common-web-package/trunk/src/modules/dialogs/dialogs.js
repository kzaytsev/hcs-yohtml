(function (angular) {
    'use strict';

    var tplAlert = 'dialogs/alert.tpl.html';
    var tplConfirm = 'dialogs/confirm.tpl.html';
    var tplYesNoCancel = 'dialogs/yes-no-cancel.tpl.html';
    var tplAutoHideAlert = 'dialogs/autohide-alert.tpl.html';

    var module = angular.module('common.dialogs',
        ['ui.bootstrap', 'common.buttons']);

    module.constant('alertDef', {
        title: 'Сообщение',
        message: '',
        okText: 'ОК'
    });
    module.constant('errorDef', {
        title: 'Ошибка',
        message: '',
        okText: 'Закрыть'
    });
    module.constant('confirmDef', {
        title: 'Подтверждение',
        message: '',
        yesText: 'Да',
        noText: 'Нет'
    });
    module.constant('yesNoCancelDef', {
        title: 'Подтверждение',
        message: '',
        yesBtnText: 'Сохранить',
        noBtnText: 'Не сохранять',
        cancelBtnText: 'Отмена',
        yesAction: function () {
        },
        noAction: function () {
        }
    });
    module.constant('autoHideAlertDef', {
        title: 'Сообщение',
        message: '',
        okText: 'ОК',
        timeOut: 3
    });
    module.factory('commonDialogs', ['$modal', '$timeout', '$rootScope', function ($modal, $timeout, $rootScope) {
        var self = this;

        self.parseMessages = function (messages, lines) {
            if (!angular.isArray(messages)) {
                messages = [messages];
            }
            if (lines) {
                if (!angular.isArray(lines)) {
                    lines = [lines];
                }
                messages = [[messages, lines]];
            }
            angular.forEach(messages, function (message, i) {
                message = angular.isArray(message) ? message : [
                    [message],
                    []
                ];
                message[0] = message[0] ? message[0] : [];
                message[1] = message[1] ? message[1] : [];
                message[0] = angular.isArray(message[0]) ? message[0] : [message[0]];
                message[1] = angular.isArray(message[1]) ? message[1] : [message[1]];
                messages[i] = message;
            });

            return messages;
        };

        return {
            alert: function (messages) {
                return $modal.open({
                    templateUrl: tplAlert,
                    backdrop: 'static',
                    'controller': function ($scope, $modalInstance, alertDef) {
                        $scope.data = alertDef;
                        $scope.data.messages = self.parseMessages(messages);
                        $scope.type = 'info';
                        $scope.no = $modalInstance.dismiss;
                        $scope.yes = $modalInstance.close;
                    }
                });
            },
            error: function (messages, callback) {
                return $modal.open({
                    templateUrl: tplAlert,
                    scope: $rootScope.$new(true),
                    backdrop: 'static',
                    'controller': function ($scope, $modalInstance, errorDef) {
                        $scope.data = _.clone(errorDef);
                        $scope.data.messages = self.parseMessages(messages);
                        $scope.type = 'danger';
                        $scope.no = function (result) {
                            $modalInstance.dismiss(result);
                            if (callback) {
                                callback();
                            }
                        };
                        $scope.yes = function (result) {
                            $modalInstance.close(result);
                            if (callback) {
                                callback();
                            }
                        };
                    }
                });
            },
            // ЭФ_П1
            confirm: function (messages, lines, footerMessage) {
                return $modal.open({
                    templateUrl: tplConfirm,
                    backdrop: 'static',
                    'controller': function ($scope, $modalInstance, confirmDef) {
                        $scope.data = confirmDef;
                        $scope.data.messages = self.parseMessages(messages, lines);
                        $scope.data.footerMessage = footerMessage;
                        $scope.no = $modalInstance.dismiss;
                        $scope.yes = $modalInstance.close;
                    }
                });
            },
            yesNoCancel: function (opts) {
                return $modal.open({
                    templateUrl: tplYesNoCancel,
                    backdrop: 'static',
                    'controller': function ($scope, $modalInstance, yesNoCancelDef) {
                        $scope.data = yesNoCancelDef;
                        $scope.data.message = opts.message;
                        $scope.dismiss = $modalInstance.dismiss;
                        $scope.close = $modalInstance.close;
                        $modalInstance.result.then(function (result) {
                            if (result == 'yes') {
                                opts.yesAction();
                            } else if (result == 'no') {
                                opts.noAction();
                            } else {
                                console.log("Warning: unknown dialog result");
                                $modalInstance.dismiss();
                            }
                        });
                    }
                });
            },
            // ЭФ_ИС2
            autoHideConfirm: function (message) {
                var dialog = $modal.open({
                    templateUrl: tplAutoHideAlert,
                    backdrop: 'static',
                    'controller': function ($scope, $modalInstance, autoHideAlertDef) {
                        $scope.data = angular.copy(autoHideAlertDef);
                        $scope.data.message = message;
                        $scope.data.secondsTxt = ['секунд', 'секунду', 'секунды', 'секунды'];
                        $scope.no = $modalInstance.dismiss;

                        $scope.deregisterClose = null;
                        $scope.yes = function () {
                            $timeout.cancel($scope.deregisterClose);
                            $modalInstance.close();
                        };
                        sleep();

                        function sleep() {
                            $scope.data.timeOut -= 1;
                            if ($scope.data.timeOut > 0) {
                                $scope.deregisterClose = $timeout(sleep, 1000);
                            } else {
                                dialog.dismiss('autohide');
                            }
                        }
                    }
                });
                return dialog;
            }
        };
    }]);
    module.directive('ngConfirm', ['commonDialogs', '$parse', function (commonDialogs, $parse) {
        return function postLink(scope, iElement, iAttrs) {
            iElement.bind('click', function () {
                commonDialogs.confirm(iAttrs['ngConfirmMessage'])
                    .result.then(function () {
                        $parse(iAttrs['ngConfirm'])(scope);
                    }
                );
            });
        };
    }]);

    //Компонент, реализующий 1.8	Требования к сохранению данных при попытке покинуть страницу
    module.directive('confirmOnExit', ['commonDialogs', '$location', '$state', function (commonDialogs, $location, $state) {
        return {
            restrict: 'A',
            link: function ($scope, elem, attrs, form) {
                var options = $scope.$eval(attrs['confirmOnExit']);
                var saveCallback = options['confirmCallback'];
                var modelChanged = options['modelChanged'];
                var validForSave = options['validForSave'];
                var msg = options['msg'] ? options['msg'] : 'Сохранить изменения?';

                var routeChange = function (event, next, nextParams, fromState, fromParams) {
                    if (modelChanged && modelChanged()) {
                        var parent = next.parent || '';
                        var newLocationPath = parent + next.url;

                        angular.forEach(nextParams, function(value, key) {
                            newLocationPath = newLocationPath.replace(':' + key, value);
                        });

                        event.preventDefault();
                        commonDialogs.yesNoCancel({
                            message: msg,
                            yesAction: function () {
                                if (!validForSave || validForSave && validForSave()) {
                                    saveCallback(newLocationPath, routeChangeOff);
                                }
                            },
                            noAction: function () {
                                routeChangeOff();
                                //$location.path(newLocationPath);
                                $state.go(next, nextParams);
                            }
                        });
                    }
                };

                var routeChangeOff = $scope.$on('$stateChangeStart', routeChange);
                options['routeChangeOff'] = routeChangeOff;
            }
        };
    }]);
}(angular));
