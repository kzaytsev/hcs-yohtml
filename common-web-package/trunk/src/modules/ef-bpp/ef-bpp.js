/**
 * Блок прикрепленных файлов (ЭФ_БПП)
 *
 * Ожидаемый формат входных данных
 * { contentGuid: '-1', fileName: 'file1.txt', contentType: 'text/plane', fileSize: '25725', description: 'Некое описание текстового файла' }
 */
angular.module('common.ef-bpp', ['common.dialogs'])
    //.value('getIconByContentType', function(contentType){
    //    switch (contentType) {
    //        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' :
    //        case 'application/msword' :
    //            return 'icon-file_word';
    //
    //        case 'application/vnd.ms-excel':
    //        case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
    //            return 'icon-file_excel';
    //
    //        case 'application/pdf':
    //            return 'icon-file_pdf';
    //
    //        default:
    //            return 'glyphicon glyphicon-file';
    //    }
    //})
    .value('getLabelByContentType', function(contentType){
        switch (contentType) {
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' :
            case 'application/msword' :
                return 'Документ Microsoft Word';

            case 'text/plain':
                return 'Текстовый документ';

            case 'application/vnd.ms-excel':
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                return 'Документ Microsoft Excel';

            case 'application/x-rar-compressed':
                return 'Архив RAR';

            case 'application/zip':
                return 'Архив ZIP';

            case 'application/vnd.oasis.opendocument.presentation':
            case 'application/vnd.oasis.opendocument.formula':
            case 'application/vnd.oasis.opendocument.spreadsheet':
            case 'application/vnd.oasis.opendocument.text':
            case 'application/vnd.sun.xml.calc':
            case 'application/vnd.sun.xml.writer':
                return 'Документ Open Office';

            default:
            {
                if (contentType && contentType.indexOf('/') > -1) {
                    var type = 'Документ ';
                    if (contentType.indexOf('image') === 0) {
                        type = 'Рисунок ';
                    }
                    var parts = contentType.split('/');
                    return parts && parts.length > 0 ? type + parts[parts.length - 1].toUpperCase() : contentType;
                }
                return contentType;
            }
        }
    })
    .value('getIconByFileExtension', function(extension){
        switch (extension.toLowerCase()) {
            case 'doc' :
            case 'docx' :
                return 'icon-file icon-file_doc';

            case 'xls':
            case 'xlsx':
                return 'icon-file icon-file_xls';

            case 'pdf':
                return 'icon-file icon-file_pdf';

            default:
                return 'icon-file';
        }
    })
    .value('getExtensionByFilename', function(filename){
        return (filename || '').split('.').pop().toLowerCase();
    })
    .value('getHumanReadableFilesize', function(filesize){
        var gaps = ['байт', 'Кб', 'Мб', 'Гб', 'Тб'] ;

        filesize = parseInt(filesize, 10);
        if (!filesize) {
            return '0 ' + gaps[0];
        }

        for (var i = 0, length = gaps.length; i < length; i++) {
            if (filesize < 1024) {
                return parseFloat(filesize.toFixed(2)) + ' ' + gaps[i];
            } else {
                filesize /= 1024;
            }
        }
    })
    .directive('efBppForm', function () {
        return {
            restrict: 'AEC',
            scope: {
                context: '@',
                files: '=',//Array
                removeCallback: '=',
                disabled: '=?',
                hideLabel: '=?',
                halfSize: '=?',
                contextPath: '@?',
                templateUrl: '=?',
                hideAttrs: '=',
                isPublic: '=' // servletUrl = isPublic ? ОЧ : ЗЧ
            },
            controller: function ($scope, getExtensionByFilename, getLabelByContentType, getHumanReadableFilesize, $templateCache) {
                $scope.ctxPath = $scope.contextPath || 'filestore';
                $scope.servletUrl = $scope.isPublic ? 'publicDownloadServlet' : 'downloadServlet';
                $scope.getExtensionByFilename = getExtensionByFilename;
                $scope.getLabelByContentType = getLabelByContentType;
                $scope.getHumanReadableFilesize = getHumanReadableFilesize;

                $scope.remove = function (contentGuid) {
                    if ($scope.removeCallback) {
                        $scope.removeCallback(contentGuid);
                    }
                };
                $scope.templateUrl = $scope.templateUrl || 'ef-bpp/ef-bpp-form.tpl.html';
            },
            templateUrl: 'ef-bpp/ef-bpp-form-wrapper.tpl.html'
        };
    })
;