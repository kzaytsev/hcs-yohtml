/**
 * ЭФ_ППА_СТР_НАП: Страница, отображаемая пользователю, при попытке доступа к функциональности закрытой части
 */
angular
    .module('common.ef-ppa-str-nap', [
        'common.auth'
    ])
    .controller('efPpaStrNapController', function ($scope, Auth) {
        $scope.isLoggedIn = function () {
            return Auth.isLoggedIn();
        };

        $scope.goBack = function () {
            window.history.back();
            window.history.back();
        };

        $scope.goToMainPage = function () {
            window.location = '/';
        };

        $scope.logIn = function () {
            Auth.login();
        };
    })
    .config(function ($stateProvider) {
        $stateProvider
            .state('no-privileges', {
                url: '/no-privileges',
                views: {
                    content: {
                        controller: 'efPpaStrNapController',
                        templateUrl: 'ef-ppa-str-nap/ef-ppa-str-nap.tpl.html'
                    }
                },
                data: {
                    permit: [
                        {
                            registered: false,
                            loggedIn: false
                        }
                    ]
                }
            });
    })
;
