angular
    .module('common.reason-form-control', [
        'lodash',
        'common.utils',
        'ru.lanit.hcs.agreements.rest.AdditionalAgreementService'
    ])
    .directive('reasonFormControl', function () {
        return {
            restrict: 'E',
            scope: {
                ngModel: '=',
                documentGuid: '=',
                documentType: '=',
                documentVersionGuid: '=',
                viewMode: '=',
                chapterProtocol: '='
            },
            templateUrl: 'agreements/reason-form-control/reason-form-control.tpl.html',
            controller: 'ReasonFormController'
        };
    })
    .controller('ReasonFormController', function ($rootScope, $scope, $AdditionalAgreementService) {
        var documentName = null;
        var isManagementAgreement = $scope.documentType == 'MANAGEMENT';

        if (isManagementAgreement) {
            documentName = 'Договор управления';
        } else {
            documentName = 'Устав';
        }

        $scope.reasons = [];

        $scope.reasons.push(
            {
                name: documentName,
                guid: $scope.documentGuid
            });

        if (isManagementAgreement) {
            $AdditionalAgreementService.getAgreementReasonsList($rootScope, $scope.documentGuid, $scope.documentVersionGuid, function (result) {
                if (result) {
                    for (var i = 0; i < result.length; i++) {
                        $scope.reasons.push(
                            {
                                name: 'Доп. соглашение №' + result[i].number + ' от ' + result[i].signDate,
                                guid: result[i].guid
                            });

                    }
                }
                if ($scope.isEditMode) {
                    var label = $scope.getReasonNameByGuid($scope.ngModel);
                    $('#reasons').select2('data', {
                        id: $scope.ngModel,
                        text: label
                    });
                } else if ($scope.reasons.length == 1) {
                    $scope.ngModel = $scope.reasons[0].guid;
                }
            });
        } else {
            if ($scope.chapterProtocol) {
                angular.forEach($scope.chapterProtocol, function (value) {
                    $scope.reasons.push({name: value.fileName, guid: value.guid});
                });
            }
            if ($scope.reasons.length == 1) {
                $scope.ngModel = $scope.reasons[0].guid;
            }
        }
        $scope.getReasonNameByGuid = function (guid) {
            for (var i = 0; i < $scope.reasons.length; i++) {
                if ($scope.reasons[i].guid == guid) {
                    return $scope.reasons[i].name;
                }
            }
            return null;
        };
    });