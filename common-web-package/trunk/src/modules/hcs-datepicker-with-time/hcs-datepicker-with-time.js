(function (angular, _, name) {
    "use strict";

    angular
        .module(name, [
            'common.hcs-datepicker',
            'common.hcs-time-input'
        ])
        .directive('hcsDatepickerWithTime', hcsDatePickerWithTime);

    function hcsDatePickerWithTime() {
        return {
            restrict: 'E',
            require: '^form',
            scope: {
                dateName: '@?',
                timeName: '@?',
                options: '=',
                date: '=',
                time: '=',
                startDate: '=',
                startTime: '='
            },
            transclude: true,
            templateUrl: 'hcs-datepicker-with-time/hcs-datepicker-with-time.tpl.html',
            link: hcsDatePickerWithTimeLink
        };
    }

    hcsDatePickerWithTimeLink.$inject = [
        '$scope',
        'elem',
        'attrs',
        'form'
    ];

    function hcsDatePickerWithTimeLink($scope, elem, attrs, form) {
        $scope.timeCtrl = elem.find('hcs-time-input').eq(0).controller('ngModel');
        $scope.dateCtrl = elem.find('hcs-datepicker').eq(0).controller('ngModel');
        $scope.options.timeErrorMsg = $scope.options.timeErrorMsg || 'Дата окончания не должна быть раньше даты начала';

        $scope.$on('$destroy', removeControl);
        $scope.$watchCollection('[time, date, startTime, startDate]', setValidityTimeErrorAndLessThanCurrentError);

        function setValidityTimeErrorAndLessThanCurrentError() {
            $scope.timeCtrl.$setValidity('timeError', timeValid());
        }

        function timeValid() {
            return _.isEmpty($scope.startDate) || _.isEmpty($scope.startTime) ||
                _.isEmpty($scope.date) || _.isEmpty($scope.time) ||
                $scope.startDate != $scope.date || $scope.startTime <= $scope.time;
        }

        function removeControl() {
            form.$removeControl($scope.dateCtrl);
            form.$removeControl($scope.timeCtrl);
        }
    }

    // Решение проблемы с прообразованием выражений в атрибуте name для ngModelDirective
    // https://github.com/angular/angular.js/issues/1404
    angular
        .module(name)
        .config(ConfigDynamicNgModelName);

    ConfigDynamicNgModelName.$inject = [
        '$provide'
    ];

    function ConfigDynamicNgModelName($provide) {
        $provide.decorator('ngModelDirective', ngModelDirective);

        function ngModelDirective($delegate) {
            var ngModel = _.first($delegate);
            ngModel.controller = proxyControllerFactory(ngModel.controller);
            return $delegate;
        }

        function proxyControllerFactory(proxy) {
            proxyController.$inject = [
                '$scope',
                '$element',
                '$attrs',
                '$injector'
            ];

            return proxyController;

            function proxyController($scope, element, attrs, $injector) {
                attrs.$set('name', $injector.get('$interpolate')(attrs.name || '')($scope));
                /*jshint validthis:true */
                $injector.invoke(proxy, this, {$scope: $scope, $element: element, $attrs: attrs});
            }
        }
    }

})(angular, _, 'common.hcs-datepicker-with-time');