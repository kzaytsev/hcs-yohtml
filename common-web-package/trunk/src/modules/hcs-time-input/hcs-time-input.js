angular.module('common.hcs-time-input', ['ui.bootstrap', 'ui.utils', 'common.utils'])

    .directive('hcsTimeInput', function () {
        return {
            restrict: 'E',
            require: 'ngModel',
            scope: {
                time: '=ngModel',
                options: '='
            },

            link: function ($scope, elem, attr, ngModel) {
                var validatorCallback = function (value) {
                    ngModel.$setValidity('required', !$scope.options.required || value);
                    ngModel.$setValidity('invalidFormat', ! $scope.checkTime(value));
                    return value;
                };

                ngModel.$parsers.unshift(validatorCallback);
                ngModel.$formatters.unshift(validatorCallback);

                if ($scope.options.manualTime) {
                    $scope.$watch('time', function () {
                        validatorCallback($scope.time);
                    });
                }

            },
            controller: function ($scope, $dateParser) {
                $scope.options.onTimeBlur = $scope.options.onTimeBlur || angular.noop;
                $scope.defaultTimeMask = "99:99";
                $scope.times = generateTimePeriod();
                $scope.invalidTimeFormat = false;

                $scope.showErrors = function () {
                    if ($scope.options.hasOwnProperty('showErrors')) {
                        return $scope.options.showErrors();
                    } else {
                        return $scope.showErrorOnEvent;
                    }
                };

                $scope.$on('showError', function () {
                    $scope.showErrorOnEvent = true;
                });
                $scope.$on('hideError', function () {
                    $scope.showErrorOnEvent = false;
                });

                $scope.isEmpty = function () {
                    return $scope.options.required && !$scope.time;
                };

                $scope.checkTime = function(timeString) {
                    if (!timeString) {
                        return $scope.invalidTimeFormat = false;
                    }
                    if (typeof timeString == 'string' && timeString.indexOf(':') > -1) {
                        timeString = timeString.replace(/:/g, '');
                    }
                    var scopeTime = $dateParser(timeString, 'HHmm');
                    $scope.invalidTimeFormat = !scopeTime;
                    if (scopeTime) {
                        $scope.time = (scopeTime.getHours() >= 10 ? scopeTime.getHours() : "0" + scopeTime.getHours())  + ":" +
                            (scopeTime.getMinutes() >= 10 ? scopeTime.getMinutes() : "0" + scopeTime.getMinutes());
                    }
                    return  $scope.invalidTimeFormat;
                };

            },
            transclude: true,
            templateUrl: 'hcs-time-input/hcs-time-input.tpl.html'
        };

        function generateTimePeriod() {
            var times = [];
            for (var i = 0; i <= 23; i++) {
                i = '' + i;
                if (i.length == 1) {
                    i = '0' + i;
                }
                times.push({id: i + ':00', name: i + ':00'});
            }
            return times;
        }
    })
;