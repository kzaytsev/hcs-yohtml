/**
 * 2.12 История событий (ЭФ_ИВС)
 *
 * Вызоваем
 * eventHistoryDialog.show('<guid>', [groupList])
 *
 * guid — это либо guid связанной сущности либо guid группирующей сущности
 * В случае переданной сущности получим истоию конкретно по ней.
 * В случае переданной группирующей сущности, получим историю по всем сущностям
 *
 * groupList - массив классов событий
 *
 */
angular.module('common.ef-ivs', [
    'common.ef-bp',
    'common.hcs-datepicker',
    'common.ef-kmn',
    'common.auth',
    'common.filter',
    'common.dialogs',
    'ru.lanit.hcs.eventjournal.rest.JournalRestService',
    'common.utils'
])
    .service('eventHistoryDialog', function ($modal, $filter, Auth, commonDialogs, $EventJournalService, $dateParser) {

        var MILLISECOND_IN_DAY = 86400000;

        var modalDefaults = {
            backdrop: 'static',
            keyboard: true,
            modalFade: true,
            size: 'lg',
            templateUrl: 'ef-ivs/ef-ivs-dialog.tpl.html'
        };

        var modalOptions = {
            closeButtonText: 'Отменить',
            actionButtonText: 'Выбрать'
        };

        this.show = function (entityGuid, groupList, entityTypes, params) {
            modalDefaults.entityGuid = entityGuid;
            modalDefaults.groupList = groupList;
            modalDefaults.entityTypes = entityTypes;
            modalDefaults.params = params || {};
            var dateFormat = 'dd.MM.yyyy';
            if (!modalDefaults.controller) {
                modalDefaults.controller = function ($scope, $modalInstance, $q, $timeout) {
                    var typeListDfd = $q.defer(),
                        typeListPromise = typeListDfd.promise;
                    $scope.searchParameters = {};
                    $scope.searchResultTable = {};
                    $scope.searchResultTable.config = {
                        dataSource: refresh,
                        sortable: false,
                        modal: true
                    };
                    $scope.searchResultTable.state = {};

                    $scope.searchService = function (searchParameters) {
                        $scope.searchParameters = searchParameters;
                        return $scope.searchResultTable.state.refresh();
                    };

                    $scope.getDateFromString = function (dateAsString) {
                        date = $dateParser(dateAsString, dateFormat);
                        return date.getTime();
                    };

                    function refresh(queryParams) {
                        queryParams = queryParams.queryParams || {};

                        var defer = $q.defer();
                        var params = angular.copy($scope.searchParameters);
                        if (modalDefaults.entityGuid) {
                            params.entityGuid = modalDefaults.entityGuid;
                        }
                        if (modalDefaults.entityTypes) {
                            params.entityTypes = modalDefaults.entityTypes;
                        }
                        if (params.eventTypes) {
                            params.eventTypes = [params.eventTypes.name];
                        }
                        if (params.dateFrom) {
                            params.dateFrom = $scope.getDateFromString(params.dateFrom);
                        }
                        if (params.dateTill) {
                            params.dateTill = $scope.getDateFromString(params.dateTill) + MILLISECOND_IN_DAY - 1;
                        }
                        if (queryParams.sortedBy === "eventDate" && queryParams.sortDir) {
                            params.sortCriteria = {
                                sortedBy: 'eventDate',
                                asceding: queryParams.sortDir === 'asc'
                            };
                        } else {
                            delete params.sortCriteria;
                        }
                        angular.forEach(params, function (value, property) {
                            if (value === '' || value === null || value === undefined) {
                                delete params[property];
                            }
                        });
                        $EventJournalService.findEvents({
                                page: queryParams.page,
                                itemsPerPage: queryParams.itemsPerPage
                            },
                            params,
                            function (data) {
                                var eventListDataHandler = function () {
                                   /* $scope.searched = data.items.length > 0;*/
                                    if (data.items && data.items.length) {
                                        data.items = data.items.map(function (item) {
                                            if (item.externalInformationSystemGuid) {
                                                item.username = item.organizationShortName + ' / ' + item.externalInformationSystemName;
                                            }
                                            if ($scope.typeListMap) {
                                                item.eventType = $scope.typeListMap[item.eventType] || item.eventType;
                                            }
                                            item.eventDateToShow = item.eventDate.eventDate  + ' (UTC+'+ item.eventDate.offset +')';
                                            return item;
                                        });
                                    }
                                    defer.resolve(angular.copy(data));
                                };
                                typeListPromise.then(eventListDataHandler, eventListDataHandler);

                            },
                            function () {
                                defer.reject();
                            }
                        );
                        return defer.promise;
                    }

                    $scope.dateFromOptions = {
                        appendToBody: false
                    };

                    $scope.dateTillOptions = {
                        appendToBody: false,
                        dateIsAfter: function () {
                            return $scope.searchParameters.dateFrom;
                        }
                    };

                    $scope.modalOptions = modalOptions;
                    $scope.modalOptions.ok = function () {
                        $modalInstance.close($scope.items);
                    };
                    $scope.modalOptions.close = function (result) {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.errorCallback = modalDefaults.params.errorCallback || function (error) {
                        commonDialogs.error('Во время работы системы произошла ошибка');
                    };

                    $scope.getEventTypes = function () {
                        $scope.typeListMap = {};
                        $EventJournalService.getEventTypeList({"groups":modalDefaults.groupList}, function (data) {
                            $scope.typeList = angular.copy(data);
                            $scope.typeList.forEach(function (item) {
                                $scope.typeListMap[item.name] = item.label;
                            });
                            typeListDfd.resolve();
                        }, function () {
                            typeListDfd.reject();
                            console.error('');
                        });
                    };

                    $scope.getEventTypes();
                };
            }

            return $modal.open(modalDefaults).result;
        };
    })
;
