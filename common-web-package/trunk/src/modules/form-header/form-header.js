/**
 * Example of format a variable breadcrumbs:
 * breadcrumbs = [
 *  {
 *      url: '#/first'
 *      label: 'First Url'
 *  },
 *  {
 *      url: '#/second'
 *      label: 'Second Url',
 *      onHref: function (breadcrumb, proceed) {
 *          // any code
 *          proceed(breadcrumb.url);
 *          // ...
 *          // or if you need to stop the transition
 *          if (valid()) {
 *              proceed(breadcrumb.url);
 *          } else {
 *              showErrors();
 *          }
 *      }
 *  },
 *  {
 *      url: ''
 *      label: 'Current Url'
 *  }
 * ]
 */
angular.module('common.form-header', [])

    .directive('formHeader', function () {

        return {
            restrict: 'E',
            templateUrl: 'form-header/form-header-form.tpl.html',
            scope: {
                breadcrumbs: '=?',
                pageTitle: '=?'
            },
            controller: ['$scope', '$window', function ($scope, $window) {
                var changePath = function (url) {
                    $window.location.href = url;
                };
                $scope.href = function (breadcrumb) {
                    if (_.isEmpty(breadcrumb.url)) {
                        return;
                    }
                    if (_.isFunction(breadcrumb.onHref)) {
                        breadcrumb.onHref(breadcrumb, changePath);
                    } else {
                        changePath(breadcrumb.url);
                    }
                };
            }],
            link: function (scope, element, attrs) {

                scope.$root.$on('$stateChangeSuccess', function (event, toState) {


                    scope.pageTitle = toState.data.pageTitle;
                    scope.breadcrumbs = toState.data.breadcrumbs;
                });

                scope.$root.$on('pageTitleChanged', function (event, title) {
                    scope.pageTitle  = title;
                    scope.$apply();
                });

                scope.$root.$on('breadcrumbLastLabelChanged', function (event, label) {
                    scope.breadcrumbs[scope.breadcrumbs.length - 1].label = label;
                });

                scope.$root.$on('breadcrumbsChanged', function (event, breadcrumbs) {
                    scope.breadcrumbs = breadcrumbs;
                });
            }

        };
    });