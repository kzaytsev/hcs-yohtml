(function (angular, console, _, name) {
    "use strict";

    angular.module(name, [
        'ru.lanit.hcs.notification.rest.NotificationService',
        'ru.lanit.hcs.notification.rest.InformingService'
    ]);

    angular.module(name).factory('NotificationModelAmount', NotificationModelAmount);

    NotificationModelAmount.$inject = [
        '$rootScope',
        '$NotificationService'
    ];

    function NotificationModelAmount($rootScope, $NotificationService) {
        var loadEventName = name + '.amount.notification.loaded';
        $rootScope.notificationAmount = 0;
        var initializationNotification = _.once(publishAmountNotification);

        return {
            subscribe: subscribeAmountNotification,
            publish: publishAmountNotification
        };

        function subscribeAmountNotification(listener) {
            initializationNotification();
            if (listener && $rootScope.notificationAmount) {
                listener($rootScope.notificationAmount);
            }
            return listener ? $rootScope.$on(loadEventName, callBackWithOutFirstArg(listener)) : angular.noop;
        }

        function publishAmountNotification() {
            $NotificationService.countNotificationsForUser(function (data) {
                $rootScope.$broadcast(loadEventName, $rootScope.notificationAmount = data.notificationCount);
            }, defaultError);
        }
    }

    /////////////////////////////////////////////////////////////////////////////

    angular.module(name).factory('InformingModelService', InformingModelService);

    InformingModelService.$inject = [
        '$rootScope',
        '$InformingService'
    ];

    function InformingModelService($rootScope, $InformingService) {
        var loadEventName = name + '.amount.informing.loaded';
        $rootScope.newsAmount = 0;
        var initializationInforming = _.once(publishAmountInforming);

        return {
            subscribe: subscribeAmountInforming,
            publish: publishAmountInforming
        };

        function subscribeAmountInforming(listener) {
            initializationInforming();
            if (listener && $rootScope.newsAmount) {
                listener($rootScope.newsAmount);
            }
            return listener ? $rootScope.$on(loadEventName, callBackWithOutFirstArg(listener)) : angular.noop;
        }

        function publishAmountInforming() {
            $InformingService.countNotificationsForUser(function (data) {
                $rootScope.$broadcast(loadEventName, $rootScope.newsAmount = data.newsCount);
            }, defaultError);
        }
    }

    function defaultError(data, status) {
        console.error("Во время работы Системы произошла ошибка:", status);
    }

    function callBackWithOutFirstArg(listener) {
        return function () {
            listener.apply(null, _.rest(arguments));
        };
    }

})(angular, console, _, 'common.notification.model');