/**
 * Блок сведений об индивидуальном предпринимателе (ЭФ_СВИП)
 */
angular
    .module('common.ef-svip', [
        'common.required-field',
        'common.hcs-datepicker',
        'common.ef-vva',
        'common.validate',
        'lodash'
    ])

    /**
     * disabledMode - признак того, используется ли директива в режиме просмотра
     * editMode - режим редактирования?
     * */
    .directive('efSvipForm', function () {
        return {
            restrict: 'E',
            scope: {
                soleTrader: '=',
                disabledMode: '=',
                editMode: '=',
                showErrors: '@'
            },
            templateUrl: 'ef-svip/ef-svip.tpl.html',
            controller: 'efSvipController'
        };
    })

    .controller('efSvipController', [
        '$scope',
        'addressChooserDialog',
        '_',
        function($scope,
                 addressChooserDialog,
                 _){

            if (!_.isUndefined($scope.$parent.$eval($scope.showErrors))) {
                $scope.$parent.$watch($scope.showErrors, function(newValue, oldValue) {
                    $scope.showError = newValue;
                });
            }

            $scope.selectAddress = function() {
                addressChooserDialog.show().then(function(result) {
                    $scope.soleTrader.mainAddress = result;

                });
            };

            $scope.stateRegistrationDateOptions = {
                date: null,
                format: 'dd.MM.yyyy',
                appendToBody: false,
                required: false
            };

        }
    ])

    .directive('residenceAddress', function () {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                ngModel.$formatters.push(function(value) {
                    if (value) {
                        return value.address || value.formattedAddress;
                    }
                    return null;
                });
            }
        };
    })

;
