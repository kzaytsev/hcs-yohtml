angular.module('common.hcs-spinner', [])

    .directive('hcsSpinner', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function ($scope, $element, $attrs, ngModel) {
                var defaultOptions = {
                    spin: function (e, ui) {
                        $scope.$apply(function () {
                            ngModel.$setViewValue(ui.value);
                        });
                        setTimeout(function () {
                            $scope.$apply(function () {
                                resetViewValueToModelValue(ngModel.$modelValue);
                            });
                        });
                    }
                };
                var initialized = false;

                $scope.$watch($attrs.hcsSpinner, function (options) {
                    options = angular.extend(options || {}, defaultOptions);
                    if (initialized) {
                        $element.spinner('destroy');
                    }
                    $element.spinner(options);
                    initialized = true;
                }, true);

                $scope.$watch($attrs.ngDisabled, function (disabled) {
                    $element.spinner('option', 'disabled', disabled);
                });

                $scope.$watch($attrs.ngModel, function (value) {
                    resetViewValueToModelValue(value);
                });

                function resetViewValueToModelValue(modelValue) {
                    if (initialized) {
                        $element.spinner('value', modelValue);
                        ngModel.$setViewValue($element.spinner('value'));
                    }
                }
            }
        };
    });