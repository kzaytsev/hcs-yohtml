angular.module('hcs-table', [])

.directive('sortBy', function () {
    return {
        restrict: 'AEC',
        transclude: true,
        replace: true,
        scope: {
            sortdir: '=',
            sortedby: '=',
            sortvalue: '@',
            onsort: '='
        },
        templateUrl: 'tables/hcs-table/sort-by.tpl.html',
        link: function (scope, a, b) {
            scope.sort = function () {
                if (scope.sortedby == scope.sortvalue) {
                    scope.sortdir = scope.sortdir == 'asc' ? 'desc' : 'asc';
                }
                else {
                    scope.sortedby = scope.sortvalue;
                    scope.sortdir = 'asc';
                }
                scope.onsort(scope.sortedby, scope.sortdir);
            };
        }
    };
})

.directive('onBlurChange', function ($parse) {
    return function (scope, element, attr) {
        var fn = $parse(attr['onBlurChange']);
        var hasChanged = false;
        element.on('change', function (event) {
            hasChanged = true;
        });

        element.on('blur', function (event) {
            if (hasChanged) {
                scope.$apply(function () {
                    fn(scope, {$event: event});
                });
                hasChanged = false;
            }
        });
    };
})

.directive('onEnterBlur', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if(event.which === 13) {
                element.blur();
                event.preventDefault();
            }
        });
    };
})



;