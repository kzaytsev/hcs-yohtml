angular.module('ng-simple-grid.filters', [
    /* none */
])
    .constant('paginationPositionConstant', {
        none: 'none',
        top: 'top',
        bottom: 'bottom',
        both: 'both'
    })
    .filter('showTopPagination', ['paginationPositionConstant', function (paginationPositionConstant) {
        return function (paginationPositionConfig) {
            switch (paginationPositionConfig) {
                case paginationPositionConstant.top:
                case paginationPositionConstant.both:
                    return true;
                default :
                    return false;
            }
        };
    }])
    .filter('showBottomPagination', ['paginationPositionConstant', function (paginationPositionConstant) {
        return function (paginationPositionConfig) {
            switch (paginationPositionConfig) {
                case paginationPositionConstant.top:
                case paginationPositionConstant.none:
                    return false;
                default :
                    return true;
            }
        };
    }]);