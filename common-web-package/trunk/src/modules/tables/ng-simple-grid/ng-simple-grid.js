/**
 * Реализация табличного представления
 * Тэги
 * ng-simple-grid - корневой тег директивы
 * column - вложенные теги описания колонок
 *
 * Аттрибуты ng-simple-grid.
 * row-click - обработчик события клика на всей строке. Пример: row-click="$parentScope.chooseHouse(rowData.guid)
 * остальное см. в секции scope для директивы
 *
 * Аттрибуты column
 * title - заголовок колонки, будет помещен в шапку таблицы
 * width - ширина колонки, задается по правилам css
 * cell - указание на источник данных для ячейки, в коде ячейки может фигурировать как cellData
 * ng-click - https://docs.angularjs.org/api/ng/directive/ngClick
 * ng-show - https://docs.angularjs.org/api/ng/directive/ngShow
 * permit - common-web-package.permit
 *
 * Специальные переменные
 * $parentScope - указывает на родительскую область видимости относительнотаблицы,
 *      нужно для прокидывания внутрь функций и данных. Желательно избегать как и работы с $parent
 * rowData - псевдоним данных текущей строки таблицы
 * cellData - псевдоним данных выбранных из объекта строки по псевбониму указанному в атрибуте cell для column
 */
(function (angular) {
    'use strict';
    var m,
        moduleNameHtml = 'ng-simple-grid',
        moduleNameJs = 'ngSimpleGrid',
        moduleDependency = [
            'ui.bootstrap',
            'lodash',
            'ng-simple-grid.filters',
            'ng-simple-grid.sorter',
            'common.hcs-pagination'
        ]
        ;
    m = angular.module(moduleNameHtml, moduleDependency);
    m.constant('defaultConfig', {
        /**
         * Источник данных.
         * Получит на вход объект
         * {
         *      queryParams: {
         *          page:
         *          itemsPerPage:
         *      }
         * }
         * Ожидаем, что должен вернуть promise.
         * Promise должен разрешаться в
         * {
         *      total: , @Deprecated в итерации 2 с новой пагинацией не обращаем внимания
         *      items: [] // массив строк для таблицы
         * }
         */
        dataSource: undefined,
        scrollY: '',
        serverSide: true,
        pagination: 'bottom', // смотри paginationPositionConstant
        sortable: true,
        defaultSearch: undefined,
        modal: false,
        /**
         * Не инициализировать таблицу после создания,
         * нужно если внешний контроллер инициализирует сам,
         * по своему событию
         */
        noRefreshOnInit: false,
        /**
         * Сообщение, если нет данных для отображения
         */
        noResultsMessage: 'Отсутствуют результаты поиска.'
    });
    m.directive(moduleNameJs, ['$compile', '$http', '$templateCache', '$log', function factory($compile, $http, $templateCache, $log) {
        var directiveDefinitionObject = {
            restrict: 'E',
            scope: {
                /**
                 * Настройки таблицы
                 * см. constant('defaultConfig'
                 */
                ngConfig: '&',
                /**
                 * Ожидаемая структура
                 * {
                 *      // текущие параметры пагинации
                 *      pagination: {
                 *         itemsPerPage: ,
                 *         isLast: ,
                 *         page:
                 *     },
                 *     // текущие параметры сортировки
                 *     sort: {
                 *          sortDir: ,
                 *          sortedBy:
                 *     },
                 *     // компонент сам добавит данную функцию для возможности внешного управления обновлением
                 *     refresh
                 */
                state: '=',
                /**
                 * Используется для подкидывания кастомных классов в колонку
                 * @Deprecated используй <column class-name='className'
                 */
                "class": '=',
                /**
                 * Используется для переопределения классов в таблицы заголовка
                 */
                headClass: '@?',
                /**
                 * Используется для переопределения классов в таблицы с данными
                 */
                bodyClass: '@?'
            },
            controller: moduleNameJs + 'Controller',
            compile: function CompilingFunction($templateElement, $templateAttributes) {
                var columns = $templateElement.children('column');
                $templateElement.empty();
                var columnsHasChildren = false;
                angular.forEach(columns, function (column, colIdx) {
                    var childrenColumns = $(column).children('column');
                    if (childrenColumns.length > 0) {
                        columnsHasChildren = true;
                    }
                });
                function renderThSorter(column, $scope, cellName) {
                    var sorter,
                        sortableAttr = column.getAttribute('sortable'),
                        sortable = $scope.opts.sortable;
                    if (sortableAttr === 'true') {
                        sortable = true;
                        $scope.opts._hasSortableCell = true;
                    } else if (sortableAttr === 'false') {
                        sortable = false;
                    }
                    if (sortable && cellName) {
                        sorter = $('<sorter></sorter>', {
                            sorthandler: "tableEvent.sortChanged",
                            sortdir: "sortConfig.sortDir",
                            sortedby: "sortConfig.sortedBy",
                            sortvalue: cellName,
                            text: column.getAttribute('title') || ""
                        });
                    } else {
                        sorter = column.getAttribute('title') || "";
                    }
                    return sorter;
                }

                function renderTd(attrs, column, td, cellName, columnAttrs, customClass) {
                    var ngClickList = [];
                    if (attrs['rowClick']) {
                        ngClickList.push(attrs['rowClick']);
                    }
                    if (columnAttrs['colspan']) {
                        td.attr('colspan', columnAttrs['colspan'].value);
                    }
                    if (column.innerHTML.trim()) {
                        var tmpStr = column.innerHTML;
                        tmpStr = tmpStr.replace('cellData', 'rowData.' + cellName);
                        td.html(tmpStr);
                    } else {
                        td.text('{{rowData.' + cellName + '}}');
                    }
                    var ngClick = columnAttrs['ng-click'];
                    if (ngClick) {
                        ngClickList.push(ngClick.value);
                    }
                    if (ngClickList.length > 0) {
                        td.attr('ng-click', ngClickList.join("; "));
                    }
                    if (customClass) {
                        td.addClass(customClass);
                    }
                }

                return function PostLinkingFunction($scope, iElement, attrs) {
                    var
                        tpl = $($templateCache.get('tables/ng-simple-grid/' + moduleNameHtml + '.tpl.html')),
                        headerRow = tpl.find('table.table-head thead tr'),
                        bodyRow = tpl.find('table.table-body tbody tr')
                        ;
                    var subHeaderRow = $('<tr>');
                    angular.forEach(columns, function (column, colIdx) {
                        var td;
                        var th = $('<th>', {
                            width: column.getAttribute('width')
                        });
                        var cellName = column.getAttribute('cell');
                        if(columnsHasChildren) {
                            (th).attr('rowspan', 2);
                        }
                        var childrenColumns = $(column).children('column');
                        var hasChildren = childrenColumns.length > 0;
                        if (hasChildren) {
                            $(th).removeAttr('rowspan');
                            $(th).attr('colspan', childrenColumns.length);
                            angular.forEach(childrenColumns, function (childColumn, colIdx) {
                                var childTh = $('<th>', {
                                    width: childColumn.getAttribute('width')
                                });
                                var childColumnAttrs = childColumn.attributes;
                                var childSorter = renderThSorter(childColumn, $scope, childColumn.getAttribute('cell'));
                                var permit = childColumnAttrs['permit'];
                                if (permit) {
                                    childTh.attr('permit', permit.value);
                                }
                                var ngShow = childColumnAttrs['ng-show'];
                                if (ngShow) {
                                    childTh.attr('ng-show', ngShow.value);
                                }
                                subHeaderRow.append(childTh.append(childSorter));
                            });
                        }
                        var columnAttrs = column.attributes;
                        var permit = columnAttrs['permit'];
                        if (permit) {
                            th.attr('permit', permit.value);
                        }
                        var ngShow = columnAttrs['ng-show'];
                        if (ngShow) {
                            th.attr('ng-show', ngShow.value);
                        }
                        var sorter = renderThSorter(column, $scope, cellName);
                        headerRow.append(th.append(sorter));
                        if (hasChildren) {
                            angular.forEach(childrenColumns, function(childColumn, colIdx) {
                                var childColumnAttrs = childColumn.attributes;
                                var childTd = $('<td>', { class: childColumn.className});
                                renderTd(attrs, childColumn, childTd, childColumn.getAttribute('cell'), childColumnAttrs, $scope.class);
                                var permit = childColumnAttrs['permit'];
                                if (permit) {
                                    childTd.attr('permit', permit.value);
                                }
                                var ngShow = childColumnAttrs['ng-show'];
                                if (ngShow) {
                                    childTd.attr('ng-show', ngShow.value);
                                }
                                bodyRow.append(childTd);
                            });
                        } else {
                            td = $('<td>', { class: column.className});
                            renderTd(attrs, column, td, cellName, columnAttrs);
                            if (permit) {
                                td.attr('permit', permit.value);
                            }
                            if (ngShow) {
                                td.attr('ng-show', ngShow.value);
                            }
                            bodyRow.append(td);
                        }
                    });
                    if (!$scope.ngConfig().scrollY) {
                        tpl.find('table.table-body thead')
                            .append(headerRow);
                        if($(subHeaderRow).children("th")) {
                            tpl.find('table.table-body thead')
                                .append(subHeaderRow);
                        }
                    }
                    iElement.append(tpl);
                    $compile(tpl)($scope);
                };
            }
        };
        return directiveDefinitionObject;
    }]);
    m.controller(moduleNameJs + 'Controller', function ($scope, $q, defaultConfig) {
        $scope = tuneStateOnLoad($scope);
        $scope.tableData = [];
        $scope.getSelected = angular.noop;
        if (_.isObject($scope.state)) {
            $scope.state.refresh = refresh;
            $scope.state.remove = function (item, equals) {
                if (!equals) {
                    equals = function (data) {
                        return data.guid == item.guid;
                    };
                }
                _.remove($scope.tableData, equals);
            };

            if (_.isObject($scope.state.selected)) {
                $scope.state.selected = _.defaults($scope.state.selected, {
                    css: 'info',
                    rows: [],
                    is: function (row, css, rows) {
                        return _.contains(rows, row.guid) ? css : '';
                    }
                });
                $scope.getSelected = function (row) {
                    var selected = $scope.state.selected;
                    return selected.is(row, selected.css, selected.rows);
                };
            }
        }
        $scope.$parentScope = $scope.$parent;
        $scope.opts = angular.extend({}, defaultConfig, $scope.ngConfig());
        // Если задано внешнее состояние

        if (typeof $scope.opts.defaultSearch === 'string') {
            $scope.sortConfig.sortedBy = $scope.opts.defaultSearch;
        }
        $scope.tableEvent = {};
        $scope.tableEvent.sortChanged = function (sortedBy, sortDir) {
            $scope.sortConfig.sortDir = sortDir;
            $scope.sortConfig.sortedBy = sortedBy;
            $scope.paginationConfig.page = 1;
            refresh();
        };
        $scope.tableEvent.pageChanged = refresh;

        if(!$scope.opts.noRefreshOnInit) {
            refresh();
        } else {
            $scope.paginationConfig.isEmpty = true;
            $scope.paginationConfig.page = 1;
        }
        function refresh(newPage) {
            if (typeof $scope.opts.dataSource === 'function') {
                /**
                 * Создаем свой defer,
                 * так как на стандартный будет разрешен при ервом запросе,
                 * а нам может понадобится искать не пустую страницу
                 */
                var defer = $q.defer();
                var eventDescriptionPromise = defer.promise;
                eventDescriptionPromise.then(htmlParsingEventDescription, htmlParsingEventDescription);
                setNewPage(newPage, defer);
                return eventDescriptionPromise;
            }
        }
        /**
         * Функция рекурсивного поиска не пустой страницы
         */
        function setNewPage(newPage, defer) {
            var queryParams = {};
            queryParams.itemsPerPage = $scope.paginationConfig.itemsPerPage;
            queryParams.page = newPage ? newPage : 1;
            if ($scope.opts.sortable || $scope.opts._hasSortableCell === true) {
                angular.extend(queryParams, $scope.sortConfig);
            }
            $scope.opts.dataSource({
                queryParams: queryParams
            }).then(function (data) {
                if (_.isEmpty(data.items) && newPage > 1) {
                    $scope.paginationConfig.isLast = true;
                    return setNewPage(newPage - 1, defer);
                }
                $scope.tableData = data.items;
                $scope.paginationConfig.page = newPage ? newPage : 1;
                $scope.paginationConfig.total = data.total;
                $scope.paginationConfig.isEmpty = data.items.length === 0;
                $scope.paginationConfig.isLast = data.items.length < $scope.paginationConfig.itemsPerPage ||
                    data.total == $scope.paginationConfig.itemsPerPage * $scope.paginationConfig.page;
                defer.resolve();
            });
        }

        function htmlParsingEventDescription() {
            setTimeout(function () {
                $('div.event_description').each(function () {
                    if ($(this).attr('parsed') === 'false') {
                        $(this).attr('parsed', 'true');
                        var value = $(this).text();
                        $(this).html(value);
                    }
                });
            }, 800);
        }

        function tuneStateOnLoad($scope) {
            var paginationDefaultConfig = {
                itemsPerPage: 10,
                total: 0,
                page: 1,
                isLast: false
            };
            var sortDefaultConfig = {
                sortDir: 'asc',
                sortedBy: 'guid'
            };
            if (_.isObject($scope.state)) {
                // Проверяем его структуру и выставляем по умолчанию в случае отсутсвия
                if (_.isObject($scope.state.pagination)) {
                    $scope.paginationConfig = $scope.state.pagination;
                    if (!$scope.paginationConfig.itemsPerPage) {
                        $scope.paginationConfig.itemsPerPage = paginationDefaultConfig.itemsPerPage;
                    }
                    if (!$scope.paginationConfig.isLast) {
                        $scope.paginationConfig.isLast = paginationDefaultConfig.isLast;
                    }
                    if (!$scope.paginationConfig.page) {
                        $scope.paginationConfig.page = paginationDefaultConfig.page;
                    }
                } else {
                    $scope.paginationConfig = paginationDefaultConfig;
                    $scope.state.pagination = $scope.paginationConfig;
                }
                // Проверяем его структуру и выставляем по умолчанию в случае отсутсвия
                $scope.sortConfig = $scope.state.sort;
                if (_.isObject($scope.state.sort)) {
                    $scope.sortConfig = $scope.state.sort;
                    if (!$scope.sortConfig.sortDir) {
                        $scope.sortConfig.sortDir = sortDefaultConfig.sortDir;
                    }
                    if (!$scope.sortConfig.sortedBy) {
                        $scope.sortConfig.sortedBy = sortDefaultConfig.sortedBy;
                    }
                } else {
                    $scope.sortConfig = sortDefaultConfig;
                    $scope.state.sort = $scope.sortConfig;
                }
            } else {
                $scope.paginationConfig = paginationDefaultConfig;
                $scope.sortConfig = sortDefaultConfig;
            }
            return $scope;
        }
    });
}(angular));