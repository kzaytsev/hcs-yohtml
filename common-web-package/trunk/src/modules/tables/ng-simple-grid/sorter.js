angular.module('ng-simple-grid.sorter', [
    /* none */
])
    .directive('sorter', function () {
        return {
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {
                sortdir: '=',
                sortedby: '=',
                sortvalue: '@',
                sorthandler: '='
            },
            templateUrl: 'tables/ng-simple-grid/sorter.tpl.html',
            controller: function ($scope) {
                $scope.sort = function () {
                    if ($scope.sortedby == $scope.sortvalue) {
                        $scope.sortdir = $scope.sortdir == 'asc' ? 'desc' : 'asc';
                    }
                    else {
                        $scope.sortedby = $scope.sortvalue;
                        $scope.sortdir = 'asc';
                    }
                    $scope.sorthandler($scope.sortedby, $scope.sortdir);
                };
            }
        };
    })
;