/* For Example */
<table class="table table-striped" style="width: auto" at-table at-paginated at-list="people" at-config="config">
    <thead></thead>
    <tbody>
    <tr>
        <td at-sortable at-attribute="name" at-title="ФИО" at-initial-sorting="asc">
            {{item.name}}&NonBreakingSpace;
            <div class="btn-group pull-right" dropdown is-open="status.isopen">
                <div class="text-right dropdown-toggle">
                    <i class="fa fa-angle-double-down"></i>
                </div>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div>
        </td>
        <td at-implicit at-sortable at-attribute="age" at-tile="Возраст"></td>
        <td at-implicit at-sortable at-attribute="birthday" at-title="Дата рождения"></td>
    </tr>
    </tbody>
</table>

<at-pagination at-list="people" at-config="config"></at-pagination>

//*********************************************************************************************

angular.module( 'ngBoilerplate.home', [
  'ui.router',
  'angular-table'
])
.controller( 'HomeCtrl', function HomeController( $scope, $filter ) {
            $scope.people = [
                {name: "Иванов Иван Иванович", age : 1, birthday: 2},
                {name: "Петров Петр Константинович", age : 3, birthday: 4},
                {name: "Сидоров Михаил Алексеевич", age : 5, birthday: 6},
                {name: "Минский Николай Валерьевич", age : 7, birthday: 8},
                {name: "Милославский Яков Илларионович", age : 9, birthday: 0}
            ];
            $scope.config = {
                itemsPerPage: 2,
                fillLastPage: true
            };
//...