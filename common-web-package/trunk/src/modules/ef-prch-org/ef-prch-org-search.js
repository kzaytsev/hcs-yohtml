angular.module('common.ef-prch-org.search', [
    'common.ef-spr',
    'ru.lanit.hcs.nsi.rest.NsiPpaService',
    'common.organization.enums',
    'resources.fiasResource',
    'common.ef-vva',
    'common.ef-voktmo'
]).controller('EfPrchOrgSearchController', [
    '$scope',
    '$NsiPpaService',
    'FiasResource',
    'referenceChooserDialog',
    'orgRoleCodeEnum',
    'orgStatusEnum',
    'addressChooserDialog',
    'oktmoChooserDialog',
    function ($scope, $NsiPpaService, FiasResource, referenceChooserDialog,
              orgRoleCodeEnum, orgStatusEnum, addressChooserDialog, oktmoChooserDialog) {
        var errorCallback = $scope.errorCallback || angular.noop;

        var orgRoleGuid = null,
            orgStatuses = null;

        if($scope.config && $scope.config.formParameters){
            var params = $scope.config.formParameters;
            if(params.orgRoleGuid && params.orgRoleGuid.values){
                orgRoleGuid = params.orgRoleGuid.values;
            }
            if(params.orgStatuses && params.orgStatuses.values){
                orgStatuses = params.orgStatuses.values;
            }
        }

        $scope.defaultSearchParameters = {
            orgName: null,            // single
            inn: null,                // single
            ogrn: null,               // single
            okopf: {},              // single
            orgRoleGuid: orgRoleGuid,        // single
            orgTerritoryRegions: null, // multiple
            orgTerritoryOktmos: null,
            orgTerritoryHousesAddresses: null, // для отображения
            orgTerritoryHouses: null,   // multiple
            orgStatuses: orgStatuses         // multiple
        };
        $scope.searchParameters = angular.copy($scope.defaultSearchParameters);
        $scope.searchService = function (searchParameters) {
            $scope.searchParameters = searchParameters;
            $scope.$broadcast('doSearch');
            return true;
        };

        $scope.organizationRoles = [];
        $NsiPpaService.findOrganizationRole().then(function (roles) {
            $scope.organizationRoles = roles;
        });

        // todo: получить список
        $scope.organizationStatuses = _.map(orgStatusEnum);

        // ОКОПФ
        $scope.chooseOkopf = function () {
            referenceChooserDialog
                .setReferenceName('ОКОПФ')
                .hasRadio(true)
                .setType('Okopf')
                .show().then(function (result) {
                    $scope.searchParameters.okopf.code = result.code; //fieldValue2;
                    $scope.searchParameters.okopf.name = result.fieldValue2;
                });
        };

        $scope.chooseOrgTerritory = function () {
            var chosenRole = getChosenOrganizationRole();

            // Если выбрано полномочие «ОМС»,
            // то по нажатию кнопки осуществляется вызов ВИ_ВОКТМО
            // с отображением ЭФ_ВОКТМО с возможностью единичного выбора значений.
            if (chosenRole.code === orgRoleCodeEnum.OMS) {
                oktmoChooserDialog.show().then(function (result) {
                    if ($scope.searchParameters.orgTerritoryOktmos) {
                        var codes = $scope.searchParameters.orgTerritoryOktmos.split(',');
                        var added = _.contains(codes, result.code);
                        if (!added) {
                            codes.push(result.code);
                        }
                        codes = codes.sort();
                        $scope.searchParameters.orgTerritoryOktmos = codes.join(',');
                    } else {
                        $scope.searchParameters.orgTerritoryOktmos = result.code;
                    }
                });
            }

            // Если выбрано полномочие
            // «Администратор общего собрания собственников»,
            // то по нажатию кнопки осуществляется вызов
            // ВИ_ВвА с отображением ЭФ_ВвА.
            if (chosenRole.code === orgRoleCodeEnum.AOS) {
                addressChooserDialog.show().then(function (result) {
                    var notAdded = _.every($scope.searchParameters.orgTerritoryHouses, function (house) {
                        return house.guid !== result.house.guid;
                    });
                    if (notAdded) {
                        $scope.searchParameters.orgTerritoryHouses.push({
                            guid: result.house.guid
                        });
                        $scope.searchParameters.orgTerritoryHousesAddresses.push(result.address);
                    }
                });
            }

        };

        // список субъектов РФ по ФИАС
        // для поля Административная территория - select
        $scope.fiasRegionsList = [];
        $scope.$watch('searchParameters.orgRoleGuid', function () {
            if ($scope.isVisibleOrgTerritorySelect() && $scope.fiasRegionsList.length === 0) {
                FiasResource.region().query([], {}, angular.noop, errorCallback).then(function (data) {
                    $scope.fiasRegionsList = data;
                });
            } else {
                $scope.searchParameters.orgTerritoryRegions = [];
            }

            if (!$scope.ifChosenRoleOMS()) {
                $scope.searchParameters.orgTerritoryOktmos = '';
            }

            if (!$scope.ifChosenRoleAOS()) {
                $scope.searchParameters.orgTerritoryHouses = [];
                $scope.searchParameters.orgTerritoryHousesAddresses = [];
            }
        });


        var getChosenOrganizationRole = function () {
            if (!$scope.searchParameters.orgRoleGuid) {
                return null;
            }
            var role;
            if(!_.isArray($scope.searchParameters.orgRoleGuid)){
                role = _.find($scope.organizationRoles,
                    {guid: $scope.searchParameters.orgRoleGuid});
            }else{
                role = _.find($scope.organizationRoles,
                    {guid: $scope.searchParameters.orgRoleGuid[0]});
            }
            return role;
        };

        $scope.isVisibleOrgTerritoryHousesSelect = function () {
            return $scope.ifChosenRoleOMS() || $scope.ifChosenRoleAOS();
        };

        $scope.ifChosenRoleOMS = function () {
            var chosenRole = getChosenOrganizationRole();
            if (chosenRole == null) {
                return false;
            }

            // Орган местного самоуправления
            if (chosenRole.code === orgRoleCodeEnum.OMS) {
                return true;
            }

            return false;
        };

        $scope.ifChosenRoleAOS = function () {
            var chosenRole = getChosenOrganizationRole();
            if (chosenRole == null) {
                return false;
            }

            // Администратор общего собрания собственников
            // помещений в многоквартирном доме
            if (chosenRole.code === orgRoleCodeEnum.AOS) {
                return true;
            }

            return false;
        };


        $scope.isVisibleOrgTerritorySelect = function () {
            var chosenRole = getChosenOrganizationRole();
            if (chosenRole == null) {
                return false;
            }

            // Орган исполнительной власти субъекта РФ в области
            // государственного регулирования тарифов
            if  (chosenRole.code === orgRoleCodeEnum.OIV_RT) {
                return true;
            }

            // Орган государственной власти субъекта РФ
            if (chosenRole.code === orgRoleCodeEnum.OGVS) {
                return true;
            }

            // Орган исполнительной власти, уполномоченный
            // на осуществление государственного жилищного надзора
            if (chosenRole.code === orgRoleCodeEnum.OIV_OGZH) {
                return true;
            }

            return false;
        };
    }
]);