angular.module('common.ef-prch-org', [
    'common.ef-prch-org.form',
    'common.ef-vprch'
]).factory('EfPrchOrgService', [
    '$modal',
    'EfVprchService',
    function ($modal, EfVprchService) {

        var defaultConfig = {
            excludeOrgs: [],
            selectionMode: 'single',
            isDefaultSearch:true

        };

        return {
            /**
             * config содержит следующие поля:
             *  - excludeOrgs: для исключение организаций, список guids
             *  - selectionMode: single или multiple
             *  - config: {
                 *      selectionMode: 'single', 'multiple'
                 *      excludedOrgs: список guid организаций, которые не должны отображаться в результатах поиска
                 *    }
             */
            chooseOrganizations: function (config) {
                return EfVprchService.openChooser({
                    dialogTitle: 'Выбор организации',
                    useContainerWidth: true,
                    contentDirective: 'ef-prch-org',
                    externalConfig: angular.extend(defaultConfig, config)
                });
            }
        };
    }
]);