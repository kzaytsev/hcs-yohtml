angular.module('common.ef-prch-org.form', [
    'common.ef-prch-org.search',
    'common.ef-prch-org.table'
]).directive('efPrchOrg', function () {
    return {
        restrict: 'E',
        templateUrl: 'ef-prch-org/ef-prch-org.tpl.html',
        controllerAs: 'orgChooser',
        controller: [
            '$scope',
            'commonDialogs',
            function ($scope, commonDialogs) {
                $scope.config = $scope.externalConfig || {};
                console.log('$ scope config = '+angular.toJson());

                var form = $scope.config.formParameters||{};
                $scope.leftColumnStyleClass = $scope.config.leftColumnStyleClass ? $scope.config.leftColumnStyleClass : 'col-md-6';

                $scope.showOrgRoleChooser = function(){
                    return !(form.orgRoleGuid && form.orgRoleGuid.hide);
                };

                $scope.showOrgTerritoryRegionsChooser = function(){
                    return !(form.orgTerritoryRegions && form.orgTerritoryRegions.hide);
                };

                $scope.showOrgTerritoryOktmosChooser = function(){
                    return !(form.orgTerritoryOktmos && form.orgTerritoryOktmos.hide);
                };

                $scope.showOrgStatusesChooser = function(){
                    return !(form.orgStatuses && form.orgStatuses.hide);
                };

                $scope.errorCallback = function () {
                    commonDialogs.error('Во время работы системы произошла ошибка');
                };
            }]
    };
});