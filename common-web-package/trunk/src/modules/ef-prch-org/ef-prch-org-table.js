angular.module('common.ef-prch-org.table', [
    'lodash',
    'ru.lanit.hcs.ppa.rest.PpaService',
    'ppa.collection-search',
    'common.organization.directives'
]).controller('EfPrchOrgTableController', [
    '$scope',
    '$PpaService',
    'CollectionSearchCriteria',
    function ($scope, $PpaService, CollectionSearchCriteria) {
        var config = $scope.config;
        var errorCallback = $scope.errorCallback || angular.noop;
        var currentPageItems = [];
        var selectedItemsResult = $scope.selectedItemsArray || [];

        $scope.selectionMode = config.selectionMode || 'single';
        $scope.isSingleSelectionMode = function () {
            return $scope.selectionMode === 'single';
        };
        $scope.isMultipleSelectionMode = function () {
            return $scope.selectionMode === 'multiple';
        };

        $scope.allSelected = false;
        $scope.checkedItem = null;
        $scope.checkedItems = {};

        $scope.handleRowClick = function (org) {
            if ($scope.isSingleSelectionMode()) {
                _.replaceArrayContent(selectedItemsResult, [org]);
                $scope.checkedItem = org.guid;
            } else if ($scope.isMultipleSelectionMode()) {
                var alreadySelected = $scope.checkedItems[org.guid];
                if (alreadySelected) {
                    _.replaceArrayContent(selectedItemsResult, _.filter(selectedItemsResult, function (orgFromSelected) {
                        return orgFromSelected.guid != org.guid;
                    }));
                    $scope.checkedItems[org.guid] = false;
                    $scope.allSelected = false;
                } else {
                    selectedItemsResult.push(org);
                    $scope.checkedItems[org.guid] = true;

                    var allSelected = _.every(currentPageItems, function (org) {
                        return $scope.checkedItems[org.guid];
                    });
                    if (allSelected) {
                        $scope.allSelected = true;
                    }
                }
            }
        };
        $scope.selectOrDeselectAll = function () {
            if ($scope.isMultipleSelectionMode()) {
                if ($scope.allSelected) {
                    _.pushAll(selectedItemsResult, _.filter(currentPageItems, function (org) {
                        return !$scope.checkedItems[org.guid];
                    }));
                    _.forEach(selectedItemsResult, function (org) {
                        $scope.checkedItems[org.guid] = true;
                    });
                } else {
                    _.forEach(selectedItemsResult, function (org) {
                        $scope.checkedItems[org.guid] = false;
                    });
                    _.clearArray(selectedItemsResult);
                }
            }
        };

        $scope.$on('clearSelectedItems', function () {
            $scope.allSelected = false;
            $scope.checkedItem = null;
            $scope.checkedItems = {};
        });

        $scope.$on('doSearch', function () {
            $scope.organizationsTable.state.refresh();
        });

        // todo: вынести мапппинг в сервис
        var organizationsDataSource = function (queryParams) {
            var searchParams = $scope.searchParameters || {};

            var criteria = {};

            criteria.sortCriteria = {
                sortedBy: 'fullName',
                ascending: true
            };

            if (searchParams.orgName) {
                criteria.name = CollectionSearchCriteria.create(searchParams.orgName);
            }

            if (searchParams.inn) {
                criteria.inn = CollectionSearchCriteria.create(searchParams.inn);
            }

            if (searchParams.ogrn) {
                criteria.ogrn = CollectionSearchCriteria.create(searchParams.ogrn);
            }

            if (searchParams.orgRoleGuid && searchParams.orgRoleGuid !== 'null') {
                if (searchParams.orgRoleGuid === '-none-') {
                    criteria.organizationRoles =
                        CollectionSearchCriteria.create([]);
                    criteria.withoutOrganizationRoles=true;
                } else if(!_.isArray(searchParams.orgRoleGuid)){
                    criteria.organizationRoles =
                        CollectionSearchCriteria.create({
                            guid: searchParams.orgRoleGuid
                        });
                } else {
                    criteria.organizationRoles =
                        CollectionSearchCriteria.createOr(searchParams.orgRoleGuid);
                }
            }

            if (searchParams.orgTerritoryRegions &&
                    searchParams.orgTerritoryRegions.length > 0) {
                criteria.organizationRoleRegions =
                    CollectionSearchCriteria.createOr(searchParams.orgTerritoryRegions);
            }

            if (searchParams.orgTerritoryOktmos) {
                criteria.organizationRoleOktmos =
                    CollectionSearchCriteria.createOr(searchParams.orgTerritoryOktmos.split(','));
            }

            if (searchParams.orgTerritoryHouses &&
                    searchParams.orgTerritoryHouses.length > 0) {
                criteria.organizationRoleHouses =
                    CollectionSearchCriteria.createOr(searchParams.orgTerritoryHouses);
            }

            if (searchParams.okopf && Object.keys(searchParams.okopf).length !== 0) {
                criteria.okopf = CollectionSearchCriteria.create({
                    code: searchParams.okopf.code
                });
            }

            if (searchParams.orgStatuses && searchParams.orgStatuses.length > 0) {
                criteria.organizationStatuses = CollectionSearchCriteria.createOr(searchParams.orgStatuses);
            }

            var excluded=[];
            if ($scope.isSingleSelectionMode()){
                if(config.excludeOrgs) {
                    excluded = config.excludeOrgs;
                }
            } else {
                excluded = _.union(config.excludeOrgs, _.map(selectedItemsResult, 'guid'));
            }

            if (excluded.length > 0) {
                criteria.excludeOrganizationGuids = CollectionSearchCriteria.createOr(excluded);
            }

            var paginationParams = queryParams.queryParams || {};

            return $PpaService.findOrganizationsForChooserComponent(
                paginationParams,
                criteria,
                angular.noop,
                errorCallback).then(function (result) {
                    _.clearArray(currentPageItems);

                    if ($scope.isMultipleSelectionMode()) {
                        _.pushAll(currentPageItems,
                            _.sortBy(selectedItemsResult, 'fullName'));
                        $scope.allSelected = false;
                    } else if ($scope.isSingleSelectionMode()) {
                        _.clearArray(selectedItemsResult);
                        $scope.checkedItem = null;
                    }

                    _.pushAll(currentPageItems, result);

                    return {
                        items: currentPageItems
                    };
                });
        };

        $scope.organizationsTable = {
            config: {
                dataSource: organizationsDataSource,
                sortable: false,
                noRefreshOnInit: !config.isDefaultSearch,
                modal:config.modal
            },
            state: {
                page: 1,
                itemsPerPage: 10
            }
        };
    }
]);