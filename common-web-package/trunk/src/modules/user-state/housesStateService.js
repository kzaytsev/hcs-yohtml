angular.module('common.houses-state-service', []
)
.factory('HousesStateService', ['$state', '$cookies', '$location', '$window', function($state, $cookies, $location, $window) {
    var self = {
        saveState: function (name, params) {
            $cookies[name] = JSON.stringify(params);
        },
        getState: function (name) {
            var params = $cookies[name];
            return params ? JSON.parse(params) : {};
        },
        saveBreadcrumb : function (stateName) {
            var pageBreadcrumbs = $state.$current.data.breadcrumbs;

            if (pageBreadcrumbs && (pageBreadcrumbs.length > 0)) {
                var name = stateName + '.from-page-breadcrumb';

                self.saveState(name, {
                    url:  "#" + $location.url(),
                    label: pageBreadcrumbs[pageBreadcrumbs.length-1].label,
                    backStateName: $state.$current.name
                });
            }
        },
        getBreadcrumb: function (stateName) {
            return self.getState(stateName + '.from-page-breadcrumb');
        },
        getBackStateName: function (stateName) {
            return self.getBreadcrumb(stateName).backStateName;
        },
        goToState: function(toStateName, toStateParams) {
            if ($state.$current && ($state.$current.name == toStateName)) {
                $location.search(toStateParams);
                $window.location.reload();
            } else {
                self.saveBreadcrumb(toStateName);
                $state.go(toStateName, toStateParams);
            }
        },
        goBack: function () {
            var pageBreadcrumbs = $state.$current.data.breadcrumbs;

            if (pageBreadcrumbs && (pageBreadcrumbs.length > 1)) {
                var backUrl = pageBreadcrumbs[pageBreadcrumbs.length - 2].url;
                if (backUrl && (backUrl.length > 0)) {
                    $location.url(backUrl.substring(1));
                    return;
                }
            }

            $window.history.back();
        }
    };

    return self;
}]);