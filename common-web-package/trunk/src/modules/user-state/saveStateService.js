angular.module('common.save-state', [
])
    .factory('hcsUserStateService', [function () {
        var store = {};

        function omitFunctions(object) {
            if (!_.isObject(object)) {
                return;
            }

            if (_.isDate(object)) {
                return object;
            }
            if (_.isArray(object)) {
                _.remove(object, _.isFunction);
            } else {
                object = _.omit(object, _.isFunction);
            }

            _.each(object, function (value, key, obj) {
                if (_.isObject(value)) {
                    object[key] = omitFunctions(object[key]);
                }
            });
            return object;
        }

        function saveState(stateId, state) {
            if (_.isFunction(state)) {
                return;
            }
            if (_.isObject(state)) {
                state = omitFunctions(state);
            }
            store[stateId] = state;
        }

        function loadState(stateId, remove) {
            var data = store[stateId];
            if (remove) {
                delete store[stateId];
            }
            return data;
        }

        var saveOrLoadState = function (stateId, state) {
            var data = loadState(stateId);
            if (_.isEmpty(data)) {
                saveState(stateId, data = state);
            }
            return data;
        };

        return {
            save: saveState,
            load: loadState,
            saveOrLoad: saveOrLoadState
        };
    }])
;