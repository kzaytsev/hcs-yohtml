angular
    .module('common.validate', [
        'lodash'
    ])
    .directive('decimal', function () {
        return {
            priority: 1,
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                var DEFAULT_FRACTION = 2;
                var fraction = scope.$eval(attrs['decimal']);
                var negative = scope.$eval(attrs['negative']);
                var maxValue = parseFloat(scope.$eval(attrs['decimalMax']));
                ngModel.$parsers.unshift(function (inputValue) {
                    var delimeter = '.';
                    var minus = '-';
                    var symbolsAfterDelimeter = fraction || fraction === 0 ? fraction : DEFAULT_FRACTION;

                    var hasDelimeter = false;
                    var hasMinus = false;
                    var digits = inputValue.split('').filter(function (s) {
                        if (s === delimeter && !hasDelimeter && inputValue.indexOf(s) !== 0) {
                            hasDelimeter = true;
                            return true;
                        } else if (negative && s === minus && !hasMinus && inputValue.indexOf(s) === 0) {
                            hasMinus = true;
                            return true;
                        } else {
                            return (!isNaN(s) && s != ' ');
                        }
                    }).join('');

                    var numberOfSymbolsToCutOff = digits.length - (digits.indexOf(delimeter) + symbolsAfterDelimeter + 1);
                    var exceededSymbolsAfterDelimeter = digits.indexOf(delimeter) > 0 && numberOfSymbolsToCutOff > 0;
                    if (exceededSymbolsAfterDelimeter) {
                        digits = digits.slice(0, -(numberOfSymbolsToCutOff));
                    }

                    if (!isNaN(maxValue) && parseFloat(digits) > maxValue) {
                        digits = maxValue.toFixed(fraction);
                    }

                    if (ngModel.$viewValue != digits) {
                        ngModel.$viewValue = digits;
                        ngModel.$render();
                    }
                    return digits;
                });
            }
        };
    })
/**
 * В отличие от директивы decimal, есть ограничение по кол-ву символов в целой части
 * */
    .directive('decimal2', function () {
        return {
            priority: 1,
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                var DEFAULT_FRACTION = 2;
                var fraction = scope.$eval(attrs['decimal']);
                var negative = scope.$eval(attrs['negative']);
                var maxSymbolsBeforeDelimeter = scope.$eval(attrs['fractal']);
                var maxValue = parseFloat(scope.$eval(attrs['decimalMax']));
                var minValue = parseFloat(scope.$eval(attrs['decimalMin']));
                ngModel.$parsers.unshift(function (inputValue) {
                    var delimeter = '.';
                    var minus = '-';
                    var symbolsAfterDelimeter = fraction || fraction === 0 ? fraction : DEFAULT_FRACTION;

                    var hasDelimeter = false;
                    var hasMinus = false;
                    var digits = inputValue.split('').filter(function (s) {
                        if (s === delimeter && !hasDelimeter && inputValue.indexOf(s) !== 0) {
                            hasDelimeter = true;
                            return true;
                        } else if (negative && s === minus && !hasMinus && inputValue.indexOf(s) === 0) {
                            hasMinus = true;
                            return true;
                        } else {
                            return (!isNaN(s) && s != ' ');
                        }
                    }).join('');

                    var numberOfSymbolsToCutOff = digits.length - (digits.indexOf(delimeter) + symbolsAfterDelimeter + 1);
                    var exceededSymbolsAfterDelimeter = digits.indexOf(delimeter) > 0 && numberOfSymbolsToCutOff > 0;
                    if (exceededSymbolsAfterDelimeter) {
                        digits = digits.slice(0, -(numberOfSymbolsToCutOff));
                    }
                    if (maxSymbolsBeforeDelimeter) {

                        var checkAndShortenFractalPart = function (fractalPart) {
                            numberOfSymbolsToCutOff = fractalPart.length - maxSymbolsBeforeDelimeter;
                            if (numberOfSymbolsToCutOff > 0) {
                                return fractalPart.substr(0, maxSymbolsBeforeDelimeter);
                            }
                            else {
                                return fractalPart;
                            }
                        };

                        if (hasDelimeter) {
                            var fractalPart = checkAndShortenFractalPart(digits.substring(0, digits.indexOf(delimeter)));
                            digits = fractalPart + digits.substring(digits.indexOf(delimeter), digits.length);
                        } else {
                            digits = checkAndShortenFractalPart(digits);
                        }
                    }

                    if (!isNaN(maxValue) && parseFloat(digits) > maxValue) {
                        digits = ngModel.$modelValue;
                    }

                    if (!isNaN(minValue) && parseFloat(digits) < minValue) {
                        digits = ngModel.$modelValue;
                    }

                    if (ngModel.$viewValue != digits) {
                        ngModel.$viewValue = digits;
                        ngModel.$render();
                    }

                    return digits;
                });
            }
        };
    })
    .directive('maxPrecision', function () {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function (viewValue) {
                    if (attrs.maxPrecision !== undefined && attrs.maxPrecision == parseInt(attrs.maxPrecision, 10) && viewValue != null && viewValue.length > 0) {
                        if ((('' + viewValue).split('.')[1] || '').length > attrs.maxPrecision) {
                            ctrl.$setValidity('maxPrecision', false);
                            return viewValue;
                        }
                    }
                    ctrl.$setValidity('maxPrecision', true);
                    return viewValue;
                });
            }
        };
    })
    .directive('minPrecision', function () {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function (viewValue) {
                    if (attrs.minPrecision !== undefined && attrs.minPrecision == parseInt(attrs.minPrecision, 10) && viewValue != null && viewValue.length > 0) {
                        if ((('' + viewValue).split('.')[1] || '').length < attrs.minPrecision) {
                            ctrl.$setValidity('minPrecision', false);
                            return viewValue;
                        }
                    }
                    ctrl.$setValidity('minPrecision', true);
                    return viewValue;
                });
            }
        };
    })
    .directive('integer', function () {
        return {
            priority: 1,
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                ngModel.$parsers.unshift(function (inputValue) {
                    if (inputValue) {
                        inputValue = '' + inputValue;
                        var digits = inputValue.replace(/[^0-9]/g, '');

                        if (ngModel.$viewValue != digits) {
                            ngModel.$viewValue = digits;
                            ngModel.$render();
                        }

                        return digits;
                    }
                });
            }
        };
    })
    .directive('positiveInt', function () {
        return {
            priority: 1,
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                var removeLeadingZeros = function (str) {
                    return str.replace(/^[0]+/g, '');
                };
                ngModel.$parsers.unshift(function (inputValue) {
                    if (inputValue) {
                        var digits = inputValue.replace(/[^0-9]/g, '');

                        digits = removeLeadingZeros(digits);

                        if (ngModel.$viewValue != digits) {
                            ngModel.$viewValue = digits;
                            ngModel.$render();
                        }

                        return digits;
                    }
                });
            }
        };
    })
    .directive('email', function () {
        /**
         * соответствие формату:
         * 1. <адрес почты>@<доменное имя сервера>.
         * 2. <адрес почты> и <доменное имя сервера>  содержат только буквы латинского алфавита, а также знаки: точка, дефис и нижнее подчеркивание.
         * (из ЧТЗ ППА ЭФ_ППА_ИНФ_АДМ_ОС_ФЗЛ.24)
         */
        function isValidEmail(email) {
            return (/^[0-9а-яА-ЯA-Za-z\.\-_]+@[0-9а-яА-ЯA-Za-z\.\-_]+\.[0-9а-яА-ЯA-Za-z]+$/).test(email);
        }

        return {
            priority: 1,
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                ngModel.$parsers.unshift(function (inputValue) {
                    if (inputValue) {
                        ngModel.$setValidity('emailFormat', isValidEmail(inputValue));
                    } else {
                        ngModel.$setValidity('emailFormat', true);
                    }
                    return inputValue;
                });
            }
        };
    })
    .directive('phone', [
        function () {
            var phoneMask = new StringMask('0 (000) 000-00-00');

            var isValid = function (ctrl, value, disableCheckPhoneFormat) {
                var valid;
                value = clearValue(value);
                if (value && !disableCheckPhoneFormat) {
                    valid = value && value.length == 11;
                    ctrl.$setValidity('phoneFormat', valid);
                } else {
                    ctrl.$setValidity('phoneFormat', true);
                }
                return value;
            };

            var clearValue = function (value) {
                if (!value) {
                    return value;
                }
                return value.replace(/[^0-9]/g, '');
            };

            var applyPhoneMask = function (value) {
                if (!value) {
                    return value;
                }
                var formatedValue = phoneMask.apply(value);
                return formatedValue.trim().replace(/[^0-9]$/, '');
            };

            return {
                restrict: 'A',
                require: '?ngModel',
                link: function (scope, element, attrs, ngModel) {
                    var disableCheckPhoneFormat = attrs.hasOwnProperty('disableCheckPhoneFormat');

                    if (!angular.isDefined(attrs.placeholder)) {
                        attrs.$set('placeholder', '8 (___) ___-__-__');
                    }
                    if (!ngModel) {
                        return;
                    }

                    element.bind('input keyup click focus', function () {
                        var value = clearValue(ngModel.$viewValue);
                        if (!ngModel.$viewValue) {
                            //do nothing
                        } else if (!value) {
                            ngModel.$setViewValue('8');
                        } else if (value && value.charAt(0) != 8) {
                            ngModel.$setViewValue('8' + value);
                        }
                    });

                    ngModel.$parsers.push(function (value) {
                        return isValid(ngModel, value, disableCheckPhoneFormat);
                    });
                    ngModel.$formatters.push(function (value) {
                        return applyPhoneMask(isValid(ngModel, value, disableCheckPhoneFormat));
                    });
                    ngModel.$parsers.push(function (value) {
                        if (!value) {
                            return value;
                        }

                        var cleanValue = clearValue(value);
                        var formatedValue = applyPhoneMask(cleanValue);

                        if (ngModel.$viewValue !== formatedValue) {
                            ngModel.$setViewValue(formatedValue);
                            ngModel.$render();
                        }

                        return clearValue(formatedValue);
                    });
                }
            };
        }
    ])
    .directive('snils', [
        function () {
            var snilsMask = new StringMask('###-###-### ##');

            var clearValue = function (value) {
                return value ? value.replace(/[^0-9]/g, '').trim().slice(0, 11) : value;
            };

            var applySnilsMask = function (value) {
                return value ? snilsMask.apply(value).replace(/[^0-9]$/, '') : value;
            };

            return {
                restrict: 'A',
                require: '?ngModel',
                link: function (scope, element, attrs, ngModel) {
                    if (!ngModel) {
                        return;
                    }
                    ngModel.$formatters.push(function (value) {
                        return applySnilsMask(clearValue(value));
                    });
                    ngModel.$parsers.push(function (value) {
                        if (!value) {
                            return value;
                        }

                        var cleanValue = clearValue(value);
                        var formattedValue = applySnilsMask(cleanValue);

                        if (ngModel.$viewValue !== formattedValue) {
                            ngModel.$setViewValue(formattedValue);
                            ngModel.$render();
                        }

                        return clearValue(formattedValue);
                    });
                }
            };
        }
    ])
    .directive('russian', function () {
        var isValid = function (s) {
            return s && s.length > 0;
        };
        return {
            priority: 1,
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                ngModel.$parsers.unshift(function (inputValue) {
                    if (inputValue) {
                        var withoutLatin = inputValue.replace(/[A-Za-z]/g, '');

                        // store cursor position - will be set by $render function
                        ngModel.hscCursorPosition = element.get(0).selectionStart + ((withoutLatin.length || 0) - (inputValue.length || 0));

                        if (element.context.required) {
                            ngModel.$setValidity('required', isValid(withoutLatin));
                        }
                        ngModel.$viewValue = withoutLatin;
                        ngModel.$render();

                        return withoutLatin;
                    }
                });
                ngModel.$formatters.unshift(function (modelValue) {
                    if (modelValue) {
                        var withoutLatin = modelValue.replace(/[A-Za-z]/g, '');

                        // store cursor position
                        ngModel.hscCursorPosition = element.get(0).selectionStart + ((withoutLatin.length || 0) - (modelValue.length || 0));

                        if (element.context.required) {
                            ngModel.$setValidity('required', isValid(withoutLatin));
                        }
                        ngModel.$viewValue = withoutLatin;
                        ngModel.$render();

                        return withoutLatin;
                    }
                });
                ngModel.$render = function () {
                    var elemAsNode = element.get(0),
                        viewValue = ngModel.$viewValue;
                    element.val(viewValue);
                    // restore cursor position
                    if (ngModel.hscCursorPosition >= 0 && elemAsNode) {
                        elemAsNode.selectionStart = elemAsNode.selectionEnd = ngModel.hscCursorPosition;
                    }
                };
            }
        };
    })
    .directive('russianOnly', function () {
        var isValid = function (s) {
            return s && s.length > 0;
        };
        return {
            priority: 1,
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                ngModel.$parsers.unshift(function (inputValue) {
                    if (inputValue) {
                        var withoutLatin = inputValue.replace(/[^А-Яа-я]/g, '');

                        // store cursor position - will be set by $render function
                        ngModel.hscCursorPosition = element.get(0).selectionStart + ((withoutLatin.length || 0) - (inputValue.length || 0));

                        if (element.context.required) {
                            ngModel.$setValidity('required', isValid(withoutLatin));
                        }
                        ngModel.$viewValue = withoutLatin;
                        ngModel.$render();

                        return withoutLatin;
                    }
                });
                ngModel.$formatters.unshift(function (modelValue) {
                    if (modelValue) {
                        var withoutLatin = modelValue.replace(/[^А-Яа-я]/g, '');

                        // store cursor position
                        ngModel.hscCursorPosition = element.get(0).selectionStart + ((withoutLatin.length || 0) - (modelValue.length || 0));

                        if (element.context.required) {
                            ngModel.$setValidity('required', isValid(withoutLatin));
                        }
                        ngModel.$viewValue = withoutLatin;
                        ngModel.$render();

                        return withoutLatin;
                    }
                });
                ngModel.$render = function () {
                    var elemAsNode = element.get(0),
                        viewValue = ngModel.$viewValue;
                    element.val(viewValue);
                    // restore cursor position
                    if (ngModel.hscCursorPosition >= 0 && elemAsNode) {
                        elemAsNode.selectionStart = elemAsNode.selectionEnd = ngModel.hscCursorPosition;
                    }
                };
            }
        };
    })
    .directive('russianAndInteger', function () {
        var isValid = function (s) {
            return s && s.length > 0;
        };
        return {
            priority: 1,
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                ngModel.$parsers.unshift(function (inputValue) {
                    if (inputValue) {
                        var withoutLatin = inputValue.replace(/[^А-Яа-я0-9]/g, '');
                        if (element.context.required) {
                            ngModel.$setValidity('required', isValid(withoutLatin));
                        }
                        ngModel.$viewValue = withoutLatin;
                        ngModel.$render();

                        return withoutLatin;
                    }
                });
                ngModel.$formatters.unshift(function (modelValue) {
                    if (modelValue) {
                        var withoutLatin = modelValue.replace(/[^А-Яа-я0-9]/g, '');
                        if (element.context.required) {
                            ngModel.$setValidity('required', isValid(withoutLatin));
                        }
                        ngModel.$viewValue = withoutLatin;
                        ngModel.$render();

                        return withoutLatin;
                    }
                });
            }
        };
    })
    .directive('exactLength', function (_) {
        return {
            priority: 0,
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }

                ngModel.$parsers.unshift(process);
                ngModel.$formatters.unshift(process);

                function process(value) {
                    var stringValue = '' + value;
                    var valid = !value || stringValue.length == attrs.exactLength;
                    ngModel.$setValidity('exactLength', valid);
                    return value;
                }
            }
        };
    })
    .directive('fio', function () {
        var isValid = function (s) {
            return s && s.length > 0;
        };
        var process = function (str, element, ngModel) {
            if (str) {
                var withoutLatin = str.replace(/[^а-яА-ЯёЁ\*\-\s]/g, '');
                if (element.context.required) {
                    ngModel.$setValidity('required', isValid(withoutLatin));
                }
                if (ngModel.$viewValue != withoutLatin) {
                    ngModel.$viewValue = withoutLatin;
                    ngModel.$render();
                }

                return withoutLatin;
            }
        };
        return {
            priority: 1,
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!angular.isDefined(attrs.maxlength)) {
                    attrs.$set('maxlength', '100');
                }
                if (!ngModel) {
                    return;
                }
                ngModel.$parsers.unshift(function (inputValue) {
                    return process(inputValue, element, ngModel);
                });
                ngModel.$formatters.unshift(function (modelValue) {
                    return process(modelValue, element, ngModel);
                });
            }
        };
    })
    .directive('addressFormatter', ['$filter', function ($filter) {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                ngModel.$formatters.push(function (value) {
                        return $filter('addressFormatter')(value);
                    }
                );
            }
        };
    }])
    .directive('lengthValidate', ['_', function (_) {
        var isValid = function (modelValue, lengthValidate) {
            return _.indexOf(lengthValidate, (modelValue + '').length.toString()) > -1 || _.indexOf(lengthValidate, (modelValue + '').length) > -1;
        };
        return {
            restrict: 'EA',
            require: '?ngModel',
            scope: {
                lengthValidate: '=lengthValidate'
            },
            link: function ($scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                ngModel.$parsers.unshift(function (inputValue) {
                    return $scope.process(inputValue, element, ngModel);
                });
                ngModel.$formatters.unshift(function (modelValue) {
                    return $scope.process(modelValue, element, ngModel);
                });

                $scope.process = function (modelValue, element, ngModel) {
                    if (modelValue) {
                        ngModel.$setValidity('lengthValidate', isValid(modelValue, $scope.lengthValidate));

                        return modelValue;
                    }
                };

            }
        };
    }])
    .directive('asStringFormatter', function () {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                ngModel.$formatters.push(function (value) {
                    return '' + value;
                });
            }
        };
    })

/**
 * Demo http://plnkr.co/edit/m1V8kiFWBfC4KbVEdpkb?p=preview
 *
 * @ngdoc directive
 * @name validityBindFix
 *
 * @description
 * Prevents ngModelController from nulling the model value when it's set invalid by some rule.
 * Works in both directions, making sure an invalid model value is copied into the view value and making sure an invalid
 * model value is copied into the model. **Warning:** totally bypasses formatters/parsers when invalid, but probably good
 * enough to use in most cases, like maxlength or pattern.
 *
 * See angular issue: https://github.com/angular/angular.js/issues/1412
 *
 * Inspired by the strategy provided by Emil van Galen here:
 * http://blog.jdriven.com/2013/09/how-angularjs-directives-renders-model-value-and-parses-user-input/
 *
 * @restrict A
 * @scope
 *
 * @param {object} ngModel Required `ng-model` value. If not present in the same element an error occurs.
 */
    .directive('validityBindFix', function () {
        'use strict';

        return {
            require: '?ngModel',
            priority: 9999,
            restrict: 'A',
            link: function ($scope, $element, $attrs, ngModelController) {
                if (!ngModelController) {
                    return;
                }
                ngModelController.$formatters.unshift(function (value) {
                    if (ngModelController.$invalid && angular.isUndefined(value)) {
                        return ngModelController.$modelValue;
                    } else {
                        return value;
                    }
                });
                ngModelController.$parsers.push(function (value) {
                    if (ngModelController.$invalid && angular.isUndefined(value)) {
                        return ngModelController.$viewValue;
                    } else {
                        return value;
                    }
                });
            }
        };
    })
/**
 * @description
 * директива для числового input-а, который становится невалидным, если его значение меньше, указанного в параметре
 * источник идеи: http://stackoverflow.com/questions/20982751/custom-form-validation-directive-to-compare-two-fields
 * */
    .directive('aboveThan', [
        function () {

            var link = function ($scope, $element, $attrs, ctrl) {

                var validate = function (viewValue) {
                    var comparisonModel = $attrs.aboveThan;
                    var andEquals = $attrs.andEquals;
                    if (!viewValue || !comparisonModel || !$attrs.required) {
                        // It's valid because we have nothing to compare against
                        ctrl.$setValidity('aboveThan', true);
                        return viewValue;
                    }

                    if (andEquals) {
                        ctrl.$setValidity('aboveThan', parseFloat(viewValue) >= parseFloat(comparisonModel));
                    } else {
                        ctrl.$setValidity('aboveThan', parseFloat(viewValue) > parseFloat(comparisonModel));
                    }

                    return viewValue;
                };

                ctrl.$parsers.unshift(validate);
                ctrl.$formatters.push(validate);

                $attrs.$observe('aboveThan', function (comparisonModel) {
                    // Whenever the comparison model changes we'll re-validate
                    return validate(ctrl.$viewValue);
                });

                $attrs.$observe('required', function (comparisonModel) {
                    // Whenever the comparison model changes we'll re-validate
                    return validate(ctrl.$viewValue);
                });

            };

            return {
                require: 'ngModel',
                link: link
            };

        }
    ])
    .directive('money', ['$timeout', '_', '$filter', function ($timeout, _, $filter) {
        var process = function (inputVal, element, ngModel, attrs) {
            if (inputVal != null && inputVal !== '') {
                if (document.activeElement != element[0]) {
                    return $filter('money')(inputVal.toString().replace(/\s/g, ''), '').trim();
                }
                inputVal = '' + inputVal;
                var keyCode = ngModel.keyCode;
                var checkPrimeryVal = inputVal;
                var oldValue = ngModel.$modelValue;
                var newValue = ngModel.$viewValue;
                var prepairOldValue = '';
                var prepairNewValue = '';
                var cursorPosition = 0;
                var minus = '';
                var negative = (attrs['negative'] === 'true');

                if (negative === true && inputVal.indexOf('-') == '0') {
                    minus = '-'; //save minus
                }

                try {
                    cursorPosition = document.activeElement.selectionStart;
                }
                catch (err) {
                    cursorPosition = 0;
                }

                inputVal = inputVal.replace(/[^\d.]/g, '');//clear string, also '-'

                //DELETE '.'(dot): compare old and new value, search place(index) where was '.' & paste in
                if (_.isString(oldValue) && _.isString(newValue) && inputVal.length > 2) {
                    prepairOldValue = oldValue.replace(/\s/g, '');
                    prepairNewValue = newValue.replace(/\s/g, '');
                    if (prepairOldValue.indexOf('.') && prepairNewValue.indexOf('.') === -1) {
                        inputVal = prepairNewValue.slice(0, prepairOldValue.indexOf('.')) + '.' + prepairNewValue.slice(prepairOldValue.indexOf('.'));
                        if (keyCode === 46) { //set cursorPosition over the dot '.'
                            cursorPosition++;
                        }
                    }
                }

                //when cutting spaces. keys:8='backspace',46='delete'
                if (prepairNewValue.length == prepairOldValue.length && keyCode == 8) {
                    cursorPosition--;
                    inputVal = newValue.slice(0, cursorPosition) + newValue.slice(cursorPosition + 1);
                }

                if (prepairNewValue.length == prepairOldValue.length && keyCode == 46) {
                    inputVal = newValue.slice(0, cursorPosition) + newValue.slice(cursorPosition + 1);
                    cursorPosition++;
                }

                //если ввели не цифру, курсор не должен реагировать
                if (inputVal.length == prepairOldValue.length && keyCode != 46 && keyCode != 8) {
                    cursorPosition--;
                }

                //clearing left side zeros
                while (inputVal.charAt(0) == '0' && inputVal.length !== 1) {
                    inputVal = inputVal.substr(1);
                    if ((minus === '-' && cursorPosition < 4 && inputVal.charAt(1) == '.') || (cursorPosition < 3 && inputVal.charAt(1) == '.')) {
                        cursorPosition--;
                    }
                }

                var point = inputVal.indexOf('.');
                if (point >= 0) {
                    inputVal = inputVal.slice(0, point + 3);
                }

                var decimalSplit = inputVal.split('.');
                var intPart = decimalSplit[0];
                var decPart = decimalSplit[1];

                intPart = intPart.replace(/[^\d]/g, '');
                if (intPart.length >= 9) {
                    intPart = intPart.slice(0, 8);
                }
                //=============== work on the spaces ==================
                if (intPart.length > 3) {
                    var intDiv = Math.floor(intPart.length / 3);
                    while (intDiv > 0) {
                        var lastComma = intPart.indexOf(' ');
                        if (lastComma < 0) {
                            lastComma = intPart.length;
                        }

                        if (lastComma - 3 > 0) {
                            intPart = intPart.slice(0, lastComma - 3) + ' ' + intPart.slice(lastComma - 3);
                        }
                        intDiv--;
                    }
                }
                //========== work on the decimal part ==================
                if (decPart === undefined || decPart === '' || decPart === '0') {
                    decPart = '.00';
                }
                else { //replace numeric to zero
                    if (decPart.length === 1) {
                        if (newValue.length == cursorPosition) {
                            decPart = decPart + '0';
                        } else {
                            decPart = '0' + decPart;
                        }
                    }
                    decPart = '.' + decPart;
                }

                //=========== concat intPart with decPart============================================================
                var money = intPart + decPart;
                if (intPart === '' && decPart === '.00') { //if set only .00
                    money = '';
                    if (keyCode == 8 || keyCode == 46) {
                        money = '0.00'; //state before empty field
                        if (prepairNewValue != '0.0') {
                            cursorPosition = 1;
                        }
                    }
                    if (minus && checkPrimeryVal != '-') { //clear minus in value 0.00
                        minus = '';
                    }
                    if (checkPrimeryVal == '-') { //don't react (0.00) if minus was inputted
                        money = '';
                    }
                    if (prepairOldValue.indexOf('0') == '0' && money == '0.00' && prepairOldValue == '0.00') {
                        money = '';
                    }
                }
                if (intPart === '' && decPart !== '.00') { //if set something after dot
                    money = '0' + decPart;
                }

                ngModel.$viewValue = minus + money;

                //==========correction cursor when adding space(s)===================================================
                if (minus === '') {
                    if ((money.charAt(cursorPosition - 1) === ' ' || money.charAt(cursorPosition + 1) === '.') && (cursorPosition !== 9) &&
                        prepairNewValue.length > prepairOldValue.length) {
                        cursorPosition++;
                    }
                } else {
                    if ((money.charAt(cursorPosition - 2) === ' ' || money.charAt(cursorPosition) === '.') && (cursorPosition !== 10) &&
                        keyCode !== 46 && keyCode !== 8) {
                        cursorPosition++;
                    }
                }

                //==========for correct delete for key 'backspace'===================================================
                if (keyCode == 8) {
                    if (prepairNewValue.length < prepairOldValue.length && (money + minus).length < newValue.length && intPart !== '') {
                        cursorPosition--;
                    }
                    if (checkPrimeryVal.charAt(cursorPosition) === '-') {
                        cursorPosition++;
                    }
                }

                //===========for correct delete for key 'delete'=====================================================
                if (keyCode == 46 && checkPrimeryVal.indexOf('.') >= 0) {
                    if ((minus && money.charAt(cursorPosition - 3)) === '.' ||  //-123.0x
                        money.charAt(cursorPosition - 2) === '.' ||  //123.0x
                        (money.charAt(cursorPosition - 1) === '.' && checkPrimeryVal.indexOf('.') !== cursorPosition) ||  //123.x0
                        (money.charAt(cursorPosition + 1) === '.' && money.charAt(cursorPosition) === '0') ||  //0.00
                        (minus && money.charAt(cursorPosition) === '.' && money.charAt(cursorPosition - 1) === '0')  //-0.00
                    ) {
                        cursorPosition++;
                    } else {
                        if (!minus && money.replace(/\s/g, '').length % 3 === 0 && inputVal.replace(/\s/g, '').length > 4) {
                            cursorPosition--;
                        }
                        if (minus && money.replace(/\s/g, '').length % 3 === 0 && inputVal.replace(/\s/g, '').length > 4 && cursorPosition != 1) {
                            cursorPosition--;
                        }
                    }
                    //to do: behavior for delete with minus
                }

                //==========rendering of value in <input>============================================================
                ngModel.$render();

                //==========set position of cursor in <input>========================================================
//                $timeout(function () {
                    var el = element[0];
                    el.focus();
                    el.selectionStart = el.selectionEnd = cursorPosition;
//                });

                return minus + money;
            }
        };
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                element.bind('keydown', function (e) {
                    ngModel.keyCode = e.keyCode;
                });
                element.bind('blur', function () {
                    scope.$apply(function () {
                        if (element.val() === '-' || element.val() === '-0.00') {
                            element.val('').trigger('change');
                        }
                    });
                });
                ngModel.$parsers.unshift(function (inputValue) {
                    var str = process(inputValue, element, ngModel, attrs);
                    if (str) {
                        str = str.replace(/\s/g, '');
                    }
                    return str;
                });
                ngModel.$formatters.unshift(function (modelValue) {
                    return process(modelValue, element, ngModel, attrs);
                });
            }
        };
    }])
    /*
     // То же, что и decimal, только ограничивает ввод по количеству всех символов сразу (целая часть + цифры после запятой)
     */
    .directive('decimal3', function () {
        return {
            priority: 1,
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return;
                }
                var DEFAULT_FRACTION = 2;
                var maxlength = scope.$eval(attrs['length']);
                var maxValue = parseFloat(scope.$eval(attrs['decimalMax']));
                var minValue = parseFloat(scope.$eval(attrs['decimalMin']));
                ngModel.$parsers.unshift(function (inputValue) {
                    var delimiter = '.';
                    var delimiterToReplace = ',';
                    var symbolsAfterDelimiter = DEFAULT_FRACTION;

                    var hasDelimiter = false;
                    var indexOfDelimiter = 0;
                    var position = 0;

                    var digits = inputValue.replace(delimiterToReplace, delimiter);

                    digits = digits.split('').filter(function (s) {
                        if (symbolsAfterDelimiter && s === delimiter && !hasDelimiter && position !== 0) {
                            hasDelimiter = true;
                            indexOfDelimiter = position;
                            position++;
                            return true;
                        } else if (!isNaN(s) && s != ' ') {
                            if (hasDelimiter) {
                                if (!maxlength && position <= indexOfDelimiter + symbolsAfterDelimiter ||
                                    maxlength && position <= indexOfDelimiter + symbolsAfterDelimiter && position <= maxlength) {
                                    position++;
                                    return true;
                                }
                            } else if (!maxlength || maxlength && position < maxlength) {
                                position++;
                                return true;
                            }
                        }
                        return false;
                    }).join('');

                    if (!isNaN(maxValue) && parseFloat(digits) > maxValue ||
                        !isNaN(minValue) && parseFloat(digits) < minValue) {
                        digits = digits.slice(0, digits.length - 1);
                    }

                    if (ngModel.$viewValue != digits) {
                        ngModel.$viewValue = digits;
                        ngModel.$render();
                    }

                    return digits;
                });
            }
        };
    })

;