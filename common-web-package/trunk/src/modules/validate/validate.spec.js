describe('validate.js test', function () {
    describe('email test', function () {
        var element;
        var form;
        var $scope;

        beforeEach(module('common.validate'));

        beforeEach(inject(function ($rootScope, $compile) {
            var elementHtml = '<ng-form name="form"><input type="text" ng-model="model.email" name="email" email></ng-form>';
            $scope = $rootScope.$new();
            element = $compile(elementHtml)($scope);
            form = $scope.form;
        }));

        it('should not pass without a "@"', inject(function () {
            form.email.$setViewValue('example.com');

            $scope.$digest();
            expect(form.email.$valid).toBe(false);
            expect(form.email.$error.emailFormat).toBe(true);
        }));

        it('should not pass with other chars than english or russian', inject(function () {
            form.email.$setViewValue('example-ў.com');

            $scope.$digest();
            expect(form.email.$valid).toBe(false);
            expect(form.email.$error.emailFormat).toBe(true);
        }));

        it('should not pass with special chars !#$%^&*()?<>/|\\', inject(function () {
            form.email.$setViewValue('!#$%^&*()?<>/|\\.com');

            $scope.$digest();
            expect(form.email.$valid).toBe(false);
            expect(form.email.$error.emailFormat).toBe(true);
        }));

        it('should pass with english and russian letters, digits and chars "-", "_", "."', inject(function () {
            var input = 'пример-example_12345..67890@example-пример.com0987654321';

            form.email.$setViewValue(input);

            $scope.$digest();
            expect(form.email.$valid).toBe(true);
            expect(form.email.$error.emailFormat).toBeFalsy();
            expect($scope.model.email).toBe(input);
        }));

        it('must be valid if no email specified', inject(function () {
            form.email.$setViewValue('123');
            $scope.$digest();
            expect(form.email.$valid).toBe(false);

            form.email.$setViewValue('');

            $scope.$digest();
            expect(form.email.$valid).toBe(true);
            expect(form.email.$error.emailFormat).toBeFalsy();
        }));
    });
});