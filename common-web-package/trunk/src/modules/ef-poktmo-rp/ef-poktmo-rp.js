/**
 * Блок расширенного поиска записи в ОКТМО (ЭФ_ПОКТМО_РП)
 */
angular
    .module('common.ef-poktmo-rp', [
        'lodash',
        'ui.select2',
        'common.ef-bp',
        'common.filter',
        'ru.lanit.hcs.nsi.rest.ClassifierService',
        'ru.lanit.hcs.nsi.rest.FiasService'
    ])
    .directive('efPoktmoRpForm', function() {
        return {
            restrict: 'E',
            scope: {
                searchParameters : '=',
                defaultSearchParameters : '=',
                regionGuid : '=?',
                excludedGuids : '=?',
                excludedCodes : '=?', // исключенные коды
                useCodes : '=?', //разрешенные коды
                /*
                * параметр управляет чек-боксом "Отображать в результатах поиска все подчиненные территории"
                * объект должен иметь два поля:
                * showCheckBox - показывать или нет чекбокс. по умолчанию true
                * allLevelValue - начальное значение чекбокса. по умолчаеию false
                *пример:
                *  $scope.showAllLevelConfig = {showCheckBox:false, allLevelValue:true};//спрятать чекбокс, чек-бокс при этом установлен
                * <ef-poktmo-rp-form search-parameters="searchParameters" default-search-parameters="defaultSearchParameters" show-all-level-config="showAllLevelConfig"></ef-poktmo-rp-form>
                * */
                showAllLevelConfig : '=?',
                oktmoSearchService: '=?'

            },
            templateUrl: 'ef-poktmo-rp/ef-poktmo-rp.tpl.html',
            controller: 'efPoktmoRpController'
        };
    })
    .controller('efPoktmoRpController', ['$scope', '$filter', '$ClassifierService',
        function ($scope, $filter, $ClassifierService) {
             //console.log('poktmo rp useCodes '+angular.toJson($scope.useCodes));
            if (!$scope.showAllLevelConfig){
                $scope.showAllLevelConfig = {showCheckBox:true, allLevelValue:false};
            }
            if (angular.isUndefined($scope.showAllLevelConfig.showCheckBox)){
                $scope.showAllLevelConfig.showCheckBox=true;
            }
            if (angular.isUndefined($scope.showAllLevelConfig.allLevelValue)){
                $scope.showAllLevelConfig.allLevelValue=false;
            }
            $scope.searchParameters.showAllLevel = $scope.showAllLevelConfig.allLevelValue;
            $scope.searchParameters.useCodes=$scope.useCodes;
            if ($scope.useCodes){
                $scope.searchParameters.useCodesHierarchyScope='CHILDREN';
            }
            $scope.searchParameters.excludedCodes=$scope.excludedCodes;
            $scope.oktmoSearchServiceLocal = $scope.oktmoSearchService?$scope.oktmoSearchService:$ClassifierService;

            if ($scope.useCodes){
                $scope.defaultSearchParameters.useCodes=$scope.useCodes;
                $scope.defaultSearchParameters.useCodesHierarchyScope='CHILDREN';
            }
            if ($scope.excludedCodes){
                $scope.defaultSearchParameters.excludedCodes=$scope.excludedCodes;
            }



            $scope.oktmoSearchServiceLocal.findOktmoRegionRootsWithParams({useCodes:$scope.useCodes,excludedCodes:$scope.excludedCodes},function(regions) {
                $scope.regions = regions;
                if ($scope.regionGuid) {
                    var found = _.find(regions, function(region) {
                        return region.regionGuid == $scope.regionGuid;
                    });
                    if (found) {
                        $scope.defaultSearchParameters.region = {
                            code: found.oktmoCode
                        };
                        $scope.searchParameters.region = {
                            code: found.oktmoCode
                        };
                    }
                    if($scope.excludedGuids){
                        $scope.searchParameters.excludedGuids = $scope.excludedGuids;
                    }
                    if($scope.excludedCodes){
                        $scope.searchParameters.excludedCodes = $scope.excludedCodes;
                    }
                    if($scope.useCodes){
                        $scope.searchParameters.useCodes = $scope.useCodes;
                    }
                }
            }, $scope.errorCallback);

            $scope.generateSelect2Options = function(parent, level) {
                return {
                    minimumResultsForSearch: 0,
                    minimumInputLength: 3,
                    allowClear: true,
                    initSelection: angular.noop,
                    id: function(object) {
                        return object.guid;
                    },
                    formatResult: function(obj) {
                        return obj.name;
                    },
                    formatSelection: function(obj) {
                        return obj.name;
                    },
                    ajax: {
                        quietMillis: 300,
                        transport: function (query) {
                            return $scope.oktmoSearchServiceLocal.findOktmoExtendedSearch(query.data, angular.noop,
                                    $scope.errorCallback).then(query.success);
                        },
                        data: function (term, page) {
                            var params = {
                                searchString: term,
                                page: page,
                                itemsPerPage: 10,
                                level: level,
                                useCodes:$scope.useCodes,
                                excludedCodes:$scope.excludedCodes,
                                useCodesHierarchyScope:$scope.useCodes?'NONE':null
                            };

                            if ($scope.searchParameters[parent]) {
                                params.parentCode = $scope.searchParameters[parent].code;
                            } else if (parent == 'level5Guid') {
                                //см. HCS-9338
                                params.parentCode = $scope.searchParameters['level3Guid'].code;
                            }

                            return params;
                        },
                        results: function (data, page, query) {
                            return {
                                more: !_.isEmpty(data.oktmoList) && data.oktmoList.length >= 10,
                                results: data.oktmoList
                            };
                        }
                    }
                };
            };

            $scope.level3Select2Options = angular.copy($scope.generateSelect2Options('region', 3));
            $scope.level5Select2Options = angular.copy($scope.generateSelect2Options('level3Guid', 5));
            $scope.level7Select2Options = angular.copy($scope.generateSelect2Options('level5Guid', 7));


            $scope.name = function (object, reverse) {
                return $filter('geographicalObjectName')(object, reverse);
            };

            $scope.errorCallback = function() {
                console.log('Произошла ошибка во время поиска ОКТМО');
            };

            $scope.level3Change = function(newValue, oldValue) {
                if (!newValue) {
                    $scope.searchParameters.level5Guid = null;
                    $scope.searchParameters.level7Guid = null;
                }
            };

            $scope.level5Change = function(newValue, oldValue) {
                if (!newValue) {
                    $scope.searchParameters.level7Guid = null;
                }
            };
            $scope.regionChange = function(newValue, oldValue) {
                $scope.searchParameters.level3Guid = null;
                $scope.searchParameters.level5Guid = null;
                $scope.searchParameters.level7Guid = null;
            };

        }])
;
