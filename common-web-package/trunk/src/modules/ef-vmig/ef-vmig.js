/**
 * Группа полей для выбора месяца и года (ЭФ_ВМИГ)
 */


angular.module('common.ef-vmig', [])

        .directive('efVmigForm', function () {
            return {
                restrict: 'AEC',
                scope: {
                    month : '=',
                    year : '=',
                    years : '=',
                    monthRequired : '=?',
                    yearRequired : '=?'
                },
                controller: function ($scope, _) {

                    $scope.isNotExcepted = function(value) {
                        value = parseInt(value, 10);
                        var year = _.find($scope.years, function(year) {
                            return year.val == $scope.year;
                        });

                        if (year && year.hasOwnProperty('exceptedMonths')) {
                            return _.indexOf(year.exceptedMonths, value) == -1;
                        }
                        return true;
                    };

                    $scope.checkExcepted = function() {
                        if ($scope.month && !$scope.isNotExcepted($scope.month)) {
                            $scope.month = null;
                        }
                    };

                },
                templateUrl: 'ef-vmig/ef-vmig-form.tpl.html'
            };
        })
;