/**
 * Модальное окно для выбора комнаты (ЭФ_ВКОМ)
 */
angular
    .module('common.ef-vkom', [
        'lodash',
        'common.ef-bp',
        'common.ef-pa',
        'hcs-table',
        'common.ef-kmn',
        'resources.houseCommonResource',
        'common.auth',
        'common.filter',
        'common.dialogs'
    ])
    .service('roomChooserDialog', function (_, $modal, $filter, Auth, commonDialogs, RoomSearchCommonResource, AppartmentSearchCommonResource) {
        var self = this,
            settings = {},
            modalDefaults = {
                backdrop: true,
                keyboard: true,
                modalFade: true,
                size: 'lg',
                templateUrl: 'ef-vkom/ef-vkom.tpl.html'
            },
            modalOptions = {};

        self.hasCheckbox = function (hasCheckbox) {
            settings.hasCheckbox = hasCheckbox;
            return self;
        };

        self.hasRadio = function (hasRadio) {
            settings.hasRadio = hasRadio;
            return self;
        };
        self.excludedGuids = function (guids) {
            settings.excludedRoomGuids = guids;
            return self;
        };

        self.excludeDwellingHouseRooms = function (exclude) {
            settings.excludeDwellingHouseRooms = !!exclude;
            return self;
        };

        self.show = function () {
            if (!modalDefaults.controller) {
                modalDefaults.controller = function ($scope, $modalInstance) {
                    $scope.defaultSearchParameters = {
                        addressFias: {address: {}}
                    };
                    $scope.searchParameters = angular.copy($scope.defaultSearchParameters);
                    $scope.items = [];
                    $scope.radioItem = null;
                    $scope.checkedItems = {};
                    $scope.hasCheckbox = settings.hasCheckbox;
                    $scope.hasRadio = settings.hasRadio;
                    if (typeof $scope.hasRadio == 'undefined'){
                        $scope.hasCheckbox = true;
                    }
                    $scope.searchService = function () {
                        $scope.items = [];
                        $scope.checkedItems = {};
                        $scope.radioItem = null;
                        $scope.result = null;

                        return $scope.searchResultTable.state.refresh();
                    };

                    $scope.modalOptions = modalOptions;
                    $scope.modalOptions.ok = function () {
                        if ($scope.hasCheckbox) {
                            $modalInstance.close($scope.items);
                        } else if ($scope.hasRadio) {
                            $modalInstance.close($scope.result);
                        }
                        settings = {};
                    };
                    $scope.modalOptions.close = function () {
                        $modalInstance.dismiss('cancel');
                        settings = {};
                    };

                    $scope.searchResults = [];
                    $scope.searchResultTable = {
                        state: {},
                        config: {
                            dataSource: function (query) {
                                var params = angular.copy($scope.searchParameters);
                                params.addressFias = _.omit(params.addressFias, ['address', 'zipCode']);

                                if (Auth && Auth.user && Auth.user.orgGuid) {
                                    params.organizationGuid = Auth.user.orgGuid;
                                }

                                if (settings.excludedRoomGuids && settings.excludedRoomGuids.length > 0) {
                                    params.excludedRoomGuids = settings.excludedRoomGuids;
                                }

                                if (settings.excludeDwellingHouseRooms) {
                                    params.excludeDwellingHouseRooms = settings.excludeDwellingHouseRooms;
                                }

                                angular.forEach(params, function (value, property) {
                                    if (!value) {
                                        delete params[property];
                                    }
                                });
                                return RoomSearchCommonResource.create(
                                    params,
                                    angular.noop,
                                    angular.noop,
                                    query.queryParams
                                ).then(
                                    function (response) {
                                        var results = response['items'];
                                        if (response['items'].length == 1 && params.roomNumber) {
                                            var room = response['items'][0];
                                            if (!$scope.checkedItems[room.guid]) {
                                                $scope.chooseRoom(room);
                                            }
                                        } else {
                                            var part1 = [], part2 = [];
                                            angular.forEach(response['items'], function (item) {
                                                if ($scope.checkedItems[item.guid]) {
                                                    part1.push(item);
                                                } else {
                                                    part2.push(item);
                                                }
                                            });

                                            results = part1.concat(part2);
                                        }

                                        response['items'] = $scope.searchResults = results;

                                        return response;
                                    },
                                    function () {
                                        commonDialogs.error('Во время работы системы произошла ошибка');
                                        return {
                                            total: 0,
                                            items: []
                                        };
                                    }
                                );
                            },
                            sortable: false,
                            noRefreshOnInit: false,
                            modal: true
                        }
                    };

                    $scope.apartmentGuidSelect2Options = {
                        minimumInputLength: 1,
                        initSelection: angular.noop,
                        ajax: {
                            quietMillis: 300,
                            transport: function (query) {
                                return AppartmentSearchCommonResource
                                    .create(query.data.data, angular.noop, angular.noop, query.data.pathParams)
                                    .then(function (data) {
                                        query.success(data);
                                    });
                            },
                            data: function (term, page) {
                                return {
                                    data: {
                                        flatNumber: term + '%',
                                        addressFias: _.omit($scope.searchParameters.addressFias, ['address', 'zipCode'])
                                    },
                                    pathParams: {
                                        page: page,
                                        itemsPerPage: 20
                                    }
                                };
                            },
                            results: function (data) {
                                return {
                                    more: _.isObject(data) && data.total >= 20,
                                    results: _.map(data.items, function (f) {
                                        return {
                                            id: f.guid,
                                            text: f.flatNumber
                                        };
                                    })
                                };

                            }
                        }
                    };

                    // FIXME select2 или кто-то еще обрабатывает изменение два раза
                    var watcher = function (newVal, oldVal) {
                        if (_.isObject(newVal)) {
                            $scope.$eval(this.exp + '=val', {val: oldVal});
                            return false;
                        }
                        return true;
                    };
                    $scope.$watch('searchParameters.apartmentGuid', watcher);

                    $scope.chooseRoom = function (room) {
                        $scope.radioItem = room.guid;
                        $scope.result = room;
                        if ($scope.checkedItems[room.guid]) {
                            $scope.items = $scope.items.filter(function (item) {
                                return !angular.equals(room, item);
                            });
                            delete $scope.checkedItems[room.guid];
                        } else {
                            $scope.items.push(room);
                            $scope.checkedItems[room.guid] = true;
                        }
                    };

                    $scope.$watch(
                        'searchParameters.addressFias.houseNumber+searchParameters.addressFias.buildingNumber+searchParameters.addressFias.structNumber',
                        function () {
                            $scope.searchParameters.apartmentGuid = null;
                            $scope.searchParameters.roomNumber = null;
                        }
                    );

                    $scope.disabled = function () {
                        return ($scope.hasRadio && !$scope.result) || ($scope.hasCheckbox && $scope.items.length === 0);
                    };
                };
            }
            return $modal.open(modalDefaults).result;
        };
    });