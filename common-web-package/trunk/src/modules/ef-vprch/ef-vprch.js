// 2.20	Экранная форма для выбора из перечня значений (ЭФ_ВПРЧ)
(function () {
    var efVprch = angular.module('common.ef-vprch', []);

    efVprch.directive('efVprch', function () {
        return {
            restrict: 'E',
            transclude: true,
            templateUrl: 'ef-vprch/ef-vprch.tpl.html'
        };
    });


    var EfVprchController = function ($scope, $modalInstance, config) {
        this.dialogTitle = config.dialogTitle;

        var selectedItems = [];
        $scope.selectedItemsArray = selectedItems;
        $scope.externalConfig = config.externalConfig || {};

        this.isDisabledSelectBtn = function () {
            return selectedItems.length === 0;
        };

        this.clear = function () {
            _.clearArray(selectedItems);
            $scope.$broadcast('clearSelectedItems');
        };

        this.choose = function () {
            $modalInstance.close({
                selected: selectedItems
            });
        };

        this.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        this.close = function () {
            $modalInstance.dismiss('close');
        };
    };
    EfVprchController.$inject = ['$scope', '$modalInstance', 'config'];
    efVprch.controller('EfVprchController', EfVprchController);

    efVprch.factory('EfVprchService', function ($modal) {

        return {

            /**
             * Параметры config:
             *  - contentDirective: имя директивы (ненормализованное)
             *  - dialogTitle: заголовок для диалогового окна
             *  - useContainerWidth: размер модального окна будет равен
             *                       основному контейнеру сайта
             *  - externalConfig: объект, может содержать любые поля
             *                    будет прокинут в scope директивы-контента
             *                    служит для передачи параметров в контент
             */
            openChooser: function (config) {
                var tpl = '<ef-vprch>' +
                            '<' + config.contentDirective + '>' +
                            '</' + config.contentDirective + '>' +
                          '</ef-vprch>';
                var windowClass = '';
                if (config.useContainerWidth) {
                    windowClass = 'expanded-width-modal';
                }

                return $modal.open({
                    template: tpl,
                    controller: 'EfVprchController as efVprchCtrl',
                    windowClass: windowClass,
                    resolve: {
                        config: function () {
                            return config;
                        }
                    },
                    size: 'lg'
                }).result;
            }
        };
    });
})();