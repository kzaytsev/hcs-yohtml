describe('utils.js test', function () {
    beforeEach(module('common.utils'));

    describe('addressSorter test', function () {
        describe('nullHighAddressIdComparator test', function () {
            var nullHighAddressIdComparator;

            beforeEach(inject(function (addressSorter) {
                nullHighAddressIdComparator = addressSorter.nullHighAddressIdComparator;
            }));

            it('must sort in alphabetical order', inject(function () {
                var before = 'a';
                var after = 'b';

                var actualComparisonResult = nullHighAddressIdComparator(before, after);

                expect(actualComparisonResult).toBeLessThan(0);
            }));

            it('must sort in alphabetical order (2)', inject(function () {
                var before = 'a';
                var after = 'b';

                var actualComparisonResult = nullHighAddressIdComparator(after, before);

                expect(actualComparisonResult).toBeGreaterThan(0);
            }));

            it('must be equal for same inputs', inject(function () {
                var id = 'a';
                var anotherId = 'a';

                var actualComparisonResult = nullHighAddressIdComparator(id, anotherId);

                expect(actualComparisonResult).toBe(0);
            }));

            it('must treat null bigger than non-null value', inject(function () {
                var before = 'a';
                var after = null;

                var actualComparisonResult = nullHighAddressIdComparator(before, after);

                expect(actualComparisonResult).toBeLessThan(0);
            }));

            it('must treat null bigger than non-null value (2)', inject(function () {
                var before = null;
                var after = 'b';

                var actualComparisonResult = nullHighAddressIdComparator(before, after);

                expect(actualComparisonResult).toBeGreaterThan(0);
            }));

            it('must treat nulls as equal values', inject(function () {
                var before = null;
                var after = null;

                var actualComparisonResult = nullHighAddressIdComparator(before, after);

                expect(actualComparisonResult).toBe(0);
            }));

            it('must treat undefined bigger than non-undefined value', inject(function () {
                var before = 'a';
                var after = void 0;

                var actualComparisonResult = nullHighAddressIdComparator(before, after);

                expect(actualComparisonResult).toBeLessThan(0);
            }));

            it('must treat undefined bigger than non-undefined value (2)', inject(function () {
                var before = void 0;
                var after = 'b';

                var actualComparisonResult = nullHighAddressIdComparator(before, after);

                expect(actualComparisonResult).toBeGreaterThan(0);
            }));

            it('must treat undefineds as equal values', inject(function () {
                var before = void 0;
                var after = void 0;

                var actualComparisonResult = nullHighAddressIdComparator(before, after);

                expect(actualComparisonResult).toBe(0);
            }));
        });

        describe('addressComparator test', function () {
            var addressComparator;

            beforeEach(inject(function (addressSorter) {
                addressComparator = addressSorter.addressComparator;
            }));

            it('must sort by structNumber', inject(function () {
                var before = {"house":{"structNumber":'1'}};
                var after = {"house":{"structNumber":null}};

                var actualComparisonResult = addressComparator(before, after);

                expect(actualComparisonResult).toBeLessThan(0);
            }));

            it('must sort by buildingNumber before structNumber', inject(function () {
                var before = {'house':{'buildingNumber': '1', 'structNumber':null}};
                var after = {'house':{'buildingNumber': null, 'structNumber':'1'}};

                var actualComparisonResult = addressComparator(before, after);

                expect(actualComparisonResult).toBeLessThan(0);
            }));

            it('must sort by houseNumber before buildingNumber', inject(function () {
                var before = {'house':{'houseNumber': '1', 'buildingNumber': null, 'structNumber':'1'}};
                var after = {'house':{'houseNumber': null, 'buildingNumber': '1', 'structNumber':null}};

                var actualComparisonResult = addressComparator(before, after);

                expect(actualComparisonResult).toBeLessThan(0);
            }));

            it('must sort by street before houseNumber', inject(function () {
                var before = {'street':{'guid':'bb13c63d-259d-4f1a-91f8-e78efb35bc27'}, 'house':{'houseNumber': null, 'buildingNumber': '1', 'structNumber':null}};
                var after = {'street':{'guid':null}, 'house':{'houseNumber': '1', 'buildingNumber': null, 'structNumber':'1'}};

                var actualComparisonResult = addressComparator(before, after);

                expect(actualComparisonResult).toBeLessThan(0);
            }));

            it('must sort by settlement before street', inject(function () {
                var before = {'settlement': {'guid': '123'}, 'street':{'guid':null}, 'house':{'houseNumber': '1', 'buildingNumber': null, 'structNumber':'1'}};
                var after = {'settlement': {'guid': null}, 'street':{'guid':'bb13c63d-259d-4f1a-91f8-e78efb35bc27'}, 'house':{'houseNumber': null, 'buildingNumber': '1', 'structNumber':null}};

                var actualComparisonResult = addressComparator(before, after);

                expect(actualComparisonResult).toBeLessThan(0);
            }));

            it('must sort by city before settlement', inject(function () {
                var before = {'city': {guid: '234'}, 'settlement': {'guid': null}, 'street':{'guid':'bb13c63d-259d-4f1a-91f8-e78efb35bc27'}, 'house':{'houseNumber': null, 'buildingNumber': '1', 'structNumber':null}};
                var after = {'city': {guid: null}, 'settlement': {'guid': '123'}, 'street':{'guid':null}, 'house':{'houseNumber': '1', 'buildingNumber': null, 'structNumber':'1'}};

                var actualComparisonResult = addressComparator(before, after);

                expect(actualComparisonResult).toBeLessThan(0);
            }));

            it('must sort by area before city', inject(function () {
                var before = {'area': {guid: '345'}, 'city': {guid: null}, 'settlement': {'guid': '123'}, 'street':{'guid':null}, 'house':{'houseNumber': '1', 'buildingNumber': null, 'structNumber':'1'}};
                var after = {'area': {guid: null}, 'city': {guid: '234'}, 'settlement': {'guid': null}, 'street':{'guid':'bb13c63d-259d-4f1a-91f8-e78efb35bc27'}, 'house':{'houseNumber': null, 'buildingNumber': '1', 'structNumber':null}};

                var actualComparisonResult = addressComparator(before, after);

                expect(actualComparisonResult).toBeLessThan(0);
            }));

            it('must sort by region before area', inject(function () {
                var before = {'region': {guid: '456'}, 'area': {guid: null}, 'city': {guid: '234'}, 'settlement': {'guid': null}, 'street':{'guid':'bb13c63d-259d-4f1a-91f8-e78efb35bc27'}, 'house':{'houseNumber': null, 'buildingNumber': '1', 'structNumber':null}};
                var after = {'region': {guid: null}, 'area': {guid: '345'}, 'city': {guid: null}, 'settlement': {'guid': '123'}, 'street':{'guid':null}, 'house':{'houseNumber': '1', 'buildingNumber': null, 'structNumber':'1'}};

                var actualComparisonResult = addressComparator(before, after);

                expect(actualComparisonResult).toBeLessThan(0);
            }));

            it('must be equal (all nulls)', inject(function () {
                var before = {'region': {guid: null}, 'area': {guid: null}, 'city': {guid: null}, 'settlement': {'guid': null}, 'street':{'guid':null}, 'house':{'houseNumber': null, 'buildingNumber': null, 'structNumber':null}};
                var after = {'region': {guid: null}, 'area': {guid: null}, 'city': {guid: null}, 'settlement': {'guid': null}, 'street':{'guid':null}, 'house':{'houseNumber': null, 'buildingNumber': null, 'structNumber':null}};

                var actualComparisonResult = addressComparator(before, after);

                expect(actualComparisonResult).toBe(0);
            }));

            it('must be equal (all non-nulls)', inject(function () {
                var before = {'region': {guid: '1'}, 'area': {guid: '2'}, 'city': {guid: '3'}, 'settlement': {'guid': '4'}, 'street':{'guid':'5'}, 'house':{'houseNumber': '6', 'buildingNumber': '7', 'structNumber': '8'}};
                var after = {'region': {guid: '1'}, 'area': {guid: '2'}, 'city': {guid: '3'}, 'settlement': {'guid': '4'}, 'street':{'guid':'5'}, 'house':{'houseNumber': '6', 'buildingNumber': '7', 'structNumber': '8'}};

                var actualComparisonResult = addressComparator(before, after);

                expect(actualComparisonResult).toBe(0);
            }));
        });
    });
});