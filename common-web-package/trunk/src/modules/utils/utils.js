angular.module('common.utils', [])

    .directive("dynamicName", function ($compile) {
        return {
            restrict: "A",
            terminal: true,
            priority: 1000,
            link: function (scope, element, attrs) {
                element.attr('name', scope.$eval(attrs.dynamicName));
                element.removeAttr("dynamic-name");
                $compile(element)(scope);
            }
        };
    })

    .directive('capitalizeFirst', function(uppercaseFilter, $parse) {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, modelCtrl) {
                var capitalize = function(inputValue) {

                    if(inputValue != null){
                        var capitalized = inputValue.charAt(0).toUpperCase() +
                            inputValue.substring(1);

                        if(capitalized !== inputValue) {
                            modelCtrl.$setViewValue(capitalized);
                            modelCtrl.$render();
                        }
                        return capitalized;
                    }
                };

                var model = $parse(attrs.ngModel);
                modelCtrl.$parsers.push(capitalize);
                capitalize(model(scope));

            }
        };
    })

    .directive('ngMultiTransclude', function () {
        return {
            controller: function ($scope, $element, $attrs, $transclude) {
                if (!$transclude) {
                    throw {
                        name: 'DirectiveError',
                        message: 'ng-multi-transclude found without parent requesting transclusion'
                    };
                }
                this.$transclude = $transclude;
            },

            link: function ($scope, $element, $attrs, controller) {
                var selector = '[name=' + $attrs.ngMultiTransclude + ']';
                var attach = function (clone) {
                    var $part = clone.find(selector).addBack(selector);
                    $element.html('');
                    $element.append($part);
                };

                if (controller.$transclude.$$element) {
                    attach(controller.$transclude.$$element);
                }
                else {
                    controller.$transclude(function (clone) {
                        controller.$transclude.$$element = clone;
                        attach(clone);
                    });
                }
            }
        };
    })
    .factory('addressSorter', function () {
        function safeGet(object, field) {
            return object ? object[field] : null;
        }

        function nullHighComparator(value, anotherValue) {
            if (!value && anotherValue) {
                return 1;
            } else if (value && !anotherValue) {
                return -1;
            }
            return 0;
        }

        function addressIdComparator(string, anotherString) {
            if (string > anotherString) {
                return 1;
            } else if (string < anotherString) {
                return -1;
            }
            return 0;
        }

        function nullHighAddressIdComparator(string, anotherString) {
            var result = nullHighComparator(string, anotherString);
            if (result === 0) {
                result = addressIdComparator(string, anotherString);
            }
            return result;
        }

        function addressComparator(address, anotherAddress) {
            var result;
            result = nullHighAddressIdComparator(safeGet(address.region, 'guid'), safeGet(anotherAddress.region, 'guid'));
            if (result === 0) {
                result = nullHighAddressIdComparator(safeGet(address.area, 'guid'), safeGet(anotherAddress.area, 'guid'));
            }
            if (result === 0) {
                result = nullHighAddressIdComparator(safeGet(address.city, 'guid'), safeGet(anotherAddress.city, 'guid'));
            }
            if (result === 0) {
                result = nullHighAddressIdComparator(safeGet(address.settlement, 'guid'), safeGet(anotherAddress.settlement, 'guid'));
            }
            if (result === 0) {
                result = nullHighAddressIdComparator(safeGet(address.street, 'guid'), safeGet(anotherAddress.street, 'guid'));
            }
            if (result === 0) {
                result = nullHighAddressIdComparator(address.house.houseNumber, anotherAddress.house.houseNumber);
            }
            if (result === 0) {
                result = nullHighAddressIdComparator(address.house.buildingNumber, anotherAddress.house.buildingNumber);
            }
            if (result === 0) {
                result = nullHighAddressIdComparator(address.house.structNumber, anotherAddress.house.structNumber);
            }
            return result;
        }

        return {
            addressComparator: addressComparator,
            nullHighAddressIdComparator: nullHighAddressIdComparator
        };
    })
    .factory("dateParserHelpers", [ function () {
        var cache = {};
        return {
            getInteger: function (string, startPoint, minLength, maxLength) {
                var val = string.substring(startPoint);
                var matcher = cache[minLength + "_" + maxLength];
                if (!matcher) {
                    matcher = new RegExp("^(\\d{" + minLength + "," + maxLength + "})");
                    cache[minLength + "_" + maxLength] = matcher;
                }
                var match = matcher.exec(val);
                if (match) {
                    return match[1];
                }
                return null;
            }
        };
    } ])
    .factory("$dateParser", [ "$locale", "dateParserHelpers", function ($locale, dateParserHelpers) {
        var datetimeFormats = $locale.DATETIME_FORMATS;
        var monthNames = datetimeFormats.MONTH.concat(datetimeFormats.SHORTMONTH);
        var dayNames = datetimeFormats.DAY.concat(datetimeFormats.SHORTDAY);
        return function (val, format) {
            if (angular.isDate(val)) {
                return val;
            }
            try {
                val = val + "";
                format = format + "";
                if (!format.length) {
                    return new Date(val);
                }
                if (datetimeFormats[format]) {
                    format = datetimeFormats[format];
                }
                var now = new Date(), i_val = 0, i_format = 0, format_token = "", year = now.getFullYear(), month = now.getMonth() + 1, /*date = now.getDate()*/ date=1, hh = 0, mm = 0, ss = 0, sss = 0, ampm = "am", z = 0, parsedZ = false;
                while (i_format < format.length) {
                    format_token = format.charAt(i_format);
                    var token = "";
                    if (format.charAt(i_format) == "'") {
                        var _i_format = i_format;
                        while (format.charAt(++i_format) != "'" && i_format < format.length) {
                            token += format.charAt(i_format);
                        }
                        if (val.substring(i_val, i_val + token.length) != token) {
                            throw "Pattern value mismatch";
                        }
                        i_val += token.length;
                        i_format++;
                        continue;
                    }
                    while (format.charAt(i_format) == format_token && i_format < format.length) {
                        token += format.charAt(i_format++);
                    }
                    if (token == "yyyy" || token == "yy" || token == "y") {
                        var minLength, maxLength;
                        if (token == "yyyy") {
                            minLength = 4;
                            maxLength = 4;
                        }
                        if (token == "yy") {
                            minLength = 2;
                            maxLength = 2;
                        }
                        if (token == "y") {
                            minLength = 2;
                            maxLength = 4;
                        }
                        year = dateParserHelpers.getInteger(val, i_val, minLength, maxLength);
                        if (year === null) {
                            throw "Invalid year";
                        }
                        i_val += year.length;
                        if (year.length == 2) {
                            if (year > 70) {
                                year = 1900 + (year - 0);
                            } else {
                                year = 2e3 + (year - 0);
                            }
                        }
                    } else if (token === "MMMM" || token == "MMM") {
                        month = 0;
                        for (var i = 0; i < monthNames.length; i++) {
                            var month_name = monthNames[i];
                            if (val.substring(i_val, i_val + month_name.length).toLowerCase() == month_name.toLowerCase()) {
                                month = i + 1;
                                if (month > 12) {
                                    month -= 12;
                                }
                                i_val += month_name.length;
                                break;
                            }
                        }
                        if (month < 1 || month > 12) {
                            throw "Invalid month";
                        }
                    } else if (token == "EEEE" || token == "EEE") {
                        for (var j = 0; j < dayNames.length; j++) {
                            var day_name = dayNames[j];
                            if (val.substring(i_val, i_val + day_name.length).toLowerCase() == day_name.toLowerCase()) {
                                i_val += day_name.length;
                                break;
                            }
                        }
                    } else if (token == "MM" || token == "M") {
                        month = dateParserHelpers.getInteger(val, i_val, token.length, 2);
                        if (month === null || month < 1 || month > 12) {
                            throw "Invalid month";
                        }
                        i_val += month.length;
                    } else if (token == "dd" || token == "d") {
                        date = dateParserHelpers.getInteger(val, i_val, token.length, 2);
                        if (date === null || date < 1 || date > 31) {
                            throw "Invalid date";
                        }
                        i_val += date.length;
                    } else if (token == "HH" || token == "H") {
                        hh = dateParserHelpers.getInteger(val, i_val, token.length, 2);
                        if (hh === null || hh < 0 || hh > 23) {
                            throw "Invalid hours";
                        }
                        i_val += hh.length;
                    } else if (token == "hh" || token == "h") {
                        hh = dateParserHelpers.getInteger(val, i_val, token.length, 2);
                        if (hh === null || hh < 1 || hh > 12) {
                            throw "Invalid hours";
                        }
                        i_val += hh.length;
                    } else if (token == "mm" || token == "m") {
                        mm = dateParserHelpers.getInteger(val, i_val, token.length, 2);
                        if (mm === null || mm < 0 || mm > 59) {
                            throw "Invalid minutes";
                        }
                        i_val += mm.length;
                    } else if (token == "ss" || token == "s") {
                        ss = dateParserHelpers.getInteger(val, i_val, token.length, 2);
                        if (ss === null || ss < 0 || ss > 59) {
                            throw "Invalid seconds";
                        }
                        i_val += ss.length;
                    } else if (token === "sss") {
                        sss = dateParserHelpers.getInteger(val, i_val, 3, 3);
                        if (sss === null || sss < 0 || sss > 999) {
                            throw "Invalid milliseconds";
                        }
                        i_val += 3;
                    } else if (token == "a") {
                        if (val.substring(i_val, i_val + 2).toLowerCase() == "am") {
                            ampm = "AM";
                        } else if (val.substring(i_val, i_val + 2).toLowerCase() == "pm") {
                            ampm = "PM";
                        } else {
                            throw "Invalid AM/PM";
                        }
                        i_val += 2;
                    } else if (token == "Z") {
                        parsedZ = true;
                        if (val[i_val] === "Z") {
                            z = 0;
                            i_val += 1;
                        } else {
                            var txStr;
                            if (val[i_val + 3] === ":") {
                                tzStr = val.substring(i_val, i_val + 6);
                                z = parseInt(tzStr.substr(0, 3), 10) * 60 + parseInt(tzStr.substr(4, 2), 10);
                                i_val += 6;
                            } else {
                                tzStr = val.substring(i_val, i_val + 5);
                                z = parseInt(tzStr.substr(0, 3), 10) * 60 + parseInt(tzStr.substr(3, 2), 10);
                                i_val += 5;
                            }
                        }
                        if (z > 720 || z < -720) {
                            throw "Invalid timezone";
                        }
                    } else {
                        if (val.substring(i_val, i_val + token.length) != token) {
                            throw "Pattern value mismatch";
                        } else {
                            i_val += token.length;
                        }
                    }
                }
                if (i_val != val.length) {
                    throw "Pattern value mismatch";
                }
                year = parseInt(year, 10);
                month = parseInt(month, 10);
                date = parseInt(date, 10);
                hh = parseInt(hh, 10);
                mm = parseInt(mm, 10);
                ss = parseInt(ss, 10);
                sss = parseInt(sss, 10);
                if (month == 2) {
                    if (year % 4 === 0 && year % 100 !== 0 || year % 400 === 0) {
                        if (date > 29) {
                            throw "Invalid date 29";
                        }
                    } else {
                        if (date > 28) {
                            throw "Invalid date 28";
                        }
                    }
                }
                if (month == 4 || month == 6 || month == 9 || month == 11) {
                    if (date > 30) {
                        throw "Invalid date";
                    }
                }
                if (hh < 12 && ampm == "PM") {
                    hh += 12;
                } else if (hh > 11 && ampm == "AM") {
                    hh -= 12;
                }
                var localDate = new Date(year, month - 1, date, hh, mm, ss, sss);
                if (parsedZ) {
                    return new Date(localDate.getTime() - (z + localDate.getTimezoneOffset()) * 6e4);
                }
                return localDate;
            } catch (e) {
                return undefined;
            }
        };
    } ])
    .directive("dateParser", [ "dateFilter", "$dateParser", function (dateFilter, $dateParser) {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                var dateFormat;
                attrs.$observe("dateParser", function (value) {
                    dateFormat = value;
                    ngModel.$render();
                });
                ngModel.$parsers.unshift(function (viewValue) {
                    var date = $dateParser(viewValue, dateFormat);
                    if (isNaN(date)) {
                        ngModel.$setValidity("date", false);
                    } else {
                        ngModel.$setValidity("date", true);
                    }
                    return date;
                });
                ngModel.$render = function () {
                    element.val(ngModel.$modelValue ? dateFilter(ngModel.$modelValue, dateFormat) : undefined);
                    scope.ngModel = ngModel.$modelValue;
                };
                ngModel.$formatters.push(function (modelValue) {
                    return modelValue ? dateFilter(modelValue, dateFormat) : "";
                });
            }
        };
    } ])
    // дополняет дробную часть числа нулями
    .filter('zerosCompleteFilter', function () {
        return function(value, zerosNumber){

            if (zerosNumber !== undefined && zerosNumber == parseInt(zerosNumber, 10) && value != null && value.toString().length > 0) {
                var currentFractionalPart = ('' + value).split('.')[1] || '';
                if (currentFractionalPart.length < zerosNumber) {
                    var integerPart = ('' + value).split('.')[0];
                    var missingZerosNumber = zerosNumber - currentFractionalPart.length;
                    var missingZeros = Array.apply(null, new Array(missingZerosNumber + 1)).join('0');
                    return integerPart + '.' + currentFractionalPart + missingZeros;
                }
            }
            return value;

        };
    })
    // дополняет дробную часть числа нулями по блюру
    .directive('zerosAutocomplete', function ($filter) {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {

                elm.on('blur', function () {
                    ctrl.$setViewValue($filter('zerosCompleteFilter')(ctrl.$viewValue, attrs.zerosAutocomplete));
                    ctrl.$render();
                });

            }
        };
    })
    //позволяет блокировать ввод символов которые не соответствуют регулярному выражению
    .directive('lockInput', function(uppercaseFilter, $parse) {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ctrl) {
                if (!ctrl) {return;}

                var pattern = attr.lockInput;
                // get the pattern between the slashes and any modifiers
                var r = new RegExp("^/(.*)/(.*)$");
                var matches = r.exec(pattern);
                var regex;
                if (matches) {
                    regex = new RegExp('^' + matches[1] + '$', matches[2]);
                }
                var lastText = '';
                var reverted = false;

                function fromUser() {
                    var text = ctrl.$viewValue;
                    var m = regex.exec(text);
                    if (m) {
                        // join matches together into a single string
                        lastText = m[0];
                        if (lastText != text) {
                            // the original text contained some invalid characters
                            ctrl.$setViewValue(lastText);
                            ctrl.$render();
                        }
                    }
                    else {
                        // nothing in the text matched the regular expression...
                        // revert to the last good value
                        if (text != lastText) {
                            ctrl.$setViewValue(lastText);
                            ctrl.$render();
                        }
                    }
                    return lastText;
                }

                ctrl.$parsers.unshift(fromUser);
            }
        };
    })
    // @see http://underscorejs.ru/#throttle
    //предотвращает повторный вызов по клику асинхронной функции, пока не завершился предыдущий
    .directive("deferredActionClick", ["$parse", function ($parse) {
        return {
            restrict: "A",
            controller: ['$scope', function ($scope) {
                // указывает, работает ли сейчас функция, вызванная кликом
                this.isRunning = false;
            }],
            compile: function ($element, attr) {
                var funct = $parse(attr.deferredActionClick);
                return function (scope, element, attr, controller) {
                    element.on("click", function (event) {
                        //если функция уже запущена - не запускаем её ещё раз
                        if (!controller.isRunning) {
                            scope.$apply(function () {
                                // если вызываемая функция возвращает promice - ожидаем его завершения
                                var res = funct(scope, { $event: event });
                                if (res['finally'] !== undefined) {
                                    controller.isRunning = true;
                                    res['finally'](function () {
                                        controller.isRunning = false;
                                    });
                                }
                            });
                        }
                    });
                };
            }
        };
    }])

    //форматирует СНИЛС по маске XXX-XXX-XXX XX
    //если переданная строка не может быть приведена к заданному формату, возвращает ''
    .filter('snils', function(){
        return function(snils){
            var formattedSnils = '';
            if(snils && snils.length === 11){
                formattedSnils = snils.substring(0, 3) + '-' +
                    snils.substring(3, 6) + '-' +
                    snils.substring(6, 9) + ' ' +
                    snils.substring(9, 11);
            }
            return formattedSnils;
        };
    })

    .service('equalService', function() {
        var self = this;
        self.equals = function(o1, o2) {
            if (angular.equals(o1, o2) || (!o1 && !o2)) {return true;}

            var data = o1 ? o1 : o2;
            var master = o1 ? o2 : o1;

            if (_.isArray(data) && _.isArray(master) && data.length != master.length) {
                return false;
            }

            var equals = true;
            angular.forEach(data, function(value, prop) {
                var masterValue = master && master.hasOwnProperty(prop) ? master[prop] : null;
                if ((!value && !masterValue) || prop == '$$hashKey') {
                    return;
                }

                if (_.isObject(value) || _.isArray(value)) {
                    equals &= self.equals(value, masterValue);
                } else {
                    equals &= angular.equals(value, masterValue);
                }
            });

            return equals;
        };
    })
    /**
    * в запросах на создание и редактирование записей в иерархических справочниках
     * бывает необходимо знать guid родительской записи
     * но он может быть по разному назван в разных объектах-запросах
     * Это сервис должен возратить гуид родителя.
     * Сюда вынесен, т.к. используется в модулях nsi и org-nsi
    */
    .service('entityParentGuidService', function() {
        var self = this;
        self.getEntityRequestParentGuid = function (entity) {
            if (entity.parentGuid) {
                return entity.parentGuid;
            }
            if (entity.parentSupportServiceGuid) {
                return entity.parentSupportServiceGuid;
            }
            if (entity.parentFuelTypeGuid) {
                return entity.parentFuelTypeGuid;
            }
            if ( entity.additionalServiceTypeGuid) {
                return entity.additionalServiceTypeGuid;
            }
            if (entity.mainMunicipalServiceGuid){
                return entity.mainMunicipalServiceGuid;
            }
            if (entity.municipalService && entity.municipalService.guid){
                return entity.municipalService.guid;
            }
            return null;
        };
    })

    /**
     * Склонение слов в зависимости от числительных
     * Первый аргумент -  число, второй - массив с тремя строками.
     Массив легко создавать провяряя числа 1, 3 и 5.
     Пример: declOfNum(5, ['секунда', 'секунды', 'секунд'])
     * */
    .service('declensionService',function(){
        var self = this;
        self.declOfNum =  function (number, titles) {
            var cases = [2, 0, 1, 1, 1, 2];
            return titles[ (number%100 > 4 && number%100 < 20)? 2 : cases[(number%10 < 5)? number%10 : 5] ];
        };
    })
    .factory('previousStateService', function ($rootScope) {

        var prevStateData = {
            prevState: null,
            prevStateParams: null
        };

        $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
            if (from && to) {
                if (from.url != to.url) {
                    prevStateData.prevState = from;
                    prevStateData.prevStateParams = fromParams;
                }
            }
        });

        return {
            getPrevStateData: prevStateData
        };
    })

;
(function (angular, _) {
    "use strict";

    angular.module('common.utils')
        .directive('setterNgModel', function () {
            return {
                priority: 100,
                restrict: 'A',
                require: '?ngModel',
                link: SetterNgModelLink
            };
        });

    function SetterNgModelLink(scope, element, attrs, ngModel) {
        if (!ngModel) {
            console.warn('common.setter-ng-modal', "have empty ngModel");
            return;
        }

        var setterHandler = angular.identity;
        var lastValue = null;

        attrs.$observe('setterNgModel', setterNgModelObserve);

        ngModel.$parsers.push(setterParser);

        function setterNgModelObserve(setterNgModel) {
            setterHandler = scope[setterNgModel];
            if (!_.isFunction(setterHandler)) {
                console.warn('common.setter-ng-modal', setterNgModel, "is not found in the", scope);
                setterHandler = angular.identity;
            }
        }

        function setterParser(inputValue) {
            var value = setterHandler(inputValue, lastValue);
            if (lastValue !== inputValue) {
                element.val(ngModel.$viewValue = lastValue = value);
            }
            return value;
        }
    }

})(angular, _);