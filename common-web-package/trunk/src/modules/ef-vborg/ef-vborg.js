/**
 * Модальное окно выбора организации (ЭФ_ВБОРГ)
 */
angular
    .module('common.ef-vborg', [
        'common.dialogs',
        'common.ef-bp',
        'ng-simple-grid',
        'common.ef-bplc',
        'common.ef-doborg',
        'ru.lanit.hcs.organizationregistry.OrganizationRegistryService',
        'common.ef-org-constants'
    ])
    .service('efVborgDialog', [
        '$modal',
        'commonDialogs',
        'efBplcNsiCache',
        'personTypesCodes',
        'efDoborgDialog',
        'organizationTypesCodes',
        '$OrganizationRegistryService',
        'OrganizationAddressConverter',
        function ($modal,
                  commonDialogs,
                  efBplcNsiCache,
                  personTypesCodes,
                  efDoborgDialog,
                  organizationTypesCodes,
                  $OrganizationRegistryService,
                  OrganizationAddressConverter) {
            var modalDefaults = {
                backdrop: 'static',
                keyboard: true,
                modalFade: true,
                templateUrl: 'ef-vborg/ef-vborg.tpl.html',
                size: 'lg'
            };

            function getDialogController(initializationParameters) {
                return function ($scope, $modalInstance) {
                    $scope.hideAddButton = initializationParameters.hideAddButton;
                    $scope.defaultSearchParameters = {
                        personType : efBplcNsiCache.getPersonTypeByCode(personTypesCodes.LEGAL_ENTITY)
                    };
                    $scope.searchParameters = angular.copy($scope.defaultSearchParameters);

                    $scope.searchResults = [];
                    $scope.searchResultTable = {};
                    $scope.searchResultTable.config = {
                        dataSource: refresh,
                        sortable: false,
                        modal: true
                    };
                    $scope.searchResultTable.state = {};
                    $scope.searchService = function (searchParameters) {
                        $scope.searchParameters = searchParameters;
                        $scope.selectedOrganization = null;
                        $scope.selectedOrgGuid = null;

                        return $scope.searchResultTable.state.refresh();
                    };

                    $scope.errorCallback = function (error) {
                        commonDialogs.error('Во время работы системы произошла ошибка');
                    };

                    function refresh(pagingConfig) {

                        var params = angular.copy($scope.searchParameters);

                        angular.forEach(params, function (value, property) {
                            if (!value || angular.equals(value, {})) {
                                delete params[property];
                            }
                        });

                        switch(params.personType.code){
                            case personTypesCodes.LEGAL_ENTITY:
                                params.organizationType = organizationTypesCodes.LEGAL_ENTITY;
                                break;
                            case personTypesCodes.SOLE_TRADER:
                                params.organizationType = organizationTypesCodes.SOLE_TRADER;
                                params.chiefFirstName = $scope.searchParameters.firstName;
                                params.chiefLastName = $scope.searchParameters.lastName;
                                params.chiefMiddleName = $scope.searchParameters.middleName;

                                delete params.firstName;
                                delete params.lastName;
                                delete params.middleName;

                                break;
                        }

                        delete params.personType;

                        return $OrganizationRegistryService.findRegistryOrganizations(params,
                            undefined,
                            function(){
                                $scope.errorCallback();
                            },
                            {
                                page: pagingConfig.queryParams.page ?  pagingConfig.queryParams.page : 1,
                                itemsPerPage: pagingConfig.queryParams.itemsPerPage
                            }
                        );

                    }

                    $scope.selectRow = function(organization){
                        $scope.selectedOrgGuid = organization.guid;
                        $scope.selectedOrganization = organization;
                    };

                    $scope.add = function(){
                        efDoborgDialog.create().show().then(function (result) {
                            $modalInstance.close(result);
                        });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.select = function () {
                        if(initializationParameters.returnDetailObject && $scope.selectedOrganization.guid != null){
                            $OrganizationRegistryService.getRegistryOrganizationDetailByGuid(
                                $scope.selectedOrganization.guid,
                                function(data, status){
                                    data.mainAddress = OrganizationAddressConverter.getAddress(data.mainAddress);
                                    $modalInstance.close(data);
                                },
                                function(data, status){
                                    commonDialogs.error('Во время работы системы произошла ошибка');
                                }
                            );
                        }else{
                            $modalInstance.close($scope.selectedOrganization);
                        }
                    };

                    $scope.organizationTypesCodes = organizationTypesCodes;
                };
            }

            function DialogInstance() {
                this.initializationParameters = {};

                /**
                 * если hideAddButton === true, кнопка "Добавить новую организацию" не отображается в диалоге, иначе отображается
                 * @param hideAddButton
                 * @returns {DialogInstance}
                 */
                this.setHideAddButton = function(hideAddButton){
                    this.initializationParameters.hideAddButton = angular.copy(hideAddButton);
                    return this;
                };

                /**
                 * если данный флаг выставлен, то будет возвращаться детализированный объект
                 * (ru.lanit.hcs.intbus.organizationregistry.api.dto.RegistryOrganizationCommonDetailWithNsi
                 * вместо ru.lanit.hcs.organizationregistry.api.dto.RegistryOrganizationCommonShortInfo)
                 *
                 * при этом объект, содержащие ФИАС адрес (mainAddress), будет иметь следующий формат (минимальный набор полей):
                 * {
                 *   region: {guid: ""},
                 *   area: {guid: ""},
                 *   city: {guid: ""},
                 *   settlement: {guid: ""},
                 *   street: {guid: ""},
                 *   house: {guid: ""},
                 *   formattedAddress: ""
                 *  }
                 *
                 * */
                this.setReturnDetailObject = function(returnDetailObject){
                    this.initializationParameters.returnDetailObject = angular.copy(returnDetailObject);
                    return this;
                };

                this.show = function () {
                    var modalOptions = _.clone(modalDefaults, true);
                    modalOptions.controller = getDialogController(this.initializationParameters);
                    return $modal.open(modalOptions).result;
                };
            }

            this.create = function () {
                return new DialogInstance();
            };
        }])

    .filter('orgName', [
        'organizationTypesCodes',
        function(organizationTypesCodes){

            function getSafeValue(value){
                return value != null ? value : '';
            }

            return function(organization){
                var orgName = '';
                switch(organization.registryOrganizationType){
                    case organizationTypesCodes.LEGAL_ENTITY:
                        orgName = organization.fullName;
                        break;
                    case organizationTypesCodes.SOLE_TRADER:
                        orgName = getSafeValue(organization.chiefLastName) + ' ' +
                                  getSafeValue(organization.chiefFirstName) + ' ' +
                                  getSafeValue(organization.chiefMiddleName);
                        break;
                }
                return orgName;
            };
    }])
;
