angular
    .module('common.auth')
    .service('User', ['Auth', function (Auth) {
        return {
            isOms: function () {
                return Auth.authorize([{
                    organizationRole: 'LOCAL_GOVERNMENT_AUTHORITY',
                    role: 'AUTHORIZED_SPECIALIST'
                }]);
            },
            isRso: function () {
                return Auth.authorize([{
                    organizationRole: 'SERVICE_PROVIDER_ORGANIZATION',
                    role: 'AUTHORIZED_SPECIALIST'
                }]);
            },
            isUo: function () {
                return Auth.authorize([{
                    organizationRole: 'MANAGEMENT_ORGANIZATION',
                    role: 'AUTHORIZED_SPECIALIST'
                }]);
            },
            isGov: function () {
                return Auth.authorize([{
                    organizationRole: 'GOV_AUTHORITY'
                }]);
            },
            isCitizen: function () {
                return Auth.authorize([{
                    globalRole: 'P'
                }]);
            },
            isIp: function () {
                return Auth.authorize([{
                    organizationType: 'B'
                }]);
            },
            isUl: function () {
                return Auth.authorize([{
                    organizationType: 'L'
                }]);
            }
        };
    }]);