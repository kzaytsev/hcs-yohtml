angular.module('common.auth')
    .factory('permissionsConfig', function (_) {
        var config = {
            globalRoles: [
                {
                    code: 'P',
                    name: 'Гражданин'
                },
                {
                    code: 'E',
                    name: 'Должностное лицо'
                }
            ],
            loginMethods: [
                {
                    code: 'PWD',
                    name: 'Вход прошел по паролю'
                },
                {
                    code: 'DS',
                    name: 'Вход прошел по сертификату'
                }
            ],
            organizationTypes: [
                {
                    code: 'B',
                    name: 'Индивидуальный предприниматель'
                },
                {
                    code: 'L',
                    name: 'Юр.лицо'
                },
                {
                    code: 'A',
                    name: 'Орган Исполнительной Власти'
                }
            ],
            roles: [
                {
                    code: 'ADMIN',
                    name: 'Администратор',
                    key: '1'
                },
                {
                    code: 'AUTHORIZED_SPECIALIST',
                    name: 'Уполномоченный специалист',
                    key: '3'
                },
                {
                    code: 'ADMIN_OF_GENERAL_MEETING_IN_APARTMENT_HOUSE',
                    name: 'Глава общего собрания собственников жилья в МКД',
                    key: '4'
                }
            ],
            privileges: [
                {
                    code: 'P_EP_1',
                    rusCode: 'П_ЭП_1',
                    name: 'Выполнение настройки структуры электронных паспортов'
                },
                {
                    code: 'P_EP_2',
                    rusCode: 'П_ЭП_2',
                    name: 'Ведение реестра электронных паспортов'
                },
                {
                    code: 'P_EP_3',
                    rusCode: 'П_ЭП_3',
                    name: 'Утверждение электронного паспорта'
                },
                {
                    code: 'P_EP_4',
                    rusCode: 'П_ЭП_4',
                    name: 'Публикация электронного паспорта, сводного электронного документа'
                },
                {
                    code: 'P_EP_5',
                    rusCode: 'П_ЭП_5',
                    name: 'Просмотр электронного паспорта'
                },
                {
                    code: 'P_EP_6',
                    rusCode: 'П_ЭП_6',
                    name: 'Создание и корректировка и удаление электронного документа в части объектов, управляемых РСО'
                },
                {
                    code: 'P_EP_7',
                    rusCode: 'П_ЭП_7',
                    name: 'Создание, корректировка и просмотр сводного электронного документа'
                },
                {
                    code: 'P_EP_8',
                    rusCode: 'П_ЭП_8',
                    name: 'Просмотр документа в части объектов, управляемых РСО'
                },
                {
                    code: 'P_EP_9',
                    rusCode: 'П_ЭП_9',
                    name: 'Доступ к реестру электронных документов'
                },
                {
                    code: 'P_DEVICE_1',
                    rusCode: 'П_ЖКХПУ_1',
                    name: 'Ведение реестра ПУ обслуживаемых домов'
                },
                {
                    code: 'P_DEVICE_2',
                    rusCode: 'П_ЖКХПУ_2',
                    name: 'Просмотр реестра ПУ муниципального образования'
                },
                {
                    code: 'P_HOUSE_1',
                    rusCode: 'П_ЖКХРАО_1',
                    name: 'Доступ к реестру и просмотру сведений об объектах жилищного фонда'
                },
                {
                    code: 'P_HOUSE_2',
                    rusCode: 'П_ЖКХРАО_2',
                    name: 'Доступ к просмотру сведений о долях и обременениях'
                },
                {
                    code: 'P_HOUSE_3',
                    rusCode: 'П_ЖКХРАО_3',
                    name: 'Операции с долями и обременениями. Операции с физ. лицами, организациями'
                },
                {
                    code: 'P_HOUSE_4',
                    rusCode: 'П_ЖКХРАО_4',
                    name: 'Операции с объектами жилищного фонда, помещениями'
                },
                {
                    code: 'P_HOUSE_5',
                    rusCode: 'П_ЖКХРАО_5',
                    name: 'Операции с услугами, предоставляемыми по прямым договорам'
                },
                {
                    code: 'P_HOUSE_6',
                    rusCode: 'П_ЖКХРАО_6',
                    name: 'Добавление, изменение, удаление информации об услугах, оказываемых УО'
                },
                {
                    code: 'P_HOUSE_7',
                    rusCode: 'П_ЖКХРАО_7',
                    name: 'Настройка структуры сведений об объектах инфраструктуры'
                },
                {
                    code: 'P_HOUSE_8',
                    name: 'Изменение согласованных основных характеристик дома. Добавление, изменение, удаление коммунальных услуг по прямым договорам с РСО в карточке дома'
                },
                {
                    code: 'P_HOUSE_9',
                    name: 'Добавление, изменение, удаление заявки на управление домом'
                },
                {
                    code: 'P_ACCOUNT_1',
                    rusCode: 'П_ЛС_1',
                    name: 'Ведение реестра ЛС  обслуживаемых домов'
                },
                {
                    code: 'P_ACCOUNT_2',
                    rusCode: 'П_ЛС_2',
                    name: 'Просмотр реестра ЛС муниципального образования'
                },
                {
                    code: 'P_ACCOUNT_3',
                    rusCode: 'П_ЛС_3',
                    name: 'Ведение реестра сведений о начислениях, платежах, задолженностях'
                },
                {
                    code: 'P_ACCOUNT_4',
                    rusCode: 'П_ЛС_4',
                    name: 'Ведение реестра платежных документов'
                },
                {
                    code: 'P_NSI_1',
                    rusCode: 'П_НСИ_1',
                    name: 'Ведение справочников'
                },
                {
                    code: 'P_NSI_2',
                    rusCode: 'П_НСИ_2',
                    name: 'Ведение справочников организации'
                },
                {
                    code: 'P_PUBLIC_1',
                    rusCode: 'П_ПОЧ_1',
                    name: 'Публикация новостей'
                },
                {
                    code: 'P_PUBLIC_2',
                    rusCode: 'П_ПОЧ_2',
                    name: 'Публикация документов «Субсидии и льготы»'
                },
                {
                    code: 'P_PUBLIC_3',
                    rusCode: 'П_ПОЧ_3',
                    name: 'Публикация документов «Законы и нормативные акты»'
                },
                {
                    code: 'P_PUBLIC_4',
                    rusCode: 'П_ПОЧ_4',
                    name: 'Публикация документов «Тарифы ЖКУ»'
                },
                {
                    code: 'P_PUBLIC_5',
                    rusCode: 'П_ПОЧ_5',
                    name: 'Публикация встреч с населением'
                },
                {
                    code: 'P_PUBLIC_6',
                    rusCode: 'П_ПОЧ_6',
                    name: 'Публикация ссылок на информационные ресурсы в Интернет'
                },
                {
                    code: 'P_PUBLIC_7',
                    rusCode: 'П_ПОЧ_7',
                    name: 'Управление виджетами'
                },
                {
                    code: 'P_PUBLIC_8',
                    rusCode: 'П_ПОЧ_8',
                    name: 'Публикация часто задаваемых вопросов и ответов на них'
                },
                {
                    code: 'P_PUBLIC_9',
                    rusCode: 'П_ПОЧ_9',
                    name: 'Публикация документов «Регламенты и инструкции»'
                },
                {
                    code: 'P_VOTING_1',
                    rusCode: 'П_ЖКХГ_1',
                    name: 'Ведение реестра сведений о голосовании'
                },
                {
                    code: 'P_PPA_1',
                    rusCode: 'П_ППА_2',
                    name: 'Просмотр информации о пользователях ГИС ЖКХ'
                },
                {
                    code: 'P_PPA_2',
                    rusCode: 'П_ППА_2',
                    name: 'Управление доступом пользователей в ГИС ЖКХ'
                },
                {
                    code: 'P_PPA_3',
                    rusCode: 'П_ППА_3',
                    name: 'Управление настройками функционирования ГИС ЖКХ'
                },
                {
                    code: 'P_PPA_4',
                    rusCode: 'П_ППА_4',
                    name: 'Просмотр журнала событий ГИС ЖКХ'
                },
                {
                    code: 'P_INFRASTRUCTURE_1',
                    rusCode: 'П_ЖКХРКИ_1',
                    name: 'Работа с Реестром коммунальной инфраструктуры для ОМС'
                },
                {
                    code: 'P_INFRASTRUCTURE_2',
                    rusCode: 'П_ЖКХРКИ_2',
                    name: 'Работа с Реестром коммунальной инфраструктуры для РСО'
                },
                {
                    code: 'P_INFRASTRUCTURE_3',
                    rusCode: 'П_ЖКХРКИ_3',
                    name: 'Добавление ОКИ и СКИ в Реестр коммунальной инфраструктуры, изменение, удаление записей Реестра коммунальной инфраструктуры'
                },
                {
                    code: 'P_INFRASTRUCTURE_4',
                    rusCode: 'П_ЖКХРКИ_4',
                    name: 'Просмотр характеристик объекта инфраструктуры, являющегося поставщиком коммунального ресурса для УО'
                },
                {
                    code: 'P_INFRASTRUCTURE_5',
                    rusCode: 'П_ЖКХРКИ_5',
                    name: 'Просмотр характеристик СКИ, в которую входят объекты РСО'
                },
                {
                    code: 'P_INFRASTRUCTURE_7',
                    rusCode: 'П_ЖКХРКИ_7',
                    name: 'Настройка структуры сведений об объектах инфраструктуры'
                },
                {
                    code: 'P_INDEX_1',
                    rusCode: 'П_СООРИ_1',
                    name: 'Доступ к реестру индексов на просмотр'
                },
                {
                    code: 'P_INDEX_2',
                    rusCode: 'П_СООРИ_2',
                    name: 'Добавление/удаление/изменение индекса по субъекту и предельного индекса'
                },
                {
                    code: 'P_INDEX_3',
                    rusCode: 'П_СООРИ_3',
                    name: 'Просмотр предельных индексов'
                },
                {
                    code: 'P_DISQUALIFIED_REGISTRY_1',
                    rusCode: 'П_РДЛ_1',
                    name: 'Доступ к реестру дисквалифицированных лиц'
                },
                {
                    code: 'P_DISQUALIFIED_REGISTRY_2',
                    rusCode: 'П_РДЛ_2',
                    name: 'Размещение информации в реестре дисквалифицированных лиц'
                },
                {
                    code: 'P_DISQUALIFIED_REGISTRY_3',
                    rusCode: 'П_РДЛ_3',
                    name: 'Исключение сведений из реестра дисквалифицированных лиц'
                },
                {
                    code: 'P_LICENSE_1',
                    rusCode: 'П_РЛС_1',
                    name: 'Доступ к реестру лицензий субъекта РФ'
                },
                {
                    code: 'P_LICENSE_2',
                    rusCode: 'П_РЛС_2',
                    name: 'Размещение информации о лицензии и домах в реестре лицензий субъекта РФ'
                },
                {
                    code: 'P_RZ_1',
                    rusCode: 'П_РЗ_1',
                    name: 'Доступ к РЗ'
                },
                {
                    code: 'P_RZ_2',
                    rusCode: 'П_РЗ_2',
                    name: 'Размещение заявок на включение сведений о дисквалифицированном лице в РДЛ'
                },
                {
                    code: 'P_RZ_3',
                    rusCode: 'П_РЗ_3',
                    name: 'Рассмотрение заявки на включение сведений о дисквалифицированном лице в РДЛ'
                },
                {
                    code: 'P_PPA_5',
                    rusCode: 'П_ППА_5',
                    name: 'Настройка оповещений пользователей'
                },
                {
                    code: 'P_INFORM_1',
                    rusCode: 'П_ЖКХИ_1',
                    name: 'Настройка новостной ленты'
                },
                {
                    code: 'P_WORK_3',
                    rusCode: 'П_ЖКХПФР_3',
                    name: 'Просмотр, добавление, изменение, удаление информации о актах выполненных работ'
                },
                {
                    code: 'P_WORK_4',
                    rusCode: 'П_ЖКХПФР_4',
                    name: 'Просмотр перечней работ и услуг'
                },
                {
                    code: 'P_WORK_5',
                    rusCode: 'П_ЖКХПФР_5',
                    name: 'Формирование, изменение перечней и изменение плана работ и услуг'
                },
                {
                    code: 'P_WORK_6',
                    rusCode: 'П_ЖКХПФР_6',
                    name: 'Просмотр плана работ и услуг'
                },
                {
                    code: 'P_WORK_8',
                    rusCode: 'П_ЖКХПФР_8',
                    name: 'Просмотр выполненных работ'
                },
                {
                    code: 'P_WORK_9',
                    rusCode: 'П_ЖКХПФР_9',
                    name: 'Фиксация выполненных работ'
                },
                {
                    code: 'P_ACCOUNT_PD',
                    rusCode: 'П_ЛС_ПД',
                    name: 'Просмотр информации о платежах за коммунальные услуги '
                },
                {
                    code: 'P_ACCOUNT_A',
                    rusCode: 'П_ЛС_ПД_К',
                    name: 'Квитирование поступивших платежей'
                },
                {
                    code: 'P_MSCI_1',
                    rusCode: 'П_МСКИ_1',
                    name: 'Доступ к реестру программ модернизации системы коммунальной инфраструктуры. Добавление новой программы. Редактирование и утверждение программы (за исключением раздела Финансирование). Публикация программы (за исключением раздела Финансирование). Просмотр программы. Удаление программы в статусе «Проект»'
                },
                {
                    code: 'P_MSCI_2',
                    rusCode: 'П_МСКИ_2',
                    name: 'Доступ к реестру программ модернизации системы коммунальной инфраструктуры. Редактирование раздела Финансирование. Публикация раздела Финансирование Просмотр программы'
                },
                {
                    code: 'P_INSPECTION_1',
                    rusCode: 'П_РП_1',
                    name: 'Размещение информации в реестре проверок'
                },
                {
                    code: 'P_INSPECTION_2',
                    rusCode: 'П_РП_2',
                    name: 'Размещение информации об отмене проверки'
                },
                {
                    code: 'P_INSPECTION_3',
                    rusCode: 'П_РП_3',
                    name: 'Размещение информации об отмене результатов проверок'
                },
                {
                    code: 'P_INSPECTION_PLAN_1',
                    rusCode: 'П_РПП_1',
                    name: 'Просмотр реестра планов проведения проверок и реестра проверок'
                },
                {
                    code: 'P_INSPECTION_PLAN_2',
                    rusCode: 'П_РПП_2',
                    name: 'Размещение информации в реестре планов проведения плановых проверок'
                },
                {
                    code: 'P_INSPECTION_PLAN_3',
                    rusCode: 'П_РПП_3',
                    name: 'Отмена плановой проверки из плана проверки'
                },
                {
                    code: 'P_AGREEMENT_1',
                    rusCode: 'П_ЖКХДОГ_1',
                    name: 'Доступ к реестру договоров управления/уставов, просмотр договоров управления/уставов/дополнительных соглашений'
                },
                {
                    code: 'P_AGREEMENT_2',
                    rusCode: 'П_ЖКХДОГ_2',
                    name: 'Операции с договорами управления/уставами/дополнительными соглашениями'
                },
                {
                    code: 'P_RENT_AGREEMENT_1',
                    rusCode: 'П_ДОГПОИ_1',
                    name: 'Операции с договорами на пользование общим имуществом'
                },
                {
                    code: 'P_RENT_AGREEMENT_2',
                    rusCode: 'П_ДОГПОИ_2',
                    name: 'Просмотр реестра договоров, просмотр карточки договора на пользование общим имуществом'
                }
            ],
            organizationRoles: [
                {
                    code: 'MANAGEMENT_ORGANIZATION',
                    name: 'Управляющая организация',
                    key: '1'
                },
                {
                    code: 'SERVICE_PROVIDER_ORGANIZATION',
                    name: 'РСО',
                    key: '2'
                },
                {
                    code: 'OPERATOR',
                    name: 'Оператор ГИС-ЖКХ',
                    key: '3'
                },
                {
                    code: 'EXECUTIVE_AUTHORITY_IN_CHARGE_OF_GOV_RESIDENTIAL_CONTROL',
                    name: 'Орган исполнительной власти, уполномоченный на осуществление государственного жилищного надзора',
                    key: '4'
                },
                {
                    code: 'EXECUTIVE_AUTHORITY_IN_CHARGE_OF_MUNICIPAL_RESIDENTIAL_CONTROL',
                    name: 'Орган исполнительной власти, осуществляющий муниципальный жилищный контроль',
                    key: '5'
                },
                {
                    code: 'FEDERAL_EXECUTIVE_AUTHORITY_IN_CHARGE_OF_TARIFF_REGULATION',
                    name: 'Федеральный орган исполнительной власти в области государственного регулирования тарифов',
                    key: '6'
                },
                {
                    code: 'REGIONAL_GOV_AUTHORITY',
                    name: 'Орган государственной власти субъекта РФ',
                    key: '7'
                },
                {
                    code: 'LOCAL_GOVERNMENT_AUTHORITY',
                    name: 'ОМС',
                    key: '8'
                },
                {
                    code: 'GOV_AUTHORITY',
                    name: 'Орган государственной власти',
                    key: '9'
                },
                {
                    code: 'EXECUTIVE_AUTHORITY_IN_CHARGE_OF_TARIFF_REGULATION',
                    name: 'Орган исполнительной власти субъекта РФ в области государственного регулирования тарифов',
                    key: '10'
                },
                {
                    code: 'ADMIN_OF_GENERAL_MEETING_IN_APARTMENT_HOUSE',
                    name: 'Администратор общего собрания собственников помещений в многоквартирном доме',
                    key: '11'
                },
                {
                    code: 'ORGANIZATION_IN_CHARGE_OF_RESIDENTIAL_PROPERTIES_ACCOUNTING',
                    name: 'Органы или организации, уполномоченные на осуществление государственного учета жилищного фонда',
                    key: '13'
                },
                {
                    code: 'MINSTROY',
                    name: 'Минстрой',
                    key: '17'
                }
            ],
            privilegesByOrganizationRole: {
                'MANAGEMENT_ORGANIZATION': [
                    'P_AGREEMENT_1',
                    'P_AGREEMENT_2',
                    'P_RENT_AGREEMENT_1',
                    'P_RENT_AGREEMENT_2',
                    'P_VOTING_1',
                    'P_INFORM_1',
                    'P_DEVICE_1',
                    'P_WORK_3',
                    'P_WORK_4',
                    'P_WORK_5',
                    'P_WORK_6',
                    'P_WORK_8',
                    'P_WORK_9',
                    'P_HOUSE_1',
                    'P_HOUSE_2',
                    'P_HOUSE_3',
                    'P_HOUSE_4',
                    'P_INFRASTRUCTURE_4',
                    'P_ACCOUNT_1',
                    'P_ACCOUNT_3',
                    'P_ACCOUNT_4',
                    'P_ACCOUNT_PD',
                    'P_ACCOUNT_A',
                    'P_NSI_2',
                    'P_PUBLIC_4',
                    'P_INDEX_1',
                    'P_INDEX_3',
                    'P_EP_1',
                    'P_EP_2',
                    'P_EP_3',
                    'P_EP_5'
                ],
                'SERVICE_PROVIDER_ORGANIZATION': [
                    'P_INFORM_1',
                    'P_DEVICE_1',
                    'P_HOUSE_1',
                    'P_HOUSE_5',
                    'P_INFRASTRUCTURE_2',
                    'P_INFRASTRUCTURE_7',
                    'P_ACCOUNT_1',
                    'P_ACCOUNT_3',
                    'P_ACCOUNT_4',
                    'P_ACCOUNT_PD',
                    'P_ACCOUNT_A',
                    'P_NSI_2',
                    'P_PUBLIC_4',
                    'P_INDEX_1',
                    'P_INDEX_3',
                    'P_EP_6',
                    'P_EP_8',
                    'P_EP_9',
                    'P_RENT_AGREEMENT_1',
                    'P_RENT_AGREEMENT_2'
                ],
                'OPERATOR': [
                    'P_NSI_1',
                    'P_PPA_1',
                    'P_PPA_2',
                    'P_PPA_3',
                    'P_PPA_4',
                    'P_PPA_5',
                    'P_PUBLIC_1',
                    'P_PUBLIC_2',
                    'P_PUBLIC_3',
                    'P_PUBLIC_4',
                    'P_PUBLIC_5',
                    'P_PUBLIC_6',
                    'P_PUBLIC_7',
                    'P_PUBLIC_8'
                ],
                'EXECUTIVE_AUTHORITY_IN_CHARGE_OF_GOV_RESIDENTIAL_CONTROL': [
                    'P_WORK_4',
                    'P_WORK_6',
                    'P_WORK_8',
                    'P_INSPECTION_1',
                    'P_INSPECTION_2',
                    'P_INSPECTION_3',
                    'P_INSPECTION_PLAN_1',
                    'P_INSPECTION_PLAN_2',
                    'P_INSPECTION_PLAN_3'
                ],
                'EXECUTIVE_AUTHORITY_IN_CHARGE_OF_MUNICIPAL_RESIDENTIAL_CONTROL': [
                    'P_INSPECTION_PLAN_2',
                    'P_INSPECTION_1',
                    'P_INSPECTION_2',
                    'P_INSPECTION_PLAN_1',
                    'P_INSPECTION_PLAN_3'
                ],
                'FEDERAL_EXECUTIVE_AUTHORITY_IN_CHARGE_OF_TARIFF_REGULATION': [
                    'P_PUBLIC_4'
                ],
                'REGIONAL_GOV_AUTHORITY': [
                    'P_PUBLIC_4',
                    'P_INDEX_1',
                    'P_INDEX_2'
                ],
                'LOCAL_GOVERNMENT_AUTHORITY': [
                    'P_AGREEMENT_1',
                    'P_RENT_AGREEMENT_2',
                    'P_DEVICE_2',
                    'P_WORK_4',
                    'P_WORK_6',
                    'P_WORK_8',
                    'P_HOUSE_1',
                    'P_HOUSE_2',
                    'P_HOUSE_3',
                    'P_HOUSE_6',
                    'P_ACCOUNT_2',
                    'P_INFRASTRUCTURE_1',
                    'P_INFRASTRUCTURE_3',
                    'P_PUBLIC_3',
                    'P_PUBLIC_4',
                    'P_PUBLIC_5',
                    'P_INDEX_1',
                    'P_INDEX_3',
                    'P_EP_1',
                    'P_EP_2',
                    'P_EP_4',
                    'P_EP_7',
                    'P_EP_8',
                    'P_EP_9'
                ],
                'GOV_AUTHORITY': [],
                'EXECUTIVE_AUTHORITY_IN_CHARGE_OF_TARIFF_REGULATION': [
                    'P_PUBLIC_4'
                ],
                'ADMIN_OF_GENERAL_MEETING_IN_APARTMENT_HOUSE': [
                    'P_VOTING_1'
                ],
                'ORGANIZATION_IN_CHARGE_OF_RESIDENTIAL_PROPERTIES_ACCOUNTING': [
                    'P_HOUSE_1',
                    'P_HOUSE_2',
                    'P_HOUSE_4'
                ],
                'MINSTROY': [
                    'P_DISQUALIFIED_REGISTRY_1',
                    'P_DISQUALIFIED_REGISTRY_2',
                    'P_DISQUALIFIED_REGISTRY_3',
                    'P_RZ_1',
                    'P_RZ_3'
                ]
            }
        };

        return {
            getGlobalRoles: function () {
                return _.indexBy(config.globalRoles, 'code');
            },
            getLoginMethods: function () {
                return _.indexBy(config.loginMethods, 'code');
            },
            getOrganizationTypes: function () {
                return _.indexBy(config.organizationTypes, 'code');
            },
            getRoles: function () {
                return _.indexBy(config.roles, 'code');
            },
            getPrivileges: function () {
                return _.indexBy(config.privileges, 'code');
            },
            getOrganizationRoles: function () {
                return _.indexBy(config.organizationRoles, 'code');
            },
            getPrivilegesByOrganizationRole: function () {
                return config.privilegesByOrganizationRole;
            }
        };
    })
;