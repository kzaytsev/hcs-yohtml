angular.module ('common.auth')
    //  <div permit = "[{loginMethod: "PWD", organizationType: "B", role: "ADMIN"}]"> TEST ACCESS </div>
    .directive ('permit', ['Auth', function (Auth) {
            return {
                restrict: 'A',
                link: function ($scope, element, attrs) {
                    var prevDisp = element.css ('display');
                    var permit;

                    attrs.$observe ('permit', function (al) {
                        if (al){
                            permit = $scope.$eval ('(' + al + ')');
                        }
                        updateCSS ();
                    });

                    function updateCSS () {
                        if (permit) {
                            if (!Auth.authorize (permit)){
                                element.css ('display', 'none');
                            }
                            else{
                                element.css ('display', prevDisp);
                            }
                        }
                    }
                }
            };
        }]);