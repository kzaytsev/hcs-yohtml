angular.module('common.auth')

    .run(['$rootScope', '$state', 'Auth', function ($rootScope, $state, Auth) {
        $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
            if (toState.data && !Auth.authorize(toState.data.permit)) {
                Auth.refresh(event);
            }
        });
    }])

    .config(['$injector', function ($injector) {
        $injector.get('$httpProvider').interceptors.push(function ($q, $cookies) {
            return {
                responseError: function (response) {
                    if (response.status === 401 || response.status === 403) {
                        $injector.get('Auth').refresh();
                    }
                    return $q.reject(response);
                },
                request: function (config) {
                    if (config.url.indexOf("/rest/") != -1) {
                        var token = $cookies.token;
                        if (angular.isDefined(token)) {
                            config.headers = config.headers || {};
                            config.headers.token = token;
                        }
                    }
                    return config;
                }
            };
        });
    }])

    .provider('Auth', [function () {

        var anon = angular.fromJson("{}");

        var authLocation = '/sp-web/sp/login';
        var logoutLocation = '/sp-web/saml/Logout';
        var noPrivilegesState = 'no-privileges';
        var noRedirection = false;

        var parameters;

        this.setAuthLocation = function (value) {
            authLocation = value;
        };

        this.setLogoutLocation = function (value) {
            logoutLocation = value;
        };

        this.setNoPrivilegesState = function (value) {
            noPrivilegesState = value;
        };

        this.setNoRedirection = function (value) {
            noRedirection = value;
        };

        this.$get = ['$injector', function ($injector) {

            function Auth($injector, authLocation, logoutLocation, noPrivilegesState, noRedirection) {

                var _ = $injector.get("_");
                var currentUser = anon;

                function _getCurrentUser() {
                    var token = _readCookie('token');
                    if (!token || !angular.equals(currentUser, anon)) {
                        return currentUser;
                    }

                    var request = new XMLHttpRequest();
                    request.open('GET', '/ppa/api/rest/services/ppa/current/user', false);  // false` makes the request synchronous
                    request.setRequestHeader("If-Modified-Since", '0');
                    request.send(null);

                    if (request.status === 200) {
                        currentUser = angular.fromJson(request.responseText);
                    } else {
                        if (noRedirection) {
                            currentUser = anon;
                        } else {
                            alert('При получении информации о пользователе произошла ошибка: ' + request.status);
                            _logout();
                        }
                    }

                    return currentUser;
                }


                function _createCookie(name, value, days, path, domain) {
                    var expires = "";
                    if (days) {
                        var date = new Date();
                        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                        expires = "; expires=" + date.toGMTString();
                    }
                    document.cookie = name + "=" + value + expires + "; path=/" + path +  ((domain)?";domain="+domain:"");
                }

                function _readCookie(name) {
                    var nameEQ = name + "=";
                    var ca = document.cookie.split(';');
                    for (var i = 0; i < ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0) === ' ') {
                            c = c.substring(1, c.length);
                        }
                        if (c.indexOf(nameEQ) === 0) {
                            return c.substring(nameEQ.length, c.length);
                        }
                    }
                    return null;
                }

                function _eraseCookie(name, path, domain) {
                    _createCookie(name, "", -1, path, domain);
                }

                function _isAccepted(required, current) {
                    if (_.isUndefined(required)) {
                        return true;
                    }
                    if (_.isUndefined(current)) {
                        return false;
                    }
                    if (_.isArray(current)) {
                        return _.contains(current, required);
                    } else {
                        return _.isEqual(current, required);
                    }
                }

                function _hasPermission(user, loggedIn, registered, globalRole, loginMethod, organizationType, organizationRole, role, privilege) {
                    return _isAccepted(loginMethod, user.loginMethod) &&
                        _isAccepted(globalRole, user.globalRole) &&
                        _isAccepted(organizationType, user.organizationType) &&
                        _isAccepted(role, user.roles) &&
                        _isAccepted(privilege, user.privileges) &&
                        _isAccepted(organizationRole, user.organizationRoles) &&
//                      Если параметр registered не указан или равен true, считаем, что метод доступен только для регистрированных.
//                      Если указан и равен false, то доступен для всех.
                        (registered === false || _isAccepted((_.isUndefined(registered) || registered === true) ? true : false, user.registered)) &&
//                      Если параметр loggedIn не указан или равен true, считаем, что метод доступен только для залогиненных.
//                      Если указан и равен false, то доступен для всех.
                        (loggedIn === false || _isAccepted(_.isUndefined(loggedIn) || loggedIn === true, isLoggedIn(user)))
                        ;
                }

                function _eraseCookies() {
                    _eraseCookie('user', '');
                    _eraseCookie('token', '');
                    _eraseCookie('Location', '');
                    _eraseCookie('JSESSIONID', 'idp');
                    _eraseCookie('JSESSIONID', 'idp2');
                    _eraseCookie('JSESSIONID', 'homemanagement');
                    _eraseCookie('JSESSIONID', 'ppa');
                    _eraseCookie('JSESSIONID', 'notification');
                    _eraseCookie('JSESSIONID', 'inspection');
                    _eraseCookie('JSESSIONID', 'nsi');
                    _eraseCookie('JSESSIONID', 'public-content');
                    _eraseCookie('JSESSIONID', 'agreements');
                    _eraseCookie('JSESSIONID', 'rki-document');
                    _eraseCookie('JSESSIONID', 'rki');
                    _eraseCookie('JSESSIONID', 'bills');
                    _eraseCookie('JSESSIONID', 'voting');
                    _eraseCookie('JSESSIONID', 'orgnsi');
                    _eraseCookie('JSESSIONID', 'passport');
                    //TODO: странное поведение nginx. если есть эта кука и пробуем зайти на /sp-web/sp/login,
                    // то nginx возвращает 404, хотя сам sp-web возвращает 302. Если почистить куку, то нормально.
                    _eraseCookie('JSESSIONID', 'sp-web');
                    _eraseCookie('_idp_session', 'idp');
                    _eraseCookie('_idp_session', 'idp2');

                    var altDomain = '.' + location.hostname;
                    _eraseCookie('user', '', altDomain);
                    _eraseCookie('token', '', altDomain);
                    _eraseCookie('Location', '', altDomain);
                    _eraseCookie('JSESSIONID', 'idp', altDomain);
                    _eraseCookie('JSESSIONID', 'idp2', altDomain);
                    _eraseCookie('JSESSIONID', 'homemanagement', altDomain);
                    _eraseCookie('JSESSIONID', 'ppa', altDomain);
                    _eraseCookie('JSESSIONID', 'notification', altDomain);
                    _eraseCookie('JSESSIONID', 'inspection', altDomain);
                    _eraseCookie('JSESSIONID', 'nsi', altDomain);
                    _eraseCookie('JSESSIONID', 'public-content', altDomain);
                    _eraseCookie('JSESSIONID', 'voting', altDomain);
                    _eraseCookie('JSESSIONID', 'orgnsi', altDomain);
                    _eraseCookie('JSESSIONID', 'passport', altDomain);
                    _eraseCookie('JSESSIONID', 'sp-web', altDomain);
                    _eraseCookie('_idp_session', 'idp', altDomain);
                    _eraseCookie('_idp_session', 'idp2', altDomain);
                }

                function _logout() {
                    currentUser = anon;
                    _eraseCookies();
                    window.location = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + logoutLocation;
                }

                function isLoggedIn(user) {
                    if (!user) {
                        user = _getCurrentUser();
                    }
                    var token = _readCookie('token');
                    return token && !angular.equals(user, anon);
                }

                return {

                    getToken: function () {
                        return _readCookie('token');
                    },

                    setParameters: function (value) {
                        parameters = value;
                    },

                    logout: function () {
                        _logout();
                    },

                    login: function () {
                        var location = authLocation;
                        if (angular.isDefined(parameters)) {
                            location += '?';
                            for (var i = parameters.length - 1; i >= 0; i--) {
                                location += parameters[i].key + '=' + parameters[i].value;
                                if (i !== 0) {
                                    location += '&';
                                }
                            }
                        }
                        window.location = location;
                    },

                    refresh: function (event) {
                        if (event) {
                            event.preventDefault();
                        }
                        if (window.location.pathname === '/' || this.isLoggedIn()) {
                            $injector.get('$state').go(noPrivilegesState);
                        } else {
                            window.location = '/#/no-privileges';
                        }
                    },

                    authorize: function (permit) {
                        if (!permit) {
                            return true;
                        }
                        for (var i = permit.length - 1; i >= 0; i--) {
                            if (_hasPermission(
                                _getCurrentUser(),
                                permit[i].loggedIn,
                                permit[i].registered,
                                permit[i].globalRole,
                                permit[i].loginMethod,
                                permit[i].organizationType,
                                permit[i].organizationRole,
                                permit[i].role,
                                permit[i].privilege
                            )) {
                                return true;
                            }
                        }
                        return false;
                    },

                    isLoggedIn: isLoggedIn,

                    reloadCurrentUser: function(){
                        currentUser = anon;
                        this.user = _getCurrentUser();
                    },

                    user: _getCurrentUser()

                };
            }

            return new Auth($injector, authLocation, logoutLocation, noPrivilegesState, noRedirection);
        }];

    }])
;