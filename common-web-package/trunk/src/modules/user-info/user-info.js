/**
 * ЭФ_ППА_ОБЩ_ИНФ_ЛК_ГРЖ
 * ЭФ_ППА_ОБЩ_ИНФ_ЛК_ОРГ
 */
angular
    .module('common.user-info', [
        'ru.lanit.hcs.ppa.rest.PpaService',
        'common.auth',
        'common.notification.model',
        'lodash'
    ])
    .directive('userInfo', [
        'Auth',
        'permissionsConfig',
        '$PpaService',
        '$injector',
        '_',
        'NotificationModelAmount',
        function (Auth, permissionsConfig, $PpaService, $injector, _, NotificationModelAmount) {
            return {
                restrict: 'E',
                scope: {},
                templateUrl: 'user-info/user-info.tpl.html',
                link: function ($scope) {
                    // Проверям гражданин или организация
                    $scope.isCitizen = (Auth.user.globalRole == 'P');
                    if ($scope.isCitizen) {
                        $scope.citizenInformation = formatUserInformation(Auth.user) || '';
                    } else {
                        $PpaService.findOrganizationDetails().then(
                            function (data) {
                                $scope.organizationName = data.shortName ? data.shortName : data.fullName;

                            }
                        );
                        $scope.organizationRolesDescription = prepareOrganizationRolesDescription(Auth.user.organizationRoles);
                        $scope.additionalInformation = !_.isEmpty(Auth.user.orgPosition) ? formatUserInformation(Auth.user) + ' - ' + Auth.user.orgPosition : formatUserInformation(Auth.user);

                        if (!_.isEmpty(Auth.user.userId) && Auth.user.userId != 'null') {
                            $scope.$on('$destroy', NotificationModelAmount.subscribe(function (data) {
                                $scope.notificationAmount = data;
                            }));
                        }

                        $scope.efPpaPrsmObschInfOrgDialogShow = function () {
                            var PpaProcessor = $injector.get("PpaProcessor");
                            PpaProcessor.efPpaPrsmObschInfOrgDialogShow();
                        };
                    }

                    $scope.logout = function () {
                        $PpaService.invalidateSession();
                        Auth.logout();
                    };

                    function formatUserInformation(user) {
                        var lastName = user.lastName || '';
                        var firstName = user.firstName || '';
                        var middleName = user.middleName || '';

                        var result = lastName;
                        if (firstName) {
                            result += ' ' + firstName.charAt(0) + '.';
                        }
                        if (middleName) {
                            result += middleName.charAt(0) + '.';
                        }

                        return result;
                    }

                    function prepareOrganizationRolesDescription(organizationRoles) {
                        var organizationRoles_ = permissionsConfig.getOrganizationRoles();
                        return _(organizationRoles)
                            .map(function (organizationRole) {
                                return organizationRoles_[organizationRole].name;
                            })
                            .uniq()
                            .value()
                            .join(', ');
                    }
                }
            };
        }]);