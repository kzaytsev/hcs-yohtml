angular.module('common.ngJqGrid', [])
    .directive('ngJqGrid', function () {
        return {
            restrict: 'E',
            scope: {
                config: '=',
                data: '=',
                api: '=?'
            },
            link: function (scope, element, attrs) {

                var getColumnIndexesByType = function (grid, formatter) {
                    var indexes = [];
                    var cm = grid.jqGrid ('getGridParam', 'colModel'), i, l;
                    for (i = 0, l = cm.length; i < l; i += 1) {
                        if (cm[i].formatter === 'checkbox') {
                            indexes.push(i);
                        }
                    }
                    return indexes;
                };

                var onCellChanged = function () {
                    scope.api.apply();
                };

                //Решение проблемы с перехватом изменений от checkBox
                scope.config. loadComplete = function () {
                    var rows = this.rows, c = rows.length;
                    var colIndexes = getColumnIndexesByType($(this), 'checkbox');

                    var onCheckBoxClick = function (colIndex, rowIndex) {
                        $(rows[rowIndex].cells[colIndex]).click(function () {
                            onCellChanged();
                        });
                    };

                    for(var i in colIndexes){
                        var colIndex = colIndexes[i];
                        for (var rowIndex = 0; rowIndex < c; rowIndex += 1) {
                            onCheckBoxClick(colIndex, rowIndex);
                        }
                    }
                };

                var table, div;
                scope.$watch('config', function (value) {

                    element.children().empty();
                    table = angular.element('<table id="' + attrs.gridid + '"></table>');
                    element.append(table);
                    if (attrs.pagerId) {
                        value.pager = '#' + attrs.pagerId;
                        var pager = angular.element(value.pager);
                        if (pager.length === 0) {
                            div = angular.element('<div id="' + attrs.pagerid + '"></div>');
                            element.append(div);
                        }
                    }
                    $(table).jqGrid(value).trigger('reloadGrid');

                    // Variadic API – usage:
                    //   view:  <ng-jqgrid … vapi="apicall">
                    //   ctrl:  $scope.apicall('method', 'arg1', …);
                    scope.vapi = function () {
                        var args = Array.prototype.slice.call(arguments, 0);
                        return $(table).jqGrid.apply(table, args);
                    };

                    // allow to getRowData(), clear(), refresh(), apply() the grid from
                    // outside (e.g. from a controller). Usage:
                    //   view:  <ng-jqgrid … api="gridapi">
                    //   ctrl:  $scope.gridapi.clear();
                    scope.api = {
                        clear: function () {
                            scope.data.length = 0;
                            $(table)
                                .jqGrid('clearGridData', {datastr: scope.data})
                                .trigger('reloadGrid');
                        },
                        refresh: function (data) {
                            scope.data = data;
                            $(table)
                                .jqGrid('clearGridData')
                                .jqGrid('setGridParam', {datastr: scope.data})
                                .trigger('reloadGrid');
                        },
                        getRowData: function () {
                            return $(table).jqGrid("getRowData");
                        },
                        apply: function () {
                            scope.$apply(function () {
                                scope.data = scope.api.getRowData();
                            });
                        }
                    };
                });
            }
        };

    })

;