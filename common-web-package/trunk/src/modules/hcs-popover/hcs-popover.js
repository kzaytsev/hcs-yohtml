angular.module('common.hcs-popover', [])
    .service('$popoverSrv', function ($document) {
        var handlers = {};

        return {
            registerTrigger: function (id, triggerEl) {
                return (function (_id, _triggerEl) {
                    return function () {
                        var elOffset = _triggerEl.position();
                        return handlers[id] ? handlers[id](elOffset.top + _triggerEl.outerHeight(), elOffset.left) : false;
                    };
                }(id, triggerEl));
            },
            registerContent: function (id, cb) {
                handlers[id] = cb;
            }
        };
    })
    .directive(
    'hcsPopoverTrigger',
    function ($document) {
        return {
            restrict: 'A',
            link: function (scope, el, attrs) {
                var cb = scope.$popoverSrv.registerTrigger(attrs.hcsPopoverTrigger, el, attrs.dataPlacement);
                el.bind('click', cb);
                window.el = el;
            },
            controller: function ($scope, $popoverSrv) {
                $scope.$popoverSrv = $popoverSrv;
            }
        };
    }
)
    .directive(
    'hcsPopoverContent',
    function ($document) {
        return {
            restrict: 'A',
            link: function (scope, el, attrs) {
                scope.$popoverSrv.registerContent(attrs.hcsPopoverContent, function (x, y) {
                    window.e = el;
                    el.css({top: x + 'px', left: y + 'px'});
                    scope.isOpen = !scope.isOpen;
                });
                scope.isOpen = false;

                var documentClickBind = function (event) {
                    if (scope.isOpen && event.target !== el[0]) {
                        scope.$apply(function () {
                            scope.isOpen = false;
                        });
                    }
                };

                scope.$watch('isOpen', function (value) {
                    if (value) {
                        //scope.position = appendToBody ? $position.offset(element) : $position.position(element);
                        //scope.position.top = scope.position.top + element.prop('offsetHeight');
                        el.show();
                        $document.bind('click', documentClickBind);
                    } else {
                        el.hide();
                        $document.unbind('click', documentClickBind);
                    }
                });

                $document.bind('click', documentClickBind);
                scope.$on('$destroy', function () {
                    $document.unbind('click', documentClickBind);
                });

                $(document).append(el);
            },
            controller: function ($scope, $popoverSrv) {
                $scope.$popoverSrv = $popoverSrv;

            }
        };
    }
);