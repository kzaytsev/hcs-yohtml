angular
    .module('common.filter', [
        'lodash'
    ])

    .filter('houseAddress', function () {
        return function (addressObj, fromDialog) {
            var propertiesMap = ['region', 'area', 'city', 'settlement', 'street'];

            var result = "";
            if (addressObj) {
                angular.forEach(propertiesMap, function (property, index) {
                    if (fromDialog) {
                        if (addressObj.address && addressObj['address'][property]) {
                            result += addressObj['address'][property] + ', ';
                        }
                    } else {
                        if (addressObj[property] && addressObj[property].formalName) {
                            result += addressObj[property].formalName + ', ';
                        }
                    }
                });

                if (result.length > 0) {
                    result = result.substring(0, result.length - 2);
                }

                var house = fromDialog ? addressObj : addressObj.house;
                if (house) {
                    if (house.houseNumber) {
                        result += ', д.' + house.houseNumber;
                    }
                    if (house.buildingNumber) {
                        result += ', к.' + house.buildingNumber;
                    }
                    if (house.structNumber) {
                        result += ', стр.' + house.structNumber;
                    }
                }
            }

            return result;

        };
    })

    .filter('esiaHouseAddress', function () {
        return function (addressObj) {
            var propertiesMap = ['index', 'region', 'district', 'settlement', 'street'];
            var result = [];
            if (addressObj) {
                angular.forEach(propertiesMap, function (property) {
                    if (addressObj[property]) {
                        result.push(addressObj[property]);
                    }
                });
                if (addressObj.house) {
                    result.push('д.' + addressObj.house);
                }
                if (addressObj.corpus) {
                    result.push('к.' + addressObj.corpus);
                }
                if (addressObj.structure) {
                    result.push('стр.' + addressObj.structure);
                }
                if (addressObj.flat) {
                    result.push('кв. ' + addressObj.flat);
                }
            }

            return result.join(', ');

        };
    })

    .filter('addressFormatter', function () {
        return function (value) {
            var stringAddress = '';
            if (_.isObject(value) && (!_.isEmpty(value.address) || !_.isEmpty(value.formattedAddress))) {
                var zipCode = (!_.isEmpty(value.street) && !_.isEmpty(value.street.postalCode)) ? value.street.postalCode : '';
                if (_.isEmpty(zipCode) && !_.isEmpty(value.zipCode)) {
                    zipCode = value.zipCode;
                }
                stringAddress = value.address || value.formattedAddress;
                if (!_.isEmpty(zipCode)) {
                    stringAddress = zipCode + ', ' + stringAddress;
                }
            } else if (_.isString(value) && !_.isEmpty(value)) {
                stringAddress = value;
            }
            return stringAddress;
        };
    })

    .filter('geographicalObjectName', function (_) {
        return function (addressObject, reverse) {
            var name = addressObject.formalName;

            var federalSubjects = ['Москва', 'Санкт-Петербург', 'Севастополь'];
            if (_.contains(federalSubjects, name)) {
                return name;
            }

            if (reverse) {
                var prefix = addressObject.shortName;
                if (prefix == 'проезд') {
                    prefix += ' ';
                } else if (_.isString(prefix)) {
                    prefix += '. ';
                }
                return prefix + name;
            }
            return addressObject.shortName ? name + ' ' + addressObject.shortName : name;
        };
    })

    .filter('money', function () {
        return function (value, currencySymbol) {
            if(value == null || value === ''){
                return '';
            }
            value = value + '';

            var point = value.indexOf('.');
            var integral = value, fraction;

            if (point > -1) {
                integral = value.substring(0, point);
                fraction = value.substring(point);
            }else{
                fraction = '.00';
            }

            var result = '';
            if (integral.length > 3) {
                var parts = [];
                for (var i = integral.length; i >= 0; i -= 3) {
                    var item = integral.substring(i - 3, i);
                    if (item) {
                        parts.push(item);
                    }
                }

                angular.forEach(parts, function (val) {
                    result = ' ' + val + result;
                });
            }else{
                result = integral;
            }

            if (fraction) {
                result += fraction;
            }

            if(currencySymbol == null){
                result += ' руб';
            }else{
                result += (' ' + currencySymbol);
            }

            return result;
        };
    })

    .filter('month', function () {
        return function (value) {
            var monthes = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
            var num = parseInt(value, 10);

            return monthes[num - 1];
        };
    })

/**
 Returns formatted phone: 8 (###) ###-##-##
 */
    .filter('phone', function () {
        return function (number) {
            if (!number) {
                return '';
            }
            number = number.replace(/[^0-9\+]+/g, '');
            number = String(number);

            var code = '8 ';
            if (number[0] == '8') {
                number = number.slice(1);
            } else if (number[0] == '+' && number[1] == '7') {
                number = number.slice(2);
            }
            // 8 (section1) section2-section3-section4*section5
            var section1 = number.substring(0, 3);
            var section2 = number.substring(3, 6);
            var section3 = number.substring(6, 8);
            var section4 = number.substring(8, 10);
            var section5 = number.substring(10, 16);
            if (section1.length === 3 && section2.length !== 0) {
                section1 = '(' + section1 + ') ';
            } else if (section1.length > 0 && section1.length !== 0) {
                section1 = '(' + section1;
            }
            section2 = (section2.length === 3 && section3.length !== 0) ? section2 + '-' : section2;
            section3 = (section3.length === 2 && section4.length !== 0) ? section3 + '-' : section3;
            if (section5.length !== 0) {
                if (section5.length > 6) {
                    section5 = section5.substring(0, 6);
                }
                section5 = '*' + section5;

            }

            return (code + section1 + section2 + section3 + section4 + section5);
        };
    })

    .filter('ucwords', function () {
        return function (value) {
            if (!value) {
                return '';
            }

            return value.replace(/^(.)|\s(.)/g, function ($1) {
                return $1.toUpperCase();
            });
        };
    })

/**
 * Фильтер ограничивает количество текста, заменяя лишний на выбраное значение(по умолчанию '...')
 */
    .filter('charLimit', function () {
        return function (text, length, end) {
            if (!end) {
                end = "...";
            }
            if (!text || !length || length < end.length || text.length - end.length <= length) {
                return text;
            }
            return text.substring(0, length - end.length) + end;
        };
    })
;