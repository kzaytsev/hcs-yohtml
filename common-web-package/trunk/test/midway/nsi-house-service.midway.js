describe("$NsiHouseService Tests", function() {
    var tester;
    var $NsiHouseService;

    function waitForResponse(promise) {
        var done = false;
        runs(function () {
            promise.then(function (result) {
                expect(result instanceof Array).toBeTruthy();
                expect(result.length > 0).toBeTruthy();
            });
            promise['finally'](function () {
                done = true;
            });
        });

        waitsFor(function () {
            return done;
        }, 5000);
    }

    beforeEach(function() {
        tester = ngMidwayTester('midway-web-package-mock');
        $NsiHouseService = tester.inject('$NsiHouseService');
    });

    it('Testing method $NsiHouseService.findHouseType', function() {
        expect($NsiHouseService).not.toEqual(null);
        var nsiApartmentPromise = $NsiHouseService.findHouseType();

        waitForResponse(nsiApartmentPromise);
    });

    it('Testing method $NsiHouseService.findHouseCondition', function() {

        expect($NsiHouseService).not.toEqual(null);
        var nsiApartmentPromise = $NsiHouseService.findHouseCondition();

        waitForResponse(nsiApartmentPromise);
    });

    it('Testing method $NsiHouseService.findHouseManagementType', function() {

        expect($NsiHouseService).not.toEqual(null);
        var nsiApartmentPromise = $NsiHouseService.findHouseManagementType();

        waitForResponse(nsiApartmentPromise);
    });

    it('Testing method $NsiHouseService.findHouseEnergyEfficiency', function() {

        expect($NsiHouseService).not.toEqual(null);
        var nsiApartmentPromise = $NsiHouseService.findHouseEnergyEfficiency();

        waitForResponse(nsiApartmentPromise);
    });

    it('Testing method $NsiHouseService.findHousePlanType', function() {

        expect($NsiHouseService).not.toEqual(null);
        var nsiApartmentPromise = $NsiHouseService.findHousePlanType();

        waitForResponse(nsiApartmentPromise);
    });

    it('Testing method $NsiHouseService.findCooperativeSupportService', function() {
        var queryJson = {
            code: '1',
            guid: '4f10e836-9dee-4171-9843-682df1953347'
        };
        expect($NsiHouseService).not.toEqual(null);
        var nsiApartmentPromise = $NsiHouseService.findCooperativeSupportService(queryJson);

        waitForResponse(nsiApartmentPromise);
    });

    it('Testing method $NsiHouseService.findStructuralComponents', function() {

        expect($NsiHouseService).not.toEqual(null);
        var nsiApartmentPromise = $NsiHouseService.findStructuralComponents();

        waitForResponse(nsiApartmentPromise);
    });

    it('Testing method $NsiHouseService.findMunicipalResources', function() {

        expect($NsiHouseService).not.toEqual(null);
        var nsiApartmentPromise = $NsiHouseService.findMunicipalResources();

        waitForResponse(nsiApartmentPromise);
    });

    it('Testing method $NsiHouseService.findInspectionObjects', function() {

        expect($NsiHouseService).not.toEqual(null);
        var nsiApartmentPromise = $NsiHouseService.findInspectionObjects();

        waitForResponse(nsiApartmentPromise);
    });
});
