describe("$FiasService Tests", function() {
    var tester;
    var $FiasService;

    function waitForResponse(promise) {
        var done = false;
        runs(function () {
            promise.then(function (result) {
                expect(result instanceof Array).toBeTruthy();
                expect(result.length > 0).toBeTruthy();
            });
            promise['finally'](function () {
                done = true;
            });
        });

        waitsFor(function () {
            return done;
        }, 5000);
    }

    beforeEach(function () {
        tester = ngMidwayTester('midway-web-package-mock');
        $FiasService = tester.inject('$FiasService');
    });

    it('Testing method $FiasService.findRegions', function () {

        expect($FiasService).not.toEqual(null);
        var fiasPromise = $FiasService.findRegions();

        waitForResponse(fiasPromise);
    });

    it('Testing method $FiasService.findAreas', function () {
        var queryJson = {
            regionGuid: '5c8b06f1-518e-496e-b683-7bf917e0d70b',
            actual: true
        };
        expect($FiasService).not.toEqual(null);
        var fiasPromise = $FiasService.findAreas(queryJson);

        waitForResponse(fiasPromise);
    });

    it('Testing method $FiasService.findCities', function () {
        var queryJson = {
            regionGuid: '5c8b06f1-518e-496e-b683-7bf917e0d70b',
            actual: true
        };

        expect($FiasService).not.toEqual(null);
        var fiasPromise = $FiasService.findCities(queryJson);

        waitForResponse(fiasPromise);
    });

    it('Testing method $FiasService.findSettlements', function () {
        var queryJson = {
            actual: true,
            areaGuid: '41451677-aad4-4cb9-ba76-2b0eeb156acb'
        };

        expect($FiasService).not.toEqual(null);
        var fiasPromise = $FiasService.findSettlements(queryJson);

        waitForResponse(fiasPromise);
    });

    it('Testing method $FiasService.findStreets', function () {
        var queryJson = {
            actual: true,
            cityGuid: '815f4311-df52-4f89-a870-948b2ea17f3f',
            regionGuid: 'd286798f-0849-4a7c-8e78-33c88dc964c6'
        };

        expect($FiasService).not.toEqual(null);
        var fiasPromise = $FiasService.findStreets(queryJson);

        waitForResponse(fiasPromise);
    });

    it('Testing method $FiasService.findHouseNumbers', function () {
        var queryJson = {
            streetGuid: '163c3ce5-aba3-4da8-99e1-fdadf95aeff1',
            actual: true
        };

        expect($FiasService).not.toEqual(null);
        var fiasPromise = $FiasService.findHouseNumbers(queryJson);

        waitForResponse(fiasPromise);
    });

    it('Testing method $FiasService.findBuildings', function () {
        var queryJson = {
            streetGuid: '95e96a9a-54a4-43db-92dd-bd8d70fe9828'
        };

        expect($FiasService).not.toEqual(null);

        var fiasPromise = $FiasService.findStructs(queryJson);
        var done = false;
        runs(function () {
            fiasPromise.then(function (result) {
                expect(result instanceof Array).toBeTruthy();
                expect(result.length == 0).toBeTruthy();
            });
            fiasPromise['finally'](function () {
                done = true;
            });
        });

        waitsFor(function () {
            return done;
        }, 5000);
    });

    it('Testing method $FiasService.findStructs', function () {
        var queryJson = {
            streetGuid: '95e96a9a-54a4-43db-92dd-bd8d70fe9828'
        };

        expect($FiasService).not.toEqual(null);
        var fiasPromise = $FiasService.findStructs(queryJson);
        var done = false;
        runs(function () {
            fiasPromise.then(function (result) {
                expect(result instanceof Array).toBeTruthy();
                expect(result.length == 0).toBeTruthy();
            });
            fiasPromise['finally'](function () {
                done = true;
            });
        });

        waitsFor(function () {
            return done;
        }, 5000);
    });

    it('Testing method $FiasService.findHouses', function () {
        var queryJson = {
            houseNumber: '8',
            streetGuid: '163c3ce5-aba3-4da8-99e1-fdadf95aeff1',
            actual: true
        };
        expect($FiasService).not.toEqual(null);
        var fiasPromise = $FiasService.findHouses(queryJson);

        waitForResponse(fiasPromise);
    });
});