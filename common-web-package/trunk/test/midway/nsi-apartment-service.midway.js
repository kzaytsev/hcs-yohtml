describe("$NsiApartmentService Tests", function() {
    var tester;
    var $NsiApartmentService;

    function waitForResponse(promise) {
        var done = false;
        runs(function () {
            promise.then(function (result) {
                expect(result instanceof Array).toBeTruthy();
                expect(result.length > 0).toBeTruthy();
            });
            promise['finally'](function () {
                done = true;
            });
        });

        waitsFor(function () {
            return done;
        }, 5000);
    }

    beforeEach(function() {
        tester = ngMidwayTester('midway-web-package-mock');
        $NsiApartmentService = tester.inject('$NsiApartmentService');
    });

    it('Testing method $NsiApartmentService.findApartmentCategory', function() {

        expect($NsiApartmentService).not.toEqual(null);
        var nsiApartmentPromise = $NsiApartmentService.findApartmentCategory();

        waitForResponse(nsiApartmentPromise);
    });

    it('Testing method $NsiApartmentService.findResidentPremiseType', function() {

        expect($NsiApartmentService).not.toEqual(null);
        var nsiApartmentPromise = $NsiApartmentService.findResidentPremiseType();

        waitForResponse(nsiApartmentPromise);
    });

    it('Testing method $NsiApartmentService.findPropertyType', function() {

        expect($NsiApartmentService).not.toEqual(null);
        var nsiApartmentPromise = $NsiApartmentService.findPropertyType();

        waitForResponse(nsiApartmentPromise);
    });

    it('Testing method $NsiApartmentService.findCooperativePremisePurpose', function() {

        expect($NsiApartmentService).not.toEqual(null);
        var nsiApartmentPromise = $NsiApartmentService.findCooperativePremisePurpose();

        waitForResponse(nsiApartmentPromise);
    });

    it('Testing method $NsiApartmentService.findApartmentNumberOfRooms', function() {

        expect($NsiApartmentService).not.toEqual(null);
        var nsiApartmentPromise = $NsiApartmentService.findApartmentNumberOfRooms();

        waitForResponse(nsiApartmentPromise);
    });

    it('Testing method $NsiApartmentService.findLivingPersonType', function() {

        expect($NsiApartmentService).not.toEqual(null);
        var nsiApartmentPromise = $NsiApartmentService.findLivingPersonType();

        waitForResponse(nsiApartmentPromise);
    });
});
