describe("$NsiDeviceService Tests", function() {
    var tester;
    var $NsiAccountService;

    function waitForResponse(promise) {
        var done = false;
        runs(function () {
            promise.then(function (result) {
                expect(result instanceof Array).toBeTruthy();
                expect(result.length > 0).toBeTruthy();
            });
            promise['finally'](function () {
                done = true;
            });
        });

        waitsFor(function () {
            return done;
        }, 5000);
    }

    beforeEach(function() {
        tester = ngMidwayTester('midway-web-package-mock');
        $NsiAccountService = tester.inject('$NsiAccountService');
    });

    it('Testing method $NsiAccountService.findMunicipalResource', function() {

        expect($NsiAccountService).not.toEqual(null);
        var nsiAccountPromise = $NsiAccountService.getAccountCloseReason();

        waitForResponse(nsiAccountPromise);
    });

    it('Testing method $NsiAccountService.getAccountAdditionalServices', function() {

        expect($NsiAccountService).not.toEqual(null);
        var nsiAccountPromise = $NsiAccountService.getAccountAdditionalServices();

        waitForResponse(nsiAccountPromise);
    });
});
