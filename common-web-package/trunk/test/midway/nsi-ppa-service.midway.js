describe("$NsiApartmentService Tests", function() {
    var tester;
    var $NsiPpaService;

    function waitForResponse(promise) {
        var done = false;
        runs(function () {
            promise.then(function (result) {
                expect(result instanceof Array).toBeTruthy();
                expect(result.length > 0).toBeTruthy();
            });
            promise['finally'](function () {
                done = true;
            });
        });

        waitsFor(function () {
            return done;
        }, 5000);
    };

    beforeEach(function() {
        tester = ngMidwayTester('midway-web-package-mock');
        $NsiPpaService = tester.inject('$NsiPpaService');
    });

    it('Testing method $NsiPpaService.findEmployeeRole', function() {

        expect($NsiPpaService).not.toEqual(null);
        var nsiPpaPromise = $NsiPpaService.findEmployeeRole();

        waitForResponse(nsiPpaPromise);
    });

    it('Testing method $NsiPpaService.findTimeZone', function() {
        expect($NsiPpaService).not.toEqual(null);
        var nsiPpaPromise = $NsiPpaService.findTimeZone();

        waitForResponse(nsiPpaPromise);
    });

    it('Testing method $NsiPpaService.findOrganizationRole', function() {

        expect($NsiPpaService).not.toEqual(null);
        var nsiPpaPromise = $NsiPpaService.findOrganizationRole();

        waitForResponse(nsiPpaPromise);
    });
});
