describe("$NsiDeviceService Tests", function() {
    var tester;
    var $NsiDeviceService;

    function waitForResponse(promise) {
        var done = false;
        runs(function () {
            promise.then(function (result) {
                expect(result instanceof Array).toBeTruthy();
                expect(result.length > 0).toBeTruthy();
            });
            promise['finally'](function () {
                done = true;
            });
        });

        waitsFor(function () {
            return done;
        }, 5000);
    }

    beforeEach(function() {
        tester = ngMidwayTester('midway-web-package-mock');
        $NsiDeviceService = tester.inject('$NsiDeviceService');
    });

    it('Testing method NsiDeviceService.findMunicipalResource', function() {

        expect($NsiDeviceService).not.toEqual(null);
        var nsiDevicePromise = $NsiDeviceService.findMunicipalResource();

        waitForResponse(nsiDevicePromise);
    });

    it('Testing method NsiDeviceService.findMunicipalServices', function() {
        var queryJson = {
            guid: '48b9cb78-73d3-498f-a903-36d5881c5b22'
        };
        expect($NsiDeviceService).not.toEqual(null);
        var nsiDevicePromise = $NsiDeviceService.findMunicipalServices(queryJson);

        waitForResponse(nsiDevicePromise);
    });

    it('Testing method NsiDeviceService.findDeviceTypes', function() {

        expect($NsiDeviceService).not.toEqual(null);
        var nsiDevicePromise = $NsiDeviceService.findDeviceTypes();

        waitForResponse(nsiDevicePromise);
    });

    it('Testing method NsiDeviceService.findDeviceCalibrationInterval', function() {

        expect($NsiDeviceService).not.toEqual(null);
        var nsiDevicePromise = $NsiDeviceService.findDeviceCalibrationInterval();

        waitForResponse(nsiDevicePromise);
    });

    it('Testing method NsiDeviceService.findActualDeviceOperationPrincipleType', function() {

        expect($NsiDeviceService).not.toEqual(null);
        var nsiDevicePromise = $NsiDeviceService.findActualDeviceOperationPrincipleType();

        waitForResponse(nsiDevicePromise);
    });

    it('Testing method NsiDeviceService.findDeviceMountType', function() {

        expect($NsiDeviceService).not.toEqual(null);
        var nsiDevicePromise = $NsiDeviceService.findDeviceMountType();

        waitForResponse(nsiDevicePromise);
    });

    it('Testing method NsiDeviceService.findDeviceConnectionType', function() {

        expect($NsiDeviceService).not.toEqual(null);
        var nsiDevicePromise = $NsiDeviceService.findDeviceConnectionType();

        waitForResponse(nsiDevicePromise);
    });

    it('Testing method NsiDeviceService.findDeviceTariffType', function() {

        expect($NsiDeviceService).not.toEqual(null);
        var nsiDevicePromise = $NsiDeviceService.findDeviceTariffType();

        waitForResponse(nsiDevicePromise);
    });
});
