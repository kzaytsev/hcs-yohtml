angular
    .module('midway-web-package-mock', [
        'ru.lanit.hcs.nsi.rest.NsiHouseService',
        'ru.lanit.hcs.nsi.rest.NsiApartmentService',
        'ru.lanit.hcs.nsi.rest.NsiAccountService',
        'ru.lanit.hcs.nsi.rest.NsiPpaService',
        'ru.lanit.hcs.nsi.rest.NsiDeviceService',
        'ru.lanit.hcs.ppa.rest.PpaService',
        'ru.lanit.hcs.nsi.rest.FiasService',
        'ru.lanit.hcs.nsi.rest.ClassifierService',
        'ru.lanit.hcs.nsi.rest.NsiService'
    ]).constant('NSI_BACKEND_CONFIG', {
        baseUrl: '/nsi/api/rest/services'
    }).constant('PPA_BACKEND_CONFIG', {
        baseUrl: '/ppa/api/rest/services'
    });
