describe("$ClassifierService Tests", function() {
    var tester;
    var $ClassifierService;

    function waitForResponse(promise) {
        var done = false;
        runs(function () {
            promise.then(function (result) {
                expect(result instanceof Array).toBeTruthy();
                expect(result.length > 0).toBeTruthy();
            });
            promise['finally'](function () {
                done = true;
            });
        });

        waitsFor(function () {
            return done;
        }, 5000);
    }

    beforeEach(function () {
        tester = ngMidwayTester('midway-web-package-mock');
        $ClassifierService = tester.inject('$ClassifierService');
    });

    it('Testing method $ClassifierService.findOkfs', function() {
        var queryJson = {
            guid: '51a52d17-02fb-4580-aa06-55dcdbc1e8ce',
            code: '10',
            actual:true
        };

        expect($ClassifierService).not.toEqual(null);
        var fiasPromise = $ClassifierService.findOkfs(queryJson);

        waitForResponse(fiasPromise);
    });

    it('Testing method $ClassifierService.findOktmo', function() {
        var queryJson = {
            code: '00000000',
            actual: true
        };

        expect($ClassifierService).not.toEqual(null);
        var fiasPromise = $ClassifierService.findOktmo(queryJson);

        waitForResponse(fiasPromise);
    });

    it('Testing method $ClassifierService.findOkogu', function() {
        var queryJson = {
            guid: 'a6a9d50e-3e34-442e-a610-70d79d87f776',
            code: '1200101',
            actual: true
        };

        expect($ClassifierService).not.toEqual(null);
        var fiasPromise = $ClassifierService.findOkogu(queryJson);

        waitForResponse(fiasPromise);
    });

    it('Testing method $ClassifierService.findOkopf', function() {
        var queryJson = {
            guid: '831335c8-4794-46a6-a531-11b954f0bdc1',
            code: '11051',
            actual: true
        };

        expect($ClassifierService).not.toEqual(null);
        var fiasPromise = $ClassifierService.findOkopf(queryJson);

        waitForResponse(fiasPromise);
    });

    it('Testing method $ClassifierService.findOkei', function() {
        var queryJson = {
            code: '959'
        };

        expect($ClassifierService).not.toEqual(null);
        var fiasPromise = $ClassifierService.findOkei(queryJson);

        waitForResponse(fiasPromise);
    });
});