module.exports = function (grunt) {
    grunt.config('shell', {
        bowerInstall: {
            command: 'bower install'
        }
    });
    grunt.loadNpmTasks('grunt-shell');
};