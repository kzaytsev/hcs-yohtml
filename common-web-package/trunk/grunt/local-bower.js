var _ = require('lodash');
var rmdir = require('rimraf');
module.exports = function (grunt) {
    grunt.registerTask('deleteLocalLibs', function (basePath) {
        localBower(basePath, deleteDir);
    });
    grunt.registerTask('localLibsInstall', function (basePath) {
        localBower(basePath, [deleteDir, copyDir]);
    });
    function localBower(basePath, actions) {
        if (!_.isString(basePath)) {
            basePath = ".";
            console.log('basePath[' + basePath + '] is not a string, we use "."');
        }
        if (!grunt.file.exists(basePath + '/bower.json')) {
            console.log('bower not exist, stop')
            return;
        }
        if (!grunt.file.exists(basePath + '/bower.local.json')) {
            console.log('local bower not exist, stop')
            return;
        }
        var bowerCfg = grunt.file.readJSON(basePath + '/bower.json');
        var localLibs = grunt.file.readJSON(basePath + '/bower.local.json');
        _.each(bowerCfg.devDependencies, function (val, key) {
            if (localLibs[key]) {
                var act = {
                    from: 'release/**',
                    to: basePath + '/vendor/' + key,
                    cwd: basePath + '/' + localLibs[key]
                };
                if (_.isFunction(actions)) {
                    actions(act);
                } else if (_.isArray(actions)) {
                    _.each(actions, function (action) {
                        if (_.isFunction(action)) {
                            action(act);
                        } else {
                            console.log('ERROR: local-bower, argument is not a function');
                        }
                    });
                } else {
                    console.log('ERROR: local-bower, argument is not a function');
                }
            }
        });
    }

    function deleteDir(act) {
        rmdir.sync(act.to, function (err) {
            console.log(err);
        });
    }

    function copyDir(act) {
        /* Не делать на чистом ноде (ncp) лагает по asynk */
        var copyMap = grunt.file.expandMapping(
            act.from, act.to,
            {
                cwd: act.cwd
            }
        );
        console.log('copyMap=  ' + copyMap);
        _.each(copyMap, function (copyItem) {
            console.log('Copy:  ' + copyItem.src[0]);
            console.log('   to: ' + copyItem.dest);
            if (grunt.file.isDir(copyItem.src[0])) {
                grunt.file.mkdir(copyItem.dest);
            } else if (grunt.file.isFile(copyItem.src[0])) {
                grunt.file.copy(copyItem.src[0], copyItem.dest);
            }
        });
    }
};